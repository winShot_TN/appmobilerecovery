/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {Dimensions} from 'react-native';
import {StatusBar} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Login from './components/Login';
import PasswordRecover from './components/PasswordRecover'
import inscription2 from './components/inscription2'
import AfterConnexion from './components/AfterConnexion';
import Imagecarto from "./components/cartographie/Imagecartographie";
import carteformulaire from "./components/cartographie/carteformulaire";
import {Provider} from 'react-native-paper';
import Colors from './components/utils/Colors';
import Welcome from './components/Welcome';
import Inscription from './components/inscription';
import GlobalFont from 'react-native-global-font';

const {width} = Dimensions.get('window');
import {fontFamily} from "./components/utils/Const";

const AppNavigator = createStackNavigator({
        welcome: {
            screen: Welcome,
            navigationOptions: {
                header: null,
            }
        },
        PasswordRecover : {
            screen: PasswordRecover,
            navigationOptions: {
                header: null,
            }
        },
        inscription: {
            screen: Inscription,
            navigationOptions: {
                header: null,
            }
        },
        inscription2: {
            screen: inscription2,
            navigationOptions: {
                header: null,
            }
        },
        login: {
            screen: Login,
            navigationOptions: {
                title: 'Connexion',
                header: null,
            }

        },
        afterconnexion: {
            screen: AfterConnexion,
            navigationOptions: {header: null,}
        },
        Imagecarto: {
            screen: Imagecarto,
            navigationOptions: {
                headerTintColor: 'rgba(34,81,125,1)',
                title: 'Photo cartographie',
                headerTitleStyle: {marginLeft: width / 7.6}
            }
        },
        carteformulaire: {
            screen: carteformulaire, navigationOptions: {
                headerTintColor: 'rgba(34,81,125,1)',
                title: 'Formulaire cartographie',
                headerTitleStyle: {alignSelf: 'center', textAlign: "center", justifyContent: "center"}
            }
        },
    },
    {
        headerMode: 'float',
        navigationOptions: ({navigation}) => ({})
    });
const Nav = createAppContainer(AppNavigator);
import {Root,} from 'native-base'

export default class App extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        GlobalFont.applyGlobal(fontFamily["Poppins-Regular"])
    }

    render() {
        return (
            <Provider>
                <StatusBar barStyle="light-content" backgroundColor={Colors.orange2}
                >
                </StatusBar>
                <Root>
                    <Nav/>
                </Root>
            </Provider>
        )
    }
}
