import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign'

import Feather from 'react-native-vector-icons/Feather'
import Entypo from 'react-native-vector-icons/Entypo'
import Ionicons from 'react-native-vector-icons/Ionicons'

import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import {InteractionManager, NetInfo, PermissionsAndroid, View} from 'react-native'
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'
//import Navigator in our project
import home from './home/home'
import NavChat from './chat/NavChat'
import Statistique from './statistuques/Statistique'

import NavActivities from "./Activites/NavActivities";
import NavCadeaux from "./Cadeaux/NavCadeaux";
import Colors from './utils/Colors'
import {fontFamily, Urh} from "./utils/Const";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import {NavSettings} from "./parametre/NavSettings";
import AsyncStorage from "@react-native-community/async-storage";
import FusedLocation from 'react-native-fused-location';
import axios from "axios";

const TabScreen = createMaterialBottomTabNavigator({
    Home: {
        screen: home, navigationOptions: ({navigation}) => {
            return {
                headerTitleStyle: {
                    fontFamily: fontFamily["Poppins-Regular"],
                },
                tabBarLabel: 'Check-list',
                header: null,
                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Feather name='clipboard' size={25} color={tintColor}/>
                    </View>),
            }
        }
    },
    Activites: {
        screen: NavActivities,
        navigationOptions: {
            headerTitleStyle: {fontFamily: fontFamily["Poppins-Regular"]},
            tabBarLabel: 'Activités',
            header: null,
            tabBarIcon: ({tintColor}) => (<View>
                <Ionicons name='md-time' size={24} color={tintColor}/>
            </View>),
        }
    },
    Statistique: {
        screen: Statistique,
        navigationOptions: {
            headerTitleStyle: {fontFamily: fontFamily["Poppins-Regular"]},
            tabBarLabel: 'Statistiques',
            tabBarIcon: ({tintColor}) => (
                <View>
                    <AntDesign name='linechart' size={24} color={tintColor}/>
                </View>),
        }
    },
    Chat: {
        screen: NavChat,
        navigationOptions: {
            tabBarLabel: 'Chat',
            header: null,
            tabBarIcon: ({tintColor}) => (
                <View>
                    <SimpleLineIcons name='bubbles' size={24} color={tintColor}/>
                </View>),
        }
    },
    Plus: {
        screen: NavSettings,
        navigationOptions: {
            headerTitleStyle: {
                fontFamily: fontFamily["Poppins-Regular"]
            },
            tabBarLabel: 'Compte',
            header: null,
            tabBarIcon: ({tintColor}) => (
                <Entypo name='dots-three-horizontal' size={25} color={tintColor}/>
            ),
        }
    },
}, {
    initialRouteName: 'Home',
    activeColor: Colors.activeTintColor,
    inactiveColor: Colors.inactiveTintColor,
    barStyle: {
        backgroundColor: Colors.white,
    },
});
export default class AfterConnexion extends React.Component {
    static router = TabScreen.router;
    state = {
        noConnection: false,
        currentPosition: null,
        isLocationEnabled: false,
        user: null,
    }

    handleFirstConnectivityChange = (connectionInfo) => {
        switch (connectionInfo.type) {
            case "none" :
                this.setState({noConnection: true})
                break;
            case "wifi" :
                this.setState({noConnection: false})
                break;
            case "cellular":
                this.setState({noConnection: false})
            default:

                break;
        }
        NetInfo.removeEventListener(
            'connectionChange',
            (connectionInfo) => this.handleFirstConnectivityChange(connectionInfo),
        );
    };
    requestLocationPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'ACCESS_FINE_LOCATION',
                    message:
                        'Cool Photo App needs access to your camera ' +
                        'so you can take awesome pictures.',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            return granted === PermissionsAndroid.RESULTS.GRANTED;
        } catch (err) {

            return false;
        }
    };

    updateUser = () => {
        this.getUser().then((user) => {

            this.setState({user: user})


        });
    }
    getUser = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            //this.state.user = JSON.parse(token)
            let id = JSON.parse(token)
            let myuser = JSON.parse(token)
            await axios.get(Urh + 'returnUser/' + id._id,)

                .then((res) => {

                    if (res.data)
                        myuser = res.data
                    else
                        return myuser


                })
                .catch((err) => {
                })
            return myuser
        } catch (error) {
        }
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.updateUser()
            this.requestLocationPermission().then(res => {
                if (res)
                    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
                        .then(data => {
                            this.setState({isLocationEnabled: true}, () => {
                                FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY);
                                // Get location once.
                                const location = FusedLocation.getFusedLocation();
                                // Set options.
                                FusedLocation.setLocationPriority(FusedLocation.Constants.BALANCED);
                                FusedLocation.setLocationInterval(20000);
                                FusedLocation.setFastestLocationInterval(15000);
                                FusedLocation.setSmallestDisplacement(10);
                                // Keep getting updated location.
                                FusedLocation.startLocationUpdates();
                                // Place listeners.
                                this.subscription = FusedLocation.on('fusedLocation', location => {
                                    this.setState({
                                        currentPosition: {
                                            coords: {
                                                longitude: location.longitude,
                                                latitude: location.latitude
                                            }
                                        }
                                    })
                                });
                                /*Network info*/
                                NetInfo.getConnectionInfo().then((connectionInfo) => {
                                    switch (connectionInfo.type) {
                                        case "none" :
                                            this.setState({noConnection: true})
                                            break;
                                        case "wifi" :
                                            this.setState({noConnection: false})
                                            break;
                                        case "cellular":
                                            this.setState({noConnection: false})
                                        default:
                                            break;
                                    }
                                }).catch(err => null);
                                NetInfo.addEventListener('connectionChange', (connectionInfo) => this.handleFirstConnectivityChange(connectionInfo));
                            })
                        }).catch(err => {
                        this.setState({isLocationEnabled: false})
                    });
            }).catch(err => {
            })
        });
    }

    render() {
        let {
            noConnection,
            currentPosition,
            isLocationEnabled,
            user
        } = this.state;
        return (
            user && <TabScreen screenProps={{
                user: user,
                isLocationEnabled: isLocationEnabled,
                noConnection: noConnection,
                currentPosition: currentPosition,
                updateUser: this.updateUser
            }} navigation={this.props.navigation}/>)
    }
}
