import * as React from 'react';
import {Text, StyleSheet, Image} from 'react-native'
import {
    View,
    TextInput,
} from '@shoutem/ui';
import Colors from './utils/Colors'
import LinearGradient from 'react-native-linear-gradient';
import {createMaterialTopTabNavigator} from "react-navigation";
import Login from "./Login";
import inscription from "./inscription";
import {Urh} from "./utils/Const";

const TabScreen = createMaterialTopTabNavigator(
    {
        Connexion: {
            screen: Login, navigationOptions: {}
        },
        Inscription: {screen: inscription},
    },
    {
        swipeEnabled: true,
        animationEnabled: true,
        tabBarOptions: {
            activeTintColor: Colors.activeTintColor,
            inactiveTintColor: Colors.inactiveTintColor,
            style: {
                backgroundColor: Colors.white,
            },
            labelStyle: {
                textAlign: 'center',
            },
            indicatorStyle: {
                borderBottomColor: Colors.orange,
                borderBottomWidth: 1,
            },
        },
    }
);

export default class First extends React.Component {
    static router = TabScreen.router;

    render() {
        return (
            <View style={styles.page}>
                <View style={styles.header}>
                    <Image
                        source={{uri: Urh+'winshot_png.png', height: 120, width: 120}}
                    />
                </View>
                <LinearGradient
                    start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
                    locations={[0.1, 0.5]}
                    colors={[Colors.lightBlue, Colors.blue]}
                    style={styles.footer}>
                    {/*login page here*/}
                    <View style={styles.container}>
                        <TabScreen navigation={this.props.navigation}/>
                    </View>
                </LinearGradient>
            </View>
        );
    };
};
const styles = StyleSheet.create({
    page: {
        flex: 1,
        justifyContent: 'center'
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: '5%',
        height: '30%',
        backgroundColor: Colors.backGroundGray
    },
    footer: {
        flexGrow: 1,
        backgroundColor: Colors.lightBlue,
        alignItems: 'center'
    },
    container: {
        position: 'absolute',
        top: '-9%',
        backgroundColor: Colors.white,
        height: '100%',
        width: '90%',
        paddingVertical: 9,
        paddingHorizontal: 3,
        borderWidth: 0,
        borderRadius: 15
    },

})