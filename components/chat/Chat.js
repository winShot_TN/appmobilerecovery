import React from 'react'
import {Block, Text as GalioText} from 'galio-framework'
import Colors from "../utils/Colors";
import {FlatList, Image, StyleSheet, TouchableOpacity} from "react-native";
import ImageLoad from "react-native-image-placeholder";
import moment from "../Activites/Activites";
import Ionicons from "react-native-vector-icons/Ionicons";
import axios from "axios";
import {Urh} from "../utils/Const";
export default class Chat extends React.Component {

    state={
        dashboardUsers:[],
        refreshing: false,


    }
    constructor(props){
        super(props)




        this.renderHeader = this.renderHeader.bind(this)
    }
    renderHeader() {
        let {user: {photo}} = this.props.screenProps;
        return (
            <Block row shadow right middle style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 10,
            }}>
                <Block flex center style={{marginLeft: 20}}>
                    <GalioText color={Colors.white} style={{fontSize: 20}}> Chat </GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    {photo && photo.length > 0 ?
                        <TouchableOpacity onPress={this.goToPlus}>
                            <Image
                                style={{
                                    height: 35,
                                    width: 35,
                                    borderRadius: 18,
                                    borderColor: Colors.white,
                                    borderWidth: 1,
                                }}
                                source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + photo}}/>
                        </TouchableOpacity>
                        : null}
                </Block>
            </Block>
        )
    }


    componentDidMount() {

        axios.get(Urh + 'brand').then((res) => {

            let dashboardUsers = res.data
            this.setState({dashboardUsers})



        }).catch((err) => {
            console.log(err)
        })
    }

    renderconvs(item, index) {
        return (
            <Block style={styles.card}>
                <Block row style={{
                    backgroundColor: Colors.white,
                    padding: 5,
                    height: 100,
                    borderRadius: 10,
                }} space='between'>
                    <Block center>
                        {item.photo && item.photo.length>1 ?  <ImageLoad
                                borderRadius={5}
                                style={{minWidth: 80, minHeight: 80, resizeMode: "cover", borderRadius: 2}}
                                loadingStyle={{size: 'small', color: 'blue'}}
                                source={{
                                    uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/'+item.photo,
                                }}/>

                            :<ImageLoad
                                borderRadius={5}
                                style={{minWidth: 60, minHeight: 60, resizeMode: "cover", borderRadius: 2}}
                                loadingStyle={{size: 'small', color: 'blue'}}
                                source={{
                                    uri: 'https://i.ya-webdesign.com/images/pictures-clipart-person-4.png',
                                }}/>}


                    </Block>
                    <Block style={{marginLeft: 2}} flex>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Conversation',{
                            Userbrand : JSON.stringify(item)
                        })}
                                          style={{flex: 1, justifyContent: 'center'}}>
                            <GalioText style={{marginBottom: 5}} numberOfLines={1} size={15} color={Colors.textColor}>
                                {/*{item.nomcamp}*/}
                            </GalioText>
                            <GalioText numberOfLines={1} muted>
                                {/*Rapport {moment(item.envoie_date).format('llll')}*/}
                                {/*Jeudi, 31 Déc 2019, 13:15*/}
                            </GalioText>
                        </TouchableOpacity>
                    </Block>
                    <Block middle style={{marginRight: 5}}>
                        <Ionicons name={'ios-arrow-forward'} color={Colors.orange} size={32}/>
                    </Block>
                </Block>
            </Block>
        )
    }

    onRefresh() {
    }

    render() {

        let {dashboardUsers,refreshing} = this.state
        return (
            <Block flex>
                {this.renderHeader()}

                <FlatList
                    refreshing={refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                    showsVerticalScrollIndicator={false}
                    removeClippedSubviews={false}
                    data={dashboardUsers}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => this.renderconvs(item, index)}
                />






            </Block>
        );
    };
};

const styles = StyleSheet.create({
    card: {
        backgroundColor: Colors.white,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 50,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 4,
    }
})