import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Alert,
    ScrollView,
    TextInput,
    FlatList,
    Button
} from 'react-native';
import axios from "axios";
import Colors from "../utils/Colors";
import {Urh} from "../utils/Const";
import SocketIOClient from "socket.io-client";

export default class Converstation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            skip:-10,
            text_message:"",
            refreshing: false,
        };
    }


    componentDidMount() {

       this.getmessages()
        let {user} = this.props.screenProps;

        this.socket = SocketIOClient(Urh)
        this.socket.on('messageSocketserver', (data) => {
            if(data.id_reciver==user._id){

                let {messages} = this.state
                let aux = messages
                messages=[data.message]
                messages=[...aux,...messages]
                this.setState({messages})

            }
        })

    }

    renderDate = (date) => {
        return(
            <Text style={styles.time}>
                {date}
            </Text>
        );
    }
    sendmessages = () => {

        let userBrand = JSON.parse(this.props.navigation.getParam("Userbrand",{}))
        let {user} = this.props.screenProps;
        let {messages} = this.state


        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data = {
            id_me: user._id,
            id_him: userBrand._id,
            message:this.state.text_message
        }

        axios.post(Urh + 'sendMessages', data, configpic).then((res) => {

            let aux = messages
    messages=[res.data.message]
            messages=[...aux,...messages]
    this.setState({messages,text_message:""})









        }).catch((err) => {
            console.log(err)
        })
    }




    getmessages = () => {

        let userBrand = JSON.parse(this.props.navigation.getParam("Userbrand",{}))
        let {user} = this.props.screenProps;
        let {messages} = this.state


        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data = {
            id_me: user._id,
            id_him: userBrand._id,
            skip : this.state.skip+10
        }
        this.setState({skip:this.state.skip+10})


        axios.post(Urh + 'getmessages', data, configpic).then((res) => {

let aux = messages
            messages=[...res.data.reverse(),...aux]

            this.setState({messages})




        }).catch((err) => {
            console.log(err)
        })
    }

    render() {
let {messages,refreshing} = this.state
        let {user} = this.props.screenProps;
        return (
            <View style={styles.container}>
                <FlatList
                     // inverted
                    style={styles.list}




                     onRefresh={() => {this.getmessages()}}
                    refreshing={refreshing}

                          data={messages}
                          keyExtractor= {(item) => {
                              return item._id;
                          }}
                          renderItem={(message) => {
                              const item = message.item


                              let   itemStyle = styles.itemIn

                              if(item.id_sender == user._id)
                                    itemStyle =  styles.itemOut





                              // let inMessage = item.type === 'in';
                              // let itemStyle = inMessage ? styles.itemIn : styles.itemOut;
                              return (
                                  <View style={[styles.item, itemStyle]}>
                                      {/*{!inMessage && this.renderDate(item.date)}*/}
                                      <View style={[styles.balloon]}>
                                          <Text>{item.message}</Text>
                                      </View>
                                      {/*{inMessage && this.renderDate(item.date)}*/}
                                  </View>
                              )
                          }}/>
                <View style={styles.footer}>
                    <View style={styles.inputContainer}>
                        <TextInput style={styles.inputs}
                                   value={this.state.text_message}
                                   placeholder="Write a message..."
                                   underlineColorAndroid='transparent'
                                   onChangeText={(text_message) => this.setState({text_message})}/>
                    </View>

                    <TouchableOpacity
                        onPress={() => this.sendmessages()}
                        style={styles.btnSend}>


                        <Image source={{uri:"https://png.icons8.com/small/75/ffffff/filled-sent.png"}} style={styles.iconSend}  />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    list:{
        paddingHorizontal: 17,
    },
    footer:{
        flexDirection: 'row',
        height:50,
        backgroundColor: '#eeeeee',
        paddingHorizontal:10,
        padding:5,
    },
    btnSend:{
        backgroundColor:"#00BFFF",
        width:40,
        height:40,
        borderRadius:360,
        alignItems:'center',
        justifyContent:'center',
    },
    iconSend:{
        width:30,
        height:30,
        alignSelf:'center',
    },
    inputContainer: {

        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius:30,
        borderBottomWidth: 1,
        height:40,
        flexDirection: 'row',
        alignItems:'center',
        flex:1,
        marginRight:10,
    },
    inputs:{

        height:40,
        marginLeft:16,
        borderBottomColor: '#FFFFFF',
        flex:1,
    },
    balloon: {
        maxWidth: 170,
        padding: 7,
        borderRadius: 20,
    },
    itemIn: {
        alignSelf: 'flex-start',
        backgroundColor: Colors.blue,
    },
    itemOut: {
        alignSelf: 'flex-end',
        backgroundColor: Colors.orange,
    },
    time: {
        alignSelf: 'flex-end',
        margin: 15,
        fontSize:12,
        color:"#808080",
    },
    item: {
        marginVertical: 12,
        flex: 1,
        flexDirection: 'row',
        backgroundColor:"#eeeeee",
        borderRadius:300,
        padding:5,
    },
});