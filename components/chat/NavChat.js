import React from 'react'
import {Animated, Easing} from "react-native";
import {createStackNavigator} from "react-navigation";
import Chat from "./Chat";
import Conversation from "./Converstation";
const transitionConfig = () => {
    return {
        transitionSpec: {
            duration: 300,
            easing: Easing.out(Easing.poly(4)),
            timing: Animated.timing,
            useNativeDriver: true,
        },
        screenInterpolator: sceneProps => {
            const {layout, position, scene} = sceneProps;
            const thisSceneIndex = scene.index;
            const width = layout.initWidth;
            const translateX = position.interpolate({
                inputRange: [thisSceneIndex - 1, thisSceneIndex],
                outputRange: [width, 0],
            });
            return {transform: [{translateX}]};
        },
    };
};
const ChatStack = createStackNavigator({
    Chat: {
        screen: Chat,
        navigationOptions: {
            header: null,
        },
    },
    Conversation: {
        screen: Conversation,
        navigationOptions: {
            header: null,
        },
    },
}, {
    transitionConfig,
    initialRouteName: 'Chat',
});
export default class NavChat extends React.Component {
    static router = ChatStack.router;
    render() {
        let {user,currentPosition}
            = this.props.screenProps;

        return <ChatStack
            screenProps={{
                user,
                currentPosition
            }}
            navigation={this.props.navigation} />
    }
}