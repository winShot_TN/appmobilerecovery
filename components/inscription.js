/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Alert,
    SafeAreaView, Dimensions
} from 'react-native';
import {Button} from "native-base";
import { Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from "axios";
import {PermissionsAndroid} from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";
import Toast from "react-native-custom-toast";
import {Button as BaseButton, Text as BaseText} from 'native-base'

import {fontFamily, Urh} from "./utils/Const";
import {
    View as ShoutemView,
    TextInput as ShoutemTextInput,
} from '@shoutem/ui'
import {Logo} from './utils/Const'
import Colors from './utils/Colors'
import Ionicons from "react-native-vector-icons/FontAwesome5";
import Modal from "react-native-modal";
import {Block, Text as GalioText} from "galio-framework";
import AntDesign from "react-native-vector-icons/AntDesign";
const {width, height} = Dimensions.get('window');
export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputs: <View/>,
            button: <View/>,
            email: '',
            password: '',
            wrongcode: 'none',
            pseudo: 'none',
            pseudoexiste: "none",
            shortpass: "none",
            passconf: "none",
            user: "",
            pass: "",
            confirmpass: "",
            visibleinsc: false,
            infomodal:false,
            message:"",

            disable: "",

            codeparrain: "",
            visible: false,

            buttons:
                <Button

                    onPress={() => this.verifcode()}
                    buttonStyle={{
                        alignSelf: 'center',
                        width: 120,
                        marginTop: 35,
                        marginBottom: 35,
                        backgroundColor: "rgba(34,81,125,1)",
                        height: 35,
                        borderRadius: 15
                    }}
                    title="vérifier code"
                    color="#53a1dc"

                />,
            afterverif: "lightgrey",
            afterverifbac: "lightgrey",
            userpass: false,
            parrainstyle: "rgba(34,81,125,1)",
            parrainstylebac: "#ffffff",
            parraindisable: true,
            isModalVisible : false,
        };
        this.verifcode = this.verifcode.bind(this)
        this.checkuser = this.checkuser.bind(this)
        this.shortpass = this.shortpass.bind(this)
        this.confirmpass = this.confirmpass.bind(this)
        this.inscription = this.inscription.bind(this)


        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )


    }

    inscription() {

        var inscrit = this.inscription
        if (this.state.pseudoexiste == "flex" ||
            this.state.pseudo == "flex" ||
            this.state.shortpass == "flex" ||
            this.state.passconf == "flex" || this.state.confirmpass == "" || this.state.pass == "" || this.state.user == "") {
            this.refs.inscrit.showToast('Compléter les contraintes', 5000);

        } else {


            this.setState({visibleinsc: true})
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const initialPosition = position


                    let configpic = {
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    };

                    let data =
                        {
                            email: this.state.user,
                            password: this.state.pass,
                            longitude: initialPosition.coords.longitude,
                            latitude: initialPosition.coords.latitude,
                            code_parrain: this.state.codeparrain,

                        }
                    axios.post(Urh + 'signup', data, configpic)
                        .then((res) => {
                            this.setState({visibleinsc: false})


                            if (res.data.success) {
                                this.refs.inscritoui.showToast('inscription terminé', 8000);
                                this.props.navigation.goBack();

                            } else if (!res.data.success) {
                                this.refs.inscritnon.showToast('code parrain expiré', 5000);

                            }


                        })
                        .catch((err) => {
                            this.setState({visibleinsc: false})

                        })


                    this.setState({visibleinsc: false})




                },
                (error) => {
                    this.setState({visibleinsc: false})
                    inscrit()
                },
                {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000}
            )


        }


    }


    confirmpass(e) {
        e = e.trim()

        this.setState({confirmpass: e, passconf: "none"})

        if (this.state.pass != e) {
            this.setState({passconf: "flex"})
        }

    }


    shortpass(e) {
        e = e.trim()
        this.setState({pass: e, shortpass: "none"})

        if (e.length < 4) {
            this.setState({shortpass: "flex"})
        }

        if (this.state.confirmpass != e) {
            this.setState({passconf: "flex"})
        }


    }


    checkuser(e) {
        e = e.trim()
        this.setState({user: e, pseudo: "none", pseudoexiste: "none"})

        if (e.length > 3) {

            let configpic = {
                headers: {
                    'Content-Type': 'application/json',
                }
            };

            let data =
                {
                    email: e,


                }


            axios.post(Urh + 'checkuser', data, configpic)
                .then((res) => {

                    if (res.data.user == 1) {
                        this.setState({pseudoexiste: "flex"})
                    }


                })
                .catch((err) => {


                })
        } else {
            this.setState({pseudo: "flex"})
        }


    }

    componentDidMount() {

    }

    verifcode() {

        if ( this.state.phone == "" || this.state.codeparrain == "") {
            this.setState({infomodal:true,message:"Veuiller remplire tous les champs"})

        } else {
            this.setState({visible: true})
            let configpic = {
                headers: {
                    'Content-Type': 'application/json',
                }
            };

            let data =
                {
                    code_parrain: this.state.codeparrain,


                }


            axios.post(Urh + 'detectparrain', data, configpic)
                .then((res) => {
                    this.setState({visible: false})

                    if (res.data.code) {
                        // this.refs.parrainoui.showToast('code parrain valide', 5000);
                        this.props.navigation.navigate('inscription2', {
                            code: this.state.codeparrain,
                            phone: this.state.phone,
                        })


                    } else {
this.setState({infomodal:true,message:"Code activation non valide"})

                    }


                })
                .catch((err) => {
                    this.setState({visible: false})

                })
        }
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    handleObtenirCode = () => {
        this.toggleModal()
    }
    render() {
        // const { navigate } = this.props.navigation;

        let {infomodal,message}= this.state
        return (
            <SafeAreaView>
                <ScrollView  contentContainerStyle={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingHorizontal: 10,
                    height: height,
                }}>

                    <Modal
                        backdropOpacity={0.3} hasBackdrop={true}
                        isVisible={infomodal}>
                        <Block style={{
                            backgroundColor: Colors.white,
                            borderRadius: 10,
                        }}>
                            <Block style={{
                                paddingVertical: 10,
                                paddingHorizontal: 15,
                            }}>
                                <Block center>
                                    <AntDesign
                                        name={"infocirlceo"}
                                        size={34}
                                        color={Colors.blue}/>
                                </Block>
                                <GalioText center style={{marginVertical: 5}}>
                                    {message}
                                </GalioText>
                            </Block>
                            <Block row style={{
                                justifyContent: 'center',
                                backgroundColor: Colors.lightGray,
                                paddingVertical: 5,
                                paddingHorizontal: 10,

                            }}>
                                <Button rounded
                                        onPress={() => this.setState({infomodal:false})}
                                        style={{
                                            paddingHorizontal: 7,
                                            elevation: 0,
                                            backgroundColor: Colors.orange
                                        }}
                                >
                                    <GalioText style={{
                                        fontFamily: fontFamily["Poppins-Regular"],
                                        fontSize: 18,
                                        color: Colors.white,
                                        paddingHorizontal: 15,
                                    }}
                                               uppercase={false}>OK</GalioText>
                                </Button>
                            </Block>


                        </Block>

                    </Modal>


                    <Image style={{
                        width: 180,
                        height: 180,
                        resizeMode: 'contain',
                        marginBottom: '10%'
                    }} source={{uri: Logo}}/>
                    <View style={{alignItems: 'center'}}>

                        {/* Téléphone */}
                        <View style={styles.SectionStyle}>
                            <MaterialCommunityIcons style={styles.iconInput} name="phone" size={24}/>
                            <TextInput
                                style={styles.input}
                                onChangeText={(e) => {
                                    this.setState({phone: e})
                                }}

                                placeholder="Numéro Téléphone"
                                value={this.state.phone}
                                keyboardType={'numeric'}
                            />
                        </View>
                        {/*parinage code*/}
                        <View style={styles.SectionStyle}>
                            <MaterialCommunityIcons style={styles.iconInput} name="account-key" size={24}/>
                            <TextInput
                                style={styles.input}
                                onChangeText={(e) => {
                                    this.setState({codeparrain: e})

                                }}
                                placeholder="Code d'activation"
                                value={this.state.codeparrain}
                            />
                        </View>
                       {/* <Text onPress={this.handleObtenirCode} style={{textAlign: 'right', alignSelf: 'stretch',marginRight : '5%'}}>
                            obtenir votre code de parrainage
                        </Text>*/}


                    </View>

                    <View style={{
                        width: '90%',
                        marginTop: '10%'
                    }}>
                        <BaseButton
                            onPress={() => this.verifcode()}
                            rounded full info style={{borderRadius: 30, backgroundColor: Colors.blue, elevation: 0}}
                        >
                            <BaseText style={{fontFamily: fontFamily["Poppins-Regular"]}}
                                      uppercase={false}>Suivant</BaseText>
                            <Ionicons name={'arrow-right'} color={Colors.white} size={24}/>
                        </BaseButton>
                    </View>
                    <View>
                        <Spinner visible={this.state.visible} textContent={"verification"} textStyle={{color: '#FFF'}}/>
                    </View>
                    <Toast ref="parrainnon" backgroundColor="red" position="bottom"/>
                    <Toast ref="parrainoui" backgroundColor="green" position="bottom"/>
                </ScrollView>
                <Modal style={{ justifyContent: 'flex-start',
                    alignItems: 'center',height : 200,width : 300,borderRadius : 10,paddingHorizontal : 15 , paddingVertical : 20}} position={"center"} ref={"modal3"} isOpen={this.state.isModalVisible}>

                    <Text>Contactez l'équipe Winshot </Text>
                    <BaseButton

                        rounded full info style={{borderRadius: 30, backgroundColor: Colors.blue, elevation: 0,marginTop : 10}}
                    >

                        <Ionicons name={'facebook-messenger'} color={Colors.white} size={24}/>
                        <BaseText style={{fontFamily: fontFamily["Poppins-Regular"]}}
                                  uppercase={false}>Messenger</BaseText>
                    </BaseButton>
                    <BaseButton
                        rounded full info style={{borderRadius: 30, backgroundColor: Colors.blue, elevation: 0,marginTop : 10}}
                    >
                        <Ionicons name={'envelope'} color={Colors.white} size={24}/>
                        <BaseText style={{fontFamily: fontFamily["Poppins-Regular"]}}
                                  uppercase={false}>E-mail</BaseText>
                    </BaseButton>
                </Modal>
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    SectionStyle: {
        backgroundColor: Colors.backGroundGray,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 0,
        height: 60,
        borderRadius: 30,
        margin: 10,
    },
    input: {
        fontSize: 16,
        paddingLeft: 10,
        flex: 1,
        borderRadius: 30,
    },
    iconInput: {
        color: Colors.blue,
        paddingRight: 7,
        borderRightWidth: 1,
        borderColor: Colors.lightBlue,
        marginStart: 12,
    },
})
