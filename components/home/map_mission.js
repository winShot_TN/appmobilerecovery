import Icon from 'react-native-vector-icons/FontAwesome5';
import React from 'react';
import {
    ActivityIndicator,
    Alert,
    Dimensions,
    FlatList,
    Image,
    InteractionManager,
    StyleSheet,
    TouchableOpacity,
    View
} from 'react-native'
import MapView, {Marker} from 'react-native-maps'
import axios from "axios"
import {fontFamily, Logo, SIZES, Urh} from "../utils/Const"
import {RenderNoConnection,RenderEmptyMissions} from '../utils/staticComponents'
import SocketIOClient from "socket.io-client";
import Colors from "../utils/Colors";
import * as Animatable from "react-native-animatable";
import {Block, Text as GalioText} from "galio-framework";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import {Button as ElementsButton} from "react-native-elements";
import Modal from "react-native-modal";
import ImageOverlay from "react-native-image-overlay";

var geolib = require('geolib');

const AnimatableText = Animatable.createAnimatableComponent(GalioText);
const {width, height} = Dimensions.get('window');

const ASPECT_RATIO = width / (height / 2); //,

const LATITUDE = 36;
const LONGITUDE = 10;
const LATITUDE_DELTA = 0.016;


const LONGITUDE_DELTA = LATITUDE_DELTA * (width / (height / 1.2));
const CARD_HEIGHT = height / 4;
const CARD_WIDTH = width - (width - (SIZES.width * 0.89));

class GoogleMapScreen extends React.PureComponent {
    map
    state = {
        didFinishInitialAnimation: false,
        region: {
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        },
        Reservedmarker: [],
        currentMarker: null,
        pageNum: 0,
    }

    constructor(props) {
        super(props);
    }

    highlightMarker = (e, missions) => {
        let {withReservedMission,currentPosition} = this.props.screenProps

        this.setState({visible: true})


        if (missions.length > 0) {
            let contentOffsetY = e.nativeEvent.contentOffset.x;
            let pageNum = Math.floor(contentOffsetY / CARD_WIDTH);
            if (pageNum < 0)
                pageNum = 0

            if (pageNum < missions.length) {
                let page = withReservedMission ? pageNum : pageNum + 1
                this.setState({visible: true})
                var region = {
                    latitude: missions[page].alldata.coordinates.lat,
                    longitude: missions[page].alldata.coordinates.long,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }
                let currentMarker = {
                    latitude: missions[page].alldata.coordinates.lat,
                    longitude: missions[page].alldata.coordinates.long,
                }



                if(missions[page].alldata.cartographie==1) {
                    this.map.animateToRegion({
                        latitude: currentPosition.coords.latitude,
                        longitude: currentPosition.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }, 350)
                }

                else {


                    this.map.animateToRegion({
                        latitude: missions[page].alldata.coordinates.lat,
                        longitude: missions[page].alldata.coordinates.long,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }, 350)

                }
                this.setState({region, pageNum: page, currentMarker: currentMarker})



                if(missions[page].alldata.cartographie==1)


                {
                    this.map.animateToRegion({
                        latitude: currentPosition.coords.latitude,
                        longitude: currentPosition.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                }, 350)



                }


                    else {


                    this.map.animateToRegion({
                        latitude: missions[page].alldata.coordinates.lat,
                        longitude: missions[page].alldata.coordinates.long,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }, 350)

                }
            }
        }
    }

    reservedmissrejet = () => {
        let {
            updateMyState,
            missions,
            user,
            fetchData
        } = this.props.screenProps
        updateMyState({isLoading: true})
        let reservedmisid = missions[0].alldata._id
        let campid = missions[0].alldata.id_campaigne
        let configpic = {
            headers: {
                'contentType': "application/json",
            }
        }
        axios.put(Urh + 'validourejet', {
            missionid: reservedmisid,
            missionstat: 1
        }, configpic)
            .then((res) => {
                axios.put(Urh + 'rejetermission', {id_campaigne: campid}, configpic)
                    .then((res) => {
                        axios.post(Urh + 'abondancemission', {id_user: user._id}, configpic)
                            .then((res3) => {
                                updateMyState({withReservedMission: false})
                                    .then(() => {

                                        this.setState({Reservedmarker: []}, () => {
                                            fetchData(user.categorie).then(() => {
                                                this.props.navigation.replace("Liste");
                                            })
                                        })

                                    })
                            })
                            .catch((err) => {
                                this.setState({visible: false})

                            })
                    })
                    .catch((err) => {
                        this.setState({visible: false})

                    })
            })
            .catch((err) => {
                this.setState({visible: false})

            })
    }

    componentWillMount() {

    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.Remarker(false).then(() => {
                this.setState({
                    didFinishInitialAnimation: true,
                });
            })
            let {updateMyState} = this.props.screenProps;
            this.socket = SocketIOClient(Urh)
            this.socket.on('Refreshlistmissions', (data) => {
                updateMyState({
                    withReservedMission: false,
                });
                this.props
                    .navigation
                    .navigate("Liste");
            })

            // 4: set didFinishInitialAnimation to false
            // This will render the navigation bar and a list of players

        });

    }

    Remarker = async (withUpdate) => {
        const {
            missions,
            withReservedMission
        } = this.props.screenProps

        let Reservedmarker = []

        missions.map((mission, index) => {
            if (!mission)
                return
            Reservedmarker[index] = {
                latitude: mission.alldata.coordinates.lat,
                longitude: mission.alldata.coordinates.long,
            }
        })
        try {


            if (withUpdate) {
                let {pageNum} = this.state
                let currentMarker = {
                    latitude: missions[pageNum].alldata.coordinates.lat,
                    longitude: missions[pageNum].alldata.coordinates.long,
                }
                this.setState({
                    Reservedmarker: Reservedmarker,
                    currentMarker: currentMarker,
                })
            } else {
                let pageNum = withReservedMission ? 0 : 1

                let currentMarker = {
                    latitude: missions[pageNum].alldata.coordinates.lat,
                    longitude: missions[pageNum].alldata.coordinates.long,
                }

                let region = {
                    latitude: currentMarker.latitude,
                    longitude: currentMarker.longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }

                this.setState({
                    Reservedmarker: Reservedmarker,
                    currentMarker: currentMarker,
                    region: region
                })
            }
        } catch (e) {
            this.setState({visible: true})
        }
        return true
    }

    renderHeader = () => {
        const {navigation} = this.props;
        const {
            user
        } = this.props.screenProps

        return (
            <Animatable.View useNativeDriver animation="slideInDown">
                <Block space="between" row style={{
                    height: 50,
                    backgroundColor: Colors.orange,
                    paddingHorizontal: 18,

                }}>
                    <Block center>
                        <AntDesign onPress={() => navigation.goBack()} name='arrowleft' size={24} color={Colors.white}/>
                    </Block>
                    <Block center style={{marginLeft: 20}}>
                        <GalioText color={Colors.white} style={{fontSize: 20}}>Carte</GalioText>
                    </Block>
                    <Block center style={{marginRight: 5}}>
                        {user.photo && user.photo.length > 0 ?
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Plus')}>
                                <Image
                                    style={{
                                        height: 35,
                                        width: 35,
                                        borderRadius: 18,
                                        borderColor: Colors.white,
                                        borderWidth: 1,
                                    }}
                                    source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + user.photo}}/>
                            </TouchableOpacity>
                            : null}
                    </Block>
                </Block>
            </Animatable.View>
        )
    }

    continuermission = (mission) => {
        const {
            missions,
            isLoading,
            withReservedMission,
            user,
            updateMyState,
        } = this.props.screenProps

        if (!isLoading) {
            updateMyState({
                selectedMission: mission
            }).then(() => {
                mission.alldata.cartographie === 1 ?
                    this.props.navigation.replace('Information') :
                    this.props.navigation.replace('ResumeMission')

            }).catch(e => this.setState({visible: true}))
        }
    }

    selectMission = (mission, index) => {
        const {
            missions,
            isLoading,
            withReservedMission,
            user,
            updateMyState,
        } = this.props.screenProps
        if (withReservedMission && index === 0) {
            this.continuermission(mission)
            return
        }

        if (!isLoading) {
            if (withReservedMission) {
                Alert.alert(
                    'WinShooter!!',
                    'كمل المهمة المحجوزة و الا استغنى عليها بش تنجم تعمل مهمة اخرى',
                    [
                        {text: 'نستغنى على المهمة المحجوزة', onPress: () => this.reservedmissrejet()},
                        {text: 'موافق', onPress: () => this.setState({visible: true})},
                    ],
                    {cancelable: false}
                )
            } else {
                updateMyState({
                    selectedMission: mission
                }).then(() => {
                    mission.alldata.cartographie === 1 ?
                        this.props.navigation.replace('Information') :
                        this.props.navigation.replace('ResumeMission')
                }).catch(e => this.setState({visible: true}))
            }
        }
    }


    renderMissions = (mission, index) => {

        let isMissionReserved = (index === 0 && mission)

        let color
        if (mission !== null) {
            if (mission.alldata.priorite === "haute")
                color = Colors.danger
            else if (mission.alldata.priorite === "normale")
                color = Colors.success
            else
                color = Colors.orange
        }

        return !mission ? null : (
            <Block
                style={{
                    backgroundColor: Colors.white,
                    marginHorizontal: 4,
                    borderRadius: 50,
                    shadowColor: "#000",
                    width: SIZES.width * 0.89,
                    elevation: 0,
                }}
            >
                <Block row style={{
                    borderColor: isMissionReserved ? Colors.orange : Colors.white,
                    borderWidth: isMissionReserved ? 2 : 0,
                    backgroundColor: Colors.white,
                    padding: 5,
                    height: 120,
                    borderRadius: 10,
                }} space='between'>
                    <Block center>
                        {
                            isMissionReserved ?
                                <Block>
                                    {mission.alldata.photo && mission.alldata.photo.length >= 1 ?
                                        <ImageOverlay
                                            overlayAlpha={0.15}
                                            width={100}
                                            source={{
                                                height: 100,
                                                width: 100,
                                                uri: `https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/${mission.alldata.photo}`
                                            }}
                                            height={100}
                                            rounded={3}
                                            style={{
                                                resizeMode: "cover",
                                            }}>
                                            <AnimatableText
                                                useNativeDriver
                                                animation="pulse" easing="ease-out"
                                                iterationCount="infinite"
                                            >
                                                <GalioText center h5 style={{
                                                    fontFamily: fontFamily["Poppins-Bold"]
                                                }} color={Colors.orange}>{`Resérvée`}</GalioText>
                                            </AnimatableText>
                                        </ImageOverlay>
                                        : <ImageOverlay
                                            overlayAlpha={0.15}
                                            width={100}
                                            source={{
                                                height: 100,
                                                width: 100,
                                                uri: `https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/Food+Grocery.png`
                                            }}
                                            height={100}
                                            rounded={3}
                                            style={{
                                                resizeMode: "cover",
                                            }}>
                                            <AnimatableText
                                                useNativeDriver
                                                animation="pulse" easing="ease-out"
                                                iterationCount="infinite"
                                            >
                                                <GalioText center h5 style={{
                                                    fontFamily: fontFamily["Poppins-Bold"]
                                                }} color={Colors.orange}>{`Resérvée`}</GalioText>
                                            </AnimatableText>
                                        </ImageOverlay>}
                                </Block> :
                                <Block>
                                    {mission.alldata.photo && mission.alldata.photo.length >= 1 ?
                                        <Image
                                            style={{minWidth: 100, minHeight: 100, resizeMode: "cover"}}
                                            source={{uri: `https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/${mission.alldata.photo}`}}
                                            resizeMode="cover"
                                        /> :
                                        <Image
                                            style={{minWidth: 100, minHeight: 100, resizeMode: "cover"}}
                                            source={{
                                                uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/Food+Grocery.png',
                                            }}
                                        />
                                    }
                                </Block>
                        }

                    </Block>
                    <Block style={{marginLeft: 2}} flex>
                        <TouchableOpacity onPress={() => this.selectMission(mission, index)}
                                          style={{flex: 1, justifyContent: 'space-between'}}>
                            <Block>
                                <GalioText numberOfLines={1} h5 color={Colors.textColor}>
                                    {mission.nomcamp}
                                </GalioText>
                            </Block>
                            <Block row flex middle style={{marginBottom: 10}}>
                                <Block flex style={{marginLeft: 5}}>

                                </Block>
                                <Block center style={{marginRight: 5}}>
                                    <Block center middle style={{
                                        backgroundColor: color,
                                        borderRadius: 30,
                                        height: 30,
                                        width: 70
                                    }}>
                                    </Block>
                                </Block>
                                {/*<Block center style={{marginRight: 5}}>*/}
                                {/*    <Block center middle style={{*/}
                                {/*        backgroundColor: Colors.blue,*/}
                                {/*        borderRadius: 30,*/}
                                {/*        height: 30,*/}
                                {/*        width: 70*/}
                                {/*    }}>*/}
                                {/*        <GalioText center style={{*/}
                                {/*            paddingTop: 3,*/}
                                {/*            paddingRight: 3,*/}
                                {/*            fontFamily: fontFamily["Poppins-Bold"],*/}
                                {/*        }} size={22} color={Colors.white}> {mission.prix} DT</GalioText>*/}
                                {/*    </Block>*/}
                                {/*</Block>*/}
                            </Block>
                            <Block row space="between">

                                <Block row>
                                    {
                                        mission.alldata.cartographie === 1 ? null :
                                            <Icon name={'map-marker-alt'} size={24}
                                                  color={Colors.orange}/>
                                    }
                                    <Block center

                                           style={{marginLeft: 5, paddingTop: 3}}>
                                        {
                                            mission.alldata.cartographie === 1 ? null :
                                                <GalioText
                                                    style={{
                                                        fontSize: 15
                                                    }}
                                                    color={Colors.textColor}>{`${mission.distance / 1000} Km `}</GalioText>
                                        }
                                    </Block>
                                </Block>



                                <Block row middle center>
                                    <Block middle>
                                        <AntDesign name={'gift'} size={24}
                                                   color={Colors.orange}/>
                                    </Block>
                                    <Block bottom style={{marginLeft: 5, paddingTop: 3}}>
                                        <GalioText color={Colors.textColor} style={{
                                            fontSize: 15
                                        }}>100 Points</GalioText>
                                    </Block>
                                </Block>
                            </Block>
                        </TouchableOpacity>
                    </Block>
                </Block>
            </Block>
        )
    }
    goToList = () => this.props.navigation.navigate('Liste');

    render() {
        const {
            noConnection,
            missions,
            renderError,
            isLoading,
            isMapClicked,
            withReservedMission
        } = this.props.screenProps
        let {
            Reservedmarker,
            currentMarker,
            pageNum,
            didFinishInitialAnimation
        } = this.state
        if (!didFinishInitialAnimation) {
            return (
                <Block flex style={{backgroundColor: Colors.white}}>
                    <Block>
                        {this.renderHeader()}
                    </Block>
                    <Block flex style={{
                        backgroundColor: Colors.white,
                    }}>
                        <MapView
                            provider={this.props.provider}
                            style={styles.map}
                            initialRegion={this.state.region}
                        />
                    </Block>
                </Block>
            )
        }
        if (noConnection) {
            return (
                <Block flex style={{backgroundColor: Colors.white}}>
                    <Block>
                        {this.renderHeader()}
                    </Block>
                    <RenderNoConnection/>
                </Block>
            )
        }


        // check if missios empty
        if(!isLoading && missions.length === 0){
            return (
                <Block flex style={{backgroundColor: Colors.white}}>
                    <Block>
                        {this.renderHeader()}
                    </Block>
                    <RenderEmptyMissions/>
                </Block>
            )
        }
        return (
            <Block flex style={{backgroundColor: Colors.white}}>
                <Block>
                    {this.renderHeader()}
                </Block>
                {isLoading &&
                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={isLoading}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        padding: 15,
                        borderRadius: 10,
                    }}>
                        <Block center style={{justifyContent: 'center'}}>
                            <Image
                                style={{
                                    height: 100,
                                    width: 100,
                                }}
                                source={{uri: Logo}}
                            />
                            <ActivityIndicator size={"large"} color={Colors.orange}/>
                            <GalioText muted> Actualisation des missions à proximité</GalioText>
                        </Block>
                    </Block>
                </Modal>
                }

                <MapView
                    ref={map => this.map = map}
                    provider={this.props.provider}
                    style={styles.map}
                    scrollEnabled={true}
                    showsUserLocation={true}
                    zoomEnabled={true}
                    pitchEnabled={true}
                    rotateEnabled={true}
                    initialRegion={this.state.region}
                >
                    {Reservedmarker.map((marker, index) => {


                        if (marker === null || !missions[index] || missions[index].alldata.cartographie==1)
                            return null
                        return (
                            <Marker
                                key={index}
                                coordinate={{
                                    latitude: marker.latitude,
                                    longitude: marker.longitude,
                                }}
                            >
                                <Image
                                    source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/empty_orange.png"}}
                                    style={{height: 50, width: 40,}}/>
                            </Marker>
                        )
                    })}
                    {currentMarker && missions[pageNum] && missions[pageNum].alldata.cartographie!=1 &&
                    <Marker
                        coordinate={currentMarker}
                    >
                        <Image
                            source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/empty_blue.png"}}
                            style={{height: 50, width: 40,}}/>
                    </Marker>
                    }
                </MapView>
                <FlatList
                    style={styles.scrollView}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={missions}
                    scrollEventThrottle={1}
                    snapToInterval={(SIZES.width * 0.89) + 6}
                    onMomentumScrollEnd={(e) => this.highlightMarker(e, missions)}
                    keyExtractor={(mission, index) => index.toString()}
                    renderItem={({item, index}) => this.renderMissions(item, index)}
                />
                <View style={{
                    position: 'absolute',
                    margin: 0,
                    right: (SIZES.width / 2) - 100,
                    bottom: 150,
                    backgroundColor: Colors.white,
                    justifyContent: 'flex-start',
                    borderWidth: 1,
                    borderColor: '#e8eaed',
                    borderRadius: 25,
                    flexDirection: 'row',
                }}>
                    <ElementsButton
                        Clear
                        title={"Liste"}
                        icon={
                            <Entypo name={'list'} color={Colors.white} size={24}/>
                        }
                        titleStyle={{
                            marginLeft: 10,
                            fontFamily: fontFamily["Poppins-Regular"],
                            fontSize: 15,
                            color: Colors.white
                        }}

                        buttonStyle={{
                            backgroundColor: Colors.orange,
                            borderTopStartRadius: 20,
                            borderBottomLeftRadius: 20,
                            paddingLeft: 10,
                            borderRightColor: Colors.white,
                        }}
                        onPress={() => this.goToList()}
                    >
                    </ElementsButton>
                    <ElementsButton

                        Clear
                        title={"Carte"}
                        icon={
                            <Icon name={'map-marked-alt'} color={Colors.white} size={22}/>
                        }
                        titleStyle={{
                            fontFamily: fontFamily["Poppins-Regular"],
                            fontSize: 15,
                            marginLeft: 10,
                            color: Colors.white
                        }}

                        buttonStyle={{
                            backgroundColor: Colors.redBrick,
                            borderTopEndRadius: 20,
                            borderBottomRightRadius: 20,
                            paddingLeft: 10,
                        }}
                    >
                    </ElementsButton>
                    {/* <Button rounded full info

                            style={{
                                backgroundColor: Colors.orange,
                                borderTopStartRadius: 20,
                                borderBottomLeftRadius: 20,
                                paddingLeft: 10,
                                elevation: 0,
                                borderRightWidth: 0,
                                borderRightColor: Colors.white,
                            }}
                            onPress={() => this.props.navigation.goBack()}>
                        <Block row>
                            <Block center>
                                <Entypo name={'list'} color={Colors.white} size={22}/>
                            </Block>
                            <GalioText center style={{
                                fontFamily: fontFamily["Poppins-Regular"],
                                fontSize: 15,
                                marginHorizontal: 5,
                                color: Colors.white
                            }}>
                                Liste
                            </GalioText>
                        </Block>
                    </Button>

                    <Button rounded full info
                            style={{
                                backgroundColor: Colors.redBrick,
                                borderTopEndRadius: 20,
                                borderBottomRightRadius: 20,
                                paddingLeft: 10,
                                elevation: 8,
                            }}
                            onPress={() => null}>
                        <Block row>
                            <Block center>
                                <Icon name={'map-marked-alt'} color={Colors.white} size={22}/>
                            </Block>
                            <GalioText center style={{
                                fontFamily: fontFamily["Poppins-Regular"],
                                fontSize: 15,
                                marginHorizontal: 5,
                                color: Colors.white
                            }}>
                                Carte
                            </GalioText>
                        </Block>
                    </Button>*/}
                </View>


                {/*<Animated.ScrollView style={styles.scrollView}
                                     scrollEventThrottle={1}
                                     snapToInterval={CARD_WIDTH}
                                     showsHorizontalScrollIndicator={false}
                                     snapToInterval={CARD_WIDTH}
                                     onMomentumScrollEnd={(e) => this.highlightMarker(e)}
                                     horizontal>
                    {this.state.notcompleted}
                    {this.state.scrollview}
                </Animated.ScrollView>*/}

            </Block>

        );
    }
}// End of MyHomeScreen class
export default GoogleMapScreen;


const styles = StyleSheet.create({
    map: {
        flex: 1
    },

    scrollView: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 12,
        marginLeft: 0,
        marginRight: 0,
    },
    card: {
        padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: {x: 2, y: -2},
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
    },
    cardscroll: {

        padding: 6,
        elevation: 2,
        backgroundColor: "rgba(204,226,226,0.6)",
        marginHorizontal: 4,
        shadowColor: "#000",
        borderRadius: 9,
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: {x: 2, y: -2},
        height: 115,
        width: CARD_WIDTH,
        overflow: "hidden",
    },
    cardscrollreserved: {
        paddingBottom: 15,
        elevation: 2,
        marginHorizontal: 4,
        shadowColor: "#000",
        borderRadius: 9,
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: {x: 2, y: -2},
        height: 115,
        width: CARD_WIDTH,
        overflow: "hidden",
        backgroundColor: 'rgba(199,180,51,0.14)',
        borderWidth: 3,
        borderColor: "rgba(199,180,51,0.92)"
    },
    cardImage: {
        flex: 3,
        width: "100%",
        height: "100%",
        alignSelf: "center",
    },
    textContent: {
        flex: 1,
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
    marker: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: "rgba(130,4,150, 0.9)",
    },
    ring: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: "rgba(130,4,150, 0.3)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(130,4,150, 0.5)",
    },
    container: {
        flex: 1,
        backgroundColor: "#ffffff",
    },
    tabsContainer: {
        alignSelf: 'stretch',
        marginTop: 15,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeContainer: {
        backgroundColor: 'white',
        marginTop: 3,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeSubContainer: {
        flexDirection: 'row',
        paddingVertical: 5,
    },
    itemThreeImage: {
        height: 100,
        width: 100,
        marginRight: 5
    },
    itemThreeContent: {
        flex: 1,
        paddingLeft: 15,
        justifyContent: 'space-between',
    },
    itemThreeBrand: {
        fontFamily: "Lato-Regular",
        fontSize: 14,
        color: '#617ae1',
        textAlign: 'left'

    },
    itemThreeTitle: {
        fontFamily: "Lato-Bold",
        fontSize: 18,
        color: '#5F5F5F',
    },
    itemThreeSubtitle: {
        fontFamily: "Lato-Regular",
        fontSize: 12,
        color: '#a4a4a4',
    },
    itemThreeMetaContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemThreePrice: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5f5f5f',
        textAlign: 'right',
        marginRight: 10,
    },
    itemThreeHr: {
        flex: 1,
        height: 1,
        backgroundColor: '#e3e3e3',
        marginRight: -15,
        marginBottom: 5
    },
    badge: {
        backgroundColor: '#6DD0A3',
        borderRadius: 10,
        paddingHorizontal: 7,
        paddingVertical: 5,
        display: 'flex', flexDirection: 'row'
    }
});
