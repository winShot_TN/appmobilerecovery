import React from 'react';
import {Alert, Animated, Easing} from 'react-native'
import {createStackNavigator,} from 'react-navigation';
import mapMission from './map_mission'
import ListMission2 from './list_mission'
import ResumeMission from "../start_mission_2/ResumeMission";
import Instruction from '../start_mission_2/instruction'
import Etaps from '../start_mission_2/Etaps'

import axios from "axios"
import moment from "moment"
import {fontFamily, Urh} from "../utils/Const"
import AsyncStorage from "@react-native-community/async-storage"
import SocketIOClient from "socket.io-client";
import {Information} from "../start_mission_2/Information";
import {Informationmodifier} from '../start_mission_2/Informationmodifier'
import EditPdv from '../start_mission_2/EditPdv'

var geolib = require('geolib');
const transitionConfig = () => {
    return {
        transitionSpec: {
            duration: 300,
            easing: Easing.out(Easing.poly(4)),
            timing: Animated.timing,
            useNativeDriver: true,
        },
        screenInterpolator: sceneProps => {
            const {layout, position, scene} = sceneProps;
            const thisSceneIndex = scene.index;
            const width = layout.initWidth;
            const translateX = position.interpolate({
                inputRange: [thisSceneIndex - 1, thisSceneIndex],
                outputRange: [width, 0],
            });
            return {transform: [{translateX}]};
        },
    };
};
const HomeStack = createStackNavigator(
    {
        Liste: {
            screen: ListMission2,
            navigationOptions: {
                header: null,
            },
        },
        Map: {
            screen: mapMission,
            navigationOptions: {
                header: null,
            },
        },
        ResumeMission: {
            screen: ResumeMission,
            navigationOptions: {
                header: null,
            }
        },
        Instruction: {
            screen: Instruction,
            navigationOptions: {
                header: null,
            }
        },
        Etaps: {
            screen: Etaps,
            navigationOptions: {
                header: null,
            }
        },
        Information: {
            screen: Information,
            navigationOptions: {
                header: null,
            }
        },
        EditPdv: {
            screen: EditPdv,
            navigationOptions: {
                header: null,
            }
        }
    },
    {
        transitionConfig,
        initialRouteName: 'Liste',
    }
);

export default class Home extends React.Component {
    static router = HomeStack.router;

    state = {
        refreshing: false,
        networkError: false,
        disable: "",
        visible: true,
        missions: [],
        missionNotCompleted: [],
        currentPosition: {},
        isLoading: true,
        renderError: false,
        withReservedMission: false,
        selectedMission: null,
        isMapClicked: false,
        user: null,
        Reservedmarker: [],
        noConnection: false,
        visiblemodale: false,
        gouvernoratlist: []

    };

    constructor(props) {
        super(props)

    };

    updateMyState = async (data) => {
        this.setState(data)
    };

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        let prevPosition = this.props.screenProps.currentPosition;
        let currentPosition = nextProps.screenProps.currentPosition;

        if (prevPosition) {
            let prevPositionCords = {
                lat: prevPosition.coords.latitude,
                long: prevPosition.coords.longitude
            }
            let currentPositionCords = {
                lat: currentPosition.coords.latitude,
                long: currentPosition.coords.longitude
            }

            let distance = geolib.getDistance(
                {
                    latitude: prevPositionCords.lat,
                    longitude: prevPositionCords.long
                },
                {
                    latitude: currentPositionCords.lat,
                    longitude: currentPositionCords.long
                }
            );

            if (distance >= 1) {
                this.updateDistances(currentPositionCords)
            }
        }
    };

    updateDistances = (currentPositionCords) => {
        let missions = [...this.state.missions]
        missions.map((mission, index) => {
            if (mission) {
                let distance = geolib.getDistance(
                    {
                        latitude: mission.alldata.coordinates.lat,
                        longitude: mission.alldata.coordinates.long,
                    },
                    {
                        latitude: currentPositionCords.lat,
                        longitude: currentPositionCords.long
                    }
                );
                mission.distance = distance
            }
        });
        this.setState({missions: missions}, () => {
            this.forceUpdate()
        })
    }

    componentDidMount = () => {

        this.getUser().then((user) => {
            this.setState({user: user}, () => {


                let today =  new Date()
let validcategories=[]
                user.categorie.map((cat) => {

                    if(moment(today).isSameOrAfter(cat.start, 'day'))
                        validcategories.push(cat.categorie)

                })


                this.fetchData(validcategories).then(() => {
                    this.socket = SocketIOClient(Urh+"?data="+user._id)


                                        this.socket.on('Refreshlistmissions', (data) => {
                        this.fetchData(validcategories)
                    })
                }).catch(e => {
                })
            })
        }).catch(e => {
        })
    };

    getUser = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');

            let id=JSON.parse(token)
            let myuser=JSON.parse(token)
            await axios.get(Urh + 'returnUser/'+id._id,)

                .then((res) => {

                    if(res.data)
                        myuser= res.data
                    else
                        return myuser


                })
                .catch((err) => {
                })
            return  myuser

        } catch (error) {
        }
    }

    fetchData = async (categorie) => {
        let {currentPosition} = this.props.screenProps;
        let {isLoading, gouvernoratlist} = this.state
        if (!currentPosition) {
            if (!isLoading) this.setState({isLoading: true})
            setTimeout(() => {
                this.fetchData(categorie)
            }, 1000)
        } else {
            let configpic = {
                headers: {
                    'Content-Type': 'application/json',
                }
            };
            let data = {
                categorie: categorie,
                lat: currentPosition.coords.latitude,
                lon: currentPosition.coords.longitude,
            }
            const fetchMissionsForUsers = [axios.post(Urh + 'missionsforuser', data, configpic),
                axios.get(Urh + 'reservedforuser/' + this.state.user._id)]
            axios.all(fetchMissionsForUsers)
                .then((res) => {
                    /*missions for user*/
                    let missions = []
                    missions[0] = null
                    let gouvernoratlist = []
                    res[0].data.forEach(mission => {
                        let dato = {
                            nomcamp: mission.nomcamp,
                            city: mission.city,
                            prix: mission.prix,
                            rue: mission.rue,
                            alldata: mission
                        }
                        let distance = geolib.getDistance(
                            {
                                latitude: mission.coordinates.lat,
                                longitude: mission.coordinates.long
                            },
                            {
                                latitude: currentPosition.coords.latitude,
                                longitude: currentPosition.coords.longitude
                            }
                        )
                        dato.distance = distance
                        missions.push(dato)


                    })
                    /*reservedforuser*/
                    if (res[1].data.length > 0) {
                        if (res[1].data && res[1].data[0] && res[1].data[0]._id) {
                            let dato = {
                                nomcamp: res[1].data[0].nomcamp,
                                city: res[1].data[0].city,
                                prix: res[1].data[0].prix,
                                rue: res[1].data[0].rue,
                                alldata: res[1].data[0]
                            }
                            let distance = geolib.getDistance(
                                {
                                    latitude: res[1].data[0].coordinates.lat,
                                    longitude: res[1].data[0].coordinates.long
                                },
                                {
                                    latitude: currentPosition.coords.latitude,
                                    longitude: currentPosition.coords.longitude
                                }
                            )
                            dato.distance = distance
                            this.reservedmisid = res[1].data[0]._id
                            this.campid = res[1].data[0].id_campaigne
                            missions[0] = dato

                        }
                    }
                    if (res[1].data.length > 0)
                        this.setState({withReservedMission: true})

                    this.setState({
                        missions: missions,
                        gouvernoratlist: gouvernoratlist,
                        isLoading: false,
                        networkError: false,
                    });

                })
                .catch(error => {

                    if (!error.status) {
                        // network error
                        this.setState({
                            isLoading: false,
                        }, () => {
                            Alert.alert(
                                'Connexion interrompue',
                                'Veuillez vérifier votre connexion et réessayer.',
                                [
                                    {
                                        text: 'Réessayer',
                                        onPress: () => this.fetchData(categorie)
                                    }
                                ],
                                {cancelable: false},
                            );
                        })
                    }
                })
        }
    }

    updateMission = async (mission, index) => {
        let newmissions = [...this.state.missions];
        newmissions[index] = mission;
        this.setState({missions: newmissions});
    }

    render() {
        let {
            noConnection,
            currentPosition,
            isLocationEnabled
        } = this.props.screenProps;

        const {
            missions,
            renderError,
            isLoading,
            isMapClicked,
            user,
            withReservedMission,
            selectedMission,
            visiblemodale,
            gouvernoratlist,
            networkError,
            refreshing
        } = this.state;
        if (!user)
            return (null)
        return <HomeStack
            screenProps={{
                networkError: networkError,
                refreshing: refreshing,
                isLocationEnabled: isLocationEnabled,
                noConnection: noConnection,
                currentPosition: currentPosition,
                missions: missions,
                renderError: renderError,
                isLoading: isLoading,
                isMapClicked: isMapClicked,
                user: user,
                gouvernoratlist: gouvernoratlist,
                withReservedMission: withReservedMission,
                selectedMission: selectedMission,
                updateMyState: this.updateMyState,
                updateMission: this.updateMission,
                fetchData: this.fetchData
            }}
            navigation={this.props.navigation}/>
    }
}
