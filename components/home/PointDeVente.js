import React, {PureComponent} from 'react';
import {View, TouchableOpacity, ActivityIndicator, FlatList, StyleSheet,Image} from "react-native";
import Colors from "../utils/Colors";
import {Block, Text as GalioText} from 'galio-framework'
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {fontFamily, SIZES, Urh} from "../utils/Const";
import {Button as ElementsButton} from "react-native-elements";
import axios from "axios";
import ImageOverlay from "react-native-image-overlay";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import AntDesign from 'react-native-vector-icons/AntDesign'
import ImageLoad from "react-native-image-placeholder";
import Icon from "react-native-vector-icons/index";
var geolib = require('geolib');
export default class PointDeVente extends PureComponent {
    state = {
        shops: [],
        isLoading: true,
        refreshing: false,

    }

    renderB() {
        return (
            <Block>
                <TouchableOpacity
                    style={{
                        backgroundColor: Colors.white,
                        marginVertical: 5,
                        borderRadius: 50,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 4,
                    }}
                >
                    <Block row style={{
                        backgroundColor: Colors.white,
                        padding: 5,
                        height: 60,
                        borderRadius: 10,
                    }} space='between'>
                        <Block style={{
                            marginLeft: 10
                        }}>
                            <Block row>
                                <Entypo size={24}
                                        name={"shop"}
                                        color={Colors.orange}
                                />
                                <GalioText
                                    style={{
                                        marginLeft: 5
                                    }}
                                    numberOfLines={1} h5 color={Colors.textColor}>
                                    Ajouter PDV</GalioText>
                            </Block>
                            <Block>
                                <GalioText muted>
                                    A réaliser en magasin n'importe où
                                </GalioText>
                            </Block>
                        </Block>
                        <Block center style={{
                            paddingRight: 10
                        }}>
                            <Ionicons
                                name={"ios-arrow-forward"}
                                size={30}
                                color={Colors.inactiveTintColor}/>
                        </Block>
                    </Block>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Informationmodifier')}
                    style={{
                        backgroundColor: Colors.white,
                        borderRadius: 50,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 4,
                    }}
                >
                    <Block row style={{
                        backgroundColor: Colors.white,
                        padding: 5,
                        height: 60,
                        borderRadius: 10,
                    }} space='between'>
                        <Block style={{
                            marginLeft: 10
                        }}>
                            <Block row>
                                <FontAwesome
                                    name={"edit"}
                                    size={24}
                                    color={Colors.orange}/>
                                <GalioText
                                    style={{
                                        marginLeft: 5
                                    }}
                                    numberOfLines={1} h5 color={Colors.textColor}>Modifier PDV</GalioText>
                            </Block>

                            <GalioText muted>
                                A réaliser en magasin n'importe où
                            </GalioText>
                        </Block>
                        <Block center style={{
                            paddingRight: 10
                        }}>
                            <Ionicons
                                name={"ios-arrow-forward"}
                                size={30}
                                color={Colors.inactiveTintColor}/>
                        </Block>
                    </Block>
                </TouchableOpacity>
            </Block>
        )
    }

    constructor(props) {
        super(props)
        this.renderShop = this.renderShop.bind(this)
        this.onRefresh = this.onRefresh.bind(this)
    }

    componentDidMount() {
        this.setState({isLoading: true})
        this.getShops()
    }

    getShops() {
        let {currentPosition} = this.props;
        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data = {
            lat: currentPosition.coords.latitude,
            lon: currentPosition.coords.longitude,
        };
        axios.post(Urh + 'getclosetshops', data, configpic)
            .then((res) => {
                let shops = [];
                res.data.map((shop, index) => {
                    let distance = geolib.getDistance(
                        {
                            latitude: shop.coordinates.lat,
                            longitude: shop.coordinates.long
                        },
                        {
                            latitude: currentPosition.coords.latitude,
                            longitude: currentPosition.coords.longitude
                        }
                    )
                    shop.distance = distance
                    shops.push(shop)
                })
                console.log(shops)
                this.setState({
                    shops: shops,
                    isLoading: false,
                    refreshing: false,
                });
            })
            .catch(e => {
            })
    }

    renderShop(item, index) {
        let img = "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/" + item.photo;
        return (
            <Block style={styles.card}>
                <ImageOverlay
                    overlayAlpha={0.15}
                    source={{
                        uri: img
                    }}
                    containerStyle={{
                        borderWidth : 1,
                        borderColor : Colors.lightGray,
                    }}
                    height={120}
                    rounded={3}>
                    <Block>
                        <GalioText center h5 style={{
                            fontFamily: fontFamily["Poppins-Bold"]
                        }} color={Colors.white}>{`${item.nom}`}</GalioText>
                        <Block row>
                            <Block middle>
                                <Feather name='map-pin' size={25} color={Colors.blue}/>
                            </Block>
                            <Block bottom style={{marginLeft: 7, marginTop: 5}}>
                                <GalioText
                                    h5
                                    color={Colors.blue}>{`${item.distance / 1000} Km`}</GalioText>
                            </Block>
                        </Block>
                    </Block>
                </ImageOverlay>
                <Block flex row style={{
                    paddingHorizontal : 5
                }}>
                    <Block row center style={{width : '33.33%'}}>
                        <Block>
                            <FontAwesome5 name='map-marked-alt' size={25} color={Colors.orange}/>
                        </Block>
                        <GalioText numberOfLines={1} color={Colors.textColor} style={{marginLeft : 2}}>{item.gouvernorat}</GalioText>
                    </Block>
                    <Block row middle flex>
                        <Block >
                            <FontAwesome5 name='walking' size={25} color={Colors.orange}/>
                        </Block>
                        <GalioText numberOfLines={1} color={Colors.textColor} style={{marginLeft : 2}}>
                            26 visites | 40 Photos
                        </GalioText>
                    </Block>
                    <Block row center
                       style={{ justifyContent : 'flex-end'}}>
                        <Block>
                            <Ionicons name='md-stats' size={25} color={Colors.orange}/>
                        </Block>
                        <GalioText  numberOfLines={1} color={Colors.textColor} style={{marginLeft : 2}}>80% </GalioText>
                    </Block>
                </Block>
            </Block>
        )
    }

    renderShopFlat(item, index) {
        let img = "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/" + item.photo;
        let a = 'https://www.mercedes-benz.com/wp-content/uploads/sites/3/2014/11/classic_store_museumsshop_d385089_3400x1440.jpg'
        return (
            <Block
                style={{
                    backgroundColor: Colors.white,
                    marginTop: 5,
                    marginBottom: 5,
                    borderRadius: 50,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 4,
                }}
            >
                <Block row style={{
                    backgroundColor: Colors.white,
                    height: 160,
                    borderRadius: 10,
                }} space='between'>
                    <Block >
                            <Image
                                style={{minWidth: 120, minHeight: 160,borderBottomLeftRadius : 5,borderTopLeftRadius : 5}}
                                source={{
                                    uri: a,
                                }}
                            />
                    </Block>
                    <Block style={{marginLeft: 2}} flex>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('EditPdv', {
                            pdv : JSON.stringify(item),
                        })} style={{flex: 1, justifyContent: 'space-around', padding : 4}}>
                               <Block>
                                   <GalioText numberOfLines={1} h5 color={Colors.textColor}>
                                       {item.nom}
                                   </GalioText>
                               </Block>
                            <Block row  >
                                <Block flex >
                                    <GalioText color={Colors.gray}
                                               numberOfLines={1}>
                                        {`${item.gouvernorat}, ${item.delegation}`}</GalioText>

                                    <Block row>
                                      {/*  <Block center>
                                            <FontAwesome5 name={'walking'} size={24}
                                                          color={Colors.orange} />
                                        </Block>
                                        <GalioText style={{marginLeft : 2,paddingTop : 2}} numberOfLines={1} color="#B2C1CC">{`26 visites | 40 Photos`}</GalioText>*/}
                                    </Block>
                                </Block>
                                <Block center style={{marginRight: 5}}>
                                </Block>
                            </Block>
                            <Block row space="between">
                                <Block row>
                                    <FontAwesome5 name={'map-marker-alt'} size={24}
                                          color={Colors.blue}/>
                                    <Block center
                                           style={{marginLeft: 5, paddingTop: 3}}>
                                        <GalioText
                                            style={{
                                                fontSize: 14
                                            }}
                                            color={Colors.textColor}>{`${item.distance / 1000} Km `}</GalioText>
                                    </Block>
                                </Block>
                                <Block row middle center>
                                    <Block center>
                                        <Ionicons name='md-stats' size={25} color={Colors.blue}/>
                                    </Block>
                                    <Block bottom style={{marginLeft: 2}}>
                                        <GalioText color={Colors.textColor} >  Conformité 80% </GalioText>
                                    </Block>
                                </Block>
                            </Block>
                        </TouchableOpacity>
                    </Block>
                </Block>
            </Block>
        )
    }
    onRefresh() {
        this.getShops();
    }
    render() {
        let {navigation} = this.props;
        let {shops, isLoading, refreshing} = this.state;
        return (
            <View style={{flex: 1}}>
                {!isLoading ?
                    <FlatList
                        refreshing={refreshing}
                        onRefresh={this.onRefresh}
                        showsVerticalScrollIndicator={false}
                        removeClippedSubviews={false}
                        data={shops}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) => this.renderShopFlat(item, index)}
                    />
                    : <Block flex middle>
                        <ActivityIndicator size={"large"}/>
                    </Block>}
                <View style={{
                    position: 'absolute',
                    backgroundColor: 'transparent',
                    margin: 0,
                    right: (SIZES.width / 2) - 85,
                    bottom: 17,
                    justifyContent: 'flex-start',
                    flexDirection: 'row',
                }}>
                    <ElementsButton
                        Clear
                        onPress={() => navigation.navigate('Information')}
                        title={"Ajouter PDV"}
                        icon={
                            <Entypo name={'plus'} color={Colors.white} size={24}/>
                        }
                        titleStyle={{
                            marginHorizontal: 10,
                            fontFamily: fontFamily["Poppins-Regular"],
                            fontSize: 15,
                            color: Colors.white
                        }}
                        buttonStyle={{
                            backgroundColor: Colors.blue,
                            borderRadius: 20,
                            paddingHorizontal : 15,
                            borderRightColor: Colors.white,
                        }}
                    >
                    </ElementsButton>
                </View>
            </View>
        );
    };
};
const styles = StyleSheet.create({
    card: {
        backgroundColor: Colors.white,
        marginVertical: 5,
        borderRadius: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 4,
        height: 170,
    }

})