import React from 'react';
import {
    ActivityIndicator, FlatList,
    Image, PermissionsAndroid, TouchableOpacity, View,
    ScrollView,
    Modal as NativeModal,
    Keyboard
} from 'react-native';
import {Header} from 'native-base';
import DatePicker from 'react-native-datepicker'
import Slider from 'react-native-slider';
import * as Animatable from "react-native-animatable";
import Icon from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {SvgUri} from "react-native-svg";
import axios from "axios";
import {fontFamily, Logo, SIZES, Urh} from "../utils/Const";
import {Text as TextPaper, Divider, Checkbox, Searchbar, Switch, Chip} from 'react-native-paper';
import Colors from "../utils/Colors";
import AsyncStorage from "@react-native-community/async-storage";
import {Block, Text as GalioText} from "galio-framework";
import {Button as ElementsButton} from "react-native-elements";
import Modal from "react-native-modal";
import ImageOverlay from "react-native-image-overlay";
import {RenderNoConnection, RenderEmptyMissions} from '../utils/staticComponents'
import {Button} from "native-base";
import set from "@babel/runtime/helpers/esm/set";
import moment from "moment";
import PointDeVente from "./PointDeVente";
import ImageLoad from 'react-native-image-placeholder';

var geolib = require('geolib');
const AnimatableText = Animatable.createAnimatableComponent(GalioText);

export default class ListMission2 extends React.PureComponent {
    user = ""
    socket
    reservedmisid
    campid

    state = {
        disable: "",
        visible: true,
        missions: [],
        missionNotCompleted: [],
        currentPosition: {},
        isLoading: true,
        renderError: false,
        isMapClicked: false,
        user: {},
        noConnection: false,
        isModalInfoVisible: false,
        missValide: false,
        profileComplete: false,
        parinage: false,
        firstMission: false,
        dixMission: false,
        abondanner: false,
        parinagenom: "",
        filtredCampaigns: [],
        filtredgouvernorat: [],
        filtredPdv: [],
        listmissions: [],
        prioritePdv: [{
            ppdv: "Haute",
            checked: true,
        }, {
            ppdv: "Moyenne",
            checked: true,
        }, {
            ppdv: "Normale",
            checked: true,
        }],
        isSearchBarVisible: false,
        query: '',
        tags: [{
            title: 'Check-list',
            isSelected: true,
        }, {
            title: 'Point de vente',
            isSelected: false,
        }],
        selectedIndex: 0,
        isModalCheckListVisible: false,
        isModalPointDeVenteVisible: false,
        range: 50,
        slideron: false,
        startDate: null,
        endDate: null,
        hautepdv: 'unchecked',
        moyennepdv: 'unchecked',
        normalpdv: 'unchecked',
        isFilterOpen: false,
        filter: {
            pdvPriority: false,
            checkListPriority: false,
            distance: 1,
            checklists: [],
            pdvType: [],
            date: {
                start: null,
                end: null,
            }
        },
        isFetching: false,
    };

    constructor(props) {
        super(props);
        this.goToMap = this.goToMap.bind(this);
        this.selectMission = this.selectMission.bind(this);
        this.goToPlus = this.goToPlus.bind(this);
        this.reservedmissrejet = this.reservedmissrejet.bind(this);
        this.handleChangePriorities = this.handleChangePriorities.bind(this)
        this.handleChangePdvPriority = this.handleChangePdvPriority.bind(this)
        this.handleChangeCheckListPriority = this.handleChangeCheckListPriority.bind(this)
        this.onRefresh = this.onRefresh.bind(this)
    }

    onRefresh() {


        let {user, fetchData, updateMyState} = this.props.screenProps;
        // updateMyState({refreshing: true}).then(() => {
        //     fetchData(user.categorie).then(() => {
        //         updateMyState({refreshing: false})
        //     })
        // })
        this.closeModalFilter()
    }


    notreserved = async () => {
        try {
            await AsyncStorage.setItem('@reserved', "false")
            await AsyncStorage.setItem('@pic1', "false")
            await AsyncStorage.setItem('@pic2', "false")
            await AsyncStorage.setItem('@quest', "false")
        } catch (e) {
            // saving error
        }
    }
    reservedmiss = async () => {
        try {
            await AsyncStorage.setItem('@reserved', "true")

        } catch (e) {
            // saving error
        }
    }

    getUser = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            //this.state.user = JSON.parse(token)
            this.setState({user: JSON.parse(token)})
            return JSON.parse(token)
        } catch (error) {
        }
    }

    storeData = async (variable) => {
        try {
            await AsyncStorage.setItem('@storage_Key', JSON.stringify(variable))
        } catch (e) {
            // saving error
        }
    }

    componentDidMount() {
        this.getUser().then((user) => {


            let configpic = {
                headers: {
                    'Content-Type': 'application/json',
                }
            };
            let data = {
                id_user: user._id,

            }
            axios.put(Urh + 'modale', data, configpic)
                .then((res) => {

                    if (res.data && res.data.alluser) {

                        let modaleinfo = res.data.alluser.modal

                        if (modaleinfo.missValide == 1)
                            this.setState({missValide: true})


                        if (modaleinfo.profileComplete == 1)
                            this.setState({profileComplete: true})


                        if (modaleinfo.parinage == 1)
                            this.setState({parinage: true, parinagenom: modaleinfo.parinagenom})

                        if (modaleinfo.firstMission == 1)
                            this.setState({firstMission: true})

                        if (modaleinfo.dixMission == 1)
                            this.setState({dixMission: true})


                    }
/////////////////////////////////reupdatemodale////////////////////////////////////////////////////////////

                    axios.put(Urh + 'reupdatemodale', data, configpic)
                        .then((res) => {

                            if (res.data && res.data.alluser)
                                this.storeData(res.data.alluser)


                        })
                        .catch((err) => {
                            this.setState({visible: true})

                        })


                })
                .catch((err) => {
                    this.setState({visible: true})

                })


            let {filtredCampaigns, filtredPdv} = this.state


            let today = new Date()
            let validcategories = []
            user.categorie.map((cat) => {

                if (moment(today).isSameOrAfter(cat.start, 'day'))
                    validcategories.push(cat.categorie)

            })

            validcategories.map((pdv, index) => {
                let Pdv = {
                    pdv: pdv,
                    checked: true,
                }
                filtredPdv.push(Pdv)
            })
            this.setState({filtredPdv})


            // categorie
            let configpic2 = {
                headers: {
                    'Content-Type': 'application/json',
                }
            };
            let data2 = {
                categorie: JSON.stringify(validcategories),

            }
            axios.post(Urh + 'getfiltrecampaigne', data2, configpic2)
                .then((res) => {
                    let filtredCampaigns = [];
                    res.data.map((camp, index) => {
                        let campagne = {
                            _id: camp._id,
                            NomCampagne: camp.NomCampagne,
                            priorite: camp.priorite,
                            checked: true,
                        }
                        filtredCampaigns.push(campagne)
                    });
                    this.setState({filtredCampaigns})
                })
                .catch(e => {
                })


        }).catch(e => {
        })

    }

    continuermission = (mission) => {
        const {
            missions,
            isLoading,
            withReservedMission,
            user,
            updateMyState,
        } = this.props.screenProps

        if (!isLoading) {
            updateMyState({
                selectedMission: mission
            }).then(() => {

                mission.alldata.cartographie === 1 ?
                    this.props.navigation.navigate('Information') :
                    this.props.navigation.navigate('ResumeMission')


            }).catch(e => {
            })
        }
    }

    selectMission(mission, index) {
        const {
            missions,
            isLoading,
            withReservedMission,
            user,
            updateMyState,
        } = this.props.screenProps

        if (!isLoading) {
            if (withReservedMission) {
                this.setState({abondanner: true})
            } else {
                updateMyState({
                    selectedMission: mission
                }).then(() => {
                    mission.alldata.cartographie === 1 ?
                        this.props.navigation.navigate('Information') :
                        this.props.navigation.navigate('ResumeMission')
                }).catch(e => {
                })
            }
        }
    }

    reservedmissrejet() {
        let {updateMyState, missions, user, fetchData} = this.props.screenProps
        updateMyState({isLoading: true})
        let reservedmisid = missions[0].alldata._id
        let campid = missions[0].alldata.id_campaigne


        let today = new Date()
        let validcategories = []
        user.categorie.map((cat) => {

            if (moment(today).isSameOrAfter(cat.start, 'day'))
                validcategories.push(cat.categorie)

        })

        let configpic = {
            headers: {
                'contentType': "application/json",
            }
        }
        axios.put(Urh + 'validourejet', {
            missionid: reservedmisid,
            missionstat: 1
        }, configpic)
            .then((res) => {
                axios.put(Urh + 'rejetermission', {id_campaigne: campid}, configpic)
                    .then((res) => {
                        // this.fetchData()
                        /*  this.updatereserved().then(() => {
                              console.log('updatereserved.....')
                          })*/


                        axios.post(Urh + 'abondancemission', {id_user: user._id}, configpic)
                            .then((res3) => {
                                updateMyState({withReservedMission: false}).then(() => {
                                    fetchData(validcategories)
                                })
                                this.setState({abondanner: false})
                            })
                            .catch((err) => {

                            })


                    })
                    .catch((err) => {
                        this.setState({visible: false})

                    })

            })
            .catch((err) => {
                this.setState({visible: false})

            })
    }

////////////////////////////reserved missions ///////////////
    renderMissionreserved = (mission) => {
        let daysleft = "-"
        if (mission !== null) {

            daysleft = moment().format()
            let dy = mission.alldata.end_date


            daysleft = moment(dy).diff(daysleft, 'days')

            if (daysleft <= 0)
                daysleft = 1


            if (mission.alldata.priorite === "haute")
                color = Colors.danger
            else if (mission.alldata.priorite === "normale")
                color = Colors.success
            else
                color = Colors.orange
        }
        return mission === null ? null : (
            <View
                style={{
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 4,
                }}
            >
                <Block row style={{
                    borderColor: Colors.orange,
                    borderWidth: 4,
                    backgroundColor: Colors.white,
                    padding: 5,
                    marginVertical: 10,
                    height: 120,
                }} space='between'>
                    <Block>
                        {mission.alldata.photo && mission.alldata.photo.length >= 1 ?
                            <ImageOverlay
                                overlayAlpha={0.15}
                                width={100}
                                source={{
                                    height: 100,
                                    width: 100,
                                    uri: `https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/${mission.alldata.photo}`
                                }}
                                height={100}
                                rounded={3}
                                style={{
                                    resizeMode: "cover",
                                }}>
                                <AnimatableText
                                    useNativeDriver
                                    animation="pulse" easing="ease-out"
                                    iterationCount="infinite"
                                >
                                    <GalioText center h5 style={{
                                        fontFamily: fontFamily["Poppins-Bold"]
                                    }} color={Colors.white}>{`Resérvée`}</GalioText>
                                </AnimatableText>
                            </ImageOverlay>
                            :
                            <ImageOverlay
                                overlayAlpha={0.15}
                                width={100}
                                source={{
                                    height: 100,
                                    width: 100,
                                    maxHeight: 100,
                                    maxWidth: 100,
                                    uri: `https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/Food+Grocery.png`
                                }}
                                height={100}
                                rounded={3}
                                style={{
                                    resizeMode: "cover",
                                }}>
                                <AnimatableText
                                    useNativeDriver
                                    animation="pulse" easing="ease-out"
                                    iterationCount="infinite"
                                >
                                    <GalioText center h5 style={{
                                        fontFamily: fontFamily["Poppins-Bold"]
                                    }} color={Colors.white}>{`Resérvée`}</GalioText>
                                </AnimatableText>
                            </ImageOverlay>}
                    </Block>
                    <Block style={{marginLeft: 2}} flex>
                        <TouchableOpacity
                            onPress={() => this.continuermission(mission)}
                            style={{flex: 1, justifyContent: 'space-between'}}>
                            <Block>
                                <GalioText numberOfLines={1} h5 color={Colors.textColor}>
                                    {mission.nomcamp}
                                </GalioText>
                            </Block>
                            <Block row flex middle style={{marginBottom: 10}}>
                                <Block flex style={{marginLeft: 5}}>

                                </Block>

                            </Block>
                            <Block row space="between">
                                <Block row>
                                    {
                                        mission.alldata.cartographie === 1 ? null :
                                            <Icon name={'map-marker-alt'} size={24}
                                                  color={Colors.orange}/>
                                    }
                                    <Block center
                                           style={{marginLeft: 5, paddingTop: 3}}>
                                        {
                                            mission.alldata.cartographie === 1 ? null :
                                                <GalioText
                                                    style={{
                                                        fontSize: 15
                                                    }}
                                                    color={Colors.textColor}>{`${mission.distance / 1000} Km `}</GalioText>
                                        }
                                    </Block>
                                </Block>
                                <Block row middle center>
                                    <Block middle>
                                        <AntDesign name={'calendar'} size={24}
                                                   color={Colors.orange}/>
                                    </Block>
                                    <Block bottom style={{marginLeft: 5, paddingTop: 3}}>
                                        {daysleft == 1 ? <GalioText color={Colors.textColor} style={{
                                                fontSize: 14
                                            }}> {daysleft} jour restant </GalioText> :
                                            <GalioText color={Colors.textColor} style={{
                                                fontSize: 14
                                            }}> {daysleft} jours restant </GalioText>}
                                    </Block>
                                </Block>
                            </Block>
                        </TouchableOpacity>
                    </Block>
                </Block>
            </View>
        )
    }

    /////////////////////////////////////////render mission////////////
    renderMissions = (mission, index, withReservedMission) => {

        let isTheFirstMission = (index === 1);
        let marginTop = isTheFirstMission && !withReservedMission ? 10 : 5
        let color

        let daysleft = "-"

        if (mission !== null) {

            daysleft = moment().format()
            let dy = mission.alldata.end_date


            daysleft = moment(dy).diff(daysleft, 'days')

            if (daysleft <= 0)
                daysleft = 1


            if (mission.alldata.priorite === "haute")
                color = Colors.danger
            else if (mission.alldata.priorite === "normale")
                color = Colors.success
            else
                color = Colors.orange
        }


        return mission === null || index === 0 ? null : (
            <Block
                style={{
                    backgroundColor: Colors.white,
                    marginTop: marginTop,
                    marginBottom: 5,
                    borderRadius: 50,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 4,
                }}
            >
                <Block row style={{
                    backgroundColor: Colors.white,
                    padding: 5,
                    height: 120,
                    borderRadius: 10,
                }} space='between'>
                    <Block center>
                        {mission.alldata.photo && mission.alldata.photo.length >= 1 ?
                            <ImageLoad
                                borderRadius={5}
                                style={{minWidth: 100, minHeight: 100, resizeMode: "cover", borderRadius: 2}}

                                loadingStyle={{size: 'small', color: Colors.orange}}
                                source={{uri: `https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/${mission.alldata.photo}`}}
                            /> :

                            <ImageLoad
                                borderRadius={5}
                                style={{minWidth: 100, minHeight: 100, resizeMode: "cover", borderRadius: 2}}
                                loadingStyle={{size: 'small', color: 'blue'}}
                                source={{
                                    uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/Food+Grocery.png',
                                }}/>}
                    </Block>
                    <Block style={{marginLeft: 2}} flex>
                        <TouchableOpacity onPress={() => this.selectMission(mission, index)}
                                          style={{flex: 1, justifyContent: 'space-between'}}>
                            <Block>
                                <GalioText numberOfLines={1} h5 color={Colors.textColor}>
                                    {mission.nomcamp}
                                </GalioText>
                            </Block>
                            <Block row flex middle style={{marginBottom: 10}}>
                                <Block flex style={{marginLeft: 5}}>
                                    {mission.alldata.nom_magasin ? <GalioText color={Colors.gray}
                                                                              numberOfLines={1}>{mission.alldata.nom_magasin}</GalioText> :
                                        <GalioText color="#2A2B31"
                                                   numberOfLines={1}>

                                        </GalioText>}

                                    <GalioText numberOfLines={1} color="#B2C1CC">{mission.rue}</GalioText>
                                </Block>
                                <Block center style={{marginRight: 5}}>
                                </Block>
                            </Block>
                            <Block row space="between">
                                <Block row>
                                    {
                                        mission.alldata.cartographie === 1 ? null :
                                            <Icon name={'map-marker-alt'} size={24}
                                                  color={Colors.orange}/>
                                    }
                                    <Block center
                                           style={{marginLeft: 5, paddingTop: 3}}>
                                        {mission.alldata.cartographie === 1 ? null : <GalioText
                                            style={{
                                                fontSize: 14
                                            }}
                                            color={Colors.textColor}>{`${mission.distance / 1000} Km `}</GalioText>}
                                    </Block>
                                </Block>
                                <Block row middle center>
                                    <Block middle>
                                        <AntDesign name={'calendar'} size={24}
                                                   color={Colors.orange}/>
                                    </Block>
                                    <Block bottom style={{marginLeft: 5, paddingTop: 3}}>

                                        {daysleft == 1 ? <GalioText color={Colors.textColor} style={{
                                                fontSize: 14
                                            }}> {daysleft} jour restant </GalioText> :
                                            <GalioText color={Colors.textColor} style={{
                                                fontSize: 14
                                            }}> {daysleft} jours restant </GalioText>}
                                    </Block>
                                </Block>
                            </Block>
                        </TouchableOpacity>
                    </Block>
                </Block>
            </Block>
        )
    }

    handleSearchInput = () => {
        let {isSearchBarVisible} = this.state
        this.setState({isSearchBarVisible: !isSearchBarVisible}, () => {
            if (this.state.isSearchBarVisible)
                this.searchBar.focus()
            else
                Keyboard.dismiss()

        })
    }

    renderHeader = () => {
        let {user, missions} = this.props.screenProps
        if (missions.length > 1)
            this.setState({listmissions: missions})

        return (
            <Block row shadow right middle style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 10,
            }}>
                <Block center style={{marginRight: 5}}>
                    <TouchableOpacity onPress={() => this.openFilter()}>
                        <AntDesign
                            name={"filter"}
                            color={Colors.white}
                            size={24}
                        />
                    </TouchableOpacity>
                </Block>
                <Block flex center style={{marginLeft: 20}}>
                    <GalioText color={Colors.white} style={{fontSize: 20}}> Check-list </GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    {user.photo && user.photo.length > 0 ?
                        <TouchableOpacity onPress={this.goToPlus}>
                            <Image
                                style={{
                                    height: 35,
                                    width: 35,
                                    borderRadius: 18,
                                    borderColor: Colors.white,
                                    borderWidth: 1,
                                }}
                                source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + user.photo}}/>
                        </TouchableOpacity>
                        : null}
                </Block>
            </Block>
        )
    }

    renderError = () => {
        return (
            <View style={{
                height: 130,
                width: '100%',
                backgroundColor: Colors.white,
                borderRadius: 3,
                elevation: 2,
                marginVertical: 5,
                justifyContent: 'space-between',
                flex: 10,
                flexDirection: 'row'
            }}>
                <Image
                    style={{flex: 3}}
                    source={{uri: `https://img.pngio.com/error-png-92-images-in-collection-page-3-error-png-900_858.png`}}
                    resizeMode="contain"
                />
                <View
                    style={{
                        flex: 7,
                        paddingHorizontal: 3,
                        justifyContent: 'space-around',
                        width: '100%'
                    }}
                >
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'center',
                        padding: 3,
                    }}>
                        <View style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                            flex: 1
                            , marginRight: 4
                        }}>
                            <View style={{}}>
                                <TextPaper style={{
                                    marginLeft: 2,
                                    fontFamily: fontFamily["Poppins-Bold"], color: '#333'
                                }}>Oops !</TextPaper>
                                <View>
                                    <View style={{
                                        alignItems: 'center'
                                    }}>
                                        <TextPaper style={{
                                            marginLeft: 5,
                                            fontFamily: fontFamily["Poppins-Regular"]
                                        }}>We can't seem to find your Location,
                                            please enable it and try again
                                            !</TextPaper>
                                    </View>
                                </View>
                                <MaterialCommunityIcons style={{alignSelf: 'flex-end'}}
                                                        name={'reload'} size={32}
                                                        color={'#2b8a9a'}/>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }


    goToMap() {
        this.props.navigation.navigate('Map');
    }

    goToPlus() {
        this.props.navigation.navigate('Plus', {
            toNavProfile: true
        });
    }

    handleChangePriorities(index) {
        let newFiltredCampaigns = [...this.state.filtredCampaigns]

        newFiltredCampaigns[index].checked = !newFiltredCampaigns[index].checked
        this.setState({filtredCampaigns: newFiltredCampaigns}, () => this.closeModalFilter())
    }


    handleChangePrioritiesPDV(index) {
        let newfiltredPdv = [...this.state.filtredPdv]

        newfiltredPdv[index].checked = !newfiltredPdv[index].checked
        this.setState({filtredPdv: newfiltredPdv}, () => this.closeModalFilter())
    }

    handleChangePrioritiesgouvernorat(index) {
        let newfiltredgouvernorat = [...this.state.filtredgouvernorat]

        newfiltredgouvernorat[index].checked = !newfiltredgouvernorat[index].checked
        this.setState({filtredgouvernorat: newfiltredgouvernorat})
    }

    handleChangePDVPRO(index) {
        let newprioritePdv = [...this.state.prioritePdv]

        newprioritePdv[index].checked = !newprioritePdv[index].checked
        this.setState({prioritePdv: newprioritePdv})
    }


    closeModalFilterCheckList = () => {
        this.setState({
            isModalCheckListVisible: false,
        })
    }
    closeModalFilterPointDeVente = () => {
        this.setState({
            isModalPointDeVenteVisible: false,
        })
    }

    motcle = (e) => {
        if (e == "") {
            this.closeModalFilter()
        } else {
            let {updateMyState} = this.props.screenProps;
            let {listmissions} = this.state
            let missions = listmissions
            let miso = []
            miso[0] = missions[0]
            for (var i = 1; i < missions.length; i++) {


                if (missions[i].alldata.nom_magasin && (missions[i].alldata.nom_magasin.indexOf(e) > -1 || missions[i].alldata.nom_magasin.indexOf(e.toLowerCase()) > -1
                    || missions[i].alldata.nom_magasin.indexOf(e.toUpperCase()) > -1))
                    miso.push(missions[i])
                else if (missions[i].alldata.nom_owner_magasin && (missions[i].alldata.nom_owner_magasin.indexOf(e) > -1 || missions[i].alldata.nom_owner_magasin.indexOf(e.toLowerCase()) > -1
                    || missions[i].alldata.nom_owner_magasin.indexOf(e.toUpperCase()) > -1))
                    miso.push(missions[i])
                else if (missions[i].alldata.nomcamp && (missions[i].alldata.nomcamp.indexOf(e) > -1 ||
                    missions[i].alldata.nomcamp.indexOf(e.toLowerCase()) > -1 || missions[i].alldata.nomcamp.indexOf(e.toUpperCase()) > -1))
                    miso.push(missions[i])
                else if (missions[i].alldata.delegation && (missions[i].alldata.delegation.indexOf(e) > -1
                    || missions[i].alldata.delegation.indexOf(e.toLowerCase()) > -1 || missions[i].alldata.delegation.indexOf(e.toUpperCase()) > -1))
                    miso.push(missions[i])
                else if (missions[i].alldata.gouvernorat && (missions[i].alldata.gouvernorat.indexOf(e) > -1
                    || missions[i].alldata.gouvernorat.indexOf(e.toLowerCase()) > -1 || missions[i].alldata.gouvernorat.indexOf(e.toUpperCase()) > -1))
                    miso.push(missions[i])


            }

            if (missions[0] != null)
                updateMyState({
                    withReservedMission: true
                })
            updateMyState({
                missions: miso,
                isLoading: false
            })
        }
    }
    ///////////////////////////end search////////////////
    closeModalFilter = () => {

        let {currentPosition, updateMyState} = this.props.screenProps;
        let {isLoading, startDate, endDate} = this.state
        let idiis = []
        let categoriespdv = []

        let prioritiespdv = []

        let distance = 50000
        if (this.state.slideron == true)

            distance = this.state.range * 1000


        this.state.filtredCampaigns.map((camp, idx) => {
            if (camp && camp.checked == true)
                idiis.push(camp._id)
        })

        this.state.filtredPdv.map((camp, idx) => {
            if (camp && camp.checked == true)
                categoriespdv.push(camp.pdv)
        })


        this.state.prioritePdv.map((camp, idx) => {
            if (camp && camp.checked == true)
                prioritiespdv.push(camp.ppdv.toLowerCase())
        })

        let startdate = "2010-12-01"

        let enddate = "2999-12-01"

        if (startDate != null)
            startdate = startDate
        if (endDate != null)
            enddate = endDate


        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data = {
            categorie: JSON.stringify(categoriespdv),
            idiis: JSON.stringify(idiis),
            prioritiespdv: JSON.stringify(prioritiespdv),
            start: startdate,
            end: enddate,
            distance: distance,
            lat: currentPosition.coords.latitude,
            lon: currentPosition.coords.longitude,

        }

        const fetchMissionsForUsers = [axios.post(Urh + 'missionsforuserfiltre', data, configpic),
            axios.get(Urh + 'reservedforuser/' + this.state.user._id)]
        axios.all(fetchMissionsForUsers)
            .then((res) => {

                /*missions for user*/
                let missions = []
                missions[0] = null
                res[0].data.forEach(mission => {
                    let dato = {
                        nomcamp: mission.nomcamp,
                        city: mission.city,
                        prix: mission.prix,
                        rue: mission.rue,
                        alldata: mission
                    }
                    let distance = geolib.getDistance(
                        {
                            latitude: mission.coordinates.lat,
                            longitude: mission.coordinates.long
                        },
                        {
                            latitude: currentPosition.coords.latitude,
                            longitude: currentPosition.coords.longitude
                        }
                    )
                    dato.distance = distance
                    missions.push(dato)
                })
                /*reservedforuser*/
                if (res[1].data.length > 0) {
                    if (res[1].data && res[1].data[0] && res[1].data[0]._id) {
                        let dato = {
                            nomcamp: res[1].data[0].nomcamp,
                            city: res[1].data[0].city,
                            prix: res[1].data[0].prix,
                            rue: res[1].data[0].rue,
                            alldata: res[1].data[0]
                        }
                        let distance = geolib.getDistance(
                            {
                                latitude: res[1].data[0].coordinates.lat,
                                longitude: res[1].data[0].coordinates.long
                            },
                            {
                                latitude: currentPosition.coords.latitude,
                                longitude: currentPosition.coords.longitude
                            }
                        )
                        dato.distance = distance
                        this.reservedmisid = res[1].data[0]._id
                        this.campid = res[1].data[0].id_campaigne
                        missions[0] = dato

                    }
                }

                this.setState({listmissions: missions})
                if (res[1].data.length > 0)
                    updateMyState({
                        withReservedMission: true
                    })

                updateMyState({
                    missions: missions,
                    isLoading: false
                })

            })
            .catch(e => {

            })
        this.setState({
            isModalCheckListVisible: false, isModalPointDeVenteVisible: false,
        })
    }


    handleChangeTages = (index) => {
        let {
            tags,
        } = this.state;

        if (tags[index].isSelected)
            return

        /* if (index === 1) {
             isModalPointDeVenteVisible = true

             let {gouvernoratlist} = this.props.screenProps
             let {filtredgouvernorat} = this.state

             filtredgouvernorat = []
             gouvernoratlist.map((gouvernorat, index) => {
                 let gouv = {
                     gouvernorat: gouvernorat,
                     checked: true,
                 }
                 filtredgouvernorat.push(gouv)
             })

             this.setState({filtredgouvernorat})


         }*/

        tags[index].isSelected = !tags[index].isSelected
        tags.map((tag, idx) => {
            if (idx !== index)
                tag.isSelected = !tags[index].isSelected
        })
        this.setState({
            tags: tags,
            selectedIndex: index + 1
        }, () => this.forceUpdate())
    };
    _renderTag = (tag, index) => {
        let textColor = tag.isSelected ? Colors.orange : "#707070"
        let borderBottomWidth = tag.isSelected ? 2 : 0
        return (
            <Block row flex style={{
                borderBottomWidth: borderBottomWidth,
                borderBottomColor: Colors.blue,
            }}>
                <Block center flex row>
                    <Block center>
                        {index === 1 ? <Entypo size={24}
                                               name={"shop"}
                                               color={textColor}
                        /> : <Feather name='clipboard' size={25} color={textColor}/>}
                    </Block>
                    <Block center style={{marginLeft: 2}}>
                        <Button
                            onPress={() => this.handleChangeTages(index)}
                            block
                            style={{
                                borderRadius: 0,
                                elevation: 0,
                                backgroundColor: Colors.white,
                            }}
                        >
                                <GalioText size={13} color={textColor}>{tag.title}</GalioText>
                        </Button>
                    </Block>
                </Block>
            </Block>
        )
    }


    handleChangePdvPriority(value) {
        let {filter} = this.state
        filter.pdvPriority = !filter.pdvPriority
        this.setState({filter: filter}, () => this.forceUpdate())

        if (value == true) {
            this.setState({
                prioritePdv: [{
                    ppdv: "Haute",
                    checked: true,
                }, {
                    ppdv: "Moyenne",
                    checked: false,
                }, {
                    ppdv: "Normale",
                    checked: false,
                }],
            }, () => this.closeModalFilter())
        } else {
            this.setState({
                prioritePdv: [{
                    ppdv: "Haute",
                    checked: true,
                }, {
                    ppdv: "Moyenne",
                    checked: true,
                }, {
                    ppdv: "Normale",
                    checked: true,
                }],
            }, () => this.closeModalFilter())
        }
    }

    handleChangeCheckListPriority(value) {

        let {filter} = this.state
        filter.checkListPriority = !filter.checkListPriority
        this.setState({filter: filter}, () => this.forceUpdate())


        let today = new Date()
        let validcategories = []
        this.state.user.categorie.map((cat) => {

            if (moment(today).isSameOrAfter(cat.start, 'day'))
                validcategories.push(cat.categorie)

        })


        let configpic2 = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data2 = {
            categorie: JSON.stringify(validcategories),

        }
        axios.post(Urh + 'getfiltrecampaigne', data2, configpic2)
            .then((res) => {
                let filtredCampaigns = [];

                if (value == true) {
                    res.data.map((camp, index) => {
                        if (camp.priorite == "haute") {
                            let campagne = {
                                _id: camp._id,
                                NomCampagne: camp.NomCampagne,
                                priorite: camp.priorite,
                                checked: true,
                            }
                            filtredCampaigns.push(campagne)
                        }
                    });
                } else {
                    res.data.map((camp, index) => {
                        let campagne = {
                            _id: camp._id,
                            NomCampagne: camp.NomCampagne,
                            priorite: camp.priorite,
                            checked: true,
                        }
                        filtredCampaigns.push(campagne)
                    });

                }


                this.setState({filtredCampaigns}, () => this.closeModalFilter())
            })
            .catch(e => {
            })


    }

    openFilter = () => {
        let {isFilterOpen} = this.state
        this.setState({isFilterOpen: !isFilterOpen})
    }


    render() {
        const {
            noConnection,
            missions,
            renderError,
            isLoading,
            refreshing,
            networkError,
            withReservedMission,
            currentPosition,
            isLocationEnabled,
        } = this.props.screenProps
        let {
            isModalInfoVisible, missValide,
            profileComplete,
            parinage,
            firstMission,
            dixMission,
            parinagenom,
            abondanner,
            filtredCampaigns,
            filtredPdv,
            filtredgouvernorat,
            prioritePdv,
            tags,
            isFilterOpen,
            filter
        } = this.state
        let selectedCampaings = 0
        filtredCampaigns.map((camp, index) => {
            if (camp.checked)
                selectedCampaings++
        })

        let {navigation} = this.props;
        // location not enabled
        if (!isLoading && !networkError && missions.length < 1) {
            return (
                <Block flex style={{backgroundColor: Colors.white}}>
                    <Block>
                        {this.renderHeader()}
                    </Block>
                    <RenderEmptyMissions/>
                </Block>
            )
        }

        if (noConnection)
            return (
                <Block flex style={{backgroundColor: Colors.white}}>
                    <Block>
                        {this.renderHeader()}
                    </Block>
                    <RenderNoConnection/>
                </Block>
            )

        // selectedIndex , 0 => default, 1 => Check-list selected , 2 => Point de vente selected, 3 => Ajouter/Modifer PDV selected

        let {
            isSearchBarVisible,
            query,
            selectedIndex,
            isModalPointDeVenteVisible,
            isModalCheckListVisible,
            hautepdv,
            normalpdv,
            moyennepdv
        } = this.state

        return (
            <View style={{flex: 1, backgroundColor: Colors.backgroundColor2}}>
                {/*///////////////////////////////////////////////////Rejeter abondanner la mission!!!!//////////////////////////////                */}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={abondanner}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Voulez-vous abandonner la mission?
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.setState({abondanner: false})}
                                    style={{
                                        paddingHorizontal: 2,
                                        paddingVertical: 4,
                                        elevation: 0,
                                        backgroundColor: Colors.blue
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 16,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                    marginRight: 10
                                }}
                                           uppercase={false}>Annuler</GalioText>
                            </Button>


                            <Button rounded
                                    onPress={this.reservedmissrejet}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 16,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>Confirmer</GalioText>
                            </Button>


                        </Block>


                    </Block>

                </Modal>
                {/*/////////////////////////////////////MODAL MISS VALIDE//////////////////////////////*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={missValide}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Bravo! Votre mission est validée. Vous gagnez 50 Points d'expérience.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.setState({missValide: false})}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>
                {/*/////////////////////////////////////MODAL profile complete//////////////////////////////*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={profileComplete}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Bravo! vous avez complété votre profil. Vous gagner 100 Points d'expérience
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.setState({profileComplete: false})}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>

                {/*/////////////////////////////////////MODAL parinage//////////////////////////////*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={parinage}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Bravo! votre ami {parinagenom} vient de valider sa 1ère mission. Winshot vous récompense
                                et vous offre 2DT & 200 Points d'expérience.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.setState({parinage: false})}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>
                {/*/////////////////////////////////////MODAL first mission//////////////////////////////*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={firstMission}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Bravo! Vous venez de valider votre 1ère mission. Winshot vous récompense et vous offre
                                2DT & 100 Points d'expérience.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.setState({firstMission: false})}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>

                {/*/////////////////////////////////////MODAL dix mission//////////////////////////////*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={dixMission}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Bravo! Vous avez complété 10 missions successifs. Vous gagner 100 Points d'expérience.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.setState({dixMission: false})}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>
                {/*Modal info !!!*/}
                <Modal style={{
                    flex: 1,
                    maxHeight: 380,
                    marginTop: 50,
                }} backdropOpacity={0.3} hasBackdrop={true}
                       isVisible={isModalInfoVisible}>
                    <Block flex style={{
                        backgroundColor: Colors.white,
                        borderRadius: 5,
                    }}
                           space={"between"}>
                        <Block>
                            {/* block Campagne */}
                            <Block style={{
                                marginTop: 5,
                                padding: 10,

                            }}>
                                <Block row>
                                    <GalioText center size={18} color={Colors.textColor}>
                                        Campagnes sélectionnées
                                    </GalioText>
                                    <GalioText center size={18} muted> {`(${selectedCampaings})`}  </GalioText>
                                </Block>
                                <ScrollView
                                    scrollEnabled={true}
                                    style={{
                                        margin: 5,
                                    }}>
                                    {filtredCampaigns.map((camp, index) => {
                                        let checked = camp.checked ? 'checked' : 'unchecked'
                                        let color
                                        if (camp.priorite === "haute")
                                            color = Colors.red
                                        else if (camp.priorite === "normale")
                                            color = Colors.success
                                        return (
                                            <Block>
                                                <Block row>
                                                    <Block center>
                                                        <Checkbox
                                                            status={checked}
                                                            color={color}
                                                            onPress={() => this.handleChangePriorities(index)}
                                                        />
                                                    </Block>
                                                    <Block center>
                                                        <TouchableOpacity
                                                            onPress={() => this.handleChangePriorities(index)}
                                                        >
                                                            <GalioText
                                                                muted={!camp.checked}
                                                                size={16}
                                                                color={camp.checked ? Colors.textColor : null}>
                                                                {camp.NomCampagne}
                                                            </GalioText>
                                                        </TouchableOpacity>
                                                    </Block>
                                                </Block>
                                                <Divider/>
                                            </Block>
                                        )
                                    })}
                                </ScrollView>
                            </Block>
                        </Block>
                        <TouchableOpacity onPress={() => this.closeModalFilter()}>
                            <Block row style={{
                                justifyContent: 'center',
                                backgroundColor: Colors.orange,
                                paddingVertical: 10,
                                paddingHorizontal: 10,
                            }}>
                                <AntDesign name={"check"} size={24} color={Colors.white}/>
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>Appliquer</GalioText>
                            </Block>
                        </TouchableOpacity>

                    </Block>
                </Modal>


                {/* Modal filter Check-list*/}
                <Modal
                    style={{
                        margin: 0,
                        marginRight: SIZES.width / 5,
                    }}
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={isFilterOpen}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        flex: 1,
                        marginTop: 0,
                        marginBottom: 0,
                        borderRadius: 0,

                    }}>
                        <Block flex>
                            <Block flex style={{
                                marginTop: 2,
                                padding: 10,
                            }}>
                                <Block row>
                                    <Block flex>
                                        <GalioText center size={20} color={Colors.textColor}>
                                            Filtre
                                        </GalioText>
                                    </Block>
                                    <Block>
                                        <TouchableOpacity onPress={() => {
                                            this.openFilter()
                                        }}>
                                            <AntDesign size={24} name={"close"} color={Colors.textColor}/>
                                        </TouchableOpacity>
                                    </Block>
                                </Block>
                                {/* priority */}
                                <Block row style={{
                                    marginVertical: 3
                                }}>
                                    <GalioText center size={15} color={Colors.orange}>
                                        Filtrer par priorité
                                    </GalioText>
                                </Block>
                                <Block style={{
                                    paddingLeft: 10,
                                }}>
                                    <Block row space="between" style={{marginTop: 5}}>
                                        <GalioText center size={15} color={Colors.textColor}>
                                            Priorité check-list
                                        </GalioText>
                                        <Block center>
                                            <Switch
                                                color={Colors.orange}
                                                value={filter.checkListPriority}
                                                onValueChange={this.handleChangeCheckListPriority}
                                            />
                                        </Block>
                                    </Block>
                                    <Block row space="between" style={{
                                        marginTop: 8,
                                    }}>
                                        <GalioText center size={15} color={Colors.textColor}>
                                            Priorité point de vente
                                        </GalioText>
                                        <Block center>
                                            <Switch
                                                color={Colors.orange}
                                                value={filter.pdvPriority}
                                                onValueChange={this.handleChangePdvPriority}
                                            />
                                        </Block>
                                    </Block>
                                </Block>
                                {/*  <Divider style={{
                                    marginVertical : 5
                                }}/>*/}

                                <Divider style={{
                                    marginTop: 5
                                }}/>
                                <ScrollView
                                    scrollEnabled={true}
                                    style={{
                                        flex: 1
                                    }}>
                                    {/*  Distance PDV */}
                                    <Block style={{
                                        marginTop: 5
                                    }}>
                                        <GalioText size={15} color={Colors.orange}>
                                            Filtrer par distance
                                        </GalioText>
                                        <Block row space={"between"}>
                                            <Block flex>
                                                <Slider
                                                    thumbTintColor={Colors.orange}
                                                    maximumTrackTintColor={Colors.inactiveTintColor}
                                                    minimumTrackTintColor={Colors.orange}
                                                    animateTransitions={true}
                                                    step={1}
                                                    minimumValue={1}
                                                    maximumValue={50}
                                                    value={this.state.range}

                                                    onValueChange={(range) => {

                                                        this.setState({
                                                            range,
                                                            slideron: true
                                                        }, () => this.closeModalFilter())
                                                    }}/>
                                            </Block>
                                            <Block center middle style={{
                                                marginHorizontal: 5
                                            }}>
                                                <GalioText size={14}
                                                           color={Colors.textColor}>{this.state.range} KM</GalioText>
                                            </Block>
                                        </Block>
                                    </Block>
                                    <Divider style={{
                                        marginVertical: 5
                                    }}/>
                                    <Block>
                                        {/* block Campagne */}
                                        <Block>
                                            <Block row style={{
                                                marginTop: 5
                                            }}>
                                                <GalioText center size={15} color={Colors.orange}>
                                                    Filtrer par check-list
                                                </GalioText>
                                                <GalioText center size={15}
                                                           muted> {`(${selectedCampaings})`}  </GalioText>
                                            </Block>
                                            <Block style={{
                                                marginLeft: 10
                                            }}>
                                                {filtredCampaigns.map((camp, index) => {
                                                    let checked = camp.checked ? 'checked' : 'unchecked'
                                                    let color
                                                    if (camp.priorite === "haute")
                                                        color = Colors.red
                                                    else
                                                        color = Colors.success

                                                    return (
                                                        <Block row style={{
                                                            paddingVertical: 5
                                                        }}>
                                                            <TouchableOpacity style={{
                                                                backgroundColor: camp.checked ? color : "#dddddd",
                                                                paddingHorizontal: 8,
                                                                paddingVertical: 4,
                                                                borderRadius: 10,
                                                            }}
                                                                              onPress={() => this.handleChangePriorities(index)}
                                                            >

                                                                <GalioText
                                                                    muted={!camp.checked}
                                                                    size={14}
                                                                    color={camp.checked ? Colors.white : Colors.textColor}>
                                                                    {camp.NomCampagne}
                                                                </GalioText>

                                                            </TouchableOpacity>
                                                        </Block>
                                                    )
                                                })}
                                            </Block>
                                        </Block>
                                    </Block>

                                    <Divider style={{
                                        marginVertical: 10
                                    }}/>
                                    {/* type pdv  */}
                                    <Block>
                                        {/* block Campagne */}
                                        <Block>
                                            <Block row>
                                                <GalioText center size={15} color={Colors.orange}>
                                                    Filtrer par type points de vente
                                                </GalioText>
                                            </Block>
                                            <Block style={{
                                                marginLeft: 10
                                            }}>


                                                {filtredPdv.map((camp, index) => {
                                                    let checked = camp.checked ? 'checked' : 'unchecked'
                                                    let color = Colors.blue
                                                    return (

                                                        <Block row style={{
                                                            paddingVertical: 5
                                                        }}>
                                                            <TouchableOpacity style={{
                                                                backgroundColor: camp.checked ? color : "#dddddd",
                                                                paddingHorizontal: 8,
                                                                paddingVertical: 4,
                                                                borderRadius: 10,
                                                            }}
                                                                              onPress={() => this.handleChangePrioritiesPDV(index)}
                                                            >
                                                                <GalioText
                                                                    size={14}
                                                                    color={Colors.white}>
                                                                    {camp.pdv == "PDR" ? "Point de recharge"
                                                                        : camp.pdv == "PDV" ? "Point de vente"
                                                                            : camp.pdv == "PDV LABELISE" ? "Point de vente labellisé"
                                                                                : camp.pdv == "boutique franchise" ? "Point de vente franchisé" : camp.pdv}
                                                                </GalioText>

                                                            </TouchableOpacity>
                                                        </Block>

                                                    )
                                                })}
                                            </Block>
                                        </Block>
                                    </Block>
                                    <Divider style={{
                                        marginVertical: 10
                                    }}/>
                                    {/* Date début check-list */}
                                    <Block style={{
                                        paddingBottom: 20
                                    }}>
                                        <Block row>
                                            <GalioText center size={15} color={Colors.orange}>
                                                Filtrer par date début / date fin
                                            </GalioText>
                                        </Block>
                                        <Block flex style={{
                                            marginVertical: 5
                                        }}>
                                            <Block row>
                                                <Block center>
                                                    <AntDesign style={{
                                                        marginRight: 2
                                                    }} name={'calendar'} color={Colors.inactiveTintColor} size={28}/>
                                                </Block>
                                                <DatePicker
                                                    showIcon={false}
                                                    androidMode={"spinner"}
                                                    minDate={`${(new Date()).getFullYear()}-01-01`}
                                                    maxDate={`${(new Date()).getFullYear() + 1}-12-31`}
                                                    format="YYYY-MM-DD"
                                                    style={{
                                                        width: 100,
                                                        marginHorizontal: 3
                                                    }}

                                                    date={this.state.startDate}
                                                    onDateChange={(date) => {
                                                        this.setState({startDate: date}, () => this.closeModalFilter())

                                                    }}
                                                />
                                                <Block center>
                                                    <GalioText>
                                                        -
                                                    </GalioText>
                                                </Block>
                                                <DatePicker
                                                    showIcon={false}
                                                    androidMode={"spinner"}
                                                    minDate={`${(new Date()).getFullYear()}-01-01`}
                                                    maxDate={`${(new Date()).getFullYear() + 1}-12-31`}
                                                    format="YYYY-MM-DD"
                                                    style={{
                                                        width: 100,
                                                        marginLeft: 3
                                                    }}
                                                    date={this.state.endDate}

                                                    onDateChange={(date) => {
                                                        this.setState({endDate: date}, () => this.closeModalFilter())
                                                    }}
                                                />
                                            </Block>
                                        </Block>
                                    </Block>
                                </ScrollView>
                            </Block>
                        </Block>

                    </Block>

                </Modal>

                {/* Modal filter Point de vente */}
                {/*<Modal*/}
                {/*    backdropOpacity={0.3} hasBackdrop={true}*/}
                {/*    isVisible={isModalPointDeVenteVisible}>*/}
                {/*    <Block style={{*/}
                {/*        backgroundColor: Colors.white,*/}
                {/*        flex: 1,*/}
                {/*        marginTop: 82,*/}
                {/*        marginBottom: 58,*/}
                {/*        borderRadius: 0,*/}

                {/*    }}>*/}
                {/*        <Block flex>*/}
                {/*            <Block flex style={{*/}
                {/*                marginTop: 5,*/}
                {/*                padding: 10,*/}
                {/*            }}>*/}
                {/*                <Block row center>*/}
                {/*                    <GalioText center size={16} color={Colors.textColor}>*/}
                {/*                        PDV sélectionnées*/}
                {/*                    </GalioText>*/}
                {/*                    <GalioText center size={16} muted> {`(3)`}  </GalioText>*/}
                {/*                </Block>*/}
                {/*                <ScrollView*/}
                {/*                    scrollEnabled={true}*/}
                {/*                    style={{*/}
                {/*                        marginTop: 15,*/}
                {/*                        flex: 1*/}
                {/*                    }}>*/}
                {/*                    /!* Proprité *!/*/}
                {/*                    <Block>*/}
                {/*                        <GalioText size={15} color={Colors.textColor}>*/}
                {/*                            Proprité point de vente*/}
                {/*                        </GalioText>*/}
                {/*                        <Block>*/}
                {/*                            {prioritePdv.map((camp, index) => {*/}
                {/*                                let checked = camp.checked ? 'checked' : 'unchecked'*/}
                {/*                                let color*/}
                {/*                                if (camp.ppdv === "Haute")*/}
                {/*                                    color = Colors.red*/}
                {/*                                else if (camp.ppdv === "Normale")*/}
                {/*                                    color = Colors.success*/}
                {/*                                else*/}
                {/*                                    color = Colors.orange*/}
                {/*                                return (*/}
                {/*                                    <Block>*/}
                {/*                                        <Block row>*/}
                {/*                                            <Block center>*/}
                {/*                                                <Checkbox*/}
                {/*                                                    status={checked}*/}
                {/*                                                    color={color}*/}
                {/*                                                    onPress={() => this.handleChangePDVPRO(index)}*/}
                {/*                                                />*/}
                {/*                                            </Block>*/}
                {/*                                            <Block center>*/}
                {/*                                                <TouchableOpacity*/}
                {/*                                                    onPress={() => this.handleChangePDVPRO(index)}*/}
                {/*                                                >*/}
                {/*                                                    <GalioText*/}
                {/*                                                        muted={!camp.checked}*/}
                {/*                                                        size={16}*/}
                {/*                                                        color={camp.checked ? Colors.textColor : null}>*/}
                {/*                                                        {camp.ppdv}*/}
                {/*                                                    </GalioText>*/}
                {/*                                                </TouchableOpacity>*/}
                {/*                                            </Block>*/}
                {/*                                        </Block>*/}
                {/*                                        <Divider/>*/}
                {/*                                    </Block>*/}
                {/*                                )*/}
                {/*                            })}*/}
                {/*                        </Block>*/}

                {/*                    </Block>*/}
                {/*                    /!* type point de vente *!/*/}
                {/*                    <Divider style={{*/}
                {/*                        marginVertical: 15*/}
                {/*                    }}/>*/}
                {/*                    <Block>*/}
                {/*                        <GalioText size={15} color={Colors.textColor}>*/}
                {/*                            Type point de vente*/}
                {/*                        </GalioText>*/}


                {/*                        {filtredPdv.map((camp, index) => {*/}
                {/*                            let checked = camp.checked ? 'checked' : 'unchecked'*/}
                {/*                            let color = Colors.orange*/}
                {/*                            return (*/}
                {/*                                <Block>*/}
                {/*                                    <Block row>*/}
                {/*                                        <Block center>*/}
                {/*                                            <Checkbox*/}
                {/*                                                status={checked}*/}
                {/*                                                color={color}*/}
                {/*                                                onPress={() => this.handleChangePrioritiesPDV(index)}*/}
                {/*                                            />*/}
                {/*                                        </Block>*/}
                {/*                                        <Block center>*/}
                {/*                                            <TouchableOpacity*/}
                {/*                                                onPress={() => this.handleChangePrioritiesPDV(index)}*/}
                {/*                                            >*/}
                {/*                                                <GalioText*/}
                {/*                                                    muted={!camp.checked}*/}
                {/*                                                    size={16}*/}
                {/*                                                    color={camp.checked ? Colors.textColor : null}>*/}
                {/*                                                    {camp.pdv == "PDV LABELISE" ? "PDV labellisé"*/}
                {/*                                                        : camp.pdv == "boutique franchise" ? "PDV franchisé" : camp.pdv}*/}

                {/*                                                </GalioText>*/}
                {/*                                            </TouchableOpacity>*/}
                {/*                                        </Block>*/}
                {/*                                    </Block>*/}
                {/*                                    <Divider/>*/}
                {/*                                </Block>*/}
                {/*                            )*/}
                {/*                        })}*/}


                {/*                    </Block>*/}

                {/*                    <Divider style={{*/}
                {/*                        marginVertical: 10*/}
                {/*                    }}/>*/}
                {/*                    /!*  Distance PDV *!/*/}
                {/*                    <Block>*/}
                {/*                        <GalioText size={15} color={Colors.textColor}>*/}
                {/*                            Distance point de vente*/}
                {/*                        </GalioText>*/}
                {/*                        <Block row space={"between"}>*/}
                {/*                            <Block flex>*/}
                {/*                                <Slider*/}
                {/*                                    thumbTintColor={Colors.orange}*/}
                {/*                                    maximumTrackTintColor={Colors.inactiveTintColor}*/}
                {/*                                    minimumTrackTintColor={Colors.orange}*/}
                {/*                                    animateTransitions={true}*/}
                {/*                                    step={1}*/}
                {/*                                    minimumValue={1}*/}
                {/*                                    maximumValue={20}*/}
                {/*                                    value={this.state.range}*/}
                {/*                                    onValueChange={(range) => {*/}

                {/*                                        this.setState({range, slideron: true})*/}
                {/*                                    }}/>*/}
                {/*                            </Block>*/}
                {/*                            <Block center middle style={{*/}
                {/*                                marginHorizontal: 5*/}
                {/*                            }}>*/}
                {/*                                <GalioText size={14}*/}
                {/*                                           color={Colors.textColor}>{this.state.range} KM</GalioText>*/}
                {/*                            </Block>*/}
                {/*                        </Block>*/}
                {/*                    </Block>*/}
                {/*                    <Divider style={{*/}
                {/*                        marginVertical: 10*/}
                {/*                    }}/>*/}
                {/*                    /!* Gouvernorat  *!/*/}
                {/*                    <Block>*/}
                {/*                        <GalioText size={15} color={Colors.textColor}>*/}
                {/*                            Gouvernorat*/}
                {/*                        </GalioText>*/}


                {/*                        {filtredgouvernorat.map((camp, index) => {*/}
                {/*                            let checked = camp.checked ? 'checked' : 'unchecked'*/}
                {/*                            let color = Colors.orange*/}
                {/*                            return (*/}
                {/*                                <Block>*/}
                {/*                                    <Block row>*/}
                {/*                                        <Block center>*/}
                {/*                                            <Checkbox*/}
                {/*                                                status={checked}*/}
                {/*                                                color={color}*/}
                {/*                                                onPress={() => this.handleChangePrioritiesgouvernorat(index)}*/}
                {/*                                            />*/}
                {/*                                        </Block>*/}
                {/*                                        <Block center>*/}
                {/*                                            <TouchableOpacity*/}
                {/*                                                onPress={() => this.handleChangePrioritiesgouvernorat(index)}*/}
                {/*                                            >*/}
                {/*                                                <GalioText*/}
                {/*                                                    muted={!camp.checked}*/}
                {/*                                                    size={16}*/}
                {/*                                                    color={camp.checked ? Colors.textColor : null}>*/}
                {/*                                                    {camp.gouvernorat}*/}


                {/*                                                </GalioText>*/}
                {/*                                            </TouchableOpacity>*/}
                {/*                                        </Block>*/}
                {/*                                    </Block>*/}
                {/*                                    <Divider/>*/}
                {/*                                </Block>*/}
                {/*                            )*/}
                {/*                        })}*/}


                {/*                    </Block>*/}
                {/*                </ScrollView>*/}
                {/*            </Block>*/}
                {/*        </Block>*/}
                {/*        <TouchableOpacity onPress={() => this.closeModalFilter()}>*/}
                {/*            <Block row style={{*/}
                {/*                justifyContent: 'center',*/}
                {/*                backgroundColor: Colors.orange,*/}
                {/*                paddingVertical: 10,*/}
                {/*                paddingHorizontal: 10,*/}
                {/*            }}>*/}
                {/*                <AntDesign name={"check"} size={24} color={Colors.white}/>*/}
                {/*                <GalioText style={{*/}
                {/*                    fontFamily: fontFamily["Poppins-Regular"],*/}
                {/*                    fontSize: 18,*/}
                {/*                    color: Colors.white,*/}
                {/*                    paddingHorizontal: 15,*/}
                {/*                }}*/}
                {/*                           uppercase={false}>Appliquer</GalioText>*/}
                {/*            </Block>*/}
                {/*        </TouchableOpacity>*/}

                {/*    </Block>*/}

                {/*</Modal>*/}

                <Animatable.View
                    useNativeDriver animation={"fadeInDown"}>
                    {this.renderHeader()}
                </Animatable.View>
                <Animatable.View useNativeDriver animation={"fadeInDown"}
                                 style={{
                                     backgroundColor: Colors.white,
                                     margin: 0
                                 }}>
                    <Block row>
                        <Block flex>
                            <Searchbar
                                placeholder="Chercher par check-list, PDV et plus..."
                                inputStyle={{
                                    placeholder: "Search",
                                    alignSelf: 'center',
                                    fontSize: 13,
                                }}
                                style={{
                                    marginHorizontal: 8,
                                    marginVertical: 5,
                                    borderColor: "#DDDDDD",
                                    borderWidth: 0.5,
                                    height: 37,
                                    backgroundColor: Colors.backgroundColor2,
                                    elevation: 0,
                                    borderRadius: 7,
                                }}
                                onChangeText={query => {

                                    this.setState({query}, () => this.motcle(query));
                                }}
                                onIconPress={() => {
                                    this.setState({query: ''})
                                    this.handleSearchInput()
                                }}
                                iconColor={Colors.inactiveTintColor}
                                value={query}
                                ref={(ref) => this.searchBar = ref}
                            />
                        </Block>
                    </Block>
                </Animatable.View>
                <Block row space={"around"} style={{
                    backgroundColor: Colors.white,
                    paddingHorizontal: 8,
                }}>
                    {tags.map((tag, index) => this._renderTag(tag, index))}
                </Block>

                {/*/////////////////////AJOUT / MODIFIER PDV////////////*/}
                {
                    selectedIndex === 2 && <PointDeVente currentPosition={currentPosition} navigation={navigation}/>
                }
                {isLoading &&
                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={isLoading}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        padding: 15,
                        borderRadius: 10,
                    }}>
                        <Block center style={{justifyContent: 'center'}}>
                            <Image
                                style={{
                                    height: 100,
                                    width: 100,
                                    resizeMode: "contain"
                                }}
                                source={{uri: Logo}}
                            />
                            <ActivityIndicator size={"large"} color={Colors.orange}/>

                            <GalioText muted center> Actualisation des missions à proximité.
                            </GalioText>
                        </Block>
                    </Block>
                </Modal>
                }
                {!renderError && selectedIndex !== 2 &&
                <View style={{flex: 1}}>
                    {missions[0] && this.renderMissionreserved(missions[0])}
                    {missions[1] && <Animatable.View animation={"fadeInUp"} duration={1500} useNativeDriver>
                        <FlatList
                            refreshing={refreshing}
                            onRefresh={this.onRefresh}
                            showsVerticalScrollIndicator={false}
                            removeClippedSubviews={false}
                            data={missions}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) => this.renderMissions(item, index, withReservedMission)}
                        />
                    </Animatable.View>
                    }
                    <View style={{
                        position: 'absolute',
                        backgroundColor: 'transparent',
                        margin: 0,
                        right: (SIZES.width / 2) - 100,
                        bottom: 17,
                        justifyContent: 'flex-start',
                        flexDirection: 'row',
                    }}>
                        <ElementsButton
                            Clear
                            title={"Liste"}
                            icon={
                                <Entypo name={'list'} color={Colors.white} size={24}/>
                            }
                            titleStyle={{
                                marginLeft: 10,
                                fontFamily: fontFamily["Poppins-Regular"],
                                fontSize: 15,
                                color: Colors.white
                            }}

                            buttonStyle={{
                                backgroundColor: Colors.orange,
                                borderTopStartRadius: 20,
                                borderBottomLeftRadius: 20,
                                paddingLeft: 10,
                                borderRightColor: Colors.white,
                            }}
                        >
                        </ElementsButton>
                        <ElementsButton
                            onPress={this.goToMap}
                            Clear
                            title={"Carte"}
                            icon={
                                <Icon name={'map-marked-alt'} color={Colors.white} size={22}/>
                            }
                            titleStyle={{
                                fontFamily: fontFamily["Poppins-Regular"],
                                fontSize: 15,
                                marginLeft: 10,
                                color: Colors.white
                            }}

                            buttonStyle={{
                                backgroundColor: Colors.redBrick,
                                borderTopEndRadius: 20,
                                borderBottomRightRadius: 20,
                                paddingLeft: 10,
                            }}
                        >
                        </ElementsButton>

                    </View>

                </View>
                }
            </View>
        );
    }
}
