import React from 'react';
import {createMaterialTopTabNavigator,} from 'react-navigation';
import Historique from './historique'
import Colors from "../utils/Colors";
import {Block, Text as GalioText} from "galio-framework";
import {Image, TouchableOpacity} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import Feather from 'react-native-vector-icons/Feather'
import Gains from './Gains'
import moneyBagGray from './money-bag-gray.png'
import moneyBagOrange from './money-bag-orange.png'
import {SvgUri} from "react-native-svg";

const TabScreen = createMaterialTopTabNavigator(
    {
        Historique: {
            screen: Historique, navigationOptions: {
                title: 'Mes statistiques',
                tabBarIcon: ({tintColor}) => (<Block center>
                    <Feather name='pie-chart' size={24} color={tintColor}/>
                </Block>),
            },
        },
        Paiments: {
            screen: Gains,
            navigationOptions: {
                title: 'Mes Gains',
                tabBarIcon: ({tintColor}) => {
                    if (tintColor === "#808B9E")
                        return (
                            <Image
                                style={{
                                    width: 24,
                                    height: 24
                                }}
                                source={moneyBagGray}
                            />
                        );
                    return (
                        <Image
                            style={{
                                width: 24,
                                height: 24
                            }}
                            source={moneyBagOrange}
                        />);
                }
            }
        },
    },
    {
        swipeEnabled: true,
        animationEnabled: true,
        activeColor: Colors.activeTintColor,
        inactiveColor: Colors.inactiveTintColor,
        tabBarOptions: {
            showIcon: true,
            upperCaseLabel: false,
            activeTintColor: Colors.activeTintColor,
            inactiveTintColor: Colors.inactiveTintColor,
            style: {
                elevation: 0,
                backgroundColor: Colors.white,
            },
            labelStyle: {
                fontSize: 16,
                paddingTop: 7,
                margin: 0,
                marginLeft: 5,
            },
            indicatorStyle: {
                borderBottomColor: Colors.orange,
                borderBottomWidth: 1,
            },
            tabStyle: {
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
            }
        },
    }
);

export default class NavPayment extends React.Component {
    // to add router to navigation props
    static router = TabScreen.router

    constructor(props) {
        super(props)

        this.state = {
            user: {},
        };
    };

    componentDidMount() {
        this.getUser();
    }

    getUser = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            this.setState({user: JSON.parse(token)})
        } catch (error) {

        }
    };

    renderHeader = (user) => {
        return (
            <Block row space={"around"} middle style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 10,
            }}>
                <Block center flex>
                    <GalioText color={Colors.white} style={{fontSize: 20}}>Gains</GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    {user.photo && user.photo.length > 0 ?
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('NavProfile')
                            }>
                            <Image
                                style={{
                                    height: 35,
                                    width: 35,
                                    borderRadius: 18,
                                    borderColor: Colors.white,
                                    borderWidth: 1,
                                }}
                                source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + user.photo}}/>
                        </TouchableOpacity>
                        : null}
                </Block>
            </Block>
        )
    };

    render() {
        let {user} = this.state
        let {noConnection, currentPosition} = this.props.screenProps;
        return (
            <Block flex style={{backgroundColor: Colors.white}}>
                {this.renderHeader(user)}
                <Block flex>
                    <TabScreen screenProps={{
                        noConnection: noConnection,
                        currentPosition: currentPosition,
                    }} navigation={this.props.navigation}/>
                </Block>
            </Block>
        )
    }
}
