import React, {Component} from "react";
import {Block, Text as GalioText, Text} from 'galio-framework'
import Colors from '../utils/Colors'
import {RenderNoConnection} from "../utils/staticComponents";
import {Divider} from 'react-native-paper'
import {fontFamily, SIZES} from '../utils/Const'
import {Image, SafeAreaView, ScrollView, StyleSheet, TextInput} from "react-native";
import {Button} from "native-base";
import AntDesign from "react-native-vector-icons/AntDesign";
import PhotoUpload from "react-native-photo-upload";
import {Button as ElementsButton} from 'react-native-elements'
import Modal from "react-native-modal";

export default class Gains extends Component {
    state = {
        tags: [
            {
                title: 'E-dinars smart',
                isSelected: true,
            },
            {
                title: 'We Bank Attijari',
                isSelected: false,
            }],

        showDemenderGain: false,
    };

    constructor(props) {
        super(props);
        this.handleModal = this.handleModal.bind(this)
    }

    handleModal() {
        this.setState({
            showDemenderGain : !this.state.showDemenderGain,
        })
    }

    handleChange = index => {
        let {tags} = this.state;
        if (tags[index].isSelected)
            return

        tags[index].isSelected = !tags[index].isSelected
        tags.map((tag, idx) => {
            if (idx !== index)
                tag.isSelected = !tags[index].isSelected
        })
        this.setState({tags: tags}, () => this.forceUpdate())
    };

    _renderTag = (tag, index) => {
        let margin = index === 0 ? 4 : 8
        let textColor = tag.isSelected ? Colors.orange : Colors.gray
        let borderBottomWidth = tag.isSelected ? 2 : 0
        return (
            <Block flex>
                <Button
                    iconLeft
                    onPress={() => this.handleChange(index)}
                    block
                    style={{
                        borderRadius: 0,
                        elevation: 0,
                        backgroundColor: Colors.white,
                        borderBottomWidth: borderBottomWidth,
                        borderBottomColor: Colors.blue,
                    }}
                >

                    <Text h5 color={textColor}>{tag.title}</Text>
                </Button>
            </Block>
        )
    }

    render() {
        let {tags, showDemenderGain} = this.state;
        const {
            noConnection,
        } = this.props.screenProps;

        if (noConnection) {
            return (
                <Block flex style={{backgroundColor: Colors.white}}>
                    <RenderNoConnection/>
                </Block>
            )
        }
        return (
            <Block flex style={{
                backgroundColor: Colors.backgroundColor2,
            }}>
                <Modal
                    backdropOpacity={0.3}
                    hasBackdrop={true}
                    isVisible={showDemenderGain}>
                    <Block style={{
                        height: 200,
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                        justifyContent: 'space-between'
                    }}>

                        <Block style={{
                            marginTop: 30,
                            paddingBottom: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <GalioText center NumberOfLines={2} size={16} color={Colors.textColor}>
                                    Vous avez demandé à récupérer vos gains de 50 Dinars
                                </GalioText>
                            </Block>
                            <GalioText center style={{marginVertical: 10}}>
                                Nous allons vérifier votre demande et vous envoyer un e-mail de confirmation.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,
                        }}>
                            <Button rounded
                                    onPress={this.handleModal}
                                    style={{
                                        marginHorizontal: 7,
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.success
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>
                <ScrollView style={{
                    paddingVertical: 20,
                }}>
                    <Block style={{
                        borderWidth: 1,
                        borderColor: Colors.lightGray,
                        backgroundColor: Colors.white,
                        marginHorizontal: 5,
                        borderRadius: 20,
                    }}>
                        <Block middle style={{
                            paddingVertical: 5,
                            borderTopEndRadius: 20,
                            borderTopStartRadius: 20,
                            backgroundColor: Colors.orange
                        }}>
                            <Text h5 color={Colors.white}>
                                Mes gains
                            </Text>
                        </Block>
                        <Block style={{
                            paddingVertical: 5,
                        }} row space={"between"}>
                            <Block style={{
                                marginLeft: 5
                            }}>
                                <Text style={{
                                    fontSize: 16,
                                }} color={Colors.textColor}>
                                    Solde disponible
                                </Text>
                            </Block>
                            <Block style={{
                                marginRight: 20
                            }}>
                                <Text h5 bold color={Colors.success}>
                                    0 DT
                                </Text>
                            </Block>
                        </Block>
                        <Divider/>
                        <Block style={{
                            paddingVertical: 5,
                        }} row space={"between"}>
                            <Block style={{
                                marginLeft: 5
                            }}>
                                <Text style={{
                                    fontSize: 16,
                                }} color={Colors.textColor}>
                                    Déjà encaissé
                                </Text>
                            </Block>
                            <Block style={{
                                marginRight: 20
                            }}>
                                <Text h5 bold color={Colors.blue}>
                                    0 DT
                                </Text>
                            </Block>
                        </Block>
                        <Divider/>
                        <Block style={{
                            paddingVertical: 5,
                        }} row space={"between"}>
                            <Block style={{
                                marginLeft: 5
                            }}>
                                <Text style={{
                                    fontSize: 16,
                                    fontFamily: fontFamily['Poppins-Bold']
                                }} color={Colors.textColor}>
                                    Total de mes gains
                                </Text>
                            </Block>
                            <Block style={{
                                marginRight: 20
                            }}>
                                <Text style={{
                                    fontSize: 20
                                }} bold color={Colors.orange}>
                                    0 DT
                                </Text>
                            </Block>
                        </Block>
                    </Block>
                    {/*Mon Adresse*/}
                    <Block style={{
                        borderWidth: 1,
                        borderColor: Colors.lightGray,
                        backgroundColor: Colors.white,
                        marginHorizontal: 5,
                        marginVertical: 15,
                        borderRadius: 20,
                    }}>
                        <Block middle style={{
                            paddingVertical: 5,
                            borderTopEndRadius: 20,
                            borderTopStartRadius: 20,
                            backgroundColor: Colors.orange
                        }}>
                            <Text h5 color={Colors.white}>
                                Mon adresse
                            </Text>
                        </Block>
                        <Divider/>
                        <Block style={{
                            paddingVertical: 5,
                        }}>
                            <Block style={styles.SectionStyle}>

                                <TextInput
                                    style={styles.input}
                                    placeholder="Adresse"

                                />
                            </Block>
                        </Block>
                        <Divider/>
                        <Block row style={{
                            paddingVertical: 5,
                        }}>
                            <Block center style={styles.SectionStyle2}>

                                <TextInput
                                    style={styles.input}
                                    placeholder="Code postal"

                                />
                            </Block>
                            <Block center style={[styles.SectionStyle2, {
                                marginLeft: 0,
                            }]}>

                                <TextInput
                                    style={styles.input}
                                    placeholder="Délégation"
                                />
                            </Block>
                        </Block>
                        <Divider/>
                        <Block row style={{
                            paddingVertical: 5,
                        }}>
                            <Block center style={styles.SectionStyle2}>

                                <TextInput
                                    style={styles.input}
                                    placeholder="Gouvernorat"

                                />
                            </Block>
                            <Block center style={[styles.SectionStyle2, {
                                marginLeft: 0,
                            }]}>

                                <TextInput
                                    style={styles.input}
                                    placeholder="Pays"
                                />
                            </Block>
                        </Block>
                    </Block>
                    {/*Mon Identité*/}

                    <Block

                        style={{
                            borderWidth: 1,
                            borderColor: Colors.lightGray,
                            backgroundColor: Colors.white,
                            marginHorizontal: 5,
                            borderRadius: 20,
                        }}>
                        <Block space={"between"} row style={{
                            paddingVertical: 5,
                            borderTopEndRadius: 20,
                            borderTopStartRadius: 20,
                            backgroundColor: Colors.orange
                        }}>
                            <Block middle flex style={{
                                marginLeft: 15
                            }}>
                                <Text h5 color={Colors.white}>
                                    Mon Identité
                                </Text>
                            </Block>
                            <Block center style={{marginRight: 15}}>
                                <AntDesign size={26} name={"questioncircle"}
                                           color={Colors.white}/>
                            </Block>
                        </Block>
                        <Divider/>
                        <Block style={{
                            paddingVertical: 5,
                        }}>
                            <Block center style={styles.SectionStyle2}>
                                <TextInput
                                    style={styles.input}
                                    placeholder="Numéro CIN"

                                />
                            </Block>
                            <Block center style={[styles.SectionStyle2]}>
                                <TextInput
                                    style={styles.input}
                                    placeholder="Délivrée le"
                                />
                            </Block>
                        </Block>
                        <Divider/>
                        <Block row style={{
                            paddingVertical: 10,
                        }}>
                            <Block flex center>
                                <PhotoUpload photoPickerTitle="CIN Recto"
                                             onPhotoSelect={(avatar) => {

                                             }}>
                                    <Image
                                        source={{uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/CIN-Recto.png'}}
                                        style={{
                                            resizeMode: 'contain',
                                            height: 120,
                                            width: (SIZES.width / 2) - 20
                                        }}
                                    />
                                </PhotoUpload>
                            </Block>
                            <Block flex center>
                                <PhotoUpload photoPickerTitle="CIN Verso"
                                             onPhotoSelect={(avatar) => {

                                             }}>
                                    <Image
                                        source={{uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/CIN-Verso.png'}}
                                        style={{resizeMode: 'contain', height: 120, width: (SIZES.width / 2) - 20}}
                                    />
                                </PhotoUpload>
                            </Block>
                        </Block>
                    </Block>

                    {/*Mon Informations*/}

                    <Block style={{
                        borderWidth: 1,
                        borderColor: Colors.lightGray,
                        backgroundColor: Colors.white,
                        marginHorizontal: 5,
                        marginTop: 15,
                        marginBottom: 100,
                        borderRadius: 20,
                    }}>
                        <Block middle style={{
                            paddingVertical: 5,
                            borderTopEndRadius: 20,
                            borderTopStartRadius: 20,
                            backgroundColor: Colors.orange
                        }}>
                            <Text h5 color={Colors.white}>
                                Mes informations
                            </Text>
                        </Block>
                        <Divider/>
                        <Block row space={"around"}>
                            {tags.map((tag, index) => this._renderTag(tag, index))}
                        </Block>
                        <Divider/>

                        {tags[0].isSelected ? <Block>
                            <Block style={{
                                paddingVertical: 5,
                            }}>
                                <Block style={styles.SectionStyle}>

                                    <TextInput
                                        style={styles.input}
                                        placeholder="Numéro carte E-dinars smart"
                                    />
                                </Block>
                            </Block>
                            <Divider/>
                            <Block style={{
                                paddingVertical: 5,
                            }}>
                                <Block style={styles.SectionStyle}>

                                    <TextInput
                                        style={styles.input}
                                        placeholder="Date d'expiration"
                                    />
                                </Block>
                            </Block>
                        </Block> : <Block style={{
                            minHeight: 128,
                        }}>
                            <Block style={{
                                paddingVertical: 5,
                            }}>
                                <Block style={styles.SectionStyle}>
                                    <TextInput
                                        style={styles.input}
                                        placeholder="RIB (20 chiffres)"
                                    />
                                </Block>
                            </Block>
                        </Block>}


                    </Block>


                </ScrollView>
                <Block style={{
                    position: 'absolute',
                    margin: 0,
                    right: (SIZES.width / 4) - 22,
                    bottom: 17,
                    backgroundColor: Colors.white,
                    justifyContent: 'center',
                    borderRadius: 20,
                    flexDirection: 'row',
                }}>
                    <ElementsButton
                        onPress={this.handleModal}
                        buttonStyle={{
                            borderRadius: 20,
                            backgroundColor: Colors.blue,
                        }}
                        titleStyle={{
                            fontSize: 18,
                            fontFamily: fontFamily["Poppins-Regular"],
                            marginHorizontal: 10,
                        }}
                        title={"Récupérer mes gains"}/>
                </Block>
            </Block>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        paddingLeft: 45,
    },
    item: {
        width: SIZES.width - 100,
        height: SIZES.width - 200,
        borderRadius: 20,
    },
    imageContainer: {
        flex: 1,
        backgroundColor: 'white',

        borderRadius: 0,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
    },
    SectionStyle: {
        backgroundColor: Colors.backGroundGray,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 0,
        height: 45,
        borderRadius: 30,
        marginVertical: 5,
        marginHorizontal: 10,
    },
    SectionStyle2: {
        flex: 1,
        backgroundColor: Colors.backGroundGray,
        flexDirection: 'row',
        borderWidth: 0,
        height: 50,
        borderRadius: 30,
        marginVertical: 5,
        marginHorizontal: 10,
    },
    input: {
        fontSize: 17,
        paddingLeft: 15,
        flex: 1,
        borderRadius: 30,
    },
    iconInput: {
        color: Colors.blue,
        paddingRight: 7,
        borderRightWidth: 1,
        borderColor: Colors.lightBlue,
        marginStart: 12,
    },
    container: {
        flex: 1,
        backgroundColor: "#ffffff",
    },
    tabsContainer: {
        alignSelf: 'stretch',
        marginTop: 30,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeContainer3: {
        backgroundColor: 'white',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,

    },
    itemThreeContainer: {
        backgroundColor: 'white',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeSubContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
    },
    itemThreeImage: {
        height: 100,
        width: 100,
        marginRight: 5,
        marginLeft: 5,
    },
    itemThreeContent: {
        paddingLeft: 15,
        textAlign: 'right',
        marginTop: 10,
        display: 'flex', flexDirection: 'row',
        position: 'absolute', right: 5
    },
    itemThreeBrand: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5F5F5F',
        textAlign: 'right',

    },
    itemThreeTitle: {
        fontFamily: "Lato-Regular",
        fontSize: 14,
        color: '#5F5F5F',

    },
    itemThreeSubtitle: {
        fontFamily: "Lato-Regular",
        fontSize: 12,
        color: '#a4a4a4',
    },
    itemThreeMetaContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemThreePrice: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5f5f5f',
        textAlign: 'right',
        marginRight: 10,
    },
    itemThreeHr: {
        flex: 1,
        height: 1,
        backgroundColor: '#e3e3e3',
        marginRight: -15,
    },
    badge: {
        backgroundColor: '#6DD0A3',
        borderRadius: 10,
        paddingHorizontal: 7,
        paddingVertical: 5,
        display: 'flex', flexDirection: 'row'
    },
});
