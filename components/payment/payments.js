/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Dimensions
} from 'react-native';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import {PermissionsAndroid} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Table, TableWrapper, Row, Rows, Col, Cols, Cell} from 'react-native-table-component';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconIonicons from 'react-native-vector-icons/Ionicons'
import Slick from 'react-native-slick';
import Colors from "../utils/Colors";
import {Divider} from "react-native-paper";
import PhotoUpload from 'react-native-photo-upload'

import Ripple from "react-native-material-ripple";
import axios from "axios";
import {fontFamily, Urh} from "../utils/Const";
import {Button as BaseButton, Text as BaseText} from "native-base";
import CinRecto from './CIN-Recto.png'
import CinVerso from './CIN-Verso.png'
const { width: screenWidth } = Dimensions.get('window')


export default class Payments extends Component {
    carousel = ''

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            showmail: 'none',
            showpass: 'none',
            disable: "",
            indexslick: 0,
            instancen: "-",
            instanced: "-",
            pretpaimentn: "-",
            payesn: "-",
            pretpaimentd: "-",
            payesd: "-",
            addresse: "",
            codepostale: "",
            delegation: "",
            gouvernorat: "",
            pays: "",
            isInfoExpended: false,
            isModeExpended: false,
            isIdentityExpended: false,
            entries : [{
                title : 'E-dinar ( La poste )',
                thumbnail : 'http://www.tunisiefocus.com/wp-content/uploads/2016/01/e-dinar-tunisie-payement-%C3%A9lectronique.jpg'
            },{
                title : 'Webank ( Attijari bank )',
                thumbnail : 'https://www.webank.com.tn/assets/images/png/visuels_cartes/007.png'
            }]

        };


        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )

        this._promiseHandling()
    }


    _promiseHandling = async () => {

        try {

            const token = await AsyncStorage.getItem('@storage_Key');

            this.user = JSON.parse(token)


            this.setState({
                instancen: this.user.missinstance,
                instanced: this.user.missinstance * 3 + "DT",
                pretpaimentn: this.user.missvalidnonpay,
                payesn: this.user.missvalidpaye,
                pretpaimentd: this.user.missvalidnonpay * 3 + "DT",
                payesd: this.user.missvalidpaye * 3 + "DT",

                addresse: this.user.lieu,
                codepostale: this.user.codepostal.toString(),
                delegation: this.user.delegation,
                gouvernorat: this.user.gouvernorat,
                pays: this.user.pays
            })


        } catch (error) {

        }

    }

    ToggelInfo = () => this.setState({isInfoExpended: !this.state.isInfoExpended})
    ToggelMode = () => this.setState({isModeExpended: !this.state.isModeExpended})
    Toggelidentity = () => this.setState({isIdentityExpended: !this.state.isIdentityExpended})
    renderIdentity = () => {
            return (
                <View style={{padding : 10}}>
                    <Text numberOfLines={2} style={{color : '#333',fontSize : 15,paddingTop : 10,textAlign : 'center',marginHorizontal : 20}}>
                        Pour recevoir votre paiement, veuillez scanner votre CIN en recto / verso</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <PhotoUpload photoPickerTitle="CIN Recto"
                                     onPhotoSelect={(avatar) => {

                                     }}>
                            <View style={{width: (screenWidth / 2) - 20, height: screenWidth / 2}}>
                                <Image
                                    source={CinRecto}
                                    style={{resizeMode : 'contain',height : '100%',width : '100%'}}
                                />
                            </View>
                        </PhotoUpload>
                        <PhotoUpload photoPickerTitle="CIN Verso"
                                     onPhotoSelect={(avatar) => {

                                     }}>
                            <View style={{width: (screenWidth / 2) - 20, height: screenWidth / 2}}>
                                <Image
                                    style={{resizeMode : 'contain',height : '100%',width : '100%'}}
                                    source={CinVerso}
                                />
                            </View>
                        </PhotoUpload>
                    </View>
                </View>
            )
    }
    renderInfo = () => {
        return (
            <View>

                <View style={{marginHorizontal: 5}}>
                    <View style={{
                        marginVertical: 3,
                        alignItems: 'center',
                    }}>
                        <View style={styles.SectionStyle}>
                            <MaterialCommunityIcons style={styles.iconInput} name="home-city" size={24}/>
                            <TextInput
                                style={styles.input}
                                placeholder="Adresse"
                                keyboardType={'email-adress'}
                            />
                        </View>
                    </View>
                    <View style={{
                        marginVertical: 3,
                        alignItems: 'center',
                    }}>
                        <View style={styles.SectionStyle}>
                            <MaterialCommunityIcons style={styles.iconInput} name="mailbox-outline" size={24}/>
                            <TextInput
                                style={styles.input}
                                placeholder="Code Postal"
                            />
                        </View>
                    </View>
                    <View style={{
                        marginVertical: 3,
                        alignItems: 'center',

                    }}>
                        <View style={styles.SectionStyle}>
                            <MaterialCommunityIcons style={styles.iconInput} name="map" size={24}/>
                            <TextInput
                                style={styles.input}
                                placeholder="Délégation"
                            />
                        </View>
                    </View>
                    <View style={{
                        marginVertical: 3,
                        alignItems: 'center',

                    }}>
                        <View style={styles.SectionStyle}>
                            <MaterialCommunityIcons style={styles.iconInput} name="city" size={24}/>
                            <TextInput
                                style={styles.input}
                                placeholder="Gouvernorat"
                            />
                        </View>
                    </View>
                    <View style={{
                        marginVertical: 3,
                        alignItems: 'center',

                    }}>
                        <View style={styles.SectionStyle}>
                            <MaterialCommunityIcons style={styles.iconInput} name="flag-variant" size={24}/>
                            <TextInput
                                style={styles.input}
                                placeholder="Pays"
                            />
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    renderMode = () => {
        return (
            <View>
                <View style={{marginTop : 10}}>
                    <Carousel
                        sliderWidth={screenWidth}
                        sliderHeight={screenWidth}
                        itemWidth={screenWidth - 60}
                        data={this.state.entries}
                        renderItem={this._renderItem}
                        hasParallaxImages={true}
                    />
                </View>
            </View>
        )
    }


    _renderItem ({item, index}, parallaxProps) {
        return (
            <View style={styles.item}>
                <ParallaxImage
                    source={{uri : item.thumbnail}}
                    containerStyle={styles.imageContainer}
                    style={styles.image}
                    parallaxFactor={0.4}
                    {...parallaxProps}
                />
                <Text style={styles.title} numberOfLines={1}>
                    { item.title }
                </Text>
            </View>
        );
    }

    render() {
        let {isInfoExpended,isModeExpended,isIdentityExpended} = this.state
        return (
            <ScrollView>
                <View style={{paddingHorizontal: 15, paddingBottom: 25, paddingTop : 15}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', margin: 0, padding: 0}}>
                        <Text style={{color: '#757575', fontSize: 18, marginLeft: 0}}>Paiement</Text>
                    </View>
                    <View style={{marginTop: 10}}>
                        <View
                            style={{flexDirection: 'row', justifyContent: 'flex-start', flex: 1, alignItems: 'center'}}>
                            <MaterialCommunityIcons style={{color: Colors.blue, marginBottom: 5, marginLeft: 10}}
                                                    name="home-city" size={24}/>
                            <Text style={{color: '#757575', fontSize: 18, marginLeft: 15}}>Solde disponible</Text>
                            <View
                                style={{
                                    height: 30,
                                    width: 1,
                                    backgroundColor: Colors.gray,
                                    marginLeft: 50,
                                }}
                            />
                            <Text style={{color: Colors.success, fontSize: 18, marginLeft: 22, fontWeight: 'bold'}}>3 DT</Text>
                        </View>
                    </View>
                    <Divider style={{marginHorizontal: 10}}/>
                    <View style={{marginTop: 10}}>
                        <View
                            style={{flexDirection: 'row', justifyContent: 'flex-start', flex: 1, alignItems: 'center'}}>
                            <MaterialCommunityIcons style={{color: Colors.blue, marginBottom: 5, marginLeft: 10}}
                                                    name="home-city" size={24}/>
                            <Text style={{color: '#757575', fontSize: 18, marginLeft: 15}}>Déjà payés</Text>
                            <View
                                style={{
                                    height: 30,
                                    width: 1,
                                    backgroundColor: Colors.gray,
                                    marginLeft: 99,
                                }}
                            />
                            <Text style={{color: Colors.warning, fontSize: 18, marginLeft: 20, fontWeight: 'bold'}}>3
                                DT</Text>
                        </View>
                    </View>
                    <View style={{marginTop: 0, flex: 4, flexDirection: 'row'}}>
                        <Divider style={{flex: 3, marginLeft: 20}}/>
                        <Divider style={{flex: 1, borderWidth: 0.5, marginLeft: 20}}/>
                    </View>
                    <View style={{marginTop: 15}}>
                        <View
                            style={{flexDirection: 'row', justifyContent: 'flex-start', flex: 1, alignItems: 'center'}}>
                            <Text style={{color: '#333', fontSize: 18, marginLeft: 15}}>Total de mes gains</Text>
                            <View
                                style={{
                                    height: 30,
                                    width: 1,
                                    backgroundColor: Colors.textColor,
                                    marginLeft: 60,
                                }}
                            />
                            <Text style={{color: Colors.success, fontSize: 18, marginLeft: 20, fontWeight: 'bold'}}>3
                                DT</Text>
                        </View>
                    </View>
                </View>
                <View style={{flexDirection : 'row',justifyContent : 'center',marginBottom : 15,paddingTop : 2}}>
                    <BaseButton  rounded info full
                                style={{borderRadius: 30, backgroundColor: Colors.blue, elevation: 0,width : '80%'}}
                    >
                        <BaseText style={{fontFamily : fontFamily["Poppins-Regular"],fontSize : 16}}
                                  uppercase={false}
                        >Demander mon paiement</BaseText>
                    </BaseButton>
                </View>
                <View style={{paddingTop: 10, paddingBottom: 5}}>
                    <Divider/>
                    {/*Info !! */}
                    <Ripple onPress={this.ToggelMode}
                            style={{borderBottomWidth: 1, borderBottomColor: Colors.lightGray, padding: 15}}>
                        <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                            <Text style={{flex: 1, marginLeft: 0, color: '#757575', fontSize: 18}}>Mode de paiement</Text>
                            <IconIonicons style={{marginRight : 10}} name={`${isModeExpended ? 'ios-arrow-up' : 'ios-arrow-down'}`} size={24}
                                          color={Colors.orange}/>
                        </View>
                    </Ripple>
                    {isModeExpended && this.renderMode()}
                </View>
                <View style={{paddingHorizontal: 0, paddingTop: 0, paddingBottom: 5}}>
                    {/*Info !! */}
                    <Ripple onPress={this.ToggelInfo}
                            style={{borderBottomWidth: 1, borderBottomColor: Colors.lightGray, padding: 15}}>
                        <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                            <Text style={{flex: 1, marginLeft: 0, color: '#757575', fontSize: 18}}>Information de
                                paiement</Text>
                            <IconIonicons style={{marginRight : 10}} name={`${isInfoExpended ? 'ios-arrow-up' : 'ios-arrow-down'}`} size={24}
                                          color={Colors.orange}/>
                        </View>
                    </Ripple>
                    {isInfoExpended && this.renderInfo()}
                </View>

                <View style={{paddingHorizontal: 0, paddingTop: 0, paddingBottom: 5}}>
                    {/*Info !! */}
                    <Ripple onPress={this.Toggelidentity}
                            style={{borderBottomWidth: 1, borderBottomColor: Colors.lightGray, padding: 15}}>
                        <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                            <Text style={{flex: 1, marginLeft: 0, color: '#757575', fontSize: 18}}>
                                Identité pour paiement
                            </Text>
                            <IconIonicons style={{marginRight : 10}} name={`${isIdentityExpended ? 'ios-arrow-up' : 'ios-arrow-down'}`} size={24}
                                          color={Colors.orange}/>
                        </View>
                    </Ripple>
                    {isIdentityExpended && this.renderIdentity()}
                </View>

                <View style={{flexDirection : 'row',justifyContent : 'center',marginVertical : 15}}>
                    <BaseButton  info full rounded
                                style={{backgroundColor: Colors.orange, elevation: 0,width : '60%'}}
                    >
                        <BaseText style={{fontFamily : fontFamily["Poppins-Regular"],fontSize : 16}} uppercase={false}>Enregistrer</BaseText>
                    </BaseButton>
                </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    title : {
      paddingLeft : 45,
    },
    item: {
        width: screenWidth - 100,
        height: screenWidth - 200,
        borderRadius : 20,
    },
    imageContainer: {
        flex: 1,
        backgroundColor: 'white',

        borderRadius: 0,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
    },
    SectionStyle: {
        backgroundColor: Colors.backGroundGray,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 0,
        height: 50,
        borderRadius: 30,
        marginVertical: 8,
    },
    input: {
        fontSize: 16,
        paddingLeft: 10,
        flex: 1,
        borderRadius: 30,
    },
    iconInput: {
        color: Colors.blue,
        paddingRight: 7,
        borderRightWidth: 1,
        borderColor: Colors.lightBlue,
        marginStart: 12,
    },
    container: {
        flex: 1,
        backgroundColor: "#ffffff",
    },
    tabsContainer: {
        alignSelf: 'stretch',
        marginTop: 30,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeContainer3: {
        backgroundColor: 'white',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,

    },
    itemThreeContainer: {
        backgroundColor: 'white',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeSubContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
    },
    itemThreeImage: {
        height: 100,
        width: 100,
        marginRight: 5,
        marginLeft: 5,
    },
    itemThreeContent: {
        paddingLeft: 15,
        textAlign: 'right',
        marginTop: 10,
        display: 'flex', flexDirection: 'row',
        position: 'absolute', right: 5
    },
    itemThreeBrand: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5F5F5F',
        textAlign: 'right',

    },
    itemThreeTitle: {
        fontFamily: "Lato-Regular",
        fontSize: 14,
        color: '#5F5F5F',

    },
    itemThreeSubtitle: {
        fontFamily: "Lato-Regular",
        fontSize: 12,
        color: '#a4a4a4',
    },
    itemThreeMetaContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemThreePrice: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5f5f5f',
        textAlign: 'right',
        marginRight: 10,
    },
    itemThreeHr: {
        flex: 1,
        height: 1,
        backgroundColor: '#e3e3e3',
        marginRight: -15,
    },
    badge: {
        backgroundColor: '#6DD0A3',
        borderRadius: 10,
        paddingHorizontal: 7,
        paddingVertical: 5,
        display: 'flex', flexDirection: 'row'
    },
});