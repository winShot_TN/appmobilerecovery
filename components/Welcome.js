import React, {Component} from 'react';
import {
    View,
    Image
} from 'react-native'
import {fontFamily, WelcomeImage, Logo} from './utils/Const'
import Colors from './utils/Colors'
import AsyncStorage from "@react-native-community/async-storage";
import * as Animatable from "react-native-animatable";
import {Button, Text} from "native-base";
import {NavigationActions, StackActions} from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
export default class Welcome extends Component {
    state = {
        index: 0,
        bounce: 'bounceIn',
        iterationDelay: 3000,
        didFinishInitial: false,
    };
    componentDidMount() {
        this.islogged().then((loged) => {
            if (loged != null) {
                if (loged != "false"){
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'afterconnexion' })],
                    });
                    this.props.navigation.dispatch(resetAction);
                }
                else
                    this.setState({didFinishInitial: true})
            } else
                this.setState({didFinishInitial: true})
        }).catch(e => {})
    }

    islogged = async () => {
        try {
            return await AsyncStorage.getItem('@storage_Key');

        } catch (error) {

            return false
        }
    };

    handleAnimation = () => {
        const {index, bounce} = this.state
        let newIndex, newBounce
        if (bounce === 'bounceIn') {
            newBounce = 'bounceOut'
            this.setState({bounce: newBounce})
            return
        } else {
            newBounce = 'bounceIn'
        }
        if (index === 4)
            newIndex = 0
        else
            newIndex = index + 1
        this.setState({index: newIndex}, () => {
            this.setState({bounce: newBounce})
        })
    };

    RenderAnimations = () => {
        const {index, bounce} = this.state
        let newIterationDelay
        if (index === 0)
            newIterationDelay = 3000
        else
            newIterationDelay = 500
        return (
            <Animatable.Image
                animation={bounce}
                iterationDelay={newIterationDelay}
                iterationCount={1}
                style={{width: 200, height: 200}}
                source={{uri: WelcomeImage[index]}}
                onAnimationEnd={this.handleAnimation}
                useNativeDriver
            />
        )

    };

    render() {
        let {didFinishInitial} = this.state
        return (
            <View
                style={{backgroundColor: Colors.white, flex: 3, alignItems: 'center', justifyContent: 'space-around'}}>
                <View style={{paddingTop: 10, flex: 2, alignItems: 'center', justifyContent: 'center'}}>
                    {/*  <this.RenderAnimations/>*/}
                    <Image
                        style={{
                            resizeMode: "contain"
                        }}
                        source={{uri: Logo}}
                        width={200}
                        height={200}
                    />
                </View>

                {didFinishInitial &&
                <View style={{width: '80%', alignItems: 'center', flex: 1, justifyContent: 'center'}}>
                    <Button rounded full info style={{borderRadius: 30, backgroundColor: Colors.blue, elevation: 0}}
                            onPress={() => this.props.navigation.navigate('login')}>
                        <Ionicons name={'ios-log-in'} color={Colors.white} size={24}/>
                        <Text style={{fontFamily: fontFamily["Poppins-Regular"]}} uppercase={false}>Connexion</Text>
                    </Button>
                    <Button rounded info full
                            style={{top: 10, borderRadius: 30, backgroundColor: Colors.blue, elevation: 0}}
                            onPress={() => this.props.navigation.navigate('inscription')}>
                        <FontAwesome5 name={'user-plus'} color={Colors.white} size={24}/>
                        <Text style={{fontFamily: fontFamily["Poppins-Regular"]}} uppercase={false}>Inscription</Text>
                    </Button>
                </View>}
            </View>
        );
    };
};

