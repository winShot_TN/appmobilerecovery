/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Button} from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import CameraContainer from 'react-native-simple-camera';

export default class carteimage extends Component {
    mission
    compteid
    camera = ""
    takedpic
    longitude = ""
    latitude = ""

    constructor(props) {
        super(props);
        this.state = {
            visiblespinpos: false,

            visible: true,
            image: null

        }

        this.pic = this.pic.bind(this)
        this.sendpic = this.sendpic.bind(this)

    }


    sendpic() {
        this.props.navigation.navigate('carteformulaire', {
            picpath: this.takedpic,
            latitude: this.latitude,
            longitude: this.longitude,
            compteid: this.compteid

        })
    }

////////////////////////////TAKE PICTURE//////////////////////////////////
    pic(pic) {
        let pico = this.pic
        this.setState({visiblespinpos: true})
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const initialPosition = position;
                console.log(initialPosition)

                this.setState({
                    image: <Image style={{

                        width: "75%",
                        height: 300,
                        alignSelf: "center",
                        marginTop: 10, marginBottom: 20
                    }} source={{uri: pic.path}}/>
                })


                this.takedpic = pic.path
                this.longitude = initialPosition.coords.longitude,
                    this.latitude = initialPosition.coords.latitude
                this.setState({disable: "okey"})

                this.setState({visiblespinpos: false})
            },
            (error) => {
                pico(pic)
                console.log('icant get position')
            },
            {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000}
        )
    }

///////////////////////////////////////////////RENDER///////////////////////////
    render() {

        return (

            <ScrollView style={{width: "100%", height: "100%", backgroundColor: 'rgba(199, 205, 208, 0.27)'}}>


                <Text style={{alignSelf: 'center', fontWeight: "bold", marginTop: 12, fontSize: 16, color: '#000'}}>
                    خوذ تصويرة للواجهة الامامية متع العطار والا الحماص
                </Text>

                <View style={{
                    display: 'flex', flexDirection: 'row', paddingLeft: 7, paddingRight: 7, justifyContent: 'center',
                    alignItems: 'center'
                }}><Image style={{

                    width: "45%",
                    height: 170,


                    marginTop: 10, marginBottom: 20
                }} source={require('../images/hammas.png')}/></View>

                {this.state.image}

                <View style={{display: 'flex', flexDirection: 'row', marginBottom: 30, alignSelf: "center"}}>
                    <CameraContainer
                        style={{width: '50%'}}
                        ref={ref => {
                            this.camera = ref;
                        }}
                        mode="photo"
                        onCapturePhoto={this.pic}
                    />< TouchableOpacity
                    style={{alignSelf: 'center', width: 100, marginLeft: 50, height: 40}}>
                    <Button
                        buttonStyle={{backgroundColor: "rgba(34,81,125,1)", borderRadius: 20, marginTop: 7}}
                        onPress={this.sendpic}
                        title="Continuer"
                        color="#53a1dc"
                        disabled={!this.state.disable}

                    />
                </TouchableOpacity></View>
                <Spinner visible={this.state.visiblespinpos} textContent={"Loading"} textStyle={{color: '#FFF'}}/>
            </ScrollView>
        );
    }
}
