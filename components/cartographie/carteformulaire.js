/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {ScrollView, Text, TextInput, View} from 'react-native';
import axios from "axios";
import {Button} from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from "@react-native-community/async-storage";
import {Urh} from '../utils/Const'

export default class carteform extends Component {
    mission
    compteid
    camera=""
    takedpic
    latitude
    longitude
    socket=""
    constructor(props) {
        super(props);
        this.state = {
            visible:true,
            disable5:"f",
            visiblesend:false

        }
        this.sendDataepicerie=this.sendDataepicerie.bind(this)
        this._promiseHandling()
        const { navigation } = this.props;
        this.takedpic = navigation.getParam('picpath', 'picpath')
        this.latitude = navigation.getParam('latitude', 'latitude')
        this.longitude=navigation.getParam('longitude', 'longitude')
        console.log(this.longitude,this.latitude,this.takedpic)
    }
    _promiseHandling = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            this.compteid = JSON.parse(token)
            this.compteid = this.compteid._id
        } catch (error) {
            console.log(error);
        }
    }
    sendDataepicerie(){
        this.setState({visiblesend:true})
        var postData = new FormData();
        postData.append('nomepicerie', this.state.nomepicerie)
        postData.append("ownername",this.state.ownername)
        postData.append("street",this.state.street)
        postData.append("id_compte",this.compteid)
        postData.append("latitude",this.latitude)
        postData.append("longitude",this.longitude)
        postData.append("status",0)
        postData.append('my_epicerie_photo', {
            uri: this.takedpic,
            name: 'my_epicerie_photo.jpg',
            type: 'image/jpg'
        })

        console.log(postData)

        let configpic = {
            headers: {
                'contentType': "application/json",

                'Content-Type': 'multipart/form-data'
            }
        };

        axios.post(Urh+'Createepicerie',postData, configpic)
            .then((res) => {

                    if(res.data.success){
//////////////////////////////////////////+++++++++++/////SECOND REQUEST FOR INCREMENT USER MISSION RESULTS///////
                        var postData2 = {id_user:this.compteid}
                        let configpic2 = {
                            headers: {
                                'contentType': "application/json",

                            }
                        };
                        axios.post(Urh+'instancemission',postData2,configpic2)
                            .then((res2) => {

                                if(res2.data.success){



                                    this.setState({visiblesend:false})
                                    this.props.navigation.navigate('afterconnexion')
                                }

                                else{this.setState({visiblesend:false})
                                    alert("Connexion error..verify your connection and try againn")}

                            })
                            .catch((err) => {
                                console.log("AXIOS ERROR: ", err);
                                this.setState({visiblesend:false})
                                alert("Connexion error..verify your connection and try again")
                            })

                    }

                    else{this.setState({visiblesend:false})
                        alert("Connexion error..verify your connection and try againn")}}


            )
            .catch((err) => {
                console.log("AXIOS ERROR: ", err);
                this.setState({visiblesend:false})
                alert("Connexion error..verify your connection and try again")
            })





    }

///////////////////////////////////////////////RENDER///////////////////////////
    render() {

        return (

            <ScrollView style={{width:"100%",height:"100%",backgroundColor: '#ffffff'}}>



                <Text style={{marginLeft:5,marginRight:5,alignSelf: 'center',fontWeight:"bold",marginTop:15,fontSize: 20,color:'#000'}}>اكتب المعلومات المطلوبهة بش تربح</Text>

                <View style={{height: 'auto', minHeight:200,width: "98%",backgroundColor:"#fff",paddingTop:10,paddingBottom:10,paddingLeft:5,paddingRight:8,alignSelf: 'center',marginTop:25}} >
                    <Text style={{alignSelf: 'center',marginTop:5,fontSize: 18,color:'#000',fontWeight:"bold"}} >
                    اسم حانوت العطار والا الحماص
                </Text><TextInput
                    style={{width:300,height: 40, borderColor: '#fff',borderBottomColor:"rgba(34,81,125,1)",textAlign: "right" , alignSelf: 'center',borderWidth: 2,backgroundColor: '#ffffff',
                        paddingLeft: 15,
                        paddingRight: 15,marginTop:5}}
                    onChangeText={(e) =>  this.setState({nomepicerie:e})}
                    placeholder="مواد غذائية عامة الامل...مغازة البركة..."
                    value={this.state.nomepicerie}
                />

                <Text style={{alignSelf: 'center',marginTop:25,fontSize: 18,color:'#000',fontWeight:"bold"}} >
                    اسم مولا حانوت العطار والا الحماص
                </Text><TextInput
                    style={{width:300,height: 40, borderColor: '#fff',borderBottomColor:"rgba(34,81,125,1)",textAlign: "right" , alignSelf: 'center',borderWidth: 2,backgroundColor: '#ffffff',
                        paddingLeft: 15,
                        paddingRight: 15,marginTop:5}}
                    onChangeText={(e) =>  this.setState({ownername:e})}
                    placeholder="عم حسن...خالتي زينة..علولو.."
                    value={this.state.ownername}
                /><Text style={{alignSelf: 'center',marginTop:25,fontSize: 18,color:'#000',fontWeight:"bold"}} >
                    اسم النهج الي فيه العطار والا الحماص
                </Text><TextInput
                    style={{width:300,height: 40, borderColor: '#fff',borderBottomColor:"rgba(34,81,125,1)",textAlign: "right" , alignSelf: 'center',borderWidth: 2,backgroundColor: '#ffffff',
                        paddingLeft: 15,
                        paddingRight: 15,marginTop:5}}
                    onChangeText={(e) =>  this.setState({street:e})}
                    placeholder="نهج المنجي سليم... شارع غانة..."
                    value={this.state.street}
                />
                    <Button
                        buttonStyle={{alignSelf: 'center',width:100,height:40,marginTop:30,backgroundColor:"rgba(34,81,125,1)",borderRadius:20,}}
                    onPress={this.sendDataepicerie}
                    title="Continuer"
                    color="#53a1dc"
                    disabled={!this.state.nomepicerie||!this.state.ownername||!this.state.street}
                          /></View>

                <Spinner visible={this.state.visiblesend} textContent={"Loading"} textStyle={{color: '#FFF'}} />

            </ScrollView>
        );
    }
}

