import * as React from 'react';
import {FlatList} from 'react-native'
import {Block, Text as GalioText, Text} from "galio-framework";
import {SvgUri} from "react-native-svg";
import Colors from "../utils/Colors";
import {fontFamily} from "../utils/Const";
import Modal from "react-native-modal";
import AntDesign from "react-native-vector-icons/AntDesign";
import {Button} from "native-base";
import {withNavigationFocus} from 'react-navigation';

export default class Cadeaux extends React.Component {
    state = {
        gifts: [{
            name: "Mug ceramic de qualité",
            points: 200,
        },
            {
                name: "Mug",
                points: 200,
            }, {
                name: "Mug",
                points: 200,
            }, {
                name: "Mug",
                points: 200,
            }]
    };


    renderGift = (item, index) => {
        return (
            <Block
                style={{
                    backgroundColor: Colors.white,
                    marginTop: 5,
                    marginBottom: 5,
                    borderRadius: 50,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 4,
                }}
            >
                <Block row flex style={{
                    backgroundColor: Colors.white,
                    padding: 5,
                    height: 100,
                    borderRadius: 10,
                }} space='between'>
                    <Block
                        center
                        style={{
                            height: 90,
                            width: 90,
                            borderRadius: 45,
                            borderWidth: 1,
                            borderColor: Colors.orange,
                        }}>
                    </Block>
                    <Block flex center space="between">
                        <Block center flex middle>
                            <Text h5 color={Colors.textColor}>
                                {item.name}
                            </Text>
                        </Block>
                        <Block row>
                            <Block flex center>
                                {/*   <AntDesign
                                    name={"lock"}
                                    color={Colors.orange}
                                    size={28}
                                />*/}
                            </Block>
                            <Block center style={{
                                marginRight: 20,
                            }}>
                                <Text h5 color={Colors.textColor}> 22 Points</Text>
                            </Block>
                        </Block>
                    </Block>
                </Block>
            </Block>
        )
    }


    componentDidMount() {

        let {isInCadeaux, updateMyState} = this.props.screenProps

        this.props.navigation
            .addListener('didFocus', (payload) => {
                updateMyState({
                    isInCadeaux: false
                })

            });
    }

    handleModalMesCadeaux = (showModalMesCadeaux) => {
        let {updateMyState} = this.props.screenProps
        updateMyState({
            showModalMesCadeaux: !showModalMesCadeaux
        })
    };

    render() {
        let {gifts} = this.state;
        let {showModalMesCadeaux} = this.props.screenProps

        return (
            <Block flex style={{
                backgroundColor: Colors.white,
            }}>
                {/*Modal info Mes cadeaux !!!*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={showModalMesCadeaux}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 10,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Augmentez vos points d'expérience et débloquer plus de cadeaux.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.handleModalMesCadeaux(showModalMesCadeaux)}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>compris</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>

                <Block row space={"between"} style={{margin: 10}}>
                    <Block row>
                        <Block center>
                            <SvgUri
                                width="24"
                                height="24"
                                uri={'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icon.svg'}
                            />
                        </Block>
                        <Block center style={{marginLeft: 4}}>
                            <Text size={17} color={Colors.textColor}>Mes Points :</Text>
                        </Block>
                    </Block>
                    <Block center row>
                        <Text style={{
                            marginRight: 2,
                            fontFamily: fontFamily["Poppins-Regular"],
                        }} size={20} color={Colors.orange}>500
                        </Text>
                        <Text style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                              color={Colors.lightBlue}>
                            Pts
                        </Text>
                    </Block>
                </Block>
                <Block center style={{
                    width: '100%',
                    height: 1,
                    backgroundColor: Colors.gray,
                }}>
                </Block>
                <Block flex style={{
                    backgroundColor: Colors.backgroundColor2,
                }}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={gifts}
                        keyExtractor={(gift, index) => index.toString()}
                        renderItem={({item, index}) => this.renderGift(item, index)}
                    />
                </Block>


            </Block>
        );
    };
};
