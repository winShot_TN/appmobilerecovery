import * as React from 'react';
import {Image, ScrollView} from 'react-native'
import {Block, Text as GalioText, Text} from "galio-framework";
import Colors from "../utils/Colors";
import {Divider} from 'react-native-paper'
import {fontFamily} from "../utils/Const";
import AntDesign from 'react-native-vector-icons/AntDesign'
import AsyncStorage from '@react-native-community/async-storage';

import {Button} from 'native-base'

import {SvgUri,} from 'react-native-svg';
import Modal from "react-native-modal";

export default class Points extends React.Component {
    user

    constructor(props) {
        super(props);
        this.state = {

            nomprenom: "- -",
            pic: "https://immedilet-invest.com/wp-content/uploads/2016/01/user-placeholder.jpg",
            missions: "0",
            parrains: "0",
            points: "0",
            ptsprofile: "0",
            ptsmission: "0",
            ptsparrain: "0",
            ptsbonus: "0"
        };

    }


    islogged = async () => {
        try {
            const loged = await AsyncStorage.getItem('@storage_Key');
            this.user = JSON.parse(loged)


        } catch (error) {

        }

    }


    componentDidMount() {
        let {isInCadeaux,updateMyState} = this.props.screenProps

        this.didFocusListener = this.props.navigation.addListener(
            'didFocus',
            () => {
                updateMyState({
                    isInCadeaux : true
                })
                this.islogged().then(() => {

                    if (this.user.photo && this.user.photo != "")
                        this.state.pic = "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + this.user.photo
                    this.setState({
                        nomprenom: this.user.nom + " " + this.user.prenom,
                        missions: this.user.missvalidnonpay,
                        parrains: this.user.old_code_parrain.length,
                        points: this.user.points,
                        ptsprofile: this.user.completedprofile,
                        ptsmission: this.user.missvalidnonpay * 50,
                        ptsparrain: this.user.old_code_parrain.length * 200,
                        ptsbonus: Math.trunc(this.user.missvalidnonpay / 10) * 100
                    })
                })


            },
        );
    }

    componentWillUnmount() {

        this.didFocusListener.remove();
    }

    handleModalMesPoints = (showModalMesPoints) => {
        let {updateMyState} = this.props.screenProps
        updateMyState({
            showModalMesPoints: !showModalMesPoints
        })
    };

    render() {

        let {showModalMesPoints} = this.props.screenProps
        return (
            <ScrollView>
                {/*Modal info Mes Points !!!*/}
                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={showModalMesPoints}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        marginTop: 120,
                        padding: 15,
                        borderRadius: 10,
                    }}>
                        <Block row style={{marginHorizontal: 10, marginBottom: 7}}>
                            <Block>
                                <SvgUri
                                    width="24"
                                    height="24"
                                    uri={'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icon.svg'}
                                />
                            </Block>
                            <GalioText size={16} color={Colors.textColor} style={{marginLeft: 5}}>
                                Comment calculer mes points
                            </GalioText>
                        </Block>
                        <Divider/>
                        <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                            <Block center>
                                <GalioText size={14} color={Colors.textColor}>Compléter mon profil</GalioText>
                            </Block>
                            <Block center row>
                                <GalioText style={{
                                    marginRight: 2,
                                    fontFamily: fontFamily["Poppins-Regular"],
                                }} size={17} color={Colors.success}>+100
                                </GalioText>
                                <GalioText style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                           color={Colors.lightBlue}>
                                    Pts
                                </GalioText>
                            </Block>
                        </Block>
                        <Divider/>
                        <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                            <Block center>
                                <GalioText size={14} color={Colors.textColor}>1ér mission Validé (Bonus) </GalioText>
                            </Block>
                            <Block center row>
                                <GalioText style={{
                                    marginRight: 2,
                                    fontFamily: fontFamily["Poppins-Regular"],
                                }} size={17} color={Colors.success}>+100
                                </GalioText>
                                <GalioText style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                           color={Colors.lightBlue}>
                                    Pts
                                </GalioText>
                            </Block>
                        </Block>
                        <Divider/>
                        <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                            <Block center>
                                <GalioText size={14} color={Colors.textColor}>1 mission Validé </GalioText>
                            </Block>
                            <Block center row>
                                <GalioText style={{
                                    marginRight: 2,
                                    fontFamily: fontFamily["Poppins-Regular"],
                                }} size={17} color={Colors.success}>+50
                                </GalioText>
                                <GalioText style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                           color={Colors.lightBlue}>
                                    Pts
                                </GalioText>
                            </Block>
                        </Block>
                        <Divider/>
                        <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                            <Block center>
                                <GalioText size={14} color={Colors.textColor}>1 Parrainage Winshooter </GalioText>
                            </Block>
                            <Block center row>
                                <GalioText style={{
                                    marginRight: 2,
                                    fontFamily: fontFamily["Poppins-Regular"],
                                }} size={17} color={Colors.success}>+200
                                </GalioText>
                                <GalioText style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                           color={Colors.lightBlue}>
                                    Pts
                                </GalioText>
                            </Block>
                        </Block>
                        <Divider/>
                        <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                            <Block center>
                                <GalioText size={14} color={Colors.textColor}>10 missions (Bonus) </GalioText>
                            </Block>
                            <Block center row>
                                <GalioText style={{
                                    marginRight: 2,
                                    fontFamily: fontFamily["Poppins-Regular"],
                                }} size={17} color={Colors.success}>+100
                                </GalioText>
                                <GalioText style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                           color={Colors.lightBlue}>
                                    Pts
                                </GalioText>
                            </Block>
                        </Block>
                        <Divider/>
                        <Block row style={{marginTop: 5, justifyContent: 'center'}}>
                            <Button
                                rounded
                                onPress={() => this.handleModalMesPoints(showModalMesPoints)}
                                style={{
                                    backgroundColor: Colors.orange,
                                    elevation: 0,
                                    paddingHorizontal: 15,
                                }}>
                                <Block>
                                    <GalioText style={{marginVertical: 5}} color={Colors.white}
                                               size={18}>Compris</GalioText>
                                </Block>
                            </Button>
                        </Block>
                    </Block>
                </Modal>
                <Block flex style={{
                    backgroundColor: Colors.white,
                }}>
                    <Block flex center style={{
                        marginVertical: 20,
                    }}>
                        <Image
                            style={{
                                height: 100,
                                width: 100,
                                borderRadius: 50,
                                borderWidth: 1,
                                borderColor: Colors.white,
                                marginLeft: 4,
                            }}
                            source={{uri: this.state.pic}}
                        />
                        <Block middle>
                            <Text style={{
                                fontSize: 22,

                            }} color={Colors.textColor}>{this.state.nomprenom}</Text>
                        </Block>
                        <Block row>
                            <Block center row>
                                <Text style={{
                                    fontSize: 12,
                                    marginRight: 2,
                                    fontFamily: fontFamily["Poppins-Regular"],
                                }} color={Colors.blue}>
                                    {this.state.missions}
                                </Text>
                                <Text
                                    style={{
                                        marginLeft: 2,
                                        fontFamily: fontFamily["Poppins-Light"],
                                        fontSize: 12,
                                    }}
                                    center color={Colors.blue}>
                                    missions
                                </Text>
                            </Block>
                            <Block center>
                                <Text color={Colors.orange} style={{marginHorizontal: 4, fontSize: 12}}>
                                    |
                                </Text>
                            </Block>
                            <Block center row>
                                <Text style={{
                                    fontSize: 12,
                                    marginRight: 2,
                                    fontFamily: fontFamily["Poppins-Regular"],
                                }} color={Colors.blue}>
                                    {this.state.parrains}
                                </Text>
                                <Text style={{
                                    marginLeft: 2,
                                    fontFamily: fontFamily["Poppins-Light"],
                                    fontSize: 12,
                                }} center h5 color={Colors.blue}>
                                    parrainages
                                </Text>
                            </Block>
                        </Block>
                    </Block>
                    <Block right style={{marginHorizontal: 30}}>

                    </Block>
                    <Block row space={"between"} style={{marginHorizontal: 10}}>
                        <Block row>
                            <Block center>
                                <SvgUri
                                    width="24"
                                    height="24"
                                    uri={'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icon.svg'}
                                />
                            </Block>
                            <Block center style={{marginLeft: 4}}>
                                <Text size={17} color={Colors.textColor}>Mes Points :</Text>
                            </Block>

                        </Block>
                        <Block center row>
                            <Text style={{
                                marginRight: 2,
                                fontFamily: fontFamily["Poppins-Regular"],
                            }} size={20} color={Colors.orange}>{this.state.points}
                            </Text>
                            <Text style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                  color={Colors.lightBlue}>
                                Pts
                            </Text>
                        </Block>
                    </Block>
                    <Block center style={{
                        width: '95%',
                        height: 1,
                        backgroundColor: Colors.gray,
                    }}>
                    </Block>
                    <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                        <Block center>
                            <Text size={14} color={Colors.textColor}>Compléter mon profil </Text>
                        </Block>
                        <Block center row>
                            <Text style={{
                                marginRight: 2,
                                fontFamily: fontFamily["Poppins-Regular"],
                            }} size={17} color={Colors.orange}>{this.state.ptsprofile}
                            </Text>
                            <Text style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                  color={Colors.lightBlue}>
                                Pts
                            </Text>
                        </Block>
                    </Block>
                    <Divider style={{width: '100%'}}/>
                    <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                        <Block center>
                            <Text size={14} color={Colors.textColor}>1ér mission Validé (Bonus) </Text>
                        </Block>
                        <Block center row>
                            <Text style={{
                                marginRight: 2,
                                fontFamily: fontFamily["Poppins-Regular"],
                            }} size={17} color={Colors.orange}>{this.state.ptsmission >0 ? 100 : 0}
                            </Text>
                            <Text style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                  color={Colors.lightBlue}>
                                Pts
                            </Text>
                        </Block>
                    </Block>
                    <Divider/>
                    <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                        <Block center>
                            <Text size={14} color={Colors.textColor}>Missions validées </Text>
                        </Block>
                        <Block center row>
                            <Text style={{
                                marginRight: 2,
                                fontFamily: fontFamily["Poppins-Regular"],
                            }} size={17} color={Colors.orange}>{this.state.ptsmission}
                            </Text>
                            <Text style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                  color={Colors.lightBlue}>
                                Pts
                            </Text>
                        </Block>
                    </Block>
                    <Divider/>
                    <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                        <Block center>
                            <Text size={14} color={Colors.textColor}>Parrainage Winshooters </Text>
                        </Block>
                        <Block center row>
                            <Text style={{
                                marginRight: 2,
                                fontFamily: fontFamily["Poppins-Regular"],
                            }} size={17} color={Colors.orange}>{this.state.ptsparrain}
                            </Text>
                            <Text style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                  color={Colors.lightBlue}>
                                Pts
                            </Text>
                        </Block>
                    </Block>
                    <Divider/>
                    <Block row space={"between"} style={{marginHorizontal: 10, marginVertical: 5}}>
                        <Block center>
                            <Text size={14} color={Colors.textColor}>Bonus missions </Text>
                        </Block>
                        <Block center row>
                            <Text style={{
                                marginRight: 2,
                                fontFamily: fontFamily["Poppins-Regular"],
                            }} size={17} color={Colors.orange}>{this.state.ptsbonus}
                            </Text>
                            <Text style={{fontFamily: fontFamily["Poppins-Light"],}} center size={15}
                                  color={Colors.lightBlue}>
                                Pts
                            </Text>
                        </Block>
                    </Block>
                    <Divider/>
                    <Block center style={{marginVertical: 20}}>
                        <Button
                            iconLeft
                            style={{
                                backgroundColor: Colors.orange,
                                elevation: 0,
                                borderRadius: 0,
                                paddingHorizontal: 15,
                            }}>
                            <Block row>
                                <AntDesign name={'Trophy'} size={24} color={Colors.white}/>
                                <Text style={{marginLeft: 5}} color={Colors.white} size={16}>Mes Trophées</Text>
                            </Block>
                        </Button>
                    </Block>
                </Block>
            </ScrollView>
        );
    };
};
