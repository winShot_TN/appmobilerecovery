import {createMaterialTopTabNavigator} from "react-navigation";

import Cadeaux from './Cadeaux'
import Points from './Points'
import Colors from "../utils/Colors";
import React from "react";
import AsyncStorage from "@react-native-community/async-storage";
import {Block, Text as GalioText} from "galio-framework";
import {Image, TouchableOpacity} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import {SvgUri} from "react-native-svg";
import Modal from "react-native-modal";
import {Button} from "native-base";
import {fontFamily} from "../utils/Const";
import {Divider} from 'react-native-paper'

const aa = () => (
    <SvgUri
        width="24"
        height="24"
        uri={'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icon.svg'}
    />
)
const TabScreen = createMaterialTopTabNavigator({
    Points: {
        screen: Points,
        navigationOptions: {
            title: 'Mes Points',
            tabBarIcon: ({tintColor}) => {
                if (tintColor === "#808B9E")
                    return (
                        <SvgUri
                            width="24"
                            height="24"
                            uri={'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icon22.svg'}
                        />
                    )
                return (
                    <SvgUri
                        width="24"
                        height="24"
                        uri={'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icon.svg'}
                    />)
            }
        }
    },
    Cadeaux: {
        screen: Cadeaux,

        navigationOptions: {
            title: 'Mes Cadeaux',
            tabBarIcon: ({tintColor}) => (<Block center>
                <AntDesign name='gift' size={24} color={tintColor}/>
            </Block>),
        },
    },

}, {
    swipeEnabled: true,
    animationEnabled: true,
    activeColor: Colors.activeTintColor,
    inactiveColor: Colors.inactiveTintColor,
    tabBarOptions: {
        showIcon: true,
        upperCaseLabel: false,
        activeTintColor: Colors.activeTintColor,
        inactiveTintColor: Colors.inactiveTintColor,
        style: {
            elevation: 0,
            backgroundColor: Colors.white,
        },
        labelStyle: {
            fontSize: 16,
            paddingTop: 7,
            margin: 0,
            marginLeft: 5,
        },
        indicatorStyle: {
            borderBottomColor: Colors.orange,
            borderBottomWidth: 1,
        },
        tabStyle: {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
        }
    },
});
export default class NavCadeaux extends React.Component {
    // to add router to navigation props
    static router = TabScreen.router


    constructor(props) {
        super(props);
        this.state = {
            user: {},
            showModalMesPoints: false,
            showModalMesCadeaux: false,
            isInCadeaux: true,
        };
    };


    updateMyState = async (data) => {
        this.setState(data)
    }

    componentDidMount() {
        this.getUser();
    }

    getUser = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            this.setState({user: JSON.parse(token)})
        } catch (error) {

        }
    };
    handleModal = () => {
        let {isInCadeaux, showModalMesPoints, showModalMesCadeaux} = this.state
        if (isInCadeaux)
            this.setState({
                showModalMesPoints: true,
                showModalMesCadeaux: false,
            })
        else
            this.setState({
                showModalMesPoints: false,
                showModalMesCadeaux: true,
            })

    }

    renderHeader = (user) => {
        return (
            <Block row space={"between"} middle style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 10,
            }}>
                <Block center style={{marginRight: 5}}>
                    <TouchableOpacity>
                        <AntDesign onPress={() => this.handleModal()} size={28} name={"questioncircle"}
                                   color={Colors.white}/>
                    </TouchableOpacity>
                </Block>
                <Block center style={{marginLeft: 20}}>
                    <GalioText color={Colors.white} style={{fontSize: 20}}>Cadeaux</GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    {user.photo && user.photo.length > 0 ?
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Plus', {
                                toNavProfile: true
                            })}>
                            <Image
                                style={{
                                    height: 35,
                                    width: 35,
                                    borderRadius: 18,
                                    borderColor: Colors.white,
                                    borderWidth: 1,
                                }}
                                source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + user.photo}}/>
                        </TouchableOpacity>
                        : null}
                </Block>
            </Block>
        )
    };

    render() {

        let {
            user,
            showModalMesPoints,
            showModalMesCadeaux,
            isInCadeaux
        } = this.state;

        return (
            <Block flex>
                {this.renderHeader(user)}
                <TabScreen screenProps={{
                    user: user,
                    updateMyState: this.updateMyState,
                    showModalMesPoints: showModalMesPoints,
                    showModalMesCadeaux: showModalMesCadeaux,
                    isInCadeaux: isInCadeaux,
                }}
                           navigation={this.props.navigation}/>
            </Block>
        )
    }

}

