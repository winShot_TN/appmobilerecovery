import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Dimensions, Image, ScrollView, SafeAreaView, ActivityIndicator
} from 'react-native';
import {
    Block,
} from 'galio-framework'
import axios from "axios";
import {PermissionsAndroid} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-custom-toast';
import {
    View as ShoutemView,
    TextInput as ShoutemInput,
} from '@shoutem/ui';
import Colors from './utils/Colors'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Ionicons from 'react-native-vector-icons/Ionicons'
import {Text as BaseText, Button as BaseButton, TextBase} from "native-base"
import {fontFamily, Logo, Urh} from "./utils/Const";
import {NavigationActions, StackActions} from 'react-navigation';
import Modal from "react-native-modal";
import AntDesign from "react-native-vector-icons/AntDesign";

const {width, height} = Dimensions.get('window');


export default class HomeScreen extends Component {
    carousel = ''
    indexslic = 0

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            showmail: 'none',
            showpass: 'none',
            disable: "",
            indexslick: 0,
            visible: false,
            isLoading: false,
            slickk: <Text>
            </Text>

        };
        this.login = this.login.bind(this)
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )
    }

    storeData = async (variable) => {
        try {
            await AsyncStorage.setItem('@storage_Key', JSON.stringify(variable))
        } catch (e) {
            // saving error
        }
    }

    login() {
        if (this.state.email && this.state.password) {
            this.setState({isLoading: true})
            let configpic = {
                headers: {
                    'Content-Type': 'application/json',
                }
            };
            let data = {
                email: this.state.email,
                password: this.state.password
            }
            axios.post(Urh + 'signinuser', data, configpic)
                .then((res) => {
                    if (res.data.success) {
                        this.storeData(res.data.alluser).then(() => {
                            this.setState({isLoading: false}, () => {
                                const resetAction = StackActions.reset({
                                    index: 0,
                                    actions: [NavigationActions.navigate({routeName: 'afterconnexion'})],
                                });
                                this.props.navigation.dispatch(resetAction);
                            })

                        }).catch(error => {})
                    } else if (!res.data.success) {
                        this.setState({isLoading: false})
                        if (res.data.failed == "username") {
                            this.setState({showmail: 'flex'})
                            this.refs.email.showToast('Le pseudo entré est incorrect', 2000);

                        } else if (res.data.failed == "password") {
                            this.setState({showmail: 'flex'})
                            this.refs.password.showToast('Le mot de passe entré est incorrect', 2000);
                        }
                    }
                })
                .catch((err) => {
                    this.setState({visible: true})

                })
        }
    }

    render() {
        let {isLoading} = this.state
        return (
            <ScrollView
                contentContainerStyle={{
                    alignItems: 'center',
                    height: height,
                    justifyContent: 'center',
                    paddingHorizontal: 10
                }}>
                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={isLoading}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        padding: 15,
                        borderRadius: 10,
                    }}>
                        <Block center style={{justifyContent: 'center'}}>
                            <Image
                                style={{
                                    height: 100,
                                    width: 100,
                                }}
                                source={{uri: Logo}}
                            />
                            <ActivityIndicator size={"large"} color={Colors.orange}/>
                        </Block>
                    </Block>
                </Modal>
                <Image style={{
                    width: 180,
                    height: 180,
                    resizeMode: 'contain',
                    marginBottom: '10%'
                }} source={{uri: Logo}}/>

                <View style={{alignItems: 'center'}}>
                    <View style={{alignItems: 'center'}}>
                        <ShoutemView style={styles.SectionStyle}>
                            <AntDesign style={styles.iconInput} name="mail" size={24}/>
                            <ShoutemInput
                                style={styles.input}
                                placeholder="Adresse mail"
                                underlineColorAndroid="transparent"
                                value={this.state.email}
                                onChangeText={(e) => this.setState({email: e, showmail: 'none', showpass: 'none'})
                                }
                            />
                        </ShoutemView>

                    </View>
                    <View style={{alignItems: 'center'}}>
                        <ShoutemView style={styles.SectionStyle}>
                            <AntDesign style={styles.iconInput} name="lock" size={24}/>
                            <ShoutemInput
                                style={styles.input}
                                placeholder="Mot de passe"
                                underlineColorAndroid="transparent"
                                onChangeText={(e) => this.setState({
                                    password: e,
                                    showmail: 'none',
                                    showpass: 'none'
                                })}
                                secureTextEntry={true}
                                value={this.state.password}
                            />
                        </ShoutemView>
                    </View>
                    <View>
                        <View style={{
                            alignItems: 'flex-end', left: '25%',

                        }}>
                           <TouchableOpacity onPress={() => this.props.navigation.navigate("PasswordRecover")}>
                               <Text style={{
                                   color: Colors.textColor
                               }}>Mot de passe oublié?</Text>
                           </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <Text style={{
                    display: this.state.showmail,
                    alignSelf: 'center',
                    marginTop: 5,
                    fontSize: 14,
                    color: 'rgba(213, 0, 11, 0.87)'
                }}>
                    Erreur / Identifiant ou mot de passe incorrect.
                </Text>
                <View style={{
                    width: '90%',
                    marginTop: '20%'
                }}>
                    <BaseButton iconLeft rounded info full
                                style={{borderRadius: 30, backgroundColor: Colors.blue, elevation: 0}}
                                onPress={() => this.login()}>
                        <Ionicons name={'ios-log-in'} style={styles.icon} size={24}/>
                        <BaseText style={{fontFamily: fontFamily["Poppins-Regular"]}}
                                  uppercase={false}>Connexion</BaseText>
                    </BaseButton>
                </View>
            </ScrollView>
        );
    }
}
var styles = StyleSheet.create({
    wrapper: {},
    icon: {
        color: Colors.white
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    SectionStyle: {
        backgroundColor: Colors.backGroundGray,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 0,
        height: 60,
        borderRadius: 30,
        margin: 10,
    },
    input: {
        fontSize: 16,
        backgroundColor: Colors.backGroundGray,
        paddingLeft: 10,
        flex: 1,
        borderRadius: 30,
    },
    iconInput: {
        color: Colors.blue,
        paddingRight: 7,
        borderRightWidth: 1,
        borderColor: Colors.lightBlue,
        marginStart: 12,
    },
})
