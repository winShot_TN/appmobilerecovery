import * as React from 'react';
import {Alert, Dimensions, SafeAreaView, ScrollView, StyleSheet, View} from 'react-native'
import {Avatar, Switch} from 'react-native-paper'
import {ActionSheet, Subtitle, Title} from 'native-base'
import {ListItem} from 'react-native-elements'
import Colors from '../utils/Colors'
import {fontFamily} from "../utils/Const";
import Ripple from 'react-native-material-ripple'
import AsyncStorage from "@react-native-community/async-storage"
import {NavigationActions, StackActions} from "react-navigation";


const languages = [
    "Français",
    "Arabic",
];
export default class Settings extends React.Component {

    list = [
        {
            title: 'Mon Profil',
            leftIcon: 'account-circle',
            rightIcon: 'keyboard-arrow-right',
        },
        {
            title: 'Notifications',
            leftIcon: 'notifications',
        },
        {
            title: 'Newsletter',
            leftIcon: 'notifications'

        },
        {
            title: `Contacter l'équipe winshot`,
            leftIcon: 'send',
            rightIcon: 'keyboard-arrow-right',
        }, {
            title: `Langue`,
            leftIcon: 'language',
            rightIcon: 'keyboard-arrow-right',
        },
        {
            title: `Déconnexion`,
            leftIcon: 'input',
            rightIcon: 'keyboard-arrow-right',
        }
    ];

    state = {
        notification: true,
        newsletter: false,
        user: {},
    };

    constructor(props) {
        super(props);

    }

    logout = async () => {
        try {
            await AsyncStorage.setItem('@storage_Key', "false")
        } catch (e) {
            // saving error
        }
    };
    _onPress = (index) => {
        switch (index) {
            case 0 :
                this.props.navigation
                    .navigate('NavProfile')
                break;
            case 1 :// notifications...
                break;
            case 2 :// newsletter...
                break;
            case 3 :
                // contacter winshot
                break;
            case 4 :// change language
                break;
            case 5 :// log out
                Alert.alert(
                    'Déconnexion',
                    'Êtes vous sûr de vouloir vous déconnecter de Winshot ?',
                    [
                        {
                            text: 'Rester avec nous',
                            onPress: () => null,
                            style: 'non',
                        },
                        {
                            text: 'oui',
                            onPress: () => this.logout().then(() => {
                                const resetAction = StackActions.reset({
                                    index: 0,
                                    actions: [NavigationActions.navigate({routeName: 'welcome'})],
                                });
                                this.props.navigation.dispatch(resetAction);
                            }).catch(err => {
                            })
                        },
                    ],
                    {cancelable: false},
                );

                break
            default:
                break
        }
    };

    render() {
        const {
           user
        } = this.props.screenProps;

        let n = "X"
        let p = "D"
        if (!!user.nom)
            n = user.nom.charAt(0).toUpperCase()
        if (!!user.prenom)
            p = user.prenom.charAt(0).toUpperCase()
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Colors.backGroundGray}}>
                <View style={styles.header}>
                    <View style={styles.headerItems}>
                        {user.photo && user.photo.length > 0 ? <Avatar.Image size={64}
                                                                             source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + user.photo}}/>
                            : null}

                        {!user.photo || user.photo.length < 0 ?
                            <Avatar.Text color={Colors.white} size={64} label={`${n} ${p}`}/>
                            : null}


                    </View>
                    <View>
                        <Title style={styles.title}>{`${user.nom} ${user.prenom}`}</Title>
                        <Subtitle style={styles.subtitle}>{user.email}</Subtitle>
                    </View>
                </View>
                <ScrollView style={{paddingTop: 0}}
                            scrollEventThrottle={16}
                >

                    {
                        this.list.map((item, index) => {
                            if (item.title === 'Notifications') {
                                return (
                                    <ListItem
                                        key={index}
                                        titleStyle={{fontFamily: fontFamily["Poppins-Regular"]}}
                                        rightElement={() => {
                                            let {notification} = this.state
                                            return (<Switch
                                                value={notification}
                                                color={Colors.orange}
                                                onValueChange={(value) => {
                                                    this.setState({notification: value});
                                                }
                                                }
                                            />)
                                        }
                                        }
                                        title={item.title}
                                        leftIcon={{name: item.leftIcon}}
                                        rightIcon={{name: item.rightIcon}}
                                        bottomDivider={true}
                                    />
                                )
                            } else if (item.title === 'Newsletter') {
                                return (
                                    <ListItem
                                        titleStyle={{fontFamily: fontFamily["Poppins-Regular"]}}
                                        key={index}
                                        rightElement={() => {
                                            let {newsletter} = this.state
                                            return (<Switch
                                                value={newsletter}
                                                color={Colors.orange}
                                                onValueChange={(value) => {
                                                    this.setState({newsletter: value});
                                                }
                                                }
                                            />)
                                        }
                                        }
                                        title={item.title}
                                        leftIcon={{name: item.leftIcon}}
                                        rightIcon={{name: item.rightIcon}}
                                        bottomDivider={true}
                                    />
                                )
                            } else if (item.title === "Langue") {
                                return (
                                    <Ripple onPress={() => {
                                        this._onPress(index)
                                        ActionSheet.show(
                                            {
                                                options: languages,
                                                cancelButtonIndex: 0,
                                                title: "Choisir une langue",
                                            },
                                            buttonIndex => {
                                                this.setState({langue: languages[buttonIndex]});
                                            }
                                        )
                                    }}
                                            rippleDuration={600}>
                                        <ListItem
                                            titleStyle={{fontFamily: fontFamily["Poppins-Regular"]}}
                                            key={index}
                                            title={item.title}
                                            leftIcon={{name: item.leftIcon}}
                                            rightIcon={{name: item.rightIcon}}
                                            bottomDivider={true}
                                        />
                                    </Ripple>
                                )
                            } else {
                                return (
                                    <Ripple onPress={() => this._onPress(index)}
                                            rippleDuration={600}>
                                        <ListItem
                                            titleStyle={{fontFamily: fontFamily["Poppins-Regular"]}}
                                            key={index}
                                            title={item.title}
                                            leftIcon={{name: item.leftIcon}}
                                            rightIcon={{name: item.rightIcon}}
                                            bottomDivider={true}
                                        />
                                    </Ripple>
                                )
                            }
                        })
                    }
                </ScrollView>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        fontFamily: fontFamily["Poppins-Regular"],
        color: Colors.white,
        fontSize: 16,
        height: 21,

    },
    subtitle: {
        fontFamily: fontFamily["Poppins-Regular"],
        color: Colors.white,
        fontSize: 14,
        height: 19
    },
    header: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        margin: 0,
        height: '17%',
        backgroundColor: Colors.orange,
        elevation: 0,
        alignItems: 'center',
    },
    headerItems: {
        margin: 10,
    }
});
