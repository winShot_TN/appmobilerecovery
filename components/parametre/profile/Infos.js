import * as React from 'react';
import {SafeAreaView, ScrollView, StyleSheet, TextInput, View} from 'react-native';

import {ActionSheet, Text} from 'native-base'
import Colors from "../../utils/Colors";
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import {Urh} from "../../utils/Const";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Ripple from 'react-native-material-ripple'
import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

const professions = [
    "Sélectionner votre profession",
    "Etudiant",
    "Chômeur/Sans emploi",
    "Femme au foyer",
    "Agriculteur",
    "Commerçant/Artisan",
    "Profession libérale",
    "Employé",
    "Fonctionnaire",
    "Commercial",
    "Ouvrier",
    "Retraité"
]
const familles = [
    "Sélectionner votre familiale",
    "Célibataire",
    "Marié(e)",
    "Marié(e) avec enfants",
    "Divorcé(e)"
]
const passions = [
    "Sélectionner votre passion",
    "Auto-Moto",
    "Animaux",
    "Art, culture & musique",
    "Cuisine",
    "Décoration",
    "Economie & politique",
    "High-tech",
    "Jardinage & bricolage",
    "Jeux vidéos",
    "Mode & beauté",
    "Sport",
    "Voyage",
]
const diplomes = [
    "Sélectionner votre diplôme",
    "Ecole Primaire",
    "Collège",
    "Lycée",
    "1er cycle universitaire",
    "Licence",
    "Maitrise",
    "Mastère",
    "Doctorat",
]

export default class Infos extends React.Component {

    // handle change Diplome
    handleChangeDiplome = (index) => {
        const {
            updateMyState,
        } = this.props.screenProps;
        updateMyState({diplome: diplomes[index]})

    }
    // handle change Profession
    handleChangeProfession = (index) => {
        const {
            updateMyState,
        } = this.props.screenProps;
        updateMyState({profession: professions[index]})

    }
    // handle change Famille
    handleChangeFamille = (index) => {
        const {
            updateMyState,
        } = this.props.screenProps;
        updateMyState({situation_familiale: familles[index]})

    }
    // handle change Profession
    handleChangePassion = (index) => {
        const {
            updateMyState,
        } = this.props.screenProps;
        updateMyState({passion: passions[index]})

    }
    render() {
        const {
            newUser
        } = this.props.screenProps;

        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Colors.lightGray}}>
                <ScrollView
                    scrollEventThrottle={16}>
                    <View style={{
                        backgroundColor: 'white',
                        justifyContent: 'space-around',
                        paddingTop: 12,
                        marginTop: 10
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            width: '25%',
                            justifyContent: 'space-between',
                            alignItems: 'center', left: 10,
                        }}>
                            <FontAwesome5 name={'school'} size={22}/>
                            <Text style={{color: '#333', fontSize: 16, marginStart: 10}}>Diplôme</Text>
                        </View>
                        <Ripple rippleOpacity={0.1} rippleDuration={200} onPress={() => {
                            ActionSheet.show(
                                {
                                    options: diplomes,
                                    cancelButtonIndex: 0,
                                    title: "List des Diplômes",
                                },
                                buttonIndex => {
                                    this.handleChangeDiplome(buttonIndex)
                                }
                            )
                        }}>
                            <View style={styles.SectionStyle}>
                                <TextInput
                                    style={styles.input}
                                    editable={false}
                                    value={newUser.diplome}

                                />
                                {(diplomes[0] === newUser.diplome) ? (
                                    <Ionicons style={styles.iconInput} name="ios-arrow-down" size={24}/>) : (
                                    <Ionicons color={Colors.orange} style={styles.iconInput} name="md-checkmark"
                                              size={24}/>)}
                            </View>
                        </Ripple>
                    </View>

                    <View style={{
                        backgroundColor: 'white',
                        justifyContent: 'space-around',
                        paddingTop: 12,
                        marginTop: 10
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            width: '25%',
                            justifyContent: 'space-between',
                            alignItems: 'center', left: 10,
                        }}>
                            <Ionicons name={'md-school'} size={22}/>
                            <Text style={{color: '#333', fontSize: 16, marginStart: 10}}>Profession</Text>
                        </View>
                        <Ripple rippleOpacity={0.1} rippleDuration={200} onPress={() => {
                            ActionSheet.show(
                                {
                                    options: professions,
                                    cancelButtonIndex: 0,
                                    title: "List des professions",
                                },
                                buttonIndex => {
                                   this.handleChangeProfession(buttonIndex)
                                }
                            )
                        }}>
                            <View style={styles.SectionStyle}>
                                <TextInput
                                    style={styles.input}
                                    editable={false}
                                    value={newUser.profession}

                                />
                                {(professions[0] === newUser.profession) ? (
                                    <Ionicons style={styles.iconInput} name="ios-arrow-down" size={24}/>) : (
                                    <Ionicons color={Colors.orange} style={styles.iconInput} name="md-checkmark"
                                              size={24}/>)}
                            </View>
                        </Ripple>
                    </View>

                    <View style={{
                        backgroundColor: 'white',
                        justifyContent: 'space-around',
                        paddingTop: 12,
                        marginTop: 10
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            width: '25%',
                            justifyContent: 'space-between',
                            alignItems: 'center', left: 10,
                        }}>
                            <MaterialCommunityIcons name={'human-male-female'} size={22}/>
                            <Text style={{color: '#333', fontSize: 16, marginStart: 10}}>Famille</Text>
                        </View>
                        <Ripple rippleOpacity={0.1} rippleDuration={200} onPress={() => {
                            ActionSheet.show(
                                {
                                    options: familles,
                                    cancelButtonIndex: 0,
                                    title: "List",
                                },
                                buttonIndex => {
                                    this.handleChangeFamille(buttonIndex)
                                }
                            )
                        }}>
                            <View style={styles.SectionStyle}>
                                <TextInput
                                    style={styles.input}
                                    editable={false}
                                    value={newUser.situation_familiale}

                                />
                                {(familles[0] === newUser.situation_familiale) ? (
                                    <Ionicons style={styles.iconInput} name="ios-arrow-down" size={24}/>) : (
                                    <Ionicons color={Colors.orange} style={styles.iconInput} name="md-checkmark"
                                              size={24}/>)}
                            </View>
                        </Ripple>
                    </View>

                    <View style={{
                        backgroundColor: 'white',
                        justifyContent: 'space-around',
                        paddingTop: 12,
                        marginTop: 10
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            width: '25%',
                            justifyContent: 'space-between',
                            alignItems: 'center', left: 10,
                        }}>
                            <Ionicons name={'ios-heart'} size={24}/>
                            <Text style={{color: '#333', fontSize: 16, marginStart: 10}}>Passions</Text>
                        </View>
                        <Ripple rippleOpacity={0.1} rippleDuration={200} onPress={() => {
                            ActionSheet.show(
                                {
                                    options: passions,
                                    cancelButtonIndex: 0,
                                    title: "List",
                                },
                                buttonIndex => {
                                  this.handleChangePassion(buttonIndex)
                                }
                            )
                        }}>
                            <View style={styles.SectionStyle}>
                                <TextInput
                                    style={styles.input}
                                    editable={false}
                                    value={newUser.passion}
                                />
                                {(passions[0] === newUser.passion) ? (
                                    <Ionicons style={styles.iconInput} name="ios-arrow-down" size={24}/>) : (
                                    <Ionicons color={Colors.orange} style={styles.iconInput} name="md-checkmark"
                                              size={24}/>)}
                            </View>
                        </Ripple>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
            ;
    };
};
const styles = StyleSheet.create({

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        elevation: 1.1,
        height: 60,
        borderRadius: 0,
        margin: 0,
    },

    input: {
        marginStart: 10,
        flex: 1,
        borderRadius: 0,
        color: "#333"
    },
    iconInput: {
        marginEnd: 12,
    }
});
