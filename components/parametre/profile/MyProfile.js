import * as React from 'react';
import {Dimensions, ScrollView, StyleSheet,} from 'react-native';
import Colors from "../../utils/Colors";
import {TextInput, View,} from '@shoutem/ui';
import {Button as ButtonBase, Content, DatePicker, Radio, Text as TextBase, Text} from 'native-base';
import moment from 'moment'
import Ionicons from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from "@react-native-community/async-storage";
import {fontFamily, Urh} from "../../utils/Const"
import axios from "axios";
import Ripple from 'react-native-material-ripple'
import TextInputMask from "react-native-text-input-mask";
import {Block} from "galio-framework";

const {width, height} = Dimensions.get('window')

export default class MyProfile extends React.Component {
    state = {
        id: "",
        adresse_mail: "",
        email: "",
        date_naissance: new Date(),
        tel: "",
        situation: "",
        passion: "",
        nom: "",
        prenom: "",
        profession: "",
        diplome: "",
        photo: "",
        modalVisible: false,
        actualpassword: '',
        password: '',
        confirmPassword: '',
        wrongPassword: false,
        PasswordColor: '#ffffff',
        maleGenderColor: '#333',
        femaleGenderColor: '#333',
        malechecked: false,
        femalechecked: false,
        isExpended: false,
        messageerror: "",
        Passwordfocused: false,
    };

    constructor(props) {
        super(props);

        this.changermotdepasse = this.changermotdepasse.bind(this)


    }


    changermotdepasse() {
        //
        if (this.state.confirmPassword == "" || this.state.actualpassword == "" || this.state.password == "") {
            this.setState({messageerror: "completer tous les champs"})
        } else {
            if (this.state.confirmPassword != this.state.password) {
                this.setState({messageerror: "Le nouveau mot de passe et la confirmation doivent être identiques"})
            } else {


                let configpic = {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
                let data = {
                    id: this.state.id,
                    password: this.state.actualpassword,
                    newpassword: this.state.confirmPassword
                }
                axios.post(Urh + 'checkPassword', data, configpic)
                    .then((res) => {


                        if (res.data.success) {
                            this.setState({isExpended: !this.state.isExpended})
                        } else {
                            this.setState({messageerror: "Mot de passe actuelle incorrecte"})
                        }
                    })
                    .catch((err) => {

                    })


            }

        }


    }


    componentDidMount() {
        let {newUser} = this.props.screenProps;
        if (newUser.sexe !== null) {

            newUser.sexe === "M" ? this.setState({
                maleGenderColor: Colors.blue, femaleGenderColor: '#333'
                , malechecked: true, femalechecked: false,
            }) : this.setState({
                femaleGenderColor: Colors.blue,
                femalechecked: true,
                malechecked: false,
                maleGenderColor: '#333',
            })
        }
    }

    storeData = async (variable) => {
        try {
            await AsyncStorage.setItem('@storage_Key', JSON.stringify(variable))
        } catch (e) {
            // saving error
        }
    }

    changeGender = (index) => {
        let {updateMyState} = this.props.screenProps;
        if (index === 0) {
            // male
            this.setState({
                maleGenderColor: Colors.blue, femaleGenderColor: '#333'
                , malechecked: true, femalechecked: false,
            })
            updateMyState({sexe: "M"})
        } else {
            // female
            this.setState({
                femaleGenderColor: Colors.blue,
                femalechecked: true,
                malechecked: false,
                maleGenderColor: '#333',
            })
            updateMyState({sexe: "F"})
        }
    }

    ToggelPassword = () => {
        const {isExpended} = this.state
        /*here...*/
        this.setState({isExpended: !isExpended})
    }

    renderPassword = () => {
        let {Passwordfocused} = this.state
        return (
            <View>

                <Text style={{
                    alignSelf: 'center',
                    marginTop: 5,
                    fontSize: 14,
                    color: 'rgba(213, 0, 11, 0.87)'
                }}>
                    {this.state.messageerror}
                </Text>
                <View
                    style={styles.SectionStyle}>
                    <TextInput
                        secureTextEntry
                        style={styles.input}
                        placeholder="ancien mot de passe"
                        underlineColorAndroid="transparent"
                        value={this.state.actualpassword}
                        onChangeText={(actualpassword) => this.setState({actualpassword})}

                    />
                </View>
                <View style={styles.SectionStyle}>
                    <TextInput
                        secureTextEntry
                        style={styles.input}
                        placeholder="Nouveau mot de passe"
                        underlineColorAndroid="transparent"
                        value={this.state.password}
                        onChangeText={(password) => this.setState({password})}
                    />
                </View>
                <View style={styles.SectionStyle}>
                    <TextInput
                        secureTextEntry
                        style={styles.input}
                        placeholder="confirmer votre mot de passe"
                        underlineColorAndroid="transparent"
                        value={this.state.confirmPassword}
                        onChangeText={(confirmPassword) => this.setState({confirmPassword})}
                    />
                </View>


                <Content style={{width: 230, alignSelf: 'center'}}>
                    <ButtonBase iconLeft style={{
                        alignSelf: 'center',
                        marginTop: 25,
                        backgroundColor: '#61cbea',
                        elevation: 0
                    }} rounded block
                                onPress={this.changermotdepasse}>

                        <TextBase style={{fontFamily: fontFamily["Poppins-Regular"]}}
                                  uppercase={false}>Changer Mot de passe</TextBase>
                    </ButtonBase>
                </Content>

            </View>
        )
    };

    // handle change Nom
    handleChangeNom = (nom) => {
        const {
            updateMyState,
        } = this.props.screenProps;
        updateMyState({nom: nom})
    }

    // handle change Prenom
    handleChangePrenom = (prenom) => {
        const {
            updateMyState,
        } = this.props.screenProps;
        updateMyState({prenom: prenom})
    }

    // handle change Prenom
    handleChangePrenom = (prenom) => {
        const {
            updateMyState,
        } = this.props.screenProps;
        updateMyState({prenom: prenom})
    }

    // handle change email address
    handleChangeEmail = (email) => {
        let {updateMyState} = this.props.screenProps;
        updateMyState({email: email})
    };

    // handle change tel
    handleChangeTel = (tel) => {
        const {
            updateMyState,
        } = this.props.screenProps;
        updateMyState({tel: tel})
    }

    // handle change dateNaiss
    handleChangeDateNaiss = (date_naissance) => {
        const {
            updateMyState,
        } = this.props.screenProps;
        updateMyState({date_naissance: date_naissance})
    }

    render() {
        const {
            newUser
        } = this.props.screenProps;
        let {isExpended} = this.state

        return (
            <ScrollView style={{padding: 5, backgroundColor: Colors.white}}
                        ref={(scrollView) => {
                            this.scrollView = scrollView;
                        }}
                        onContentSizeChange={(contentWidth, contentHeight) => {
                            this.scrollView.scrollToEnd({animated: true, duration: 1000});
                        }}>
                <View style={styles.SectionStyle}>
                    <Ionicons style={styles.iconInput} name="envelope" size={24}/>
                    <TextInput
                        style={styles.input}
                        placeholder="Adresse email"
                        underlineColorAndroid="transparent"
                        value={newUser.email}
                        onChangeText={(email) => {
                            this.handleChangeEmail(email);
                        }}
                    />

                </View>
                {/*gender*/}
                <View style={{
                    flexDirection: 'row',
                    margin: 10,
                    height: 60,
                    justifyContent: 'space-around',
                    alignItems: 'center'
                }}>
                    <View style={{alignItems: 'center'}}>
                        <View style={{alignItems: 'center'}}>
                            <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                <MaterialCommunityIcons size={32} name={'gender-male'}
                                                        style={{color: this.state.maleGenderColor}}/>
                                <Text style={{color: this.state.maleGenderColor}}>Homme</Text>
                            </View>
                            <Ripple onPress={() => this.changeGender(0)}>
                                <Radio
                                    selectedColor={Colors.blue}
                                    selected={this.state.malechecked}
                                />
                            </Ripple>
                        </View>
                    </View>
                    <View>
                        <View style={{alignItems: 'center'}}>
                            <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                <MaterialCommunityIcons size={32} name={'gender-female'}
                                                        style={{
                                                            color: this.state.femaleGenderColor
                                                        }}/>
                                <Text style={{color: this.state.femaleGenderColor}}>Femmme</Text>
                            </View>

                            <Ripple onPress={() => this.changeGender(1)}>
                                <Radio
                                    selectedColor={Colors.blue}
                                    selected={this.state.femalechecked}
                                />
                            </Ripple>
                        </View>
                    </View>
                </View>
                <View style={styles.SectionStyle}>
                    <Ionicons style={styles.iconInput} name="user-alt" size={24}/>
                    <TextInput
                        style={styles.input}
                        placeholder="Nom"
                        value={newUser.nom}
                        onChangeText={(nom) => {
                            this.handleChangeNom(nom)
                        }}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <View style={styles.SectionStyle}>
                    <Ionicons style={styles.iconInput} name="user-alt" size={24}/>
                    <TextInput
                        style={styles.input}
                        placeholder="Prénom"
                        value={newUser.prenom}
                        onChangeText={(prenom) => {
                            this.handleChangePrenom(prenom)
                        }}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <View style={styles.SectionStyle}>
                    <Ionicons style={styles.iconInput} name="birthday-cake" size={24}/>
                    <DatePicker
                        style={styles.input}
                        defaultDate={new Date(1999, 4, 4)}
                        minimumDate={new Date(1920, 1, 1)}
                        maximumDate={new Date(2018, 12, 31)}
                        locale={"en"}
                        modalTransparent={false}
                        animationType={"fade"}
                        androidMode={"default"}
                        placeHolderText={moment(newUser.date_naissance).format("DD/MM/YYYY")}
                        onDateChange={(date_naissance) => {
                            this.handleChangeDateNaiss(date_naissance)
                        }}
                        disabled={false}
                    />
                </View>
                <View style={styles.SectionStyle}>
                    <Ionicons style={styles.iconInput} name="phone" size={24}/>
                    <TextInputMask
                        style={styles.input}
                        keyboardType={"numeric"}
                        onChangeText={(formatted, extracted) => {
                            this.handleChangeTel(extracted)
                        }}
                        type={"phone-pad"}
                        placeholder={"+216"}
                        value={newUser.tel}
                        mask={"+216 [00] [000] [000]"}
                        keyboardType={"numeric"}
                    />
                </View>

                <View style={{
                    flex: 1, flexDirection: 'column',
                    backgroundColor: Colors.white, margin: 20
                }}>
                    <Ripple onPress={this.ToggelPassword}
                            style={{borderBottomWidth: 1, borderBottomColor: Colors.lightGray, padding: 5}}>
                        <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                            <Icon name={'ios-lock'} style={styles.iconInput} size={24}/>
                            <Text style={{flex: 1, marginLeft: 10}}>Changer voter mot de passe</Text>
                            <Icon name={`${isExpended ? 'ios-arrow-up' : 'ios-arrow-down'}`} size={24}
                                  color={Colors.orange}/>
                        </View>
                    </Ripple>
                    {isExpended && this.renderPassword()}
                </View>
            </ScrollView>
        );
    };

};
const styles = StyleSheet.create({
    headerStyle: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        padding: 20,
        backgroundColor: "#0002f527"
    },
    headerTextStyle: {
        fontSize: 20
    },
    collapsViewStyle: {
        backgroundColor: "white"
    },
    title: {
        color: Colors.white,
        fontSize: 16,
        height: 21,

    },
    subtitle: {
        color: Colors.white,
        fontSize: 14,
        height: 19
    },
    header: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        margin: 0,
        height: '20%',
        backgroundColor: Colors.orange,
        elevation: 0,
        alignItems: 'center',
    },
    headerItems: {
        margin: 10,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,

    },
    paragraph: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    SectionStyle2: {
        marginVertical: 10,
        flexDirection: 'row',
        backgroundColor: Colors.white,
        alignItems: 'center',
        borderWidth: 0,
        height: 60,
        maxWidth: width * 0.466666666,
        flex: 1,
        borderRadius: 30,
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Colors.backGroundGray,
        borderWidth: 0,
        height: 60,
        borderRadius: 30,
        margin: 10,
    },
    SectionStyleError: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: 'red',
        height: 60,
        borderRadius: 30,
        margin: 10,
    },
    textError: {
        borderColor: 'red',
        borderWidth: 1,
    },
    input: {
        backgroundColor: Colors.backGroundGray,
        fontSize: 16,
        flex: 1,
        borderRadius: 30,
    },
    input2: {
        backgroundColor: Colors.white,
        fontSize: 16,
        flex: 1,
        borderRadius: 30,
    },
    iconInput: {
        color: Colors.blue,
        paddingRight: 7,
        borderRightWidth: 1,
        borderColor: Colors.lightBlue,
        marginStart: 12,
    },
    wrongPassword: {
        borderColor: 'red',
    }
});
