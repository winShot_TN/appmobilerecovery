import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    InteractionManager,
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    View
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import {createMaterialTopTabNavigator, Header} from "react-navigation"
import Colors from '../../utils/Colors'

import {fontFamily, Logo, Urh} from "../../utils/Const";
import PhotoUpload from 'react-native-photo-upload'
import {Button, Text,} from 'native-base'
import Infos from './Infos'
import MyProfile from './MyProfile'
import AsyncStorage from "@react-native-community/async-storage";
import Ionicons from 'react-native-vector-icons/Ionicons'
import axios from "axios";
import Modal from "react-native-modal";
import {Block, Text as GalioText} from "galio-framework";
import AntDesign from "react-native-vector-icons/AntDesign";

const emailExpression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
const telExpression = /^[0-9]{8}$/;
const TabScreen = createMaterialTopTabNavigator(
    {
        monProfile: {
            screen: MyProfile,
            navigationOptions: {
                title: 'Mon profile',

            },
        },
        Infos: {
            screen: Infos,
            navigationOptions: {
                title: 'Mes infos',
                navigationOptions: {title: 'Mes infos'},

            }
        },
    },
    {
        swipeEnabled: true,
        animationEnabled: true,
        activeColor: Colors.activeTintColor,
        inactiveColor: Colors.inactiveTintColor,
        tabBarOptions: {
            showIcon: true,
            upperCaseLabel: false,
            activeTintColor: Colors.activeTintColor,
            inactiveTintColor: Colors.inactiveTintColor,
            style: {
                elevation: 0,
                backgroundColor: Colors.white,
            },
            labelStyle: {
                fontSize: 16,
                paddingTop: 7,
                margin: 0,
                marginLeft: 5,
            },
            indicatorStyle: {
                borderBottomColor: Colors.orange,
                borderBottomWidth: 1,
            },
            tabStyle: {
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
            }
        },
    }
);
const headerHeight = Header.HEIGHT
export default class NavProfile extends Component {
    // to add router to navigation props
    static router = TabScreen.router

    state = {
        id_user: '',
        ProfileDidUpdate: false,
        email: '',
        nom: '',
        date_naissance: new Date(),
        photo: '',
        prenom: '',
        sexe: '',
        tel: '',
        diplome: 'Sélectionner votre diplôme',
        profession: 'Sélectionner votre profession',
        passion: 'Sélectionner votre passion',
        situation_familiale: 'Sélectionner votre familiale',
        ready: false,
        islLoading: false,
    };

    constructor() {
        super();
        this.handleModal = this.handleModal.bind(this)
        this.changeAvatar = this.changeAvatar.bind(this)
        this.updateProfile = this.updateProfile.bind(this)
    }

    valid

    // CHANGE IMAGE PROFIL
    changeAvatar(avatar) {
        this.setState({isLoading: true})
        let {
            id_user
        } = this.state
        var postData = new FormData();
        postData.append("id_user", id_user)

        postData.append('my_photo', avatar)


        let configpic = {
            headers: {
                'contentType': "application/json",

                'Content-Type': 'multipart/form-data'
            }
        };

        axios.post(Urh + 'profilepic', postData, configpic)
            .then((res) => {
                this.storeData(res.data).then(() => {
                    this.setState({isLoading: false})
                })
            }).catch((err) => {
            this.setState({isLoading: false})
        })
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            let {
                user
            } = this.props.screenProps;
            // for Mes infos
            if (user.passion.length >= 1) this.setState({passion: user.passion})
            if (user.diplome.length >= 1) this.setState({diplome: user.diplome,})
            if (user.profession.length >= 1) this.setState({profession: user.profession,})
            if (user.situation_familiale.length >= 1) this.setState({situation_familiale: user.situation_familiale,})
            //
            this.setState({
                // id + photo
                id_user: user._id,
                photo: user.photo,

                // for Mon Profile
                email: user.email,
                sexe: user.sexe,
                nom: user.nom,
                prenom: user.prenom,
                date_naissance: user.date_naissance,
                tel: user.tel,
            }, () => {
                this.setState({ready: true})
            })

        })
    }

    // updating profile
    updateMyState = async (data) => {
        this.setState(data, () => {

        })
    }

    handleModal() {
        this.setState({ProfileDidUpdate: false})
    }

    storeData = async (variable) => {
        try {
            await AsyncStorage.setItem('@storage_Key', JSON.stringify(variable))
            let {updateUser} = this.props.screenProps;
            // update
            updateUser()
        } catch (e) {
            // saving error
        }
    }


    updateProfile() {
        this.setState({isLoading: true})
        let {
            user
        } = this.props.screenProps;

        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let {
            email, diplome, nom, prenom, tel, date_naissance, sexe, situation_familiale,
            profession,
            passion,
            id_user
        } = this.state

        let data = {
            id_user: id_user,
            adresse_mail: email,
            email: email,
            date_naissance: date_naissance,
            tel: tel,
            nom: nom,
            prenom: prenom,
            sexe: sexe,
            famille: situation_familiale,
            diplome: diplome,
            proffesion: profession,
            passion: passion,
        }
        axios.post(Urh + 'savechangesinfo', data, configpic)
            .then((res) => {
                this.storeData(res.data.alluser).then(() => {

                    this.setState({
                        isLoading: false,
                        ProfileDidUpdate: true
                    }, () => {

                    })
                }).catch(err => {
                    this.setState({
                        isLoading: false,
                    })
                })
            })
            .catch((err) => {
                this.setState({
                    isLoading: false,
                })
            })
    };

    validate = () => {
        const {
            user
        } = this.props.screenProps;

        let {
            email, diplome, nom, prenom, tel, date_naissance, sexe, situation_familiale,
            profession,
            passion,
            id_user,
            ready
        } = this.state
        if (!ready)
            return false

        let valid = false
        if ((email.trim() !== user.email.trim() ||
            (diplome.trim() !== user.diplome && diplome.trim() !== "Sélectionner votre diplôme") ||
            (profession.trim() !== user.profession && profession.trim() !== "Sélectionner votre profession") ||
            (situation_familiale.trim() !== user.situation_familiale && situation_familiale.trim() !== "Sélectionner votre familiale") ||
            nom.trim() !== user.nom.trim() ||
            prenom.trim() !== user.prenom.trim() ||
            date_naissance !== user.date_naissance ||
            sexe.trim() !== user.sexe.trim() ||
            tel.toString().trim() !== user.tel.toString().trim() ||
            situation_familiale.trim() !== user.situation_familiale.trim() ||
            profession.trim() !== user.profession.trim() ||
            (passion.trim() !== user.passion.trim() && passion.trim() !== "Sélectionner votre passion")) &&
            emailExpression.test(String(email).toLowerCase()) &&
            telExpression.test(String(tel)) &&
            nom.trim().length >= 1 &&
            prenom.trim().length >= 1

        )
            valid = true

        return valid


    }

    render() {
        let {ready} = this.state
        if (!ready)
            return (null)
        let {ProfileDidUpdate, isLoading} = this.state
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Colors.backGroundGray}}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} rippleColor={Colors.orange}>
                        <Ionicons name={'ios-arrow-back'} size={32} color={Colors.white}/>
                    </TouchableOpacity>
                    <View style={{alignItems: 'center'}}>
                        <Text style={{color: Colors.white, fontSize: 16, fontFamily: fontFamily["Poppins-Regular"]}}>
                            Mon profil
                        </Text>
                    </View>
                    <TouchableOpacity onPress={() => this.validate() ? this.updateProfile() : null}
                                      rippleColor={Colors.orange}>
                        <Ionicons name={'md-checkmark-circle-outline'} size={32}
                                  color={this.validate() ? Colors.white : Colors.gray}/>
                    </TouchableOpacity>
                </View>
                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={isLoading}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        padding: 15,
                        borderRadius: 10,
                    }}>
                        <Block center style={{justifyContent: 'center'}}>
                            <Image
                                style={{
                                    height: 100,
                                    width: 100,
                                }}
                                source={{uri: Logo}}
                            />
                            <ActivityIndicator size={"large"} color={Colors.orange}/>
                        </Block>
                    </Block>
                </Modal>

                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={ProfileDidUpdate}>
                    <Block style={{
                        height: 200,
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            marginTop: 30,
                            paddingBottom: 10,
                            paddingHorizontal: 15,
                        }} flex>
                            <Block center>
                                <GalioText NumberOfLines={2} h4 color={Colors.textColor}>
                                    {this.state.nom + " " + this.state.prenom}
                                </GalioText>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Vos informations a été mise a jour avec succès
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,
                        }}>
                            <Button rounded
                                    onPress={this.handleModal}
                                    style={{
                                        marginHorizontal: 7,
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.success
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>


                <View style={{height: 120, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{justifyContent: 'center'}}>
                        <PhotoUpload photoPickerTitle="Modifier votre photo de profil"
                                     onPhotoSelect={this.changeAvatar}>
                            {this.state.photo && this.state.photo.length > 0 ? <Image
                                    style={{width: 100, height: 100, borderRadius: 50}}
                                    source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + this.state.photo}}
                                /> :
                                <Image
                                    style={{width: 100, height: 100, borderRadius: 50}}
                                    source={{uri: "https://immedilet-invest.com/wp-content/uploads/2016/01/user-placeholder.jpg"}}
                                />}
                        </PhotoUpload>
                        <View style={{
                            position: 'absolute',
                            flexGrow: 1,
                            top: 80,
                            left: 70,
                            backgroundColor: Colors.blue,
                            height: 30,
                            width: 30,
                            borderRadius: 15,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <Icon color={Colors.white} name='camera' siz={100}/>
                        </View>
                    </View>
                </View>
                <TabScreen screenProps={{
                    newUser: this.state,
                    updateMyState: this.updateMyState
                }}
                           navigation={this.props.navigation}/>

            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    title: {
        color: Colors.white,
        fontSize: 16,
        height: 21,

    },
    subtitle: {
        color: Colors.white,
        fontSize: 14,
        height: 19
    },
    header: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        padding: 15,
        height: headerHeight,
        backgroundColor: Colors.orange,
        elevation: 0,
        alignItems: 'center',
    },
    headerItems: {
        margin: 10,
    },
    container: {
        flex: 1,
        justifyContent: 'center',

    },
    paragraph: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    card: {
        margin: 4
    }
});
