import {createStackNavigator} from "react-navigation";
import Settings from "./Settings";
import NavProfile from "./profile/NavProfile";
import React, {Component} from 'react'
import AsyncStorage from "@react-native-community/async-storage";
import {Animated, Easing} from "react-native";
const transitionConfig = () => {
    return {
        transitionSpec: {
            duration: 600,
            easing: Easing.out(Easing.poly(4)),
            timing: Animated.timing,
            useNativeDriver: true,
        },
        screenInterpolator: sceneProps => {
            const {layout, position, scene} = sceneProps;
            const thisSceneIndex = scene.index;
            const width = layout.initWidth;
            const translateX = position.interpolate({
                inputRange: [thisSceneIndex - 1, thisSceneIndex],
                outputRange: [width, 0],
            });
            return {transform: [{translateX}]};
        },
    };
};

const TabScreen = createStackNavigator({
    Settings: {
        screen: Settings,
        navigationOptions: {
            header: null,
        }
    },
    NavProfile: {
        screen: NavProfile,
        navigationOptions: {
            header: null,
        }
    }
}, {
    transitionConfig,
    initialRouteName: "Settings"
});

export class NavSettings extends Component {
    static router = TabScreen.router;

    render() {
        let {
            user,
            currentPosition,
            updateUser
        } = this.props.screenProps;

        return (
            <TabScreen
                screenProps={{
                    user: user,
                    updateUser : updateUser,
                    currentPosition: currentPosition,
                }}
                navigation={this.props.navigation}/>
        )
    }
}
