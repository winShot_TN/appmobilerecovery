import React from "react";

import {Block, Text, Input, Text as GalioText} from 'galio-framework'
import Colors from "./utils/Colors";
import {Image, TouchableOpacity} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import {Button} from "native-base";
import Modal from "react-native-modal";
import {SvgUri} from "react-native-svg";
import {Divider} from "react-native-paper";
import {fontFamily, Urh} from "./utils/Const";
import axios from "axios";
import {TextInput as ShoutemInput} from "@shoutem/ui";

export default class PasswordRecover extends React.Component {
    state = {
        ModalIsVisible: false,
        nav:false,
        message:""
    };

    componentDidMount(){

    };



    handleModal= () => {
        let {ModalIsVisible,nav} = this.state;

        this.setState({ModalIsVisible:!ModalIsVisible})

        if(nav)
            this.props.navigation.navigate("login")


    }

    handlValider = () => {


        let {ModalIsVisible} = this.state;





        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data = {
            email: this.state.email,

        }
        axios.post(Urh + 'recoverpassword', data, configpic)
            .then((res) => {


                if(res.data.user==1)
                this.setState({
                    ModalIsVisible: !ModalIsVisible,message:"Un E-mail pour réinitialisé votre mot de passe viens d'être envoyé, n'oubliez pas\n" +
                        "de vérifier aussi vos spams !",nav:true
                })

                else
                    this.setState({
                        ModalIsVisible: !ModalIsVisible,message:"Cet email n'est pas inscrit sur WinShot veuillez verifier votre adresse email et essayer de nouveau"
                    })


            })
            .catch((err) => {


            })




    };

    render() {
        let {ModalIsVisible,message,nav} = this.state
        return (
            <Block>
                <Block row space={"around"} middle style={{
                    height: 50,
                    backgroundColor: Colors.orange,
                    paddingHorizontal: 10,
                }}>

                    <Block center>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <AntDesign name='arrowleft' size={22}
                                       color={Colors.white}/>
                        </TouchableOpacity>
                    </Block>
                    <Block style={{
                        marginLeft: 20
                    }} flex>
                        <GalioText color={Colors.white} style={{fontSize: 20}}>Mot de passe oublié</GalioText>
                    </Block>
                </Block>
                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={ModalIsVisible}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        borderTopRightRadius : 20,
                        borderTopLeftRadius : 20,
                    }}>
                        <Block style={{
                            padding: 15,
                            borderRadius: 10,
                        }}>
                            <Block row style={{marginHorizontal: 10, marginBottom: 7}}>
                                <GalioText size={16} color={Colors.textColor}>
                                    Réinitialisation mot de passe
                                </GalioText>
                            </Block>
                            <Block style={{marginLeft: 5}}>
                                <GalioText>{message}</GalioText>
                            </Block>
                        </Block>
                        <Block row style={{
                            marginTop: 5,
                            backgroundColor : Colors.backgroundColor2,
                            justifyContent: 'center'
                        }}>
                            <Block style={{
                                marginVertical : 5,
                            }}>
                                <Button
                                    rounded
                                    onPress={() => this.handleModal()}
                                    style={{
                                        backgroundColor: Colors.orange,
                                        elevation: 0,
                                        paddingHorizontal: 15,
                                    }}>
                                    <Block>
                                        <GalioText style={{marginVertical: 5,marginHorizontal : 10}} color={Colors.white}
                                                   size={18}>OK</GalioText>
                                    </Block>
                                </Button>
                            </Block>

                        </Block>
                    </Block>
                </Modal>
                <Block style={{
                    marginHorizontal: 10,
                    marginVertical: 40,
                }}>
                    <Text color={Colors.textColor} size={16}>
                        {`Mot de passe oublié`}
                    </Text>
                    <Input rounded
                           ref={input => this.input = input}
                           style={{
                               marginVertical: 10,
                               backgroundColor: Colors.lightGray
                           }}
                           value={this.state.email}
                           onChangeText={(e) => this.setState({email: e})}


                           placeholder={"E-mail"}
                    />
                    <Block center>
                        <Button
                            onPress={() => this.handlValider()}
                            style={{
                                minWidth: 200,
                                backgroundColor: Colors.blue,
                                elevation: 0,
                                paddingHorizontal: 10
                            }}
                            block
                            rounded
                        >
                            <Block>
                                <Text color={Colors.white} size={18}>Valider</Text>
                            </Block>
                        </Button>
                    </Block>

                </Block>

            </Block>
        )
    }

}
