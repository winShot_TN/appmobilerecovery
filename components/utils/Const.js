import {StyleSheet,Dimensions} from 'react-native';
import Colors from './Colors';
export const Urh = 'http://192.168.2.80:3000/';


//export const Urh = 'http://192.168.42.211:3000/';
// export const Urh = 'https://winshotserver.winshot.net/';
//
// export const Urh = 'https://winshotserverorange.winshot.net/';
export const Logo = 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/WINSHOT_PNG.png';
export const UrhImages = 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecampagne/';

export const WelcomeImage = [
    Logo,
    'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icone_1.png',
    'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icone_2.png',
    'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icone_3.png',
    'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/icone_4.png',];
export const fontFamily = {
    'Poppins-Regular': 'Poppins-Regular',
    'Poppins-Thin': 'Poppins-Thin',
    'Poppins-Medium': 'Poppins-Medium',
    'Poppins-Light': 'Poppins-Light',
    'Poppins-Bold': 'Poppins-Bold'
};
const styles = StyleSheet.create({
    Buttons: {
        borderRadius: 30,
        elevation: 0,
        backgroundColor: Colors.blue
    }
});
// screen width and height as object
export  const SIZES = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
};
