import {Block, Text as GalioText} from "galio-framework";
import Colors from "./Colors";
import {Image} from "react-native";
import Disconnect from "../../assets/disconnect.png";
import React from "react";
const RenderEmptyMissions = () => (

    <Block
        style={{
            backgroundColor: Colors.white,
            padding: 10,
            flex: 3,
            marginBottom: 100,
        }}>
        <Block center style={{flex: 2, justifyContent: 'flex-end', marginBottom: 30}}>

        </Block>
        <Block center style={{
            justifyContent: 'flex-start',
            flex: 1,
        }}>
            <GalioText center style={{marginHorizontal: 0}} color={Colors.textColor} h5>
                Pas de nouvelles missions à proximité en ce moment.
            </GalioText>
            <GalioText center style={{marginHorizontal: 10,marginTop : 20}}>
                {`Connectez-vous ultérieurement, réalisez plus de missions, augmentez vos gains, vos points d'expérience et débloquez plus de cadeaux.`}
            </GalioText>

        </Block>
    </Block>

);
const RenderNoConnection = () => (
    <Block
        style={{
            backgroundColor: Colors.white,
            padding: 10,
            flex: 4,
            marginBottom: 100,
        }}>
        <Block center style={{flex: 3, justifyContent: 'flex-end', marginBottom: 30}}>
            <Image
                style={{
                    height: 130,
                    width: 130,
                }}
                resizeMode={"contain"}
                source={Disconnect}
            />
        </Block>
        <Block center style={{
            justifyContent: 'flex-start',
            flex: 1,
        }}>
            <GalioText color={Colors.textColor} h5>
                Dommage !
            </GalioText>
            <GalioText center style={{marginLeft: 20}}>
                {`Votre connexion est interrompu. \nVeuillez activez la connexion internet sur votre smartphone.`}
            </GalioText>

        </Block>
    </Block>
);

export {
    RenderNoConnection,
    RenderEmptyMissions,
}
