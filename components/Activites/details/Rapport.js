import * as React from 'react';
import {ScrollView, TouchableOpacity,processColor} from 'react-native'
import {Block, Text as GalioText} from 'galio-framework'
import Colors from "../../utils/Colors";
import  {SIZES} from '../../utils/Const'
import Ionicons from "react-native-vector-icons/Ionicons";
import moment from "moment";
import 'moment/locale/fr'
moment.locale('fr')
export default class Rapport extends React.Component {
    state = {
        legend: {
            enabled: true,
            textSize: 14,
            form: 'SQUARE',
            formSize: 14,
            xEntrySpace: 10,
            yEntrySpace: 5,
            formToTextSpace: 5,
            wordWrapEnabled: true,
            maxSizePercent: 0.5
        },
        data: {
            dataSets: [{
                values: [{y: 100}, {y: 105}, {y: 102}, {y: 110}, {y: 114}, {y: 109}, {y: 105}, {y: 99}, {y: 95}],
                label: 'Question 2',
                config: {
                    color: processColor('teal'),
                    barShadowColor: processColor('lightgrey'),
                    highlightAlpha: 90,
                    highlightColor: processColor('red'),
                }
            }],

            config: {
                barWidth: 0.7,
            }
        },
        highlights: [{x: 3}, {x: 6}],
        xAxis: {
            valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
            granularityEnabled: true,
            granularity : 1,
        }
    }

    handleSelect(event){

    }
    render() {
     let activite = this.props.mission


        return (
            <Block style={{flex : 1}}>
                <Block style={{backgroundColor : Colors.white,marginVertical : 5,padding : 5}}>
                    <GalioText size={15} color={Colors.textColor}> <GalioText color={Colors.orange}>Check-list:</GalioText> {activite.nomcamp} </GalioText>
                    <Block row style={{marginLeft: 10}}>
                        <Block middle>
                            <Ionicons name='md-time' size={24} color={Colors.inactiveTintColor}/>
                        </Block>
                       <Block middle style={{marginLeft : 3}}>
                           <GalioText numberOfLines={1} muted>
                               Rapport {moment(activite.envoie_date).format('llll')}
                           </GalioText>
                       </Block>
                    </Block>
                </Block>

                {activite.questions_user.map((quest, index) => {

                    return (
                        <Block style={{backgroundColor : Colors.white,marginVertical : 5,width : SIZES.width,minHeight : 75,padding : 5}}>
                            <GalioText numberOfLines={5} h5
                                       color={Colors.textColor}>{quest.question} ?</GalioText>


                            {activite.reponse[index].map((rep, indrep) => {

                                return (

                            <GalioText numberOfLines={5} h6
                                       color={Colors.textColor}>&nbsp;&nbsp;&nbsp; {rep}</GalioText>
                                )})}


                        </Block>

                    )})

                }





            </Block>
        );
    };
};
