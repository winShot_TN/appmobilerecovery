import * as React from 'react';
import {Block, Text as GalioText} from 'galio-framework'
import Colors from "../../utils/Colors";
import {FlatList, Image} from "react-native";
import {Badge} from 'react-native-paper'

export default class Galerie extends React.PureComponent {
    constructor(props) {
        super(props)
        this.renderImage = this.renderImage.bind(this)
    }

    renderImage(item, index,activite) {
        return (
            <Block middle flex style={{
                backgroundColor: Colors.white,
                marginVertical: 2,
                marginHorizontal: 2,
            }}>
                <Image
                    style={{flex: 1, resizeMode: 'cover'}}
                    width={'100%'}
                    height={150}
                    source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagesmission/"+item}}
                />
                <Block center style={{marginVertical : 5}}>
                    <Block center style={{backgroundColor : Colors.blue,paddingHorizontal : 10,paddingVertical : 2,borderRadius : 20}}>
                        <GalioText size={14} color={Colors.white}>Section {activite.etapes[activite.photo_user[index].etape]} </GalioText>
                    </Block>
                </Block>
            </Block>
        )
    }

    render() {
        let images = []

        let activite = this.props.mission
        return (
            <Block flex style={{backgroundColor: Colors.backgroundColor2}}>
                <FlatList
                    style={{flex: 1}}
                    numColumns={2}
                    showsVerticalScrollIndicator={false}
                    removeClippedSubviews={false}
                    data={activite.Photos}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => this.renderImage(item, index,activite)}
                />
            </Block>
        );
    };
};
