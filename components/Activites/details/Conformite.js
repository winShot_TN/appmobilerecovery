import * as React from 'react';
import {ScrollView, TouchableOpacity,processColor} from 'react-native'
import {Block, Text as GalioText} from 'galio-framework'
import Colors from "../../utils/Colors";
import  {SIZES} from '../../utils/Const'
import Ionicons from "react-native-vector-icons/Ionicons";

import { Switch } from 'react-native-paper';
export default class Conformite extends React.Component {
    state = {
        data: {
                dataSets: [{
                    values: [{value: 100}, {value: 110}, {value: 105}, {value: 115}, {value: 110}],
                    label: 'DS 1',
                    config: {
                        color: processColor(Colors.success),

                        drawFilled: true,
                        fillColor: processColor(Colors.darkGray),
                        fillAlpha: 100,
                        lineWidth: 2
                    }
                }, {
                    values: [{value: 115}, {value: 100}, {value: 105}, {value: 110}, {value: 120}],
                    label: 'DS 2',
                    config: {
                        color: processColor(Colors.success),

                        drawFilled: true,
                        fillColor: processColor(Colors.success),
                        fillAlpha: 150,
                        lineWidth: 1.5
                    }
                }, {
                    values: [{value: 105}, {value: 115}, {value: 121}, {value: 110}, {value: 105}],
                    label: 'DS 3',
                    config: {
                        color: processColor(Colors.blue),

                        drawFilled: true,
                        fillColor: processColor(Colors.blue)
                    }
                }],
        },
        xAxis: {
            valueFormatter: ['Extérieur', 'Intérieur', 'Merchandising', 'Trade', 'Staff']
        },
        isSwitchOn : false,
    }

    componentDidMount() {

    }

    handleSelect(event){

    }

    render() {
        const {isSwitchOn} = this.state
        return (
            <Block flex>
                <Block style={{backgroundColor : Colors.white,marginVertical : 5,padding : 5}}>
                    <GalioText size={15} color={Colors.textColor}>Résultat par section</GalioText>
                    <Block center row style={{marginLeft: 10}}>
                        <Block>
                            <GalioText muted>Liste</GalioText>
                        </Block>
                        <Block middle style={{marginLeft : 3}}>
                            <Switch
                                color={Colors.orange}
                                value={isSwitchOn}
                                onValueChange={() =>
                                { this.setState({ isSwitchOn: !isSwitchOn }); }
                                }
                            />
                        </Block>
                        <Block>
                            <GalioText color={Colors.textColor}> Graphique</GalioText>
                        </Block>
                    </Block>
                </Block>
                <Block style={{backgroundColor : Colors.white,marginVertical : 5,width : SIZES.width,minHeight : 400,padding : 5}}>

                </Block>
            </Block>
        );
    };
};
