import * as React from 'react';
import {Block, Text as GalioText} from 'galio-framework'
import Colors from "../utils/Colors";
import {ActivityIndicator, Image, TouchableOpacity, StyleSheet, View,ScrollView} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import ImageOverlay from "react-native-image-overlay";
import Feather from "react-native-vector-icons/Feather";
import {Button} from "native-base";
import Rapport from "./details/Rapport";
import Galerie from "./details/Galerie";
import Conformite from "./details/Conformite";
var geolib = require('geolib');
export default class DetailsActivitie extends React.Component {
    state = {
        activite: null,
        isLoading: false,
        tags: [{
            title: 'Rapport',
            isSelected: true,
        }, {
            title: 'Galerie',
            isSelected: false,
        }, {
            title: 'Conformité',
            isSelected: false,
        }],
        selectedIndex: 0,
    };

    constructor(props) {
        super(props);
        this.renderHeader = this.renderHeader.bind(this);

    }

    componentDidMount() {
        let activite = JSON.parse(this.props.navigation.getParam('activite', null))
        this.setState({
            activite,
            isLoading: false,
        })
    }

    renderHeader() {
        let {user: {photo}} = this.props.screenProps;
        const {navigation} = this.props;
        let {activite} = this.state




        return (
            <Block row shadow right middle style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 10,
            }}>
                <Block center style={{marginRight: 5}}>
                    <Block center>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <AntDesign name='arrowleft' size={24} color={Colors.white}/>
                        </TouchableOpacity>
                    </Block>
                </Block>
                <Block flex center style={{marginLeft: 20}}>
                    <GalioText numberOfLines={1} color={Colors.white} style={{fontSize: 18}}> Contrôler PLV en point de venter </GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    {photo && photo.length > 0 ?
                        <TouchableOpacity onPress={this.goToPlus}>
                            <Image
                                style={{
                                    height: 35,
                                    width: 35,
                                    borderRadius: 18,
                                    borderColor: Colors.white,
                                    borderWidth: 1,
                                }}
                                source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + photo}}/>
                        </TouchableOpacity>
                        : null}
                </Block>
            </Block>
        )
    }

    renderFacade() {

        let {user,currentPosition} = this.props.screenProps;

        const {navigation} = this.props;

        let activite = JSON.parse(navigation.getParam("activite",{}))



        let img= "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/Fa%C3%A7ade+boutique+Orange+2.jpg"
        if(activite.photo && activite.photo.length>1)
            img="https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/"+activite.photo


        // let dist = geolib.getDistance(
        //     {
        //         latitude: activite.coordinates.lat,
        //         longitude: activite.coordinates.long
        //     },
        //     {
        //         latitude: currentPosition.coords.latitude,
        //         longitude: currentPosition.coords.longitude
        //     }
        // )


        return (
            <View>
                <ImageOverlay
                    overlayAlpha={0.15}
                    source={{uri: img}}
                    height={170}
                    style={{
                        resizeMode: "cover"
                    }}
                    contentPosition="center">
                    {/*<Block center style={{marginTop: 5}}>*/}
                    {/*    <GalioText h4 color={Colors.white}>TEST </GalioText>*/}
                    {/*    <Block row>*/}
                    {/*        <Block middle>*/}
                    {/*            <Feather name='map-pin' size={25} color={Colors.blue}/>*/}
                    {/*        </Block>*/}
                    {/*        <Block bottom style={{marginLeft: 7, marginTop: 5}}>*/}
                    {/*            <GalioText*/}
                    {/*                h4*/}
                    {/*                color={Colors.blue}>{dist+' Km'}</GalioText>*/}
                    {/*        </Block>*/}
                    {/*    </Block>*/}
                    {/*</Block>*/}
                </ImageOverlay>
            </View>
        )
    };

    handleChangeTages(index) {
        let {tags,} = this.state;
        if (tags[index].isSelected)
            return

        tags[index].isSelected = !tags[index].isSelected
        tags.map((tag, idx) => {
            if (idx !== index)
                tag.isSelected = !tags[index].isSelected
        })
        this.setState({
            tags: tags,
            selectedIndex: index
        })
    };

    _renderTag(tag, index) {
        let textColor = tag.isSelected ? Colors.orange : "#707070"
        let borderBottomWidth = tag.isSelected ? 2 : 0
        return (
            <Block row flex style={{
                borderBottomWidth: borderBottomWidth,
                borderBottomColor: Colors.blue,
            }}>
                <Block center flex >
                   {/* <Block center>
                        {index === 1 ? <Entypo size={24}
                                               name={"shop"}
                                               color={textColor}
                        /> : <Feather name='clipboard' size={25} color={textColor}/>}
                    </Block>*/}
                    <Block center style={{marginLeft: 2}}>
                        <Button
                            onPress={() => this.handleChangeTages(index)}
                            block
                            style={{
                                borderRadius: 0,
                                elevation: 0,
                                backgroundColor: Colors.white,
                            }}
                        >
                            <GalioText size={13} color={textColor}>{tag.title}</GalioText>
                        </Button>
                    </Block>
                </Block>
            </Block>
        )
    }

    render() {
        let {isLoading, tags,selectedIndex} = this.state;
        let activite = JSON.parse(this.props.navigation.getParam("activite",{}))
        return (
            <Block flex style={{backgroundColor: Colors.backgroundColor2}}>
                {this.renderHeader()}
                {!isLoading ? <ScrollView style={{flex : 1}}>
                    {this.renderFacade()}
                    <Block row space={"around"} style={{
                        backgroundColor: Colors.white,
                        paddingHorizontal: 8,
                    }}>
                        {tags.map((tag, index) => this._renderTag(tag, index))}
                    </Block>
                    {selectedIndex === 0 && <Rapport mission={activite} />}

                    {selectedIndex === 1 && <Galerie mission={activite} />}
                    {selectedIndex === 2 && <Conformite />}
                </ScrollView> : <Block flex middle>
                    <ActivityIndicator size={"large"}/>
                </Block>}

            </Block>
        );
    };
};
const styles = StyleSheet.create({
    card: {
        backgroundColor: Colors.white,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 50,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 4,
    }
})