import React from 'react'
import {Animated, Easing} from "react-native";
import {createStackNavigator} from "react-navigation";
import Activites from "./Activites";
import DetailsActivite from "./DetailsActivite";
const transitionConfig = () => {
    return {
        transitionSpec: {
            duration: 300,
            easing: Easing.out(Easing.poly(4)),
            timing: Animated.timing,
            useNativeDriver: true,
        },
        screenInterpolator: sceneProps => {
            const {layout, position, scene} = sceneProps;
            const thisSceneIndex = scene.index;
            const width = layout.initWidth;
            const translateX = position.interpolate({
                inputRange: [thisSceneIndex - 1, thisSceneIndex],
                outputRange: [width, 0],
            });
            return {transform: [{translateX}]};
        },
    };
};
const ActiviteStack = createStackNavigator({
    Activites: {
        screen: Activites,
        navigationOptions: {
            header: null,
        },
    },
    DetailsActivite: {
        screen: DetailsActivite,
        navigationOptions: {
            header: null,
        },
    },
}, {
    transitionConfig,
    initialRouteName: 'Activites',
});
export default class NavActivities extends React.Component {
    static router = ActiviteStack.router;
    render() {
        let {user,currentPosition}
            = this.props.screenProps;
        return <ActiviteStack
            screenProps={{
                user,
                currentPosition
            }}
            navigation={this.props.navigation}/>
    }
}