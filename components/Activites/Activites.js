import * as React from 'react';
import {Block, Text as GalioText} from 'galio-framework'
import Colors from "../utils/Colors";
import {ActivityIndicator, FlatList, Image, TouchableOpacity, StyleSheet, ScrollView, View} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import axios from "axios";
import {SIZES, Urh} from "../utils/Const";
import ImageLoad from "react-native-image-placeholder";
import Ionicons from "react-native-vector-icons/Ionicons";
import {Checkbox, Divider, Switch} from "react-native-paper";
import Slider from "react-native-slider";
import DatePicker from "react-native-datepicker";
import Modal from "react-native-modal";
import AsyncStorage from "@react-native-community/async-storage";
import moment from "moment";
import 'moment/locale/fr'
import {Button} from "native-base";  // without this line it didn't work
moment.locale('fr')
let today = new Date()
today.setDate(today.getDate() + 1)
export default class Activites extends React.Component {



    state = {
        activities: [],
        refreshing: false,
        isLoading: false,
        isFilterOpen: false,
        filtredPdv:[],
        filtredgouvernorat:[],
        startDate: "2010-01-01",
        endDate:moment(today).format("YYYY-MM-DD"),

    };




    constructor(props) {
        super(props);
        this.renderHeader = this.renderHeader.bind(this);
        this.renderActivite = this.renderActivite.bind(this);

    }


    openFilter = () => {
        let {isFilterOpen} = this.state
        this.setState({isFilterOpen: !isFilterOpen})
    }

    getUser = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            //this.state.user = JSON.parse(token)
            this.setState({user: JSON.parse(token)})
            return JSON.parse(token)
        } catch (error) {
        }
    }

    componentDidMount() {


        let {user} = this.props.screenProps;

            let {filtredgouvernorat, filtredPdv} = this.state


        let today = new Date()
            let validcategories = []
            user.categorie.map((cat) => {

                if (moment(today).isSameOrAfter(cat.start, 'day'))
                    validcategories.push(cat.categorie)

            })

            validcategories.map((pdv, index) => {
                let Pdv = {
                    pdv: pdv,
                    checked: true,
                }
                filtredPdv.push(Pdv)
            })

            console.log("batatattatatatatattata",user,filtredPdv)



            this.setState({filtredPdv})






        let validcategoriesgouv = []
        user.gouvernorats.map((cat) => {

            if (moment(today).isSameOrAfter(cat.start, 'day'))
                validcategoriesgouv.push(cat.gouvernorat)

        })

        validcategoriesgouv.map((pdv, index) => {
            let Pdv = {
                gouvernorat: pdv,
                checked: true,
            }
            filtredgouvernorat.push(Pdv)
        })



        this.setState({filtredgouvernorat})



this.filteroldmissions()

    }



    filteroldmissions= () => {
        let {user} = this.props.screenProps;

let categoriespdv = []
let gouvs = []

        this.state.filtredPdv.map((camp, idx) => {
            if (camp && camp.checked == true)
                categoriespdv.push(camp.pdv)
        })


        this.state.filtredgouvernorat.map((camp, idx) => {
            if (camp && camp.checked == true)
                gouvs.push(camp.gouvernorat)
        })


        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data = {
            categorie: JSON.stringify(categoriespdv),
            gouvernorat: JSON.stringify(gouvs),
            iduser: user._id,
            start: this.state.startDate,
            end: this.state.endDate,

        }

        console.log(categoriespdv,this.state.endDate,gouvs)

        axios.post(Urh + 'oldmissionsfouser', data, configpic).then((res) => {
console.log("patato",res.data)
let activities = res.data
            this.setState({activities,isFilterOpen:false})



        }).catch((err) => {
            console.log(err)
        })





    }

    renderHeader() {
        let {user: {photo}} = this.props.screenProps;
        return (
            <Block row shadow right middle style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 10,
            }}>
                <Block center style={{marginRight: 5}}>
                    <TouchableOpacity onPress={() => this.openFilter()}>
                        <AntDesign
                            name={"filter"}
                            color={Colors.white}
                            size={24}
                        />
                    </TouchableOpacity>
                </Block>
                <Block flex center style={{marginLeft: 20}}>
                    <GalioText color={Colors.white} style={{fontSize: 20}}> Activité </GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    {photo && photo.length > 0 ?
                        <TouchableOpacity onPress={this.goToPlus}>
                            <Image
                                style={{
                                    height: 35,
                                    width: 35,
                                    borderRadius: 18,
                                    borderColor: Colors.white,
                                    borderWidth: 1,
                                }}
                                source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + photo}}/>
                        </TouchableOpacity>
                        : null}
                </Block>
            </Block>
        )
    }

    handleChangePrioritiesPDV(index) {
        let newfiltredPdv = [...this.state.filtredPdv]

        newfiltredPdv[index].checked = !newfiltredPdv[index].checked
        this.setState({filtredPdv: newfiltredPdv})
    }



    handleChangePrioritiesgouvernorat(index) {
        let newfiltredgouvernorat = [...this.state.filtredgouvernorat]

        newfiltredgouvernorat[index].checked = !newfiltredgouvernorat[index].checked
        this.setState({filtredgouvernorat: newfiltredgouvernorat})
    }


    onRefresh() {
    }

    renderActivite(item, index) {
        return (
            <Block style={styles.card}>
                <Block row style={{
                    backgroundColor: Colors.white,
                    padding: 5,
                    height: 100,
                    borderRadius: 10,
                }} space='between'>
                    <Block center>
                        {item.photo && item.photo.length>1 ?  <ImageLoad
                            borderRadius={5}
                            style={{minWidth: 80, minHeight: 80, resizeMode: "cover", borderRadius: 2}}
                            loadingStyle={{size: 'small', color: 'blue'}}
                            source={{
                                uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/'+item.photo,
                            }}/>
                            :<ImageLoad
                            borderRadius={5}
                            style={{minWidth: 80, minHeight: 80, resizeMode: "cover", borderRadius: 2}}
                            loadingStyle={{size: 'small', color: 'blue'}}
                            source={{
                                uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/Food+Grocery.png',
                            }}/>}


                    </Block>
                    <Block style={{marginLeft: 2}} flex>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailsActivite',{
                            activite : JSON.stringify(item)
                        })}
                                          style={{flex: 1, justifyContent: 'center'}}>
                            <GalioText style={{marginBottom: 5}} numberOfLines={1} size={15} color={Colors.textColor}>
                                {item.nomcamp}
                            </GalioText>
                            <GalioText numberOfLines={1} muted>
                                Rapport {moment(item.envoie_date).format('llll')}
                                {/*Jeudi, 31 Déc 2019, 13:15*/}
                            </GalioText>
                        </TouchableOpacity>
                    </Block>
                    <Block middle style={{marginRight: 5}}>
                        <Ionicons name={'ios-arrow-forward'} color={Colors.orange} size={32}/>
                    </Block>
                </Block>
            </Block>
        )
    }

    render() {
        let {activities, refreshing, isLoading,isFilterOpen,filtredPdv,filtredgouvernorat} = this.state

        return (
            <Block flex>
                {this.renderHeader()}
                {!isLoading ? <FlatList
                    refreshing={refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                    showsVerticalScrollIndicator={false}
                    removeClippedSubviews={false}
                    data={activities}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => this.renderActivite(item, index)}
                /> : <Block flex middle>
                    <ActivityIndicator size={"large"}/>
                </Block>}



                <Modal
                    style={{
                        margin: 0,
                        marginRight: SIZES.width / 5,
                    }}
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={isFilterOpen}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        flex: 1,
                        marginTop: 0,
                        marginBottom: 0,
                        borderRadius: 0,

                    }}>
                        <Block flex>
                            <Block flex style={{
                                marginTop: 2,
                                padding: 10,
                            }}>
                                <Block row>
                                    <Block flex>
                                        <GalioText center size={20} color={Colors.textColor}>
                                            Filtre
                                        </GalioText>
                                    </Block>
                                    <Block>
                                        <TouchableOpacity onPress={() => {
                                            this.openFilter()
                                        }}>
                                            <AntDesign size={24} name={"close"} color={Colors.textColor}/>
                                        </TouchableOpacity>
                                    </Block>
                                </Block>
                                {/* priority */}



                                <Divider style={{
                                    marginTop: 5
                                }}/>
                                <ScrollView
                                    scrollEnabled={true}
                                    style={{
                                        flex: 1
                                    }}>
                                    {/*  Distance PDV */}




                                    <Divider style={{
                                        marginVertical: 10
                                    }}/>
                                    {/* type pdv  */}
                                    <Block>
                                        {/* block Campagne */}
                                        <Block>
                                            <Block row>
                                                <GalioText center size={15} color={Colors.orange}>
                                                    Filtrer par type points de vente
                                                </GalioText>
                                            </Block>
                                            <Block style={{
                                                marginLeft: 10
                                            }}>


                                                {filtredPdv.map((camp, index) => {
                                                    let checked = camp.checked ? 'checked' : 'unchecked'
                                                    let color = Colors.blue
                                                    return (

                                                        <Block row style={{
                                                            paddingVertical: 5
                                                        }}>
                                                            <TouchableOpacity style={{
                                                                backgroundColor: camp.checked ? color : "#dddddd",
                                                                paddingHorizontal: 8,
                                                                paddingVertical: 4,
                                                                borderRadius: 10,
                                                            }}
                                                                              onPress={() => this.handleChangePrioritiesPDV(index)}
                                                            >
                                                                <GalioText
                                                                    size={14}
                                                                    color={Colors.white}>
                                                                    {camp.pdv == "PDR" ? "Point de recharge"
                                                                        : camp.pdv == "PDV" ? "Point de vente"
                                                                            : camp.pdv == "PDV LABELISE" ? "Point de vente labellisé"
                                                                                : camp.pdv == "boutique franchise" ? "Point de vente franchisé" : camp.pdv}
                                                                </GalioText>

                                                            </TouchableOpacity>
                                                        </Block>

                                                    )
                                                })}
                                            </Block>
                                        </Block>
                                    </Block>
                                    <Divider style={{
                                        marginVertical: 10
                                    }}/>




                                    <Block>
                                        {/* block Campagne */}
                                        <Block>
                                            <Block row>
                                                <GalioText center size={15} color={Colors.orange}>
                                                    Filtrer par gouvernorat
                                                </GalioText>
                                            </Block>
                                            <Block style={{
                                                marginLeft: 10
                                            }}>


                                                {filtredgouvernorat.map((camp, index) => {
                                                    let checked = camp.checked ? 'checked' : 'unchecked'
                                                    let color = Colors.blue
                                                    return (

                                                        <Block row style={{
                                                            paddingVertical: 5
                                                        }}>
                                                            <TouchableOpacity style={{
                                                                backgroundColor: camp.checked ? color : "#dddddd",
                                                                paddingHorizontal: 8,
                                                                paddingVertical: 4,
                                                                borderRadius: 10,
                                                            }}
                                                                              onPress={() => this.handleChangePrioritiesgouvernorat(index)}
                                                            >
                                                                <GalioText
                                                                    size={14}
                                                                    color={Colors.white}>
                                                                    {camp.gouvernorat}
                                                                </GalioText>

                                                            </TouchableOpacity>
                                                        </Block>

                                                    )
                                                })}
                                            </Block>
                                        </Block>
                                    </Block>
                                    <Divider style={{
                                        marginVertical: 10
                                    }}/>


                                    {/* Date début check-list */}
                                    <Block style={{
                                        paddingBottom: 20
                                    }}>
                                        <Block row>
                                            <GalioText center size={15} color={Colors.orange}>
                                                Filtrer par date début / date fin
                                            </GalioText>
                                        </Block>
                                        <Block flex style={{
                                            marginVertical: 5
                                        }}>
                                            <Block row>
                                                <Block center>
                                                    <AntDesign style={{
                                                        marginRight: 2
                                                    }} name={'calendar'} color={Colors.inactiveTintColor} size={28}/>
                                                </Block>
                                                <DatePicker
                                                    showIcon={false}
                                                    androidMode={"spinner"}
                                                    minDate={`${(new Date()).getFullYear()}-01-01`}
                                                    maxDate={`${(new Date()).getFullYear() + 1}-12-31`}
                                                    format="YYYY-MM-DD"
                                                    style={{
                                                        width: 100,
                                                        marginHorizontal: 3
                                                    }}

                                                    date={this.state.startDate}
                                                    onDateChange={(date) => {
                                                        this.setState({startDate: date})

                                                    }}
                                                />
                                                <Block center>
                                                    <GalioText>
                                                        -
                                                    </GalioText>
                                                </Block>
                                                <DatePicker
                                                    showIcon={false}
                                                    androidMode={"spinner"}
                                                    minDate={`${(new Date()).getFullYear()}-01-01`}
                                                    maxDate={`${(new Date()).getFullYear() + 1}-12-31`}
                                                    format="YYYY-MM-DD"
                                                    style={{
                                                        width: 100,
                                                        marginLeft: 3
                                                    }}
                                                    date={this.state.endDate}

                                                    onDateChange={(date) => {
                                                        this.setState({endDate: date})
                                                    }}
                                                />
                                            </Block>
                                        </Block>
                                    </Block>
                                </ScrollView>
                            </Block>
                        </Block>

                        <Button
                            onPress={() =>this.filteroldmissions() }
                            block
                            style={{
                                borderRadius: 0,
                                elevation: 0,
                                backgroundColor: Colors.blue,
                            }}
                        >
                            <GalioText size={13} color={Colors.orange2}>Appliquer</GalioText>
                        </Button>
                    </Block>

                </Modal>

            </Block>
        );
    };
};
const styles = StyleSheet.create({
    card: {
        backgroundColor: Colors.white,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 50,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 4,
    }
})