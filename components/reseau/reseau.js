import React from 'react';
import {createMaterialTopTabNavigator,} from 'react-navigation';
import {Image, TouchableOpacity, View} from 'react-native'
import code from './code';
import Colors from "../utils/Colors";
import classement from './Classement'
import {Block, Text as GalioText} from "galio-framework";
import AsyncStorage from "@react-native-community/async-storage";
import Feather from "react-native-vector-icons/Feather";
import Modal from "react-native-modal";
import AntDesign from "react-native-vector-icons/AntDesign";
import {Button} from "native-base";
import {fontFamily} from "../utils/Const";
//import Navigator in our project

const TabScreen = createMaterialTopTabNavigator(
    {
        Code: {
            screen: code,
            navigationOptions: {
                title: 'Parrainage',
                tabBarIcon: ({tintColor}) => (<Block center>
                    <AntDesign name='adduser' size={24} color={tintColor}/>
                </Block>),

            }
        },
        Amis: {
            screen: classement,
            navigationOptions: {
                title: 'Classement',
                tabBarIcon: ({tintColor}) => (<Block center>
                    <Feather name='bar-chart-2' size={24} color={tintColor}/>
                </Block>),
            }
        }
    },
    {
        swipeEnabled: true,
        animationEnabled: true,
        activeColor: Colors.activeTintColor,
        inactiveColor: Colors.inactiveTintColor,
        tabBarOptions: {
            showIcon: true,
            upperCaseLabel: false,
            activeTintColor: Colors.activeTintColor,
            inactiveTintColor: Colors.inactiveTintColor,
            style: {
                elevation: 0,
                backgroundColor: Colors.white,
            },
            labelStyle: {
                fontSize: 16,
                paddingTop: 7,
                margin: 0,
                marginLeft: 5,
            },
            indicatorStyle: {
                borderBottomColor: Colors.orange,
                borderBottomWidth: 1,
            },
            tabStyle: {

                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
            }
        },
    }
);
export default class Reseau extends React.Component {
    // to add router to navigation props
    static router = TabScreen.router;

    state = {
        user: {},
        isModalInfoVisible: false,
    };

    componentDidMount() {
        this.getUser();
    }

    getUser = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            this.setState({user: JSON.parse(token)})
        } catch (error) {

        }
    };
    handleModalInfo = () => {
        let {isModalInfoVisible} = this.state;
        this.setState({isModalInfoVisible: !isModalInfoVisible})
    };
    renderHeader = (user) => {
        return (
            <Block row space={"between"} middle style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 10,
            }}>
                <Block center style={{marginRight: 5}}>
                    <AntDesign onPress={() => this.handleModalInfo()} size={28} name={"questioncircle"}
                               color={Colors.white}/>
                </Block>
                <Block center style={{marginLeft: 20}}>
                    <GalioText color={Colors.white} style={{fontSize: 20}}>Réseau</GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    {user.photo && user.photo.length > 0 ?
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('NavProfile')}>
                            <Image
                                style={{
                                    height: 35,
                                    width: 35,
                                    borderRadius: 18,
                                    borderColor: Colors.white,
                                    borderWidth: 1,
                                }}
                                source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + user.photo}}/>
                        </TouchableOpacity>
                        : null}
                </Block>
            </Block>
        )
    };


    render() {
        let {user, isModalInfoVisible} = this.state;

        return (
            <Block flex>
                {this.renderHeader(user)}
                {/*Modal info !!!*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={isModalInfoVisible}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 10,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Augmentez vos points d'expérience et débloquer plus de cadeaux.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.handleModalInfo()}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>compris</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>
                <TabScreen screenProps={{user: user}} navigation={this.props.navigation}/>
            </Block>
        )
    }

}
