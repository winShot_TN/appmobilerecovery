import React, {Component} from 'react';
import {FlatList, Image, PermissionsAndroid, View} from 'react-native';

import axios from "axios";
import AsyncStorage from '@react-native-community/async-storage';

import {Button} from 'native-base'
import {Divider} from 'react-native-paper'
import {fontFamily, Urh} from '../utils/Const'
import {Block, Text} from 'galio-framework'
import Colors from "../utils/Colors";

export default class Classement extends Component {

    constructor(props) {
        super(props);
        this.state = {
            friends: [{
                nom: 'Aaaaaaa',
                prenom: 'NEW',
                missions: 22,
                points: 500,
                filleuls: 5,
                img: 'https://mpng.pngfly.com/20180713/ig/kisspng-user-profile-linkedin-netwerk-money-order-fulfillm-round-face-5b4944092212b3.5336384915315282011396.jpg',
            }, {
                nom: 'name',
                prenom: 'FFFFF',
                missions: 10,
                points: 50,
                filleuls: 6,
                img: 'https://mpng.pngfly.com/20180713/ig/kisspng-user-profile-linkedin-netwerk-money-order-fulfillm-round-face-5b4944092212b3.5336384915315282011396.jpg',
            }, {
                nom: 'Last',
                prenom: 'Name',
                missions: 250,
                points: 2000,
                filleuls: 6,
                img: 'https://i1.wp.com/www.clicksitter.ca/wp-content/uploads/2017/04/avatar-round-3.png?fit=300%2C300&ssl=1',
            }, {
                nom: 'Bouhouch',
                prenom: 'Lamjed',
                missions: 22,
                points: 703,
                filleuls: 9,
                img: 'https://mpng.pngfly.com/20180713/ig/kisspng-user-profile-linkedin-netwerk-money-order-fulfillm-round-face-5b4944092212b3.5336384915315282011396.jpg',
            }],
            tags: [
                {
                    title: 'Amis',
                    isSelected: true,
                },
                {
                    title: 'Zone',
                    isSelected: false,
                },
                {
                    title: 'Général',
                    isSelected: false,
                }],
        };
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )
    }

    componentDidMount() {
         this._promiseHandling()
    }

    _promiseHandling = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            this.user = JSON.parse(token)
            let data = {id_parrain: this.user._id}
            this.getusers(data)
        } catch (error) {

        }
    }


    getusers = (data)=>{


        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };

        axios.post(Urh + 'findparain', data, configpic)
            .then((res) => {
                let friends=[]

                res.data.map((user)=>{
                    let img ="https://www.pngfind.com/pngs/m/610-6104451_image-placeholder-png-user-profile-placeholder-image-png.png"
                    if(user.photo && user.photo!="")
                    {img="https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/"+user.photo}

                    friends.push({
                        nom: user.nom,
                        prenom: user.prenom,
                        missions: user.missvalidnonpay+user.missvalidpaye,
                        points: user.points,
                        filleuls: user.old_code_parrain.length,
                        img: img
                    })
                })


                this.setState({friends: friends})




            })
            .catch((err) => {

            })

    }


    handleChange = index => {
        let {tags} = this.state;
        if (tags[index].isSelected)
            return

        tags[index].isSelected = !tags[index].isSelected
        tags.map((tag, idx) => {
            if (idx !== index)
                tag.isSelected = !tags[index].isSelected
        })
        this.setState({tags: tags}, () => this.forceUpdate())

        switch(index) {
            case 0:
                this.getusers({id_parrain: this.user._id})
                break


            case 1:

                this.getusers({gouvernorat: this.user.gouvernorat})
                break

            case 2:

                this.getusers({})
                break

        }


    };

    _renderTag = (tag, index) => {
        let margin = index === 0 ? 4 : 8
        let textColor = tag.isSelected ? Colors.orange : Colors.gray
        let borderBottomWidth = tag.isSelected ? 2 : 0
        return (
            <Block flex>
                <Button
                    iconLeft
                    onPress={() => this.handleChange(index)}
                    block
                    style={{
                        borderRadius : 0,
                        elevation : 0,
                        backgroundColor : Colors.white,
                        borderBottomWidth : borderBottomWidth,
                        borderBottomColor : Colors.blue,
                    }}
                >

                    <Text color={textColor}>{tag.title}</Text>
                </Button>
            </Block>
        )
    }

    renderFriend = (friend, index) => {
        let first = friend.nom.slice(0, 1);
        let points = friend.points <= 999 ? friend.points : friend.points / 1000 + 'K';
        return (
            <Block row style={{
                backgroundColor: Colors.white,
                paddingHorizontal: 5,
                justifyContent: 'flex-start'
            }}>
                <Block center>
                    <Text h4
                          color={Colors.textColor}>{`${index + 1}`}.</Text>
                </Block>
                <Block center style={{marginHorizontal: 10}}>
                    <Image
                        style={{
                            height: 58,
                            width: 58,
                            borderRadius: 29,
                            borderWidth: 1,
                            borderColor: Colors.white,
                            marginLeft: 4,
                        }}
                        source={{uri: friend.img}}
                    />
                </Block>
                <Block style={{
                    justifyContent: 'space-around'
                }}>
                    <Block>
                        <Text>
                        </Text>
                    </Block>
                    <Block>
                        <Block>
                            <Text style={{
                                fontSize: 19,
                            }} color={Colors.textColor}>
                                {`${friend.prenom}.${first}`}
                            </Text>
                        </Block>
                        <Block row>
                            <Block center row>
                                <Text style={{
                                    fontSize: 12,
                                    marginRight: 2,
                                    fontFamily: fontFamily["Poppins-Regular"],
                                }} color={Colors.blue}>
                                    {`${friend.missions}`}
                                </Text>
                                <Text
                                    style={{
                                        marginLeft: 2,
                                        fontFamily: fontFamily["Poppins-Light"],
                                        fontSize: 12,
                                    }}
                                    center color={Colors.blue}>
                                    missions
                                </Text>
                            </Block>
                            <Block center>
                                <Text color={Colors.orange} style={{marginHorizontal: 4, fontSize: 12}}>
                                    |
                                </Text>
                            </Block>
                            <Block center row>
                                <Text style={{
                                    fontSize: 12,
                                    marginRight: 2,
                                    fontFamily: fontFamily["Poppins-Regular"],
                                }} color={Colors.blue}>
                                    {`${friend.filleuls}`}
                                </Text>
                                <Text style={{
                                    marginLeft: 2,
                                    fontFamily: fontFamily["Poppins-Light"],
                                    fontSize: 12,
                                }} center h5 color={Colors.blue}>
                                    parrainages
                                </Text>
                            </Block>
                        </Block>
                    </Block>
                </Block>
                <Block center right flex>
                    <Block center row>
                        <Text style={{
                            fontSize: 24,
                            marginRight: 2,
                            fontFamily: fontFamily["Poppins-Regular"],
                        }} h5 color={Colors.orange}>
                            {`${friend.points}`}
                        </Text>
                        <Text style={{fontFamily: fontFamily["Poppins-Light"],}} center h5
                              color={Colors.lightBlue}>
                            Pts
                        </Text>
                    </Block>
                </Block>
            </Block>
        )
    };
    renderSeparator = () => {
        return (
            <View
                style={{
                    flex: 1,
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }}
            />
        );
    };
    render() {
        const {friends, tags} = this.state
        let sorted_friends =friends

        return (

            <Block flex style={{
                backgroundColor: Colors.white,
            }}>
                <Block row space={"around"} style={{
                    paddingHorizontal: 5,
                    paddingVertical: 20
                }}>
                    {tags.map((tag, index) => this._renderTag(tag, index))}
                </Block>
                <Block style={{backgroundColor: "#f7f7f7", flex: 1}}>
                    <FlatList
                        ItemSeparatorComponent={() => (<Divider/>)}
                        showsVerticalScrollIndicator={true}
                        data={sorted_friends}
                        keyExtractor={(friend, index) => index.toString()}
                        renderItem={({item, index}) => this.renderFriend(item, index)}
                        disableVirtualization
                    />
                </Block>
            </Block>
        )
    }
}
