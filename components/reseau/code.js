import React, {Component} from 'react'
import {Clipboard, Share, StyleSheet, ToastAndroid} from "react-native"
import {Button} from 'native-base'
import {SIZES,} from '../utils/Const'
import Colors from "../utils/Colors"
import {Block, Text} from 'galio-framework'
import AsyncStorage from "@react-native-community/async-storage";

export default class code extends Component {

user

    constructor(props) {
        super(props);
        this.state = {

            code:""

        }

        }

    componentDidMount() {
        this._promiseHandling()
    }

    _promiseHandling = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            this.user = JSON.parse(token)
            this.setState({code:this.user})

        } catch (error) {

        }
    }


    shareTo = async (codeparrain) => {
        // copy
        Clipboard.setString(codeparrain);
        // show toast
        ToastAndroid.showWithGravityAndOffset(
            'Code parrain a été copie',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
            0,
            200,
        );
        // get copy String
        let code =  await Clipboard.getString();

        Share.share({
            message : codeparrain,
            title : "Code parrain",
        })
    }
    render() {
    let {code}= this.state
        return (
            <Block flex   space={"around"} style={{padding: 20,backgroundColor : Colors.white}}>
                <Block row  style={{justifyContent : 'flex-start'}}>
                    <Block center>
                        <Block style={{
                            height: 100,
                            width : 100,
                            borderColor : Colors.orange,
                            borderWidth : 1,
                            borderRadius : 50,
                        }}>
                        </Block>

                    </Block>
                    <Block center style={{marginLeft : 1}}>
                        <Text h2 color={Colors.textColor}> 1</Text>
                    </Block>
                    <Block center flex  style={{marginLeft : 7,justifyContent : 'flex-end'}}>
                        <Text  size={17} color={Colors.textColor}>{`Envoi ton code  à un ami`}</Text>
                    </Block>
                </Block>

                <Block row   style={{justifyContent : 'flex-start'}}>
                    <Block center>
                        <Block style={{
                            height: 100,
                            width : 100,
                            borderColor : Colors.orange,
                            borderWidth : 1,
                            borderRadius : 50,
                        }}>
                        </Block>
                    </Block>
                    <Block  center style={{marginLeft : 1}}>
                        <Text h2 color={Colors.textColor}> 2</Text>
                    </Block>
                    <Block center flex  style={{marginLeft : 7,justifyContent : 'flex-end'}}>
                        <Text  size={17} color={Colors.textColor}>{`Ton ami télécharge l'application`}</Text>
                    </Block>
                </Block>

                <Block row  style={{justifyContent : 'flex-start'}}>
                    <Block center>
                        <Block style={{
                            height: 100,
                            width : 100,
                            borderColor : Colors.orange,
                            borderWidth : 1,
                            borderRadius : 50,
                        }}>
                        </Block>

                    </Block>
                    <Block center style={{marginLeft : 1}}>
                        <Text  h2 color={Colors.textColor}> 3</Text>
                    </Block>
                    <Block center flex  style={{marginLeft : 7,justifyContent : 'flex-end'}}>
                        <Text  size={17} color={Colors.textColor}>{`Gagnez 2DT et 200 Pts après validation de sa 1ère mission`}</Text>
                    </Block>
                </Block>
                <Block row  style={{justifyContent : 'center'}}>
                    <Button
                        onPress={() => this.shareTo(code.mon_code_parrain)}
                        rounded
                        style={{
                            width : SIZES.width / 2,
                            backgroundColor: Colors.blue,
                            elevation: 0,
                            paddingHorizontal: 15,
                            justifyContent : 'center'
                        }}>
                        <Text center bold  color={Colors.white} size={24}></Text>
                    </Button>
                </Block>

                {/*<Image
                    source={Gift}
                    style={styles.contain}/>

                <View>
                    <Title style={{
                        fontSize: 24,
                        fontFamily: fontFamily["Poppins-Regular"],
                        textAlign: 'center'
                    }}>{`Envoi une invitation à 1 ami Gagnez tous les 2 ${'\n'} envoi une invitation `}</Title>
                </View>
                <View>
                    <Button
                        rounded info full
                        style={{borderRadius: 30, backgroundColor: Colors.blue, elevation: 0, width: '50%'}}
                    >
                        <BaseText style={{fontFamily: fontFamily["Poppins-Regular"]}}
                                  uppercase={false}>Partager</BaseText>
                    </Button>
                </View>*/}
            </Block>
        )
    }
}

const styles = StyleSheet.create({
    contain: {
        width: '50%',
        height: '50%',
        alignSelf: 'center',
        resizeMode: 'contain'
    }
});
