import React, {Component} from 'react';
import {
    StyleSheet,
    ScrollView,
    Alert,
    TouchableOpacity,
    ActivityIndicator,View
} from 'react-native';
import {

    Text,

} from 'react-native';
import {Image} from 'react-native-elements'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Camera from 'react-native-camera';
import {
    Button
} from "native-base";
import Modal from "react-native-modal";
import axios from "axios";
import AsyncStorage from '@react-native-community/async-storage';
import {Header} from 'react-navigation'
import Colors from "../utils/Colors"
import {fontFamily, Logo, Urh, UrhImages} from "../utils/Const"
import {Block, Text as GalioText, Input} from 'galio-framework'
import AntDesign from "react-native-vector-icons/AntDesign";
import Stars from 'react-native-stars';
import {SelectMultipleGroupButton} from 'react-native-selectmultiple-button'
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {SIZES} from '../utils/Const'
import {ProgressBar} from "react-native-paper";
import ImageLoad from 'react-native-image-placeholder';

export default class Etaps extends Component {
    mission
    allimages = []
    Reponse = []

    constructor(props) {
        super(props);
        this.state = {
            progress : 0,
            photo_user: [],
            questions_user: [],
            pictosend: [],
            reponse: [],
            isNextDisabled: true,
            successMission: false,
            termine: false,
            isModalInfoVisible: false,
            cameramodal: false,
            picrang: null,
            idx: null
        }
        this.goToList = this.goToList.bind(this);
    }

    storequestimg = async () => {
        try {
            await AsyncStorage.setItem('images', JSON.stringify(this.allimages))
            await AsyncStorage.setItem('@reponse', JSON.stringify(this.Reponse))

        } catch (e) {
            // saving error
        }
    };

    getmission = async () => {
        try {
            const token = await AsyncStorage.getItem('@reponse');
            const images = await AsyncStorage.getItem('images');
            this.Reponse = JSON.parse(token)
            this.allimages = JSON.parse(images)
        } catch (error) {

        }
    };


    updateState = data => {
        this.setState(data);
    };

    handleModalInfo = () => {
        let {isModalInfoVisible} = this.state
        this.setState({isModalInfoVisible: !isModalInfoVisible})
    }

    componentDidMount() {
        /*   BackHandler.addEventListener('hardwareBackPress', this.goBack);*/
        const {navigation} = this.props;
        this.mission = navigation.getParam('missionid', null)
        let Reponse = navigation.getParam('reponse', [])
        let pictosend = navigation.getParam('pictosend', [])


        this.setState({reponse: Reponse, pictosend: pictosend})
        if (Reponse)
            this.Reponse = Reponse

        if (pictosend)
            this.pictosend = pictosend

        let user = navigation.getParam('user', null)
        let etape = navigation.getParam('etape', 0)
        let etapes = navigation.getParam('etapes', 0)

        let photo_user = []
        this.mission.alldata.photo_user
            .filter(photo => (photo.etape) === etape)
            .sort((q1, q2) => q1.rang - q2.rang)
            .map((value, index) => photo_user.push(value))

        let questions_user = this.mission.alldata.questions_user
            .filter(question => (question.etape) === etape)
            .sort((q1, q2) => q1.rang - q2.rang)


        // add to state
        this.setState({
            questions_user: questions_user,
            photo_user: photo_user,
        })
    }

    renderHeader = (nomcamp, user, etap, numbreEtaps) => {
        return (
            <Block space="between" row style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 8,
            }}>
                <Block center row>
                    {
                        (etap > 1) ? null : <AntDesign
                            onPress={this.goBack}
                            name='arrowleft'
                            size={22}
                            color={Colors.white}/>

                    }

                    <GalioText numberOfLines={1}
                               color={Colors.white}
                               style={{
                                   fontSize: 18,
                                   marginLeft: 8,
                                   maxWidth: SIZES.width * 0.5
                               }}>{`${nomcamp}`}</GalioText>
                </Block>
                <Block center row>
                    <GalioText color={Colors.white} h5 style={{marginRight: 8}}>
                        {`${etap} / ${numbreEtaps}`}
                    </GalioText>
                    <Block center style={{marginRight: 5}}>
                        <TouchableOpacity onPress={() => this.handleModalInfo()}>
                            <AntDesign size={28} name={"questioncircle"}
                                       color={Colors.white}/>
                        </TouchableOpacity>
                    </Block>
                </Block>
            </Block>
        )
    };

    evaluationetoile = (id, rang) => {
        let reponse = [...this.state.reponse]
        reponse[rang] = 0
        reponse[rang] = id
        this.setState({reponse})
    }

    selected = (valueTap, rang, choices) => {
        this.Reponse[rang] = []
        let reponse = [...this.state.reponse]
        reponse[rang] = []
        valueTap.map((value, idx) => {
            let i = choices.indexOf(value)
            this.Reponse[rang].push(this.mission.alldata.questions_user[rang].choices[i])
            reponse[rang].push(this.mission.alldata.questions_user[rang].choices[i])
        })
        this.setState({reponse: reponse})
    }

    /**
     *
     * @param question
     * @param idx
     * @returns {null|*}
     */
    renderQuestion = (question, idx) => {
        let reponse = [...this.state.reponse]
        const hasImage = question.img.length > 1

        switch (question.type) {

            case 'Numérique':

                return (
                    <Block key={idx}
                           style={{backgroundColor: Colors.white, marginVertical: 5, paddingVertical: 5}}>
                        <Block style={{
                            paddingHorizontal: 5,
                        }}>
                            <GalioText numberOfLines={5} h5
                                       color={Colors.textColor}>{`${idx + 1}. ${question.question} ?`}</GalioText>
                        </Block>
                        {
                            hasImage && <Block style={{
                                                backgroundColor: Colors.lightGray
                                            }}>
                                                <Block center>
                                                    <Image
                                                        PlaceholderContent={<ActivityIndicator color={Colors.orange}  />}
                                                        style={styles.defaultImage}
                                                        source={{uri: `${UrhImages + question.img}`}}
                                                    />
                                                </Block>
                                            </Block>
                        }
                        <Block flex style={{justifyContent: 'center', marginHorizontal: 10}}>
                            <Input
                                style={{
                                    borderColor: Colors.lightBlue
                                }}
                                keyboardType={'numeric'}
                                color={Colors.textColor}
                                rounded
                                onChangeText={text => {
                                    reponse[question.rang] = [text]
                                    this.setState({reponse, [question.rang + "qsyu"]: text})
                                }}
                                value={this.state[question.rang + "qsyu"]}
                            />
                        </Block>

                    </Block>
                )
                break;
            case 'Réponse unique':
                let choicess = []
                question.choices.map((choice, idx) => choicess.push({value: choice}))
                let SelectedIndexUnique = []
                if (reponse[question.rang])
                    reponse[question.rang].map((value) => {
                        SelectedIndexUnique.push(question.choices.indexOf(value))
                    })


                return (
                    <Block key={idx} style={{backgroundColor: Colors.white, marginVertical: 5, padding: 5}}>
                        <Block>
                            <GalioText numberOfLines={5} h5
                                       color={Colors.textColor}>{`${idx + 1}. ${question.question} ?`}</GalioText>
                        </Block>
                        <Block flex style={{marginTop: 15}}>
                            <Block left flex row style={{flexWrap: 'wrap'}}>
                                <SelectMultipleGroupButton
                                    multiple={false}
                                    containerViewStyle={{
                                        flex: 1,
                                        justifyContent: 'space-around',
                                        alignItems: 'center',
                                    }}
                                    highLightStyle={{
                                        borderColor: 'gray',
                                        backgroundColor: 'transparent',
                                        textColor: '#333',
                                        borderTintColor: "#ff9e19",
                                        backgroundTintColor: 'transparent',
                                        textTintColor: "#ff9e19",
                                    }}
                                    defaultSelectedIndexes={SelectedIndexUnique}
                                    onSelectedValuesChange={valueTap => {
                                        this.selected(valueTap, question.rang, question.choices)
                                    }}
                                    // selected={valueTap => {this.selected(valueTap,question.rang,question.choices)}}
                                    group={choicess}
                                />
                            </Block>
                        </Block>
                    </Block>
                )
                break
            case 'Textuelle':

                return (
                    <Block key={idx}
                           style={{backgroundColor: Colors.white, marginVertical: 5, padding: 5}}>
                        <Block>
                            <GalioText numberOfLines={5} h5
                                       color={Colors.textColor}>{`${idx + 1}. ${question.question} ?`}</GalioText>
                        </Block>
                        <Block flex>
                            <Input
                                style={{
                                    borderColor: Colors.lightBlue
                                }}
                                color={Colors.textColor}
                                rounded

                                onChangeText={text => {
                                    reponse[question.rang] = [text]
                                    this.setState({reponse, [question.rang + "qsyu"]: text})
                                }}
                                value={this.state[question.rang + "qsyu"]}

                            />
                        </Block>
                    </Block>
                )
                break;
            case 'Réponses multiples':
                let choices = []
                question.choices.map((choice, idx) => choices.push({value: choice}))
                let SelectedIndexesMultiple = []
                if (reponse[question.rang])
                    reponse[question.rang].map((value) => {
                        SelectedIndexesMultiple.push(question.choices.indexOf(value))
                    })

                return (
                    <Block key={idx} style={{backgroundColor: Colors.white, marginVertical: 5, padding: 5}}>
                        <Block>
                            <GalioText numberOfLines={5} h5
                                       color={Colors.textColor}>{`${idx + 1}. ${question.question} ?`}</GalioText>
                        </Block>
                        <Block flex style={{marginTop: 15}}>
                            <Block left flex row style={{flexWrap: 'wrap'}}>
                                <SelectMultipleGroupButton
                                    multiple={true}
                                    containerViewStyle={{
                                        flex: 1,
                                        justifyContent: 'space-around',
                                        alignItems: 'center'
                                    }}
                                    highLightStyle={{
                                        borderColor: 'gray',
                                        backgroundColor: 'transparent',
                                        textColor: '#333',
                                        borderTintColor: "#ff9e19",
                                        backgroundTintColor: 'transparent',
                                        textTintColor: "#ff9e19",
                                    }}
                                    defaultSelectedIndexes={SelectedIndexesMultiple}
                                    onSelectedValuesChange={valueTap => {
                                        this.selected(valueTap, question.rang, question.choices)
                                    }}
                                    group={choices}/>
                            </Block>
                        </Block>
                    </Block>
                )
                break;
            case 'Évaluation par étoiles':
                let defaultStars = parseInt(reponse[question.rang]) || 0
                return (
                    <Block key={idx} style={{backgroundColor: Colors.white, marginVertical: 5, padding: 5}}>
                        <Block>
                            <GalioText numberOfLines={5} h5
                                       color={Colors.textColor}>{`${idx + 1}. ${question.question} ?`}</GalioText>
                        </Block>
                        <Block flex style={{marginTop: 15}}>
                            <Stars
                                update={(val) => {
                                    this.evaluationetoile(val, question.rang)
                                }}
                                default={defaultStars}
                                count={5}
                                half={false}
                                starSize={1000}
                                fullStar={<MaterialCommunityIcons size={35} name={'star'}
                                                                  style={[styles.myStarStyle]}/>}
                                emptyStar={<MaterialCommunityIcons size={35} name={'star-outline'}
                                                                   style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}

                            />
                        </Block>
                    </Block>
                )
                break;
            default:
                return null
        }
    }

    takePicture = (idx, rang) => {
        let photo_user = [...this.state.photo_user];
        let pictosend = [...this.state.pictosend];
        const options = {fixOrientation: true};
        this.camera.capture(options)
            .then((data) => {
                photo_user[idx].picture = data
                pictosend[rang] = data.path;
                this.setState({pictosend, photo_user, cameramodal: false,});
            })
            .catch(err => {

                this.setState({
                    cameramodal: false,
                });
            });
    }
    /**
     *
     * @param photo
     * @param idx
     * @returns {*}
     */
    renderPhoto = (photo, idx) => {
        let {photo_user} = this.state
        const isImageExist = photo.imgexiste === 1
        let picture = photo_user[idx].picture || null

        return (
            <Block key={idx} style={{backgroundColor: Colors.white, marginVertical: 5, paddingVertical: 5}}>
                <Block>
                    <GalioText
                        style={{
                            paddingHorizontal: 5,
                        }}
                        numberOfLines={5} h5
                        color={Colors.textColor}>
                        {`${photo.describe}`}
                    </GalioText>
                </Block>
                {isImageExist && !picture &&
                <Block style={{marginTop: 0, backgroundColor: Colors.lightGray}}>
                    <Block center>
                        <Image
                            PlaceholderContent={<ActivityIndicator color={Colors.orange}  />}
                            style={styles.defaultImage}
                            source={{uri: `${UrhImages + photo.img}`}}
                        />
                    </Block>
                </Block>}
                <Block>
                    <Block center middle style={{marginVertical: 10}}>
                        {!picture ?

                            <TouchableOpacity onPress={() => {
                                this.setState({cameramodal: true, idx: idx, picrang: photo.rang})

                            }}>

                                <MaterialIcons name={'camera-alt'} color={Colors.orange2} size={28}/>
                            </TouchableOpacity>
                            :
                            <Block>
                                <Block style={{
                                    backgroundColor: Colors.lightGray
                                }}>
                                    <Block center>
                                        <Image
                                            PlaceholderContent={<ActivityIndicator color={Colors.orange}  />}
                                            style={styles.defaultImage}
                                            source={{uri: `${picture.path}`}}
                                        />
                                    </Block>
                                </Block>

                                <Block center style={{
                                    marginTop: 10
                                }}>
                                    <TouchableOpacity onPress={() => {
                                        this.setState({cameramodal: true, idx: idx, picrang: photo.rang})

                                    }}>
                                        <MaterialIcons name={'camera-alt'} color={Colors.orange2} size={28}/>
                                    </TouchableOpacity>
                                </Block>
                            </Block>

                        }
                    </Block>
                </Block>
            </Block>
        )
    }

    goToList() {
        const {navigation} = this.props;
        this.setState({successMission: false}, () => {
            navigation.navigate("Liste")
        })
    }

    sendtoserver = (mission, user) => {
        const {navigation} = this.props;

        let reponse = [...this.state.reponse]
        let photo = [...this.state.pictosend]
        this.setState({isLoading: true}, () => {
            let qsu = mission.alldata.questions_user
            for (var wx = 0; wx < qsu.length; wx++) {
                if (qsu[wx].type == "Évaluation par étoiles") {
                    reponse[wx] = [qsu[wx].choices[(reponse[wx] - 1)]]
                }
            }
            var datetoday = new Date()
            var postData = new FormData();
            postData.append("envoiedate", JSON.stringify(datetoday))
            postData.append("id_mission", mission.alldata._id)
            postData.append('Reponses', JSON.stringify(reponse))
            postData.append('Reponsesmetadonne', JSON.stringify([]))
            postData.append("piclength", JSON.stringify(photo))

            for (var i = 0; i < photo.length; i++) {
                if (photo[i])
                    postData.append('my_photo' + i, {
                        uri: photo[i],
                        name: 'my_photo' + i + '.jpg',
                        type: 'image/jpg'
                    })
            }

            let configpic = {
                headers: {
                    'contentType': "application/json",

                    'Content-Type': 'multipart/form-data'
                },
                onUploadProgress: (progressEvent) => {
                    const { loaded, total } = progressEvent;
                    this.setState({progress : Math.round((loaded * 100) / total)});
                },
            };
            axios.post(Urh + 'Reponsemission', postData, configpic)
                .then((res) => {
                        if (res.data.success) {
                            var postData2 = {id_user: user._id}
                            let configpic2 = {
                                headers: {
                                    'contentType': "application/json",

                                }
                            };
                            axios.post(Urh + 'instancemission', postData2, configpic2)
                                .then((res2) => {
                                    if (res2.data.success) {
                                        let {fetchData, updateMyState} = this.props.screenProps
                                        fetchData(user.categorie).then(() => {
                                            updateMyState({withReservedMission: false}).then(() => {
                                                this.setState({
                                                    isLoading: false,
                                                    successMission: true,
                                                })
                                            })

                                        })

                                    } else {
                                        this.setState({isLoading: false})
                                        Alert.alert(
                                            'Connexion est interrompue ',
                                            'vérifiez votre connexion et réessayez',
                                            [

                                                {
                                                    text: 'Okay', onPress: () => {
                                                    }
                                                },
                                            ],
                                            {cancelable: true}
                                        )
                                    }

                                })
                                .catch((err) => {

                                    this.setState({isLoading: false})
                                    Alert.alert(
                                        'Connexion est interrompue ',
                                        'vérifiez votre connexion et réessayez',
                                        [

                                            {
                                                text: 'Ok', onPress: () => {
                                                }
                                            },
                                        ],
                                        {cancelable: true}
                                    )
                                })


                        } else {
                            this.setState({isLoading: false})
                            Alert.alert(
                                'Connexion est interrompue ',
                                'vérifiez votre connexion et réessayez',
                                [

                                    {
                                        text: 'Okay', onPress: () => {

                                        }
                                    },
                                ],
                                {cancelable: true}
                            )
                        }
                    }
                )
                .catch((err) => {

                    this.setState({isLoading: false})
                    Alert.alert(
                        'WinShooter!',
                        'Connexion error..verify your connection and try againn4',
                        [

                            {
                                text: 'Okay', onPress: () => {
                                }
                            },
                        ],
                        {cancelable: true}
                    )
                })

        })
        //////////////////////////////////////////////

    }

    goNext = (mission, user, nextEtape, etapes) => {
        const {navigation} = this.props;
        let reponse = [...this.state.reponse]
        let pictosend = [...this.state.pictosend]

        navigation.push("Etaps", {
            etapes: etapes,
            etape: nextEtape,
            missionid: mission,
            reponse: reponse,
            pictosend: pictosend,
            user: user,
            updateState: this.updateState,
        })
    }


    goBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    }

    validate = () => {
        let valide = true
        let {photo_user, questions_user, reponse} = this.state


        photo_user.map(photo => {
            if (!photo.picture) {
                valide = false
                return valide
            }
        });
        questions_user.map((question, index) => {
            if (!reponse[question.rang] || reponse[question.rang] == "") {
                valide = false

                return valide
            }
        });
        return valide

    }


    terminer = (mission) => {
        let valide = true

        let {
            photo_user,
            questions_user,
            reponse,
            pictosend
        } = this.state


        if (pictosend.length < mission.alldata.photo_user.length) {
            valide = false

            return valide
        }

        if (reponse.length < mission.alldata.questions_user.length) {
            valide = false


            return valide
        }

        pictosend.map(photo => {
            if (!photo) {


                valide = false
                return valide
            }
        });
        reponse.map((question, index) => {
            if (!question) {

                valide = false
                return valide
            }
        });
        return valide
    }


    render() {
        const {navigation} = this.props;
        let mission = navigation.getParam('missionid', null);
        let user = navigation.getParam('user', null);
        let etape = navigation.getParam('etape', 0);
        let etapes = navigation.getParam('etapes', 0);
        let {isLoading, successMission, isModalInfoVisible,
            cameramodal} = this.state


        // questions and photos to render in this step
        const {questions_user, photo_user, isNextDisabled} = this.state
        return (
            <Block flex style={{backgroundColor: Colors.white}}>


                <Modal
                    style={{
                        flex: 1,
                        margin: 0
                    }}
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={cameramodal}>


                    <View style={styles.container}>
                        <Camera
                            ref={(cam) => {
                                this.camera = cam;
                            }}
                            style={styles.preview}
                            aspect={Camera.constants.Aspect.fill}
                            captureQuality={"1080p"}

                        >
                            <Text style={styles.capture}
                                  onPress={() => this.takePicture(this.state.idx, this.state.picrang)}>
                                <MaterialIcons name={'camera-alt'} color={Colors.orange2} size={35}/>
                            </Text>
                        </Camera>
                    </View>

                    <TouchableOpacity onPress={() => this.setState({cameramodal: false})}>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: "rgba(169,5,0,0.82)",
                            paddingVertical: 10,
                            paddingHorizontal: 10,
                        }}>

                            <AntDesign name={"closecircleo"} size={24} color={Colors.white}/>
                            <GalioText style={{
                                fontFamily: fontFamily["Poppins-Regular"],
                                fontSize: 18,
                                color: Colors.white,
                                paddingHorizontal: 15,
                            }}
                                       uppercase={false}>Annuler</GalioText>
                        </Block>
                    </TouchableOpacity>


                </Modal>


                {/*Modal info !!!*/}

                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={isModalInfoVisible}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 10,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Répondez soigneusement au questionnaire, augmentez vos gains et vos points
                                d'expériences, et transformez vos points en cadeaux.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.handleModalInfo()}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>Compris</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>
                {/*Modal loding*/}
                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={isLoading}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        padding: 15,
                        borderRadius: 10,
                    }}>
                        <Block center>

                            <ImageLoad
                                style={{
                                    height: 100,
                                    width: 100,
                                    resizeMode : 'contain'
                                }}
                                loadingStyle={{ size: 'small', color: Colors.orange }}
                                source={{ uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/WINSHOT_PNG.png' }}
                            />
                        </Block>
                        <Block>
                            <ProgressBar progress={(this.state.progress / 100)} color={Colors.orange} />
                        </Block>
                        <Block center>
                            <GalioText h5 color={Colors.blue}> {`${this.state.progress}%`} </GalioText>
                        </Block>
                    </Block>
                </Modal>
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={successMission}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>

                        <Block style={{
                            marginTop: 30,
                            paddingBottom: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <GalioText h4 color={Colors.textColor}>Mission terminée</GalioText>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Bravo! vous venez de terminer votre mission. Nous allons à présent vérifier vos
                                réponses.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={this.goToList}
                                    style={{
                                        marginHorizontal: 7,
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.success
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>
                {this.renderHeader(mission.nomcamp, user, etape + 1, etapes)}
                <ScrollView showsVerticalScrollIndicator={true}
                            style={{
                                backgroundColor: Colors.backgroundColor2,
                                paddingVertical: 5,
                            }}>

                    {photo_user.length >= 1 && photo_user.map((photo, idx) => {
                        return this.renderPhoto(photo, idx)
                    })}

                    <Block>
                        {questions_user.length >= 1 && questions_user.map((question, idx) => {
                            return <Block key={idx} style={{backgroundColor: Colors.white, marginVertical: 5}}>
                                {this.renderQuestion(question, idx)}
                            </Block>
                        })}
                    </Block>
                    <Block row style={{
                        marginTop: 5,
                        marginBottom: 15,
                        justifyContent: "space-between",
                        marginHorizontal: 10,

                    }}>
                        {etape !== 0 ? <Block middle>
                            <TouchableOpacity
                                onPress={this.goBack}>
                                <AntDesign
                                    name="arrowleft" size={28}
                                    color={Colors.blue}/>
                            </TouchableOpacity>
                        </Block> : <Block></Block>}
                        {
                            etapes !== etape + 1 ?
                                <Block>
                                    <Button
                                        disabled={!this.validate()}
                                        style={{
                                            paddingHorizontal: 20,
                                            elevation: 0,
                                            borderRadius: 0,
                                            backgroundColor: !this.validate() ? Colors.gray : Colors.orange
                                        }}
                                        onPress={() => this.goNext(mission, user, etape + 1, etapes)}
                                    >
                                        <GalioText size={18} color={Colors.white}>SUIVANT</GalioText>
                                        <AntDesign
                                            style={{marginLeft: 5}}
                                            name="arrowright" size={24}
                                            color={Colors.white}/>
                                    </Button>
                                </Block> : <Block>
                                    <Button
                                        disabled={!this.terminer(mission)}
                                        icon
                                        style={{

                                            paddingHorizontal: 14,
                                            backgroundColor: !this.terminer(mission) ? Colors.gray : Colors.orange,
                                            elevation: 0,
                                            borderRadius: 0,
                                        }}

                                        onPress={() => {
                                            this.sendtoserver(mission, user)
                                        }}
                                    >

                                        <GalioText color={Colors.white} size={18}>TERMINER</GalioText>

                                        <AntDesign
                                            name="check" size={24}
                                            style={{
                                                marginLeft: 4
                                            }}
                                            color={Colors.white}/>
                                    </Button>
                                </Block>

                        }
                    </Block>
                </ScrollView>
            </Block>
        )
    }
}

const styles = StyleSheet.create({
    defaultImage: {
        borderRadius: 5,
        width: SIZES.width,
        height: SIZES.width,
        resizeMode: "contain",
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40
    },
    myStarStyle: {
        color: '#ff9e19',
        backgroundColor: 'transparent',
        textShadowColor: 'black',
        textShadowOffset: {width: .5, height: .5},
        textShadowRadius: 1,
    },
    myEmptyStarStyle: {
        color: 'white',
    },
});
