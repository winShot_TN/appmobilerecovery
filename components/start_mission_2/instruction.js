import React, {Component} from 'react';
import {Dimensions, Image, ScrollView, TouchableOpacity, View} from 'react-native';
import {Button,} from "native-base";
import {Header} from 'react-navigation'
import HTML from 'react-native-render-html';
import Colors from "../utils/Colors"
import {fontFamily, SIZES} from "../utils/Const"
import {Block, Text as GalioText} from 'galio-framework'
import AntDesign from "react-native-vector-icons/AntDesign";
import ImageOverlay from "react-native-image-overlay";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Feather from "react-native-vector-icons/Feather";
import Modal from "react-native-modal";
import openMap from "react-native-open-maps";
import getDirections from "react-native-google-maps-directions";

var geolib = require('geolib');
var dataArray
const headerHeight = Header.HEIGHT
export default class HomeScreen extends Component {
    state = {
        isModalInfoVisible: false,
        isTooFar: false,
    };

    constructor(props) {
        super(props);
        this.openGoogleMap = this.openGoogleMap.bind(this)
    }

    componentDidMount() {

    }

    openGoogleMap(selectedMission) {

        let {currentPosition} = this.props.screenProps;

        getDirections({
            source: {
                latitude: currentPosition.coords.latitude,
                longitude: currentPosition.coords.longitude
            },
            destination: {
                latitude: selectedMission.alldata.coordinates.lat,
                longitude:  selectedMission.alldata.coordinates.long
            },
            params: [
                {
                    key: "travelmode",
                    value: "walking"        // may be "walking", "bicycling" or "transit" as well
                }
            ]
        })
    }

    goToQuestions = (mission, user) => {
        // TODO test if isTooFar this.setState({isTooFar : true})
        let {
            currentPosition
        } = this.props.screenProps;

        this.props.navigation.navigate('Etaps', {
            etapes: mission.alldata.etapes.length,
            etape: 0,
            reponse: [],
            missionid: mission,
            user: user,

        });
    };


    handleModalInfo = () => {
        let {isModalInfoVisible} = this.state
        this.setState({isModalInfoVisible: !isModalInfoVisible})
    }
    renderHeader = (user) => {
        let {navigation} = this.props
        return (
            <Block space="between" row style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 8,
            }}>
                <Block
                    center>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <AntDesign name='arrowleft' size={24} color={Colors.white}/>
                    </TouchableOpacity>
                </Block>
                <Block center style={{marginLeft: 20}}>
                    <GalioText color={Colors.white} style={{fontSize: 18}}>Introductions de la check-list</GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    <AntDesign onPress={() => this.handleModalInfo()} size={28} name={"questioncircle"}
                               color={Colors.white}/>
                </Block>
            </Block>
        )
    }

    renderFacade = (photo, nom_magasin, distance,selectedMission) => {
        let {
            currentPosition
        } = this.props.screenProps;



        let dist = geolib.getDistance(
            {
                latitude: selectedMission.alldata.coordinates.lat,
                longitude: selectedMission.alldata.coordinates.long
            },
            {
                latitude: currentPosition.coords.latitude,
                longitude: currentPosition.coords.longitude
            }
        )
        distance =dist

        return (
            <View>
                {photo.length >= 1 ?
                    <ImageOverlay
                        overlayAlpha={0.15}
                        source={{uri: `https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/${photo}`}}
                        height={240}
                        style={{
                            resizeMode: "cover"
                        }}
                        contentPosition="center">
                        <Block center style={{marginTop: 5}}>
                            <GalioText h4 color={Colors.white}>{nom_magasin}</GalioText>
                            <Block row>
                                <Block middle>
                                    <Feather name='map-pin' size={25} color={Colors.blue}/>
                                </Block>
                                <Block bottom style={{marginLeft: 7, marginTop: 5}}>
                                    <GalioText
                                        h4
                                        color={Colors.blue}>{`${distance / 1000}Km`}</GalioText>
                                </Block>
                            </Block>
                        </Block>
                    </ImageOverlay>
                    :
                    <ImageOverlay
                        overlayAlpha={0.15}

                        source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/Fa%C3%A7ade+boutique+Orange+2.jpg"}}
                        height={170}
                        style={{
                            resizeMode: "cover"
                        }}
                        contentPosition="center">
                        <Block center style={{marginTop: 5}}>
                            <GalioText h4 color={Colors.white}>{nom_magasin}</GalioText>
                            <Block row>
                                <Block middle>
                                    <Feather name='map-pin' size={25} color={Colors.blue}/>
                                </Block>
                                <Block bottom style={{marginLeft: 7, marginTop: 5}}>
                                    <GalioText
                                        h4
                                        color={Colors.blue}>{`${distance / 1000}Km`}</GalioText>
                                </Block>
                            </Block>
                        </Block>
                    </ImageOverlay>
                }
            </View>
        )
    };

    render() {
        const {navigation} = this.props;
        let {isModalInfoVisible, isTooFar} = this.state
        let {
            user,
            selectedMission
        } = this.props.screenProps;
        let color;

        if (selectedMission.alldata.priorite === "haute")
            color = Colors.red;
        else if (selectedMission.alldata.priorite === "normale")
            color = Colors.success;
        else
            color = Colors.orange;
        return (
            <Block flex style={{backgroundColor: Colors.white}}>
                {this.renderHeader(user)}
                {/*Modal info !!!*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={isModalInfoVisible}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Lisez attentivement les instructions de la mission et augmentez vos chances de gagner
                                plus d'argent et de points d'expérience.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.handleModalInfo()}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>Compris</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>
                {/*Vous devez être devant le magasin pour pouvoir valider le questionnaire*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={isTooFar}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                            </Block>

                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    style={{
                                        marginHorizontal: 7,
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 10,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>
                    </Block>
                </Modal>


                <ScrollView style={{backgroundColor: Colors.backgroundColor2}}>

                    {this.renderFacade(selectedMission.alldata.photo, selectedMission.alldata.nom_magasin, selectedMission.distance,selectedMission)}

                    <Block row space="between" style={{
                        marginTop: 10,
                        marginBottom: 5,
                        backgroundColor: Colors.white,
                        padding: 5,
                    }}>
                        <Block>
                            <GalioText size={16} color={Colors.orange}>Adresse</GalioText>
                            <GalioText style={{marginLeft: 3}} size={17}
                                       color={Colors.textColor}>{selectedMission.city}</GalioText>
                        </Block>
                        <Block center style={{marginRight: 5}}>
                            <Block center middle style={{
                                borderWidth: 1,
                                backgroundColor: "#4285F4",
                                borderColor: Colors.white,
                                borderRadius: 25,
                                height: 40,
                                width: 40
                            }}>
                                <TouchableOpacity onPress={() => this.openGoogleMap(selectedMission)}>
                                <MaterialCommunityIcons  name='directions' size={32} color={Colors.white}/>
                                </TouchableOpacity>
                            </Block>
                        </Block>
                    </Block>
                    <Block center style={{
                        paddingVertical: 5,
                        marginTop: 5,
                        backgroundColor: Colors.white,
                        marginBottom: 60
                    }}>
                        <GalioText style={{marginBottom: 10}} size={16}
                                   color={Colors.orange}>{selectedMission.nomcamp}</GalioText>
                        <GalioText
                            center
                            style={{paddingHorizontal: 5, lineHeight: 20}}
                            size={13}
                            color={Colors.textColor}>

                        </GalioText>
                        <Block middle center
                               style={{paddingHorizontal: 5, lineHeight: 20, width: Dimensions.get('window').width}}>

                            <HTML html={selectedMission.alldata.description}
                                  imagesMaxWidth={Dimensions.get('window').width}/>
                        </Block>
                        {/*<Block middle style={{marginVertical: 10}}>*/}
                        {/*    <GalioText center h4 color={Colors.danger}>*/}
                        {/*        ATTENTION :*/}
                        {/*    </GalioText>*/}
                        {/*    <GalioText center size={13} color={Colors.danger}>*/}
                        {/*        La qualité des photos est très importante.*/}
                        {/*    </GalioText>*/}
                        {/*</Block>*/}
                    </Block>
                </ScrollView>
                <View style={{
                    position: 'absolute',
                    margin: 0,
                    right: (SIZES.width / 2) - 115,
                    bottom: 10,
                    backgroundColor: Colors.white,
                    justifyContent: 'center',
                    borderRadius: 25,
                    flexDirection: 'row',
                }}>
                    <Button iconLeft

                            onPress={() => this.goToQuestions(selectedMission, user)} block
                            style={{backgroundColor: Colors.blue, elevation: 0, paddingHorizontal: 10}} rounded>
                        <AntDesign style={{marginBottom: 3, marginRight: 10}} name="caretright" size={22}
                                   color={Colors.white}/>
                        <GalioText size={18} color={Colors.white} style={{marginLeft: 7}}>Voir le
                            questionnaire</GalioText>
                    </Button>
                </View>

            </Block>
        )
    }

}

