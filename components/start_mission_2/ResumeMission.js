import React, {Component} from 'react'
import {Image, InteractionManager, ScrollView, View, TouchableOpacity} from 'react-native'
import {Block, Text as GalioText} from "galio-framework";
import Colors from "../utils/Colors";
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Feather from 'react-native-vector-icons/Feather'
import Octicons from 'react-native-vector-icons/Octicons'
import ImageOverlay from "react-native-image-overlay";
import {Button, Text as NativeText} from 'native-base'
import {fontFamily, SIZES, Urh} from "../utils/Const";
import axios from "axios";
import Modal from "react-native-modal";
import Entypo from "react-native-vector-icons/Entypo";
import * as Animatable from 'react-native-animatable';
import SocketIOClient from "socket.io-client";
import openMap from 'react-native-open-maps'
import getDirections from 'react-native-google-maps-directions'

var geolib = require('geolib');

const AnimatableIcon = Animatable.createAnimatableComponent(Entypo);
const Animations = {
    rotate: {
        0: {
            rotate: '0deg',
        },
        1: {
            rotate: '360deg',
        },
    }
};

export default class HomeScreen extends Component {

    mission

    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            isnewmission: false,
            isTooFar: false,
            didFinishInitialAnimation: false,
            isModalVisible: false,
        }
        this.openGoogleMap = this.openGoogleMap.bind(this)
    }

    openGoogleMap(selectedMission) {

        let {currentPosition} = this.props.screenProps;

        getDirections({
            source: {
                latitude: currentPosition.coords.latitude,
                longitude: currentPosition.coords.longitude
            },
            destination: {
                latitude: selectedMission.alldata.coordinates.lat,
                longitude:  selectedMission.alldata.coordinates.long
            },
            params: [
                {
                    key: "travelmode",
                    value: "walking"        // may be "walking", "bicycling" or "transit" as well
                }
            ]
        })
    }


    componentDidMount() {
        // 1: Component is mounted off-screen
        InteractionManager.runAfterInteractions(() => {
            let {selectedMission, updateMyState, user, fetchData} = this.props.screenProps;
            const {navigation} = this.props;


            updateMyState({withReservedMission: false}).then(() => {
                fetchData(user.categorie)
            })

            if (selectedMission.alldata.realcartographie == 1 && selectedMission.alldata.status != 2) {
                var today = new Date();
                let configpic = {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                };
                let data = {
                    missionid: selectedMission.alldata._id,
                    iduser: user._id,
                    prenom: user.prenom,
                    nom: user.nom,
                    reserve_date: today
                }
                axios.put(Urh + 'reserver', data, configpic)
                    .then((res) => {

                        if (res.data.success) {

                            axios.put(Urh + 'reserveamission', {id_campaigne: selectedMission.alldata.id_campaigne}, configpic)
                                .then((res) => {

                                    if (res.data.success) {

                                        updateMyState({withReservedMission: true}).then(() => {
                                            fetchData(user.categorie)
                                        }).catch(reason => {
                                        })

                                    }
                                })
                                .catch((err) => {

                                })

                        } else {


                            navigation.navigate("Liste")
                        }
                    })
                    .catch((err) => {

                    })


            }


            if (!selectedMission) {
                //return to list
                navigation.navigate("Liste")
            }
            if (selectedMission.alldata.realcartographie == 0) {
                this.socket = SocketIOClient(Urh)

                this.socket.on('Refreshlistmissions', (data) => {
                    const {reserve} = data
                    if (reserve === 0) {
                        // abondonnée
                        const {id_mission} = data
                    } else if (reserve === 1) {
                        // reserve
                        const {id_mission, id_user} = data

                        if (selectedMission.alldata._id === id_mission && id_user !== user._id) {
                            updateMyState({
                                withReservedMission: false,
                            });
                            navigation.navigate("Liste");
                        }
                        ;
                    }
                })

            }

            this.setState({
                didFinishInitialAnimation: true,
            });
        });
    }

    handleInfoModal = () => {
        let {isModalVisible} = this.state;
        this.setState({isModalVisible: !isModalVisible});
    };

    poursuivremission = (mission) => {
        const {navigation} = this.props;
        let {user} = this.props.screenProps
        navigation.navigate('Instruction', {
            missionid: mission,
            user: user
        })

    }

    continuermission2 = (mission) => {
        const {navigation} = this.props;
        let {isTooFar, isLoading} = this.state
        if (isTooFar)
            this.setState({isTooFar: false})
        if (!isLoading)
            this.setState({isLoading: true})
        let {user, currentPosition, updateMyState, fetchData} = this.props.screenProps;
        var today = new Date();
        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data = {
            missionid: mission.alldata._id,
            iduser: user._id,
            prenom: user.prenom,
            nom: user.nom,
            reserve_date: today
        }
        axios.put(Urh + 'reserver', data, configpic)
            .then((res) => {
                if (res.data.success) {
                    axios.put(Urh + 'reserveamission', {id_campaigne: mission.alldata.id_campaigne}, configpic)
                        .then((res) => {

                            if (res.data.success) {
                                updateMyState({withReservedMission: true}).then(() => {
                                    fetchData(user.categorie).then(() => {
                                        this.setState({isLoading: false}, () => {
                                            navigation.navigate('Instruction')
                                        })
                                    })
                                }).catch(reason => {
                                })
                            }
                        })
                        .catch((err) => {

                        })
                } else {


                    navigation.navigate("Liste")
                }
            })
            .catch((err) => {

            })
    }
    continuermission = (mission) => {
        const {navigation} = this.props;
        let {currentPosition} = this.props.screenProps;

        this.setState({isLoading: true}, () => {
            let dist = geolib.getDistance(
                {
                    latitude: mission.alldata.coordinates.lat,
                    longitude: mission.alldata.coordinates.long
                },
                {
                    latitude: currentPosition.coords.latitude,
                    longitude: currentPosition.coords.longitude
                }
            )
            dist > 150 ? this.setState({isLoading: false, isTooFar: true,}) : this.continuermission2(mission)
        });

    }
    renderHeader = () => {
        const {navigation} = this.props;
        let {user} = this.props.screenProps

        return (
            <Block space="between" row style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 18,
            }}>
                <Block center>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <AntDesign name='arrowleft' size={24} color={Colors.white}/>
                    </TouchableOpacity>
                </Block>
                <Block center style={{marginLeft: 20}}>
                    <GalioText color={Colors.white} style={{fontSize: 18}}>
                        Résumé de la check-list
                    </GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    <AntDesign onPress={() => this.handleInfoModal()} size={28} name={"questioncircle"}
                               color={Colors.white}/>
                </Block>
            </Block>
        )
    }

    renderFacade = (photo, nom_magasin, distance, selectedMission) => {


        let {
            currentPosition
        } = this.props.screenProps;


        let dist = geolib.getDistance(
            {
                latitude: selectedMission.alldata.coordinates.lat,
                longitude: selectedMission.alldata.coordinates.long
            },
            {
                latitude: currentPosition.coords.latitude,
                longitude: currentPosition.coords.longitude
            }
        )

        distance = dist

        return (
            <Animatable.View animation={"fadeInDown"} useNativeDriver>

                {photo && photo.length >= 1 ?
                    <ImageOverlay
                        overlayAlpha={0.15}
                        source={{uri: `https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/${photo}`}}
                        height={240}
                        style={{
                            resizeMode: "contain"
                        }}
                        contentPosition="center">
                        <Block center style={{marginTop: 5}}>
                            <GalioText h4 color={Colors.white}>{nom_magasin}</GalioText>
                            <Block row>
                                <Block middle>
                                    <Feather name='map-pin' size={25} color={Colors.blue}/>
                                </Block>
                                <Block bottom style={{marginLeft: 7, marginTop: 5}}>
                                    <GalioText
                                        h4
                                        color={Colors.blue}>{`${distance / 1000}Km`}</GalioText>
                                </Block>
                            </Block>
                        </Block>
                    </ImageOverlay>
                    :
                    <ImageOverlay
                        overlayAlpha={0.15}
                        source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/Fa%C3%A7ade+boutique+Orange+2.jpg"}}
                        height={240}
                        style={{
                            resizeMode: "cover"
                        }}
                        contentPosition="center">
                        <Block center style={{marginTop: 5}}>
                            <GalioText h4 color={Colors.white}>{nom_magasin}</GalioText>
                            <Block row>
                                <Block middle>
                                    <Feather name='map-pin' size={25} color={Colors.blue}/>
                                </Block>
                                <Block bottom style={{marginLeft: 7, marginTop: 5}}>
                                    <GalioText
                                        h4
                                        color={Colors.blue}>{`${distance / 1000}Km`}</GalioText>
                                </Block>
                            </Block>
                        </Block>
                    </ImageOverlay>
                }
            </Animatable.View>
        )
    };

    render() {
        const {navigation} = this.props;
        let {isLoading, isTooFar, didFinishInitialAnimation} = this.state;

        let {
            withReservedMission,
            selectedMission
        } = this.props.screenProps;
        let numberPhoto = selectedMission.alldata.photo_user.length - 2.
        if (numberPhoto <= 0)
            numberPhoto = 1
        let numberQuestion = selectedMission.alldata.questions_user.length - 2
        if (numberQuestion <= 0)
            numberQuestion = 1
        let photos = numberPhoto + "-" + (selectedMission.alldata.photo_user.length + 2) + " Photos";
        let questions = numberQuestion + "-" + (selectedMission.alldata.questions_user.length + 2) + " Questions";
        let color;

        if (selectedMission.alldata.priorite === "haute")
            color = Colors.red;
        else if (selectedMission.alldata.priorite === "normale")
            color = Colors.success;
        else
            color = Colors.orange;
        if (!didFinishInitialAnimation) {
            return (
                <Block flex style={{backgroundColor: Colors.white}}>
                    <Block>
                        {this.renderHeader()}
                    </Block>
                    <Block flex style={{
                        backgroundColor: Colors.white,
                    }}>
                    </Block>
                </Block>
            )
        }

        return (
            <Block flex style={{backgroundColor: Colors.white}}>
                {/*get position */}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={isLoading}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        padding: 15,
                        borderRadius: 10,
                    }}>
                        <Block center style={{justifyContent: 'center'}}>
                            <Block center middle style={{
                                height: 60,
                                width: 60,
                                borderRadius: 30,
                                backgroundColor: Colors.lightGray,
                                position: 'absolute',
                                bottom: 58,
                                borderTopWidth: 1,
                                borderTopColor: Colors.orange,
                            }}>
                                <AnimatableIcon
                                    useNativeDriver
                                    animation="rotate" iterationCount="infinite" duration={2000}>
                                    <Entypo name={"hour-glass"} size={38} color={Colors.blue}/>
                                </AnimatableIcon>

                            </Block>
                            <GalioText style={{
                                marginTop: 20,
                                marginBottom: 10,
                            }} color={Colors.textColor} center>Patientez pendant que nous récupérons votre
                                position</GalioText>
                        </Block>
                    </Block>
                </Modal>
                {/*Trop loin du magasin*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={isTooFar}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <AntDesign name={"warning"} size={50} color={Colors.warning}/>

                            </Block>
                            <Block center>
                                <GalioText h5 color={Colors.textColor}>Attention !</GalioText>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Trop loin du point de vente, pour pouvoir réserver la mission.
                            </GalioText>
                            <GalioText style={{marginVertical: 5}} center color={Colors.textColor}>
                                Est ce que vous voulez reserver comme même la mission ?
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.setState({
                                        isTooFar: false,
                                    })}
                                    style={{
                                        marginHorizontal: 7,
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.white,
                                        borderWidth: 1,
                                        borderColor: Colors.orange2
                                    }}
                            >
                                <NativeText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.orange,
                                }}
                                            uppercase={false}>Non</NativeText>
                            </Button>
                            <Button rounded
                                    onPress={
                                        () => this.continuermission2(selectedMission)
                                    }
                                    style={{
                                        marginHorizontal: 7,
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <NativeText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,

                                }}
                                            uppercase={false}>Oui</NativeText>
                            </Button>
                        </Block>
                    </Block>
                </Modal>

                {/*mission terminer*/}
                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={false}>

                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>

                        <Block style={{
                            marginTop: 50,
                            paddingBottom: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center middle style={{
                                height: 60,
                                width: 60,
                                borderRadius: 30,
                                bottom: 140,
                                backgroundColor: Colors.white,
                                position: 'absolute',
                            }}>
                                <AntDesign name="checkcircleo" size={60} color={Colors.success}/>
                            </Block>
                            <Block center>
                                <GalioText h4 color={Colors.textColor}>Mission terminée</GalioText>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                Bravo! vous venez de terminer votre mission. Nous allons à présent vérifier vos
                                réponses.
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    style={{
                                        marginHorizontal: 7,
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.success
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 10,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>
                {this.renderHeader()}
                <ScrollView style={{backgroundColor: Colors.backgroundColor2}}>
                    {this.renderFacade(selectedMission.alldata.photo, selectedMission.alldata.nom_magasin, selectedMission.distance, selectedMission)}

                    <Block row space="between" style={{
                        marginTop: 5,
                        marginBottom: 5,
                        backgroundColor: Colors.white,
                        padding: 5,
                    }}>
                        <Block>
                            <GalioText size={15} color={Colors.orange}>Adresse</GalioText>
                            <GalioText style={{marginLeft: 3}} size={17}
                                       color={Colors.textColor}>{selectedMission.city}</GalioText>
                        </Block>
                        <Block center style={{marginRight: 5}}>
                            <Block center middle style={{
                                borderWidth: 1,
                                backgroundColor: "#4285F4",
                                borderColor: Colors.white,
                                borderRadius: 25,
                                height: 40,
                                width: 40
                            }}>
                                <TouchableOpacity onPress={() => this.openGoogleMap(selectedMission)}>
                                    <MaterialCommunityIcons

                                        name='directions' size={32} color={Colors.white}/>
                                </TouchableOpacity>
                            </Block>
                        </Block>
                    </Block>
                    <Block row space="between" style={{
                        marginVertical: 5,
                        backgroundColor: Colors.white,
                        padding: 5,
                    }}>
                        <Block>
                            <GalioText size={15} color={Colors.orange}>Nom de la Mission</GalioText>
                            <GalioText style={{marginLeft: 3}} size={17}
                                       color={Colors.textColor}>{selectedMission.nomcamp}</GalioText>
                        </Block>

                    </Block>
                    <Block style={{
                        marginVertical: 5,
                        backgroundColor: Colors.white,
                        padding: 5,
                    }}>
                        <Block>
                            <GalioText size={15} color={Colors.orange}>Détails de la mission</GalioText>
                        </Block>
                        <Block row space="between">
                            <Block center style={{marginLeft: 5}}>
                                <Block>
                                    <GalioText size={15} color={Colors.textColor}>Durées</GalioText>
                                </Block>
                                <Block>
                                    <Block center middle style={{
                                        backgroundColor: Colors.gray,
                                        width: 56,
                                        height: 56,
                                        borderRadius: 28,
                                        borderWidth: 1,
                                        borderColor: Colors.orange
                                    }}>
                                        <MaterialIcons name="access-time" size={45} color={Colors.white}/>
                                    </Block>
                                </Block>
                                <Block>
                                    <GalioText muted color={Colors.textColor} size={13}>5-7 minutes</GalioText>
                                </Block>
                            </Block>
                            <Block center>
                                <Block>
                                    <GalioText size={14} color={Colors.textColor}>Caméra</GalioText>
                                </Block>
                                <Block>
                                    <Block center middle style={{
                                        backgroundColor: Colors.gray,
                                        width: 56,
                                        height: 56,
                                        borderRadius: 28,
                                        borderWidth: 1,
                                        borderColor: Colors.orange
                                    }}>
                                        <Feather name="camera" size={40} color={Colors.white}/>
                                    </Block>
                                </Block>
                                <Block>
                                    <GalioText color={Colors.textColor} size={13}>{photos}</GalioText>
                                </Block>
                            </Block>
                            <Block center style={{marginRight: 5}}>
                                <Block>
                                    <GalioText size={14} color={Colors.textColor}>Réponses</GalioText>
                                </Block>
                                <Block>
                                    <Block center middle style={{
                                        backgroundColor: Colors.gray,
                                        width: 56,
                                        height: 56,
                                        borderRadius: 28,
                                        borderWidth: 1,
                                        borderColor: Colors.orange
                                    }}>
                                        <Octicons name="comment-discussion" size={40} color={Colors.white}/>
                                    </Block>
                                </Block>
                                <Block>
                                    <GalioText size={14} color={Colors.textColor}>{questions}</GalioText>
                                </Block>
                            </Block>
                        </Block>
                    </Block>
                    <Block style={{
                        backgroundColor: Colors.white,
                        padding: 5,
                        marginTop: 5,
                    }}>
                        <Block>
                            <GalioText size={16} color={Colors.orange}>A quel moment je dois appuyer sur Demarrer la
                                mission</GalioText>
                            <GalioText style={{marginLeft: 3}} size={17}
                                       color={Colors.textColor}>Lorsque vous arrivez devant le point de vente</GalioText>
                        </Block>
                    </Block>
                    {/*   extra  bloc en bas*/}
                    <Block style={{
                        backgroundColor: Colors.white,
                        padding: 5,
                        marginTop: 10,
                        marginBottom: 70,
                    }}>
                        <Block>
                            <GalioText size={16} color={Colors.orange}></GalioText>
                            <GalioText style={{marginLeft: 3}} size={17}
                                       color={Colors.textColor}></GalioText>
                        </Block>
                    </Block>
                </ScrollView>
                <View style={{
                    position: 'absolute',
                    margin: 0,
                    right: (SIZES.width / 2) - 110,
                    bottom: 17,
                    backgroundColor: Colors.white,
                    justifyContent: 'center',
                    borderRadius: 20,
                    flexDirection: 'row',
                }}>
                    {withReservedMission || selectedMission.alldata.realcartographie == 1 ?
                        <Button rounded full info
                                style={{
                                    backgroundColor: Colors.orange,
                                    borderRadius: 20,
                                    paddingLeft: 10,
                                    elevation: 0,
                                    borderRightColor: Colors.white,
                                }}
                                onPress={() => {
                                    this.setState({isnewmission: false})
                                    this.poursuivremission(selectedMission)
                                }
                                }>
                            <NativeText style={{
                                fontFamily: fontFamily["Poppins-Regular"],
                                fontSize: 18,
                                color: Colors.white,
                            }}
                                        uppercase={false}>Poursuivre la mission</NativeText>
                        </Button> :
                        <Button rounded full info
                                style={{
                                    backgroundColor: Colors.orange,
                                    borderRadius: 20,
                                    paddingLeft: 10,
                                    elevation: 0,
                                    borderRightColor: Colors.white,
                                }}
                                onPress={() => this.continuermission(selectedMission)}>
                            <NativeText style={{
                                fontFamily: fontFamily["Poppins-Regular"],
                                fontSize: 18,
                                color: Colors.white,
                            }}
                                        uppercase={false}>Démarrer la mission</NativeText>
                        </Button>}

                </View>
            </Block>
        )
    }
}
