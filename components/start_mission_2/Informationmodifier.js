import * as React from 'react';

import {Block, Text, Input, Text as GalioText, Radio} from 'galio-framework'
import Colors from "../utils/Colors";
import {
    Picker,
    StyleSheet,
    TouchableOpacity,
    View,
    TextInput,
    ScrollView,
    ActivityIndicator, Alert
} from "react-native";
import {Image} from 'react-native-elements'
import AntDesign from "react-native-vector-icons/AntDesign";
import {fontFamily, Logo, SIZES, Urh, UrhImages} from "../utils/Const";
import {Divider, ProgressBar} from 'react-native-paper'
import {ActionSheet, Button} from "native-base";
import axios from "axios";
import {RadioButton} from 'react-native-paper';
import Modal from "react-native-modal";
import SocketIOClient from "socket.io-client";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import Camera from 'react-native-camera';
import Ripple from "react-native-material-ripple";
import Ionicons from "react-native-vector-icons/Ionicons";
import TextInputMask from "react-native-text-input-mask";
import ImageLoad from 'react-native-image-placeholder';
const CodePostals = require('../utils/CodePostals')
var geolib = require('geolib');
const pdv = ['PDV Labellisé', 'PDV', 'PDV Franchisé', 'PDR', "Autre"]
const operateur = ['Orange', 'Ooredoo', 'Tunisie Telecom (TT)', 'Lyca mobile', "Bee", "Autre"]


export class Informationmodifier extends React.Component {
    takedpic = null
    shops
    state = {
        progress: 0,
        image: null,
        nomMagasin: '',
        adresse: '',
        codePostal: '',
        delegation: '',
        gouvernorat: '',
        gouvernorats: ["Gouvernorat"],
        delegations: ["Délégation"],
        nom: '',
        prenom: '',
        telephone: "",
        isLoading: false,
        pdv: "selectionner type PDV",
        operateur: "selectionner un operateur",
        typepdv: "",
        branche: "",
        shops: [],
        selectedShop: "Selectionner un PDV",
        idmagasin: null,
        lat: 0,
        long: 0,
        modaltoofar: false,
        imagecaptured: false,
        cameramodal: false,
        newPDV: {
            coords: {
                longitude: null,
                latitude: null
            },
            nomMagasin: {
                value: '',
                valid: false,
            },
            adresse: {
                value: '',
                valid: true,
            },
            codePostal: {
                value: "",
                valid: true,
                isEditable: true,
            },
            delegation: {
                isEnabled: false,
                value: "",
                valid: false,
            },
            gouvernorat: {
                isEnabled: true,
                value: "gouvernorat",
                valid: false,
            },
            nom: {
                value: "",
                valid: true,
            },
            prenom: {
                value: "",
                valid: true,
            },
            telephone: {
                value: "",
                valid: true,
            },
            typepdv: {
                value: "",
                valid: false,
            },
            branche: {
                value: "",
                valid: false,
            },
            image: {
                value: null,
                valid: false,
            },
            valid: false
        }
    }

    constructor(props) {
        super(props);
        this.pic = this.pic.bind(this);
        this.goNext = this.goNext.bind(this)
    }

    champsremplire = (nom, buttonIndex) => {
        let {
            shops
        } = this.state
        let item = shops[buttonIndex]
        if (!item)
            return
        let gouvernorats = ["Gouvernorat"]
        this.setState({gouvernorats})
        let delegations = ["Délégation"]
        this.setState({delegations})
        CodePostals.map(elem => {
            let isNewGov = true
            gouvernorats.map(gov => {
                if (gov === elem.gouvernorat)
                    isNewGov = false;
            })
            if (isNewGov)
                gouvernorats.push(elem.gouvernorat)
        });

        this.setState({
            image: null,
            gouvernorats: gouvernorats,
            nomMagasin: '',
            idmagasin: item._id,
            adresse: '',
            codePostal: '',
            delegations: ["Délégation"],
            delegation: '',
            gouvernorat: '',
            lat: item.coordinates.lat,
            long: item.coordinates.long,
            nom: '',
            prenom: '',
            telephone: "",
            pdv: "selectionner type PDV",
            operateur: "selectionné un operateur",
            typepdv: "",
            branche: "",
            imagecaptured: false,
            newPDV: {
                coords: {
                    longitude: null,
                    latitude: null
                },
                nomMagasin: {
                    value: '',
                    valid: false,
                },
                adresse: {
                    value: '',
                    valid: true,
                },
                codePostal: {
                    value: "",
                    valid: true,
                    isEditable: true,
                },
                delegation: {
                    isEnabled: false,
                    value: "",
                    valid: false,
                },
                gouvernorat: {
                    isEnabled: true,
                    value: "gouvernorat",
                    valid: false,
                },
                nom: {
                    value: "",
                    valid: true,
                },
                prenom: {
                    value: "",
                    valid: true,
                },
                telephone: {
                    value: "",
                    valid: true,
                },
                typepdv: {
                    value: "",
                    valid: false,
                },
                branche: {
                    value: "",
                    valid: false,
                },
                image: {
                    value: null,
                    valid: false,
                },
                valid: false
            }
        }, () => {
            this.champsremplire2(item)
        })
    }
    champsremplire2 = (item) => {

        let {newPDV} = this.state

        // set coords
        newPDV.coords.latitude = item.coordinates.lat;
        newPDV.coords.longitude = item.coordinates.long;

        // set fullname
        if (item.nom_owner) {
            let nomprenom = item.nom_owner.split(" ")
            if (nomprenom && nomprenom[0]) {
                this.setState({nom: nomprenom[0]})
                newPDV.nom.value = nomprenom[0]
            }
            if (nomprenom && nomprenom[1]) {
                newPDV.prenom.value = nomprenom[1]
                this.setState({prenom: nomprenom[1]})
            }
        }
        // set branche
        if (item.branche) {
            this.setState({operateur: item.branche, branche: item.branche})
            newPDV.branche.value = item.branche

            let valid = item.branche && item.branche.length >= 1 ? true : false
            newPDV.branche.valid = valid
        }

        if (item.categorie && item.categorie[0]) {

            let tpdv = item.categorie[0]
            if (item.categorie[0] == "PDV LABELISE")
                tpdv = "PDV Labellisé"

            else if (item.categorie[0] == 'boutique franchise')
                tpdv = "PDV Franchisé"

            this.setState({
                pdv: tpdv,
                typepdv: item.categorie[0]
            })
            // validate
            let valid = true
            if (tpdv === "" && tpdv === "selectionner type PDV")
                valid = false
            newPDV.typepdv.value = tpdv
            newPDV.typepdv.valid = valid
        }

        if (item.photo && item.photo.length > 0) {
            let img = "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/" + item.photo
            let image = {path: img}
            newPDV.image.value = image.path
            newPDV.image.valid = true
            this.setState({image: image})
        }


        if (item.ville) {
            this.setState({adresse: item.ville})
            newPDV.adresse.value = item.ville

        }

        if (item.nom) {
            this.setState({nomMagasin: item.nom})
            // validate
            let valid = true
            if (item.nom === "")
                valid = false
            newPDV.nomMagasin.value = item.nom
            newPDV.nomMagasin.valid = valid
        }

        if (item.gouvernorat) {
            let {gouvernorats} = this.state
            let valid = false;
            gouvernorats.map(g => {
                if (g === item.gouvernorat && item.gouvernorat !== "Gouvernorat") {
                    valid = true
                    return
                }
            })

            if (valid) {

                newPDV.gouvernorat.valid = true
                newPDV.gouvernorat.value = item.gouvernorat
                // delegation
                if (item.delegation) {
                    let delegations = []
                    // get delegation from CodePostals
                    CodePostals.map((elem) => {
                        let newDelegation = true
                        if (elem.gouvernorat === item.gouvernorat) {
                            delegations.map((del) => {
                                if (elem.delegation === del) {
                                    newDelegation = false
                                    return
                                }
                            })
                            if (newDelegation)
                                delegations.push(elem.delegation)
                        }
                    });

                    if (delegations.includes(item.delegation)) {
                        newPDV.delegation.valid = true
                        newPDV.delegation.value = item.delegation

                    } else {
                        newPDV.delegation.valid = false
                        let newArray = ["Délégation"].concat(delegations)
                        newPDV.delegation.value = "Délégation"

                    }
                    this.setState({delegations})

                    if (item.codepostal) {
                        newPDV.codePostal.value = item.codepostal
                        if (!newPDV.codePostal.valid)
                            newPDV.codePostal.valid = true
                    }
                }
            } else {
                newPDV.gouvernorat.valid = false
                newPDV.gouvernorat.value = "Gouvernorat"
                newPDV.delegation.valid = false
                newPDV.delegation.value = "Délégation"
                newPDV.delegation.isEnabled = false
                newPDV.codePostal.value = ""
                newPDV.codePostal.valid = false
            }
        }


        if (item.telephone) {
            this.setState({telephone: item.telephone.toString()})
            newPDV.telephone.value = item.telephone.toString()
            newPDV.telephone.valid = newPDV.telephone.value.length === 8
        }
        this.setState({newPDV: newPDV}, () => {

        })
    }

    renderShops = (item, index) => {

        return (
            <TouchableOpacity
                onPress={() => this.champsremplire(item)}>
                <Block space="between" row style={{
                    marginVertical: 5,
                    paddingVertical: 10,
                    height: 50,
                    backgroundColor: Colors.lightGray,
                    paddingHorizontal: 18,
                }}>

                    <Block center flex>
                        <GalioText color={Colors.orange} style={{fontSize: 18}}>{item.nom}</GalioText>
                    </Block>


                </Block>
            </TouchableOpacity>
        )
    }


    renderHeader = () => {
        const {navigation} = this.props;

        return (
            <Block space="between" row style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 18,
            }}>
                <Block center>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <AntDesign name='arrowleft' size={24} color={Colors.white}/>
                    </TouchableOpacity>
                </Block>
                <Block center flex>
                    <GalioText color={Colors.white} style={{fontSize: 18}}>Modifier PDV</GalioText>
                </Block>
            </Block>
        )
    };


    pic(pic) {
        const options = {fixOrientation: true};
        this.camera.capture(options)
            .then((data) => {
                let {currentPosition} = this.props.screenProps;
                let {newPDV} = this.state
                newPDV.coords = {
                    longitude: currentPosition.coords.longitude,
                    latitude: currentPosition.coords.latitude
                };

                this.setState({
                    image: data,
                    imagecaptured: true,
                    cameramodal: false,
                    newPDV: newPDV
                });
                this.takedpic = data.path


            })


            .catch(err => {

                this.setState({
                    cameramodal: false,
                });
            });

    }


    goNext() {
        this.setState({isLoading: true})
        let {user, updateMyState, currentPosition} = this.props.screenProps
        let {
            idmagasin,
            lat,
            long,
            imagecaptured
        } = this.state


        let distance = geolib.getDistance(
            {
                latitude: lat,
                longitude: long
            },
            {
                latitude: currentPosition.coords.latitude,
                longitude: currentPosition.coords.longitude
            }
        )

        if (distance < 100) {
            let {newPDV} = this.state
            let configpic = {
                headers: {
                    'contentType': "application/json",
                    'Content-Type': 'multipart/form-data'
                },
                onUploadProgress: (progressEvent) => {
                    const {loaded, total} = progressEvent;

                    this.setState({progress: Math.round((loaded * 100) / total)});
                },
            };
            let postData = new FormData()
            postData.append('id_compte', user._id)

            let {
                typepdv,
                branche,
                prenom,
                telephone,
                nom,
                adresse,
                codePostal,
                delegation,
                gouvernorat,
                nomMagasin,
                coords
            } = newPDV
            // append data
            postData.append('categorie', typepdv.value)
            postData.append('branche', branche.value)
            postData.append('prenom', prenom.value)
            postData.append('telephone', telephone.value)
            postData.append('nom', nom.value)
            postData.append('adresse', adresse.value)
            postData.append('codePostal', codePostal.value)
            postData.append('delegation', delegation.value)
            postData.append('gouvernorat', gouvernorat.value)
            postData.append('nomMagasin', nomMagasin.value)
            postData.append('idmagasin', idmagasin)

            // append coords
            postData.append('longitude', coords.longitude)
            postData.append('latitude', coords.latitude)

            if (imagecaptured == true) {
                postData.append('my_epicerie_photo', {
                    uri: this.takedpic,
                    name: 'my_epicerie_photo.jpg',
                    type: 'image/jpg'
                })
            }


            axios.post(Urh + 'Modifiermagasin', postData, configpic).then((res) => {
                if (res.data.success) {
                    this.setState({isLoading: false}, () => this.props.navigation.navigate('Liste'))
                } else {
                    this.setState({isLoading: false}, () => this.props.navigation.navigate('Liste'))
                }
            })
                .catch(err => {
                    this.setState({isLoading: false}, () => {
                        Alert.alert(
                            'Connexion interrompue',
                            'Veuillez vérifier votre connexion et réessayer.',
                            [
                                {
                                    text: 'Ok',
                                    onPress: () => null
                                },
                            ],
                            {cancelable: false},
                        );
                    })
                })

        } else {
            this.setState({modaltoofar: true, isLoading: false})
        }


    };


    validate = () => {
        let {newPDV} = this.state
        if (!newPDV.image.valid ||
            !newPDV.typepdv.valid ||
            !newPDV.branche.valid ||
            !newPDV.nom.valid ||
            !newPDV.prenom.valid ||
            !newPDV.gouvernorat.valid ||
            !newPDV.delegation.valid ||
            !newPDV.telephone.valid ||
            !newPDV.codePostal.valid ||
            !newPDV.adresse.valid ||
            !newPDV.nomMagasin.valid)
            return false
        return true
    }


    componentDidMount() {
        let {user, currentPosition} = this.props.screenProps;
        const {navigation} = this.props;
        let configpic = {
            headers: {
                'Content-Type': 'application/json',
            }
        };
        let data = {
            lat: currentPosition.coords.latitude,
            lon: currentPosition.coords.longitude,
        };
        axios.post(Urh + 'getclosetshops', data, configpic)
            .then((res) => {

                let gouvernorats = ["Gouvernorat"]
                let delegations = ["Délégation"]
                this.setState({
                    shops: res.data,
                    gouvernorats: gouvernorats,
                    delegations:delegations,
                })
                this.shops = []

                res.data.map((shop) => {
                    this.shops.push(shop.nom)
                })
            })
            .catch(e => {

            })
    }

    // change Type PDV
    handleChangeTypePdv = (buttonIndex) => {
        let tpdv = pdv[buttonIndex]

        if (pdv[buttonIndex] == "PDV Labellisé")
            tpdv = "PDV LABELISE"

        else if (pdv[buttonIndex] == "PDV Franchisé")
            tpdv = 'boutique franchise'

        // validate
        let valid = true
        if (tpdv === "" && tpdv === "selectionner type PDV")
            valid = false
        let {newPDV} = this.state

        newPDV.typepdv.value = tpdv
        newPDV.typepdv.valid = valid
        this.setState({
            pdv: pdv[buttonIndex],
            newPDV: newPDV,
        });
    }
    // change Choix opérateur
    handleChangeOperateur = (buttonIndex) => {

        let branche = operateur[buttonIndex]
        // validate branche
        let valid = true
        if (branche === "" && branche === "selectionner un operateur")
            valid = false
        //setState
        let {newPDV} = this.state
        newPDV.branche.value = branche
        newPDV.branche.valid = valid
        this.setState({
            operateur: operateur[buttonIndex],
            newPDV
        });
    }

    // change nom magasin
    handleChangeNomMagasin = (nommagasin) => {
        let {newPDV} = this.state
        newPDV.nomMagasin.value = nommagasin
        //validate
        nommagasin === "" ? newPDV.nomMagasin.valid = false : newPDV.nomMagasin.valid = true
        //setState
        this.setState({newPDV})

    }
    // change Adresse
    handleChangeAdresse = (adresse) => {
        let {newPDV} = this.state
        newPDV.adresse.value = adresse
        //validate
        if (!newPDV.adresse.valid)
            newPDV.adresse.valid = true
        //setState
        this.setState({newPDV})
    }

    // change Gouvernorat
    handleChangeGouvernorat = (gouvernorat) => {

        let {newPDV} = this.state

        newPDV.gouvernorat.value = gouvernorat
        //validate
        gouvernorat === "Gouvernorat" ? newPDV.gouvernorat.valid = false : newPDV.gouvernorat.valid = true;

        if (gouvernorat === "Gouvernorat") {
            if (newPDV.delegation.valid)
                newPDV.delegation.valid = false
            if (newPDV.delegation.isEnabled)
                newPDV.delegation.isEnabled = false
            this.setState({
                newPDV
            })
            return;
        }
        let delegations = []
        // get delegation from
        CodePostals.map((elem) => {
            let newDelegation = true
            if (elem.gouvernorat === gouvernorat) {
                delegations.map((del) => {
                    if (elem.delegation === del) {
                        newDelegation = false
                        return
                    }
                })
                if (newDelegation)
                    delegations.push(elem.delegation)
            }
        });
        if (delegations.length === 0) {
            newPDV.delegation.isEnabled = false
            newPDV.delegation.valid = false
        } else {
            newPDV.delegation.isEnabled = true
            newPDV.delegation.valid = true
        }
        //setState
        this.setState({
            newPDV,
            delegations
        }, () => {

        })
    }

    // change Delegation
    handleChangeDelegation = (delegation) => {
        let {newPDV} = this.state
        newPDV.delegation.value = delegation
        //validate
        if (!newPDV.delegation.valid)
            newPDV.delegation.valid = true
        //setState
        CodePostals.map((elem) => {
            if (elem.delegation === delegation) {
                newPDV.codePostal.value = elem.codePostal
            }
        });
        newPDV.codePostal.isEditable = false;
        this.setState({newPDV})
    }


    // change Telephone
    handleChangeTel = (telephone) => {
        let {newPDV} = this.state
        newPDV.telephone.value = telephone
        //validate
        telephone.length === 8 || telephone.length === 0 ? newPDV.telephone.valid = true : newPDV.telephone.valid = false
        //setState
        this.setState({newPDV}, () => {
            this.forceUpdate()
        })
    }

    // change Adresse
    handleChangeNom = (nom) => {
        let {newPDV} = this.state
        newPDV.nom.value = nom
        //validate
        if (!newPDV.nom.valid)
            newPDV.nom.valid = true
        //setState
        this.setState({newPDV})
    }

    // change Adresse
    handleChangePrenom = (prenom) => {
        let {newPDV} = this.state
        newPDV.prenom.value = prenom
        //validate
        if (!newPDV.prenom.valid)
            newPDV.prenom.valid = true
        //setState
        this.setState({newPDV})
    }


    render() {
        const {navigation} = this.props;
        let {
            nomMagasin,
            adresse,
            codePostal,
            delegation,
            gouvernorat,
            nom,
            prenom,
            telephone,
            image,
            isLoading,
            shops,
            selectedShop,
            modaltoofar,
            cameramodal,
            newPDV,
            delegations,
            gouvernorats
        } = this.state;

        return (
            <Block flex style={{backgroundColor: Colors.backgroundColor2}}>
                {this.renderHeader()}
                <Modal
                    style={{
                        flex: 1,
                        margin: 0
                    }}
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={cameramodal}>
                    <View style={styles.container}>
                        <Camera
                            ref={(cam) => {
                                this.camera = cam;
                            }}
                            style={styles.preview}
                            aspect={Camera.constants.Aspect.fill}>
                            <Text style={styles.capture} onPress={this.pic}>
                                <MaterialIcons name={'camera-alt'} color={Colors.orange2} size={35}/>
                            </Text>
                        </Camera>
                    </View>
                    <TouchableOpacity onPress={() => this.setState({cameramodal: false})}>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: "rgba(169,5,0,0.82)",
                            paddingVertical: 10,
                            paddingHorizontal: 10,
                        }}>

                            <AntDesign name={"closecircleo"} size={24} color={Colors.white}/>
                            <GalioText style={{
                                fontFamily: fontFamily["Poppins-Regular"],
                                fontSize: 18,
                                color: Colors.white,
                                paddingHorizontal: 15,
                            }}
                                       uppercase={false}>Annuler</GalioText>
                        </Block>
                    </TouchableOpacity>


                </Modal>
                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={isLoading}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        padding: 15,
                        borderRadius: 10,
                    }}>
                        <Block center>
                            <ImageLoad
                                style={{
                                    height: 100,
                                    width: 100,
                                    resizeMode : 'contain'
                                }}
                                loadingStyle={{ size: 'small', color: Colors.orange }}
                                source={{ uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/WINSHOT_PNG.png' }}
                            />
                        </Block>
                        <Block>
                            <ProgressBar progress={(this.state.progress / 100)} color={Colors.orange}/>
                        </Block>
                        <Block center>
                            <GalioText h5 color={Colors.blue}> {`${this.state.progress}%`} </GalioText>
                        </Block>
                    </Block>
                </Modal>

                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={modaltoofar}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                    }}>
                        <Block style={{
                            padding: 15,
                            borderRadius: 10,
                        }}>
                            <Block row style={{marginHorizontal: 10, marginBottom: 7}}>
                                <GalioText size={16} color={Colors.textColor}>
                                    Position
                                </GalioText>
                            </Block>
                            <Block style={{marginLeft: 5}}>
                                <GalioText>Vous etes très loin du point de vente</GalioText>
                            </Block>
                        </Block>
                        <Block row style={{
                            marginTop: 5,
                            backgroundColor: Colors.backgroundColor2,
                            justifyContent: 'center'
                        }}>
                            <Block style={{
                                marginVertical: 5,
                            }}>
                                <Button
                                    rounded
                                    onPress={() => this.setState({modaltoofar: !modaltoofar})}
                                    style={{
                                        backgroundColor: Colors.orange,
                                        elevation: 0,
                                        paddingHorizontal: 15,
                                    }}>
                                    <Block>
                                        <GalioText style={{marginVertical: 5, marginHorizontal: 10}}
                                                   color={Colors.white}
                                                   size={18}>OK</GalioText>
                                    </Block>
                                </Button>
                            </Block>

                        </Block>
                    </Block>
                </Modal>
                {
                    shops.length === 0 &&
                    <Block flex middle>
                        <ActivityIndicator size={"large"}/>
                    </Block>
                }
                {
                    shops.length !== 0 &&
                    <ScrollView style={{
                        flex: 1
                    }}>

                        {/* select pdv */}
                        <Block style={{
                            marginVertical: 5,
                            paddingVertical: 10,
                            backgroundColor: Colors.white
                        }}>
                            <Block row style={{
                                padding: 10,
                            }}>
                                <Block center>
                                    <Entypo name={'shop'} size={24} color={Colors.orange}/>
                                </Block>
                                <Text style={{color: '#333', fontSize: 16, marginStart: 10}}>Sélectionner PDV<GalioText
                                    color={Colors.red}>{` *`}</GalioText></Text>
                            </Block>
                            <Ripple rippleOpacity={0.1} rippleDuration={200} onPress={() => {
                                ActionSheet.show(
                                    {
                                        options: this.shops,
                                        title: "Selectionner un PDV",
                                    },
                                    buttonIndex => {
                                        this.setState({
                                            selectedShop: this.shops[buttonIndex]
                                        }, () => {
                                            if (this.shops[buttonIndex] !== "" &&
                                                this.shops[buttonIndex] !== "Selectionner un PDV")
                                                this.champsremplire(this.state.selectedShop, buttonIndex)
                                        });
                                    }
                                )
                            }}>
                                <View style={styles.SectionStyle}>
                                    <TextInput
                                        style={styles.input}
                                        editable={false}
                                        value={this.state.selectedShop}
                                    />
                                    {this.state.selectedShop !== "" && this.state.selectedShop !== "Selectionner un PDV" ? (
                                        <Ionicons style={styles.iconInput} color={Colors.orange} name="md-checkmark"
                                                  size={24}/>) : (
                                        <Ionicons color={Colors.iconInput} style={styles.iconInput}
                                                  name="ios-arrow-down"
                                                  size={24}/>)}

                                </View>
                            </Ripple>
                        </Block>
                        {/* take picture */}
                        <Block style={{
                            marginVertical: 5,
                            paddingVertical: 10,
                            backgroundColor: Colors.white
                        }}>
                            <Block style={{
                                paddingVertical: 10,
                            }}>
                                <Block style={{
                                    padding: 10,
                                }}>
                                    <Block row>
                                        <GalioText size={15} color={Colors.textColor}>
                                            {`1. `}
                                        </GalioText>
                                        <GalioText size={15} color={Colors.textColor}>
                                            Prenez en photo la façade du point de vente comme la photo en
                                            dessous.<GalioText
                                            color={Colors.red}>{` *`}</GalioText>
                                        </GalioText>
                                    </Block>
                                </Block>
                                <Block row space={"between"} style={{
                                    marginTop: 7,
                                }}>
                                    <Block style={{
                                        backgroundColor: Colors.lightGray
                                    }}>
                                        <Block center>
                                            <Image
                                                source={{uri: image ? image.path : 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/Fa%C3%A7ade+boutique+Orange+2.jpg'}}
                                                style={styles.defaultImage}
                                                PlaceholderContent={<ActivityIndicator color={Colors.orange}  />}
                                            />
                                        </Block>
                                    </Block>
                                </Block>
                                <Block center style={{
                                    marginTop: 10,
                                }}>
                                    <TouchableOpacity onPress={() => this.setState({cameramodal: true})}>
                                        <MaterialIcons name={'camera-alt'} color={Colors.orange2} size={28}/>
                                    </TouchableOpacity>
                                </Block>
                            </Block>
                        </Block>
                        {/* type pdv */}
                        <Block style={{
                            marginVertical: 5,
                            paddingVertical: 10,
                            backgroundColor: Colors.white
                        }}>
                            <Block row style={{
                                padding: 10
                            }}>
                                <Entypo name={'shop'} color={Colors.orange} size={22}/>
                                <Text style={{color: '#333', fontSize: 16, marginStart: 10}}>Type PDV<GalioText
                                    color={Colors.red}>{` *`}</GalioText></Text>
                            </Block>
                            <Ripple
                                rippleOpacity={0.1}
                                rippleDuration={200}
                                onPress={() => {
                                    ActionSheet.show(
                                        {
                                            options: pdv,
                                            cancelButtonIndex: 0,
                                            title: "Type PDV",
                                        },
                                        buttonIndex => {
                                            this.handleChangeTypePdv(buttonIndex)
                                        }
                                    )
                                }}>

                                <View style={styles.SectionStyle}>
                                    <TextInput
                                        style={styles.input}
                                        editable={false}
                                        value={this.state.pdv}
                                    />
                                    {this.state.pdv === "" || this.state.pdv === "selectionner type PDV" ? (
                                        <Ionicons style={styles.iconInput} name="ios-arrow-down" size={24}/>) : (
                                        <Ionicons color={Colors.orange} style={styles.iconInput} name="md-checkmark"
                                                  size={24}/>)}
                                </View>

                            </Ripple>
                        </Block>


                        {/*///////////////////////////////////////Operateur////////////////////////////////////////////////////*/}
                        <Block style={{
                            marginVertical: 5,
                            paddingVertical: 10,
                            backgroundColor: Colors.white
                        }}>
                            <Block row style={
                                {
                                    padding: 10
                                }
                            }>
                                <MaterialIcons color={Colors.orange} name={'sim-card'} size={22}/>
                                <Text style={{color: '#333', fontSize: 16, marginStart: 10}}>Choix opérateur<GalioText
                                    color={Colors.red}>{` *`}</GalioText></Text>
                            </Block>
                            <Ripple rippleOpacity={0.1} rippleDuration={200} onPress={() => {
                                ActionSheet.show(
                                    {
                                        options: operateur,
                                        cancelButtonIndex: 0,
                                        title: "Operateur",
                                    },
                                    buttonIndex => {
                                        this.handleChangeOperateur(buttonIndex)
                                    }
                                )
                            }}>
                                <View style={styles.SectionStyle}>
                                    <TextInput
                                        style={styles.input}
                                        editable={false}
                                        value={newPDV.branche.value}

                                    />
                                    {newPDV.branche.value === "" ? (
                                        <Ionicons style={styles.iconInput} name="ios-arrow-down" size={24}/>) : (
                                        <Ionicons color={Colors.orange} style={styles.iconInput} name="md-checkmark"
                                                  size={24}/>)}

                                </View>
                            </Ripple>
                        </Block>


                        <Block style={{
                            marginVertical: 5,
                            paddingVertical: 10,
                            backgroundColor: Colors.white
                        }}>
                            <Block style={{
                                padding: 10,
                            }}>
                                <GalioText size={15} color={Colors.textColor}>
                                    2. Quel-est le nom du point de vente ?<GalioText
                                    color={Colors.red}>{` *`}</GalioText>
                                </GalioText>
                                <Block style={{
                                    marginTop: 7
                                }}>
                                    <Block>
                                        <Input
                                            style={{
                                                borderColor: Colors.gray,
                                            }}
                                            color={Colors.textColor}
                                            rounded
                                            value={newPDV.nomMagasin.value}
                                            onChangeText={(nomMagasin) => {
                                                this.handleChangeNomMagasin(nomMagasin)
                                            }}
                                        />
                                    </Block>
                                </Block>
                            </Block>
                        </Block>

                        {/* new block adress */}
                        <Block style={{
                            marginVertical: 5,
                            paddingVertical: 10,
                            backgroundColor: Colors.white
                        }}>
                            <Block style={{
                                padding: 10,
                            }}>
                                <GalioText style={{
                                    marginBottom: 10,
                                }} center h5 color={Colors.textColor}>
                                    Adresse du point de vente
                                </GalioText>
                                <Block row>
                                    <Block>
                                        <GalioText size={15} color={Colors.textColor}>{`3. `}</GalioText>
                                    </Block>
                                    <Block>
                                        <GalioText size={15} color={Colors.textColor}>Indiquez l'adresse du
                                            point de vente <GalioText size={14} color={Colors.red}>{`*`}</GalioText>
                                        </GalioText>
                                    </Block>
                                </Block>
                                <Block>
                                    {/* gouvernorats */}
                                    <Block style={{
                                        marginTop: 7
                                    }}>
                                        {gouvernorats.length >= 1 &&
                                        <Picker
                                            mode={"dialog"}
                                            selectedValue={newPDV.gouvernorat.value}
                                            style={{
                                                borderColor: Colors.gray,
                                                borderWidth: 1,
                                                paddingHorizontal: 20,
                                                borderRadius: 50,
                                            }}
                                            onValueChange={(itemValue, itemIndex) =>
                                                this.handleChangeGouvernorat(itemValue)
                                            }>
                                            {gouvernorats.map((gov,index) => (
                                                <Picker.Item key={index} label={gov} value={gov}/>))}
                                        </Picker>
                                        }
                                    </Block>

                                    <Block row space={"around"}>
                                        <Block flex>
                                            <Picker
                                                enabled={newPDV.delegation.isEnabled}
                                                mode={"dialog"}
                                                selectedValue={newPDV.delegation.value}
                                                style={{
                                                    borderColor: Colors.gray,
                                                    borderWidth: 1,
                                                    paddingHorizontal: 20,
                                                    borderRadius: 50,
                                                }}
                                                onValueChange={(itemValue, itemIndex) =>
                                                    this.handleChangeDelegation(itemValue)
                                                }>
                                                {delegations.map((gov,index) => (
                                                    <Picker.Item label={gov} key={index} value={gov}/>))}
                                            </Picker>
                                        </Block>
                                        <Block flex>
                                            <TextInput
                                                editable={false}
                                                style={{
                                                    borderColor: Colors.gray,
                                                    borderWidth: 1,
                                                    paddingHorizontal: 20,
                                                    borderRadius: 50,
                                                }}
                                                placeholder={"Code postal"}
                                                value={newPDV.codePostal.value}
                                                keyboardType={"numeric"}
                                            />
                                        </Block>
                                    </Block>


                                    <Block style={{
                                        marginTop: 7
                                    }}>
                                        <Input
                                            style={{
                                                borderColor: Colors.gray,

                                            }}
                                            color={Colors.textColor}
                                            rounded
                                            placeholder={"Adresse (facultatif)"}
                                            value={newPDV.adresse.value}
                                            onChangeText={(adresse) => {
                                                this.handleChangeAdresse(adresse)
                                            }}
                                        />
                                    </Block>


                                </Block>

                            </Block>
                        </Block>
                        {/*end new block adress*/}


                        <Block style={{
                            marginTop: 5,
                            marginBottom: 10,
                            paddingVertical: 10,
                            backgroundColor: Colors.white
                        }}>
                            <Block style={{
                                padding: 10,
                            }}>
                                <GalioText style={{
                                    marginBottom: 10,
                                }} center h5 color={Colors.textColor}>
                                    Propriétaire du point de vente
                                </GalioText>
                                <Block row>
                                    <Block>
                                        <GalioText size={15} color={Colors.textColor}>{`4. `}</GalioText>
                                    </Block>
                                    <Block>
                                        <GalioText size={15} color={Colors.textColor}>Quel est le prénom et le nom du
                                            propriétaire ?<GalioText size={14} muted>{` (facultatif)`} </GalioText>
                                        </GalioText>
                                    </Block>
                                </Block>
                                <Block>
                                    <Block row space="between" style={{
                                        marginTop: 5,
                                        width: SIZES.width - 60,
                                    }}>
                                        <Input
                                            style={{
                                                width: '99%',
                                                borderColor: Colors.gray,
                                            }}
                                            color={Colors.textColor}
                                            rounded
                                            placeholder={"Prénom"}
                                            value={newPDV.prenom.value}
                                            onChangeText={(prenom) => {
                                                this.handleChangePrenom(prenom)
                                            }}
                                        />
                                        <Input
                                            style={{
                                                width: '99%',
                                                borderColor: Colors.gray,
                                            }}
                                            color={Colors.textColor}
                                            rounded
                                            placeholder={"Nom"}
                                            value={newPDV.nom.value}
                                            onChangeText={(nom) => {
                                                this.handleChangeNom(nom)
                                            }}
                                        />
                                    </Block>
                                </Block>
                                <Block row style={{
                                    marginTop: 7
                                }}>
                                    <Block>
                                        <GalioText size={15} color={Colors.textColor}>{`5. `}</GalioText>
                                    </Block>
                                    <Block>
                                        <GalioText size={15} color={Colors.textColor}>Veuillez noter le numéro de
                                            téléphone
                                            du propriétaire du point de vente?<GalioText size={14}
                                                                                         muted>{` (facultatif)`} </GalioText>
                                        </GalioText>
                                    </Block>
                                </Block>
                                <Block>
                                    <Block style={{
                                        marginTop: 5,
                                    }}>

                                        <TextInputMask
                                            style={{
                                                borderColor: Colors.gray,
                                                borderWidth: 1,
                                                paddingHorizontal: 20,
                                                borderRadius: 50,
                                            }}
                                            keyboardType={"numeric"}
                                            onChangeText={(formatted, extracted) => {
                                                this.handleChangeTel(extracted)
                                            }}
                                            type={"phone-pad"}
                                            placeholder={"+216"}
                                            value={newPDV.telephone.value}
                                            mask={"+216 [00] [000] [000]"}
                                            keyboardType={"numeric"}
                                        />
                                    </Block>
                                </Block>
                                <Block style={{
                                    paddingTop: 7,
                                    marginLeft: SIZES.width / 2
                                }}>
                                    <Button
                                        disabled={!this.validate()}
                                        style={{
                                            paddingHorizontal: 20,
                                            elevation: 0,
                                            borderRadius: 0,
                                            backgroundColor: !this.validate() ? Colors.gray : Colors.orange
                                        }}
                                        onPress={this.goNext}
                                    >
                                        <GalioText size={18} color={Colors.white}>APPLIQUER</GalioText>
                                        <AntDesign
                                            style={{marginLeft: 5}}
                                            name="arrowright" size={24}
                                            color={Colors.white}/>
                                    </Button>
                                </Block>
                            </Block>
                        </Block>
                    </ScrollView>
                }
            </Block>
        );
    };
};


const styles = StyleSheet.create({
    defaultImage: {
        borderRadius: 5,
        width: SIZES.width,
        height: SIZES.width,
        resizeMode: "contain",
    },
    container2: {
        flex: 1,
        flexDirection: 'row',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40
    },
    title: {
        paddingLeft: 45,
    },
    item: {
        width: SIZES.width - 100,
        height: SIZES.width - 200,
        borderRadius: 20,
    },
    imageContainer: {
        flex: 1,
        backgroundColor: 'white',

        borderRadius: 0,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        elevation: 1.1,
        height: 60,
        marginHorizontal: 5,
        borderWidth: 0.5,
        borderColor: Colors.inactiveTintColor,

        margin: 0,
    },
    SectionStyle2: {
        flex: 1,
        backgroundColor: Colors.backGroundGray,
        flexDirection: 'row',
        borderWidth: 0,
        height: 50,
        borderRadius: 30,
        marginVertical: 5,
        marginHorizontal: 10,
    },
    input: {
        marginStart: 10,
        flex: 1,
        borderRadius: 0,
        color: "#333"
    },
    iconInput: {
        marginEnd: 12,
    },
    container: {
        flex: 1,
        backgroundColor: "#ffffff",
    },
    tabsContainer: {
        alignSelf: 'stretch',
        marginTop: 30,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeContainer3: {
        backgroundColor: 'white',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,

    },
    itemThreeContainer: {
        backgroundColor: 'white',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeSubContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
    },
    itemThreeImage: {
        height: 100,
        width: 100,
        marginRight: 5,
        marginLeft: 5,
    },
    itemThreeContent: {
        paddingLeft: 15,
        textAlign: 'right',
        marginTop: 10,
        display: 'flex', flexDirection: 'row',
        position: 'absolute', right: 5
    },
    itemThreeBrand: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5F5F5F',
        textAlign: 'right',

    },
    itemThreeTitle: {
        fontFamily: "Lato-Regular",
        fontSize: 14,
        color: '#5F5F5F',

    },
    itemThreeSubtitle: {
        fontFamily: "Lato-Regular",
        fontSize: 12,
        color: '#a4a4a4',
    },
    itemThreeMetaContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemThreePrice: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5f5f5f',
        textAlign: 'right',
        marginRight: 10,
    },
    itemThreeHr: {
        flex: 1,
        height: 1,
        backgroundColor: '#e3e3e3',
        marginRight: -15,
    },
    badge: {
        backgroundColor: '#6DD0A3',
        borderRadius: 10,
        paddingHorizontal: 7,
        paddingVertical: 5,
        display: 'flex', flexDirection: 'row'
    },
});
