import * as React from 'react';

import {Block, Text, Input, Text as GalioText} from 'galio-framework'
import Colors from "../utils/Colors";
import {
    Picker,
    StyleSheet,
    TouchableOpacity,
    View,
    TextInput,
    ScrollView,
    ActivityIndicator, Alert
} from "react-native";
import {Image} from 'react-native-elements'
import AntDesign from "react-native-vector-icons/AntDesign";
import {fontFamily, SIZES, Urh} from "../utils/Const";
import {ProgressBar} from 'react-native-paper'
import {ActionSheet, Button} from "native-base";
import axios from "axios";
import Modal from "react-native-modal";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import Camera from 'react-native-camera';
import Ripple from "react-native-material-ripple";
import Ionicons from "react-native-vector-icons/Ionicons";
import TextInputMask from "react-native-text-input-mask";
import ImageLoad from 'react-native-image-placeholder';

const CodePostals = require('../utils/CodePostals');
var geolib = require('geolib');
const pdv = ['PDV Labellisé', 'PDV', 'PDV Franchisé', 'PDR', "Autre"];
const operateur = ['Orange', 'Ooredoo', 'Tunisie Telecom (TT)', 'Lyca mobile', "Bee", "Autre"];

export default class EditPdv extends React.Component {
    takedpic = null;
    state = {
        pdv: null,
        isLoading: true,
    };

    constructor(props) {
        super(props);
        this.pic = this.pic.bind(this);
        this.goNext = this.goNext.bind(this)
    }

    champsremplire = (nom, buttonIndex) => {
        let {
            shops
        } = this.state
        let item = shops[buttonIndex]
        if (!item)
            return
        let gouvernorats = ["Gouvernorat"]
        this.setState({gouvernorats})
        let delegations = ["Délégation"]
        this.setState({delegations})
        CodePostals.map(elem => {
            let isNewGov = true
            gouvernorats.map(gov => {
                if (gov === elem.gouvernorat)
                    isNewGov = false;
            })
            if (isNewGov)
                gouvernorats.push(elem.gouvernorat)
        });

        this.setState({
            image: null,
            gouvernorats: gouvernorats,
            nomMagasin: '',
            idmagasin: item._id,
            adresse: '',
            codePostal: '',
            delegations: ["Délégation"],
            delegation: '',
            gouvernorat: '',
            lat: item.coordinates.lat,
            long: item.coordinates.long,
            nom: '',
            prenom: '',
            telephone: "",
            pdv: "selectionner type PDV",
            operateur: "selectionné un operateur",
            typepdv: "",
            branche: "",
            imagecaptured: false,
            newPDV: {
                coords: {
                    longitude: null,
                    latitude: null
                },
                nomMagasin: {
                    value: '',
                    valid: false,
                },
                adresse: {
                    value: '',
                    valid: true,
                },
                codePostal: {
                    value: "",
                    valid: true,
                    isEditable: true,
                },
                delegation: {
                    isEnabled: false,
                    value: "",
                    valid: false,
                },
                gouvernorat: {
                    isEnabled: true,
                    value: "gouvernorat",
                    valid: false,
                },
                nom: {
                    value: "",
                    valid: true,
                },
                prenom: {
                    value: "",
                    valid: true,
                },
                telephone: {
                    value: "",
                    valid: true,
                },
                typepdv: {
                    value: "",
                    valid: false,
                },
                branche: {
                    value: "",
                    valid: false,
                },
                image: {
                    value: null,
                    valid: false,
                },
                valid: false
            }
        }, () => {
            this.champsremplire2(item)
        })
    }
    champsremplire2 = (item) => {

        let {newPDV} = this.state

        // set coords
        newPDV.coords.latitude = item.coordinates.lat;
        newPDV.coords.longitude = item.coordinates.long;

        // set fullname
        if (item.nom_owner) {
            let nomprenom = item.nom_owner.split(" ")
            if (nomprenom && nomprenom[0]) {
                this.setState({nom: nomprenom[0]})
                newPDV.nom.value = nomprenom[0]
            }
            if (nomprenom && nomprenom[1]) {
                newPDV.prenom.value = nomprenom[1]
                this.setState({prenom: nomprenom[1]})
            }
        }
        // set branche
        if (item.branche) {
            this.setState({operateur: item.branche, branche: item.branche})
            newPDV.branche.value = item.branche

            let valid = item.branche && item.branche.length >= 1 ? true : false
            newPDV.branche.valid = valid
        }

        if (item.categorie && item.categorie[0]) {

            let tpdv = item.categorie[0]
            if (item.categorie[0] == "PDV LABELISE")
                tpdv = "PDV Labellisé"

            else if (item.categorie[0] == 'boutique franchise')
                tpdv = "PDV Franchisé"

            this.setState({
                pdv: tpdv,
                typepdv: item.categorie[0]
            })
            // validate
            let valid = true
            if (tpdv === "" && tpdv === "selectionner type PDV")
                valid = false
            newPDV.typepdv.value = tpdv
            newPDV.typepdv.valid = valid
        }

        if (item.photo && item.photo.length > 0) {
            let img = "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/imagecartographie/" + item.photo
            let image = {path: img}
            newPDV.image.value = image.path
            newPDV.image.valid = true
            this.setState({image: image})
        }


        if (item.ville) {
            this.setState({adresse: item.ville})
            newPDV.adresse.value = item.ville

        }

        if (item.nom) {
            this.setState({nomMagasin: item.nom})
            // validate
            let valid = true
            if (item.nom === "")
                valid = false
            newPDV.nomMagasin.value = item.nom
            newPDV.nomMagasin.valid = valid
        }

        if (item.gouvernorat) {
            let {gouvernorats} = this.state
            let valid = false;
            gouvernorats.map(g => {
                if (g === item.gouvernorat && item.gouvernorat !== "Gouvernorat") {
                    valid = true
                    return
                }
            })

            if (valid) {

                newPDV.gouvernorat.valid = true
                newPDV.gouvernorat.value = item.gouvernorat
                // delegation
                if (item.delegation) {
                    let delegations = []
                    // get delegation from CodePostals
                    CodePostals.map((elem) => {
                        let newDelegation = true
                        if (elem.gouvernorat === item.gouvernorat) {
                            delegations.map((del) => {
                                if (elem.delegation === del) {
                                    newDelegation = false
                                    return
                                }
                            })
                            if (newDelegation)
                                delegations.push(elem.delegation)
                        }
                    });

                    if (delegations.includes(item.delegation)) {
                        newPDV.delegation.valid = true
                        newPDV.delegation.value = item.delegation

                    } else {
                        newPDV.delegation.valid = false
                        let newArray = ["Délégation"].concat(delegations)
                        newPDV.delegation.value = "Délégation"

                    }
                    this.setState({delegations})

                    if (item.codepostal) {
                        newPDV.codePostal.value = item.codepostal
                        if (!newPDV.codePostal.valid)
                            newPDV.codePostal.valid = true
                    }
                }
            } else {
                newPDV.gouvernorat.valid = false
                newPDV.gouvernorat.value = "Gouvernorat"
                newPDV.delegation.valid = false
                newPDV.delegation.value = "Délégation"
                newPDV.delegation.isEnabled = false
                newPDV.codePostal.value = ""
                newPDV.codePostal.valid = false
            }
        }


        if (item.telephone) {
            this.setState({telephone: item.telephone.toString()})
            newPDV.telephone.value = item.telephone.toString()
            newPDV.telephone.valid = newPDV.telephone.value.length === 8
        }
        this.setState({newPDV: newPDV}, () => {

        })
    }

    renderShops = (item, index) => {

        return (
            <TouchableOpacity
                onPress={() => this.champsremplire(item)}>
                <Block space="between" row style={{
                    marginVertical: 5,
                    paddingVertical: 10,
                    height: 50,
                    backgroundColor: Colors.lightGray,
                    paddingHorizontal: 18,
                }}>

                    <Block center flex>
                        <GalioText color={Colors.orange} style={{fontSize: 18}}>{item.nom}</GalioText>
                    </Block>


                </Block>
            </TouchableOpacity>
        )
    }


    renderHeader = () => {
        const {navigation} = this.props;
        return (
            <Block space="between" row style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 18,
            }}>
                <Block center>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <AntDesign name='arrowleft' size={24} color={Colors.white}/>
                    </TouchableOpacity>
                </Block>
                <Block center flex>
                    {this.state.pdv && <GalioText numberOfLines={1} color={Colors.white} style={{fontSize: 18}}>{this.state.pdv.nom}</GalioText>}
                </Block>
            </Block>
        )
    };


    pic(pic) {
        const options = {fixOrientation: true};
        this.camera.capture(options)
            .then((data) => {
                let {currentPosition} = this.props.screenProps;
                let {newPDV} = this.state
                newPDV.coords = {
                    longitude: currentPosition.coords.longitude,
                    latitude: currentPosition.coords.latitude
                };

                this.setState({
                    image: data,
                    imagecaptured: true,
                    cameramodal: false,
                    newPDV: newPDV
                });
                this.takedpic = data.path


            })


            .catch(err => {

                this.setState({
                    cameramodal: false,
                });
            });

    }


    goNext() {
        this.setState({isLoading: true})
        let {user, updateMyState, currentPosition} = this.props.screenProps
        let {
            idmagasin,
            lat,
            long,
            imagecaptured
        } = this.state


        let distance = geolib.getDistance(
            {
                latitude: lat,
                longitude: long
            },
            {
                latitude: currentPosition.coords.latitude,
                longitude: currentPosition.coords.longitude
            }
        )

        if (distance < 100) {
            let {newPDV} = this.state
            let configpic = {
                headers: {
                    'contentType': "application/json",
                    'Content-Type': 'multipart/form-data'
                },
                onUploadProgress: (progressEvent) => {
                    const {loaded, total} = progressEvent;

                    this.setState({progress: Math.round((loaded * 100) / total)});
                },
            };
            let postData = new FormData()
            postData.append('id_compte', user._id)

            let {
                typepdv,
                branche,
                prenom,
                telephone,
                nom,
                adresse,
                codePostal,
                delegation,
                gouvernorat,
                nomMagasin,
                coords
            } = newPDV
            // append data
            postData.append('categorie', typepdv.value)
            postData.append('branche', branche.value)
            postData.append('prenom', prenom.value)
            postData.append('telephone', telephone.value)
            postData.append('nom', nom.value)
            postData.append('adresse', adresse.value)
            postData.append('codePostal', codePostal.value)
            postData.append('delegation', delegation.value)
            postData.append('gouvernorat', gouvernorat.value)
            postData.append('nomMagasin', nomMagasin.value)
            postData.append('idmagasin', idmagasin)

            // append coords
            postData.append('longitude', coords.longitude)
            postData.append('latitude', coords.latitude)

            if (imagecaptured == true) {
                postData.append('my_epicerie_photo', {
                    uri: this.takedpic,
                    name: 'my_epicerie_photo.jpg',
                    type: 'image/jpg'
                })
            }


            axios.post(Urh + 'Modifiermagasin', postData, configpic).then((res) => {
                if (res.data.success) {
                    this.setState({isLoading: false}, () => this.props.navigation.navigate('Liste'))
                } else {
                    this.setState({isLoading: false}, () => this.props.navigation.navigate('Liste'))
                }
            })
                .catch(err => {
                    this.setState({isLoading: false}, () => {
                        Alert.alert(
                            'Connexion interrompue',
                            'Veuillez vérifier votre connexion et réessayer.',
                            [
                                {
                                    text: 'Ok',
                                    onPress: () => null
                                },
                            ],
                            {cancelable: false},
                        );
                    })
                })

        } else {
            this.setState({modaltoofar: true, isLoading: false})
        }


    };


    validate = () => {
        let {newPDV} = this.state
        if (!newPDV.image.valid ||
            !newPDV.typepdv.valid ||
            !newPDV.branche.valid ||
            !newPDV.nom.valid ||
            !newPDV.prenom.valid ||
            !newPDV.gouvernorat.valid ||
            !newPDV.delegation.valid ||
            !newPDV.telephone.valid ||
            !newPDV.codePostal.valid ||
            !newPDV.adresse.valid ||
            !newPDV.nomMagasin.valid)
            return false
        return true
    };
    componentDidMount() {
        let pdv = JSON.parse(this.props.navigation.getParam('pdv', null))
        this.setState({
            pdv,
            isLoading : false,
        })
    };
    // change Type PDV
    handleChangeTypePdv = (buttonIndex) => {
        let tpdv = pdv[buttonIndex]

        if (pdv[buttonIndex] == "PDV Labellisé")
            tpdv = "PDV LABELISE"

        else if (pdv[buttonIndex] == "PDV Franchisé")
            tpdv = 'boutique franchise'

        // validate
        let valid = true
        if (tpdv === "" && tpdv === "selectionner type PDV")
            valid = false
        let {newPDV} = this.state

        newPDV.typepdv.value = tpdv
        newPDV.typepdv.valid = valid
        this.setState({
            pdv: pdv[buttonIndex],
            newPDV: newPDV,
        });
    }
    // change Choix opérateur
    handleChangeOperateur = (buttonIndex) => {
        let branche = operateur[buttonIndex]
        // validate branche
        let valid = true
        if (branche === "" && branche === "selectionner un operateur")
            valid = false
        //setState
        let {newPDV} = this.state
        newPDV.branche.value = branche
        newPDV.branche.valid = valid
        this.setState({
            operateur: operateur[buttonIndex],
            newPDV
        });
    }

    // change nom magasin
    handleChangeNomMagasin = (nommagasin) => {
        let {newPDV} = this.state
        newPDV.nomMagasin.value = nommagasin
        //validate
        nommagasin === "" ? newPDV.nomMagasin.valid = false : newPDV.nomMagasin.valid = true
        //setState
        this.setState({newPDV})

    }
    // change Adresse
    handleChangeAdresse = (adresse) => {
        let {newPDV} = this.state
        newPDV.adresse.value = adresse
        //validate
        if (!newPDV.adresse.valid)
            newPDV.adresse.valid = true
        //setState
        this.setState({newPDV})
    }

    // change Gouvernorat
    handleChangeGouvernorat = (gouvernorat) => {

        let {newPDV} = this.state

        newPDV.gouvernorat.value = gouvernorat
        //validate
        gouvernorat === "Gouvernorat" ? newPDV.gouvernorat.valid = false : newPDV.gouvernorat.valid = true;

        if (gouvernorat === "Gouvernorat") {
            if (newPDV.delegation.valid)
                newPDV.delegation.valid = false
            if (newPDV.delegation.isEnabled)
                newPDV.delegation.isEnabled = false
            this.setState({
                newPDV
            })
            return;
        }
        let delegations = []
        // get delegation from
        CodePostals.map((elem) => {
            let newDelegation = true
            if (elem.gouvernorat === gouvernorat) {
                delegations.map((del) => {
                    if (elem.delegation === del) {
                        newDelegation = false
                        return
                    }
                })
                if (newDelegation)
                    delegations.push(elem.delegation)
            }
        });
        if (delegations.length === 0) {
            newPDV.delegation.isEnabled = false
            newPDV.delegation.valid = false
        } else {
            newPDV.delegation.isEnabled = true
            newPDV.delegation.valid = true
        }
        //setState
        this.setState({
            newPDV,
            delegations
        }, () => {

        })
    }

    // change Delegation
    handleChangeDelegation = (delegation) => {
        let {newPDV} = this.state
        newPDV.delegation.value = delegation
        //validate
        if (!newPDV.delegation.valid)
            newPDV.delegation.valid = true
        //setState
        CodePostals.map((elem) => {
            if (elem.delegation === delegation) {
                newPDV.codePostal.value = elem.codePostal
            }
        });
        newPDV.codePostal.isEditable = false;
        this.setState({newPDV})
    }


    // change Telephone
    handleChangeTel = (telephone) => {
        let {newPDV} = this.state
        newPDV.telephone.value = telephone
        //validate
        telephone.length === 8 || telephone.length === 0 ? newPDV.telephone.valid = true : newPDV.telephone.valid = false
        //setState
        this.setState({newPDV}, () => {
            this.forceUpdate()
        })
    }

    // change Adresse
    handleChangeNom = (nom) => {
        let {newPDV} = this.state
        newPDV.nom.value = nom
        //validate
        if (!newPDV.nom.valid)
            newPDV.nom.valid = true
        //setState
        this.setState({newPDV})
    }

    // change Adresse
    handleChangePrenom = (prenom) => {
        let {newPDV} = this.state
        newPDV.prenom.value = prenom
        //validate
        if (!newPDV.prenom.valid)
            newPDV.prenom.valid = true
        //setState
        this.setState({newPDV})
    }


    render() {
        let {
            isLoading,
            pdv
        } = this.state;
        return (
            <Block flex style={{backgroundColor: Colors.backgroundColor2}}>
                {this.renderHeader()}

            </Block>
        );
    };
};