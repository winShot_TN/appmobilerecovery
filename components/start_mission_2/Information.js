import * as React from 'react';
import {Block, Text, Input, Text as GalioText, Radio} from 'galio-framework'
import Colors from "../utils/Colors";
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Picker,
    TextInput,
    ScrollView,
    ActivityIndicator,
    Dimensions, Alert
} from "react-native";
import {Image} from 'react-native-elements'
import AntDesign from "react-native-vector-icons/AntDesign";
import CameraContainer from "react-native-simple-camera";
import ImageLoad from 'react-native-image-placeholder'
import {
    AppRegistry,
    TouchableHighlight,
} from 'react-native';
import Camera from 'react-native-camera';
import {fontFamily, Logo, SIZES, Urh, UrhImages} from "../utils/Const";
import {Divider, ProgressBar} from 'react-native-paper'
import {ActionSheet, Button} from "native-base";
import code from "../reseau/code";
import axios from "axios";
import {RadioButton} from 'react-native-paper';
import Modal from "react-native-modal";
import SocketIOClient from "socket.io-client";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ripple from "react-native-material-ripple";
import Ionicons from "react-native-vector-icons/Ionicons";
import TextInputMask from 'react-native-text-input-mask';
import PatternInput from '../utils/Input'
import {NavigationActions, StackActions} from "react-navigation";

const pdv = ['PDV Labellisé', 'PDV', 'PDV Franchisé', 'PDR', "Autre"]
const operateur = ['Orange', 'Ooredoo', 'Tunisie Telecom (TT)', 'Lyca mobile', "Bee", "Autre"]
const {width, height} = Dimensions.get('window');
const CodePostals = require('../utils/CodePostals')

export class Information extends React.Component {
    state = {
        progress: 0,
        gouvernorats: ["Gouvernorat"],
        delegations: ["Délégation"],
        pdv: "selectionner type PDV",
        operateur: "selectionner un operateur",
        cameramodal: false,
        isLoading: false,
        newPDV: {
            coords: null,
            nomMagasin: {
                value: '',
                valid: false,
            },
            adresse: {
                value: '',
                valid: true,
            },
            codePostal: {
                value: "",
                valid: true,
                isEditable: true,
            },
            delegation: {
                isEnabled: false,
                value: "",
                valid: false,
            },
            gouvernorat: {
                isEnabled: true,
                value: "gouvernorat",
                valid: false,
            },
            nom: {
                value: "",
                valid: true,
            },
            prenom: {
                value: "",
                valid: true,
            },
            telephone: {
                value: "",
                valid: true,
            },
            typepdv: {
                value: "",
                valid: false,
            },
            branche: {
                value: "",
                valid: false,
            },
            image: {
                value: null,
                valid: false,
            },
            valid: false
        }
    };

    constructor(props) {
        super(props);
        this.pic = this.pic.bind(this);
        this.goNext = this.goNext.bind(this)
    }

    renderHeader = () => {
        const {navigation} = this.props;
        return (
            <Block space="between" row style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 18,
            }}>
                <Block center>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <AntDesign name='arrowleft' size={24} color={Colors.white}/>
                    </TouchableOpacity>
                </Block>
                <Block center flex>
                    <GalioText color={Colors.white} style={{fontSize: 18}}>Ajouter PDV</GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    <AntDesign onPress={() => null} size={28} name={"questioncircle"}
                               color={Colors.white}/>
                </Block>
            </Block>
        )
    };

    pic(pic) {
        const options = {fixOrientation: true};

        this.camera.capture(options)
            .then((data) => {
                let {currentPosition} = this.props.screenProps;

                let {newPDV} = this.state
                newPDV.coords = {
                    longitude: currentPosition.coords.longitude,
                    latitude: currentPosition.coords.latitude
                };

                newPDV.image.value = data.path
                if (!newPDV.image.valid)
                    newPDV.image.valid = true
                this.setState({
                    newPDV,
                    cameramodal: false,
                });
            })
            .catch(err => {
                this.setState({
                    cameramodal: false,
                });
            });

    }

    goNext() {
        this.setState({
            isLoading: true,
        });
        let {newPDV} = this.state
        let configpic = {
            headers: {
                'contentType': "application/json",
                'Content-Type': 'multipart/form-data'
            },
            onUploadProgress: (progressEvent) => {
                const {loaded, total} = progressEvent;
                this.setState({progress: Math.round((loaded * 100) / total)});
            },
        };
        let {user} = this.props.screenProps
        let postData = new FormData()
        postData.append('id_compte', user._id)
        postData.append('categorie', newPDV.typepdv.value)
        postData.append('branche', newPDV.branche.value)
        postData.append('prenom', newPDV.prenom.value)
        postData.append('telephone', newPDV.telephone.value)
        postData.append('nom', newPDV.nom.value)
        postData.append('adresse', newPDV.adresse.value)
        postData.append('codePostal', newPDV.codePostal.value)
        postData.append('delegation', newPDV.delegation.value)
        postData.append('gouvernorat', newPDV.gouvernorat.value)
        postData.append('nomMagasin', newPDV.nomMagasin.value)
        postData.append('longitude', newPDV.coords.longitude)
        postData.append('latitude', newPDV.coords.latitude)


        postData.append('my_epicerie_photo', {
            uri: newPDV.image.value,
            name: 'my_epicerie_photo.jpg',
            type: 'image/jpg'
        })
        axios.post(Urh + 'Createepicerie', postData, configpic).then((res) => {
            if (res.data.success) {
                this.setState({isLoading: false}, () => this.props.navigation.navigate('Liste'))
            } else {
                this.setState({isLoading: false}, () => this.props.navigation.navigate('Liste'))
            }
        }).catch(err => {
            this.setState({isLoading: false}, () => {
                Alert.alert(
                    'Connexion interrompue',
                    'Veuillez vérifier votre connexion et réessayer.',
                    [
                        {
                            text: 'Ok',
                            onPress: () => null
                        },
                    ],
                    {cancelable: false},
                );
            })
        })
    };

    componentDidMount() {
        let {
            gouvernorats
        } = this.state
        CodePostals.map(elem => {
            let isNewGov = true
            gouvernorats.map(gov => {
                if (gov === elem.gouvernorat)
                    isNewGov = false;
            })
            if (isNewGov)
                gouvernorats.push(elem.gouvernorat)
        });
        this.setState({gouvernorats})
    }

    // change Type PDV
    handleChangeTypePdv = (buttonIndex) => {
        let tpdv = pdv[buttonIndex]

        if (pdv[buttonIndex] == "PDV Labellisé")
            tpdv = "PDV LABELISE"

        else if (pdv[buttonIndex] == "PDV Franchisé")
            tpdv = 'boutique franchise'

        // validate
        let valid = true
        if (tpdv === "" && tpdv === "selectionner type PDV")
            valid = false
        let {newPDV} = this.state

        newPDV.typepdv.value = tpdv
        newPDV.typepdv.valid = valid
        this.setState({
            pdv: pdv[buttonIndex],
            newPDV: newPDV,
        }, () => {

        });
    }

    // change Choix opérateur
    handleChangeOperateur = (buttonIndex) => {

        let branche = operateur[buttonIndex]
        // validate branche
        let valid = true
        if (branche === "" && branche === "selectionner un operateur")
            valid = false
        //setState
        let {newPDV} = this.state
        newPDV.branche.value = branche
        newPDV.branche.valid = valid
        this.setState({
            operateur: operateur[buttonIndex],
            newPDV
        });
    }

    // change nom magasin
    handleChangeNomMagasin = (nommagasin) => {
        let {newPDV} = this.state
        newPDV.nomMagasin.value = nommagasin
        //validate
        nommagasin === "" ? newPDV.nomMagasin.valid = false : newPDV.nomMagasin.valid = true
        //setState
        this.setState({newPDV})

    }
    // change Adresse
    handleChangeAdresse = (adresse) => {
        let {newPDV} = this.state
        newPDV.adresse.value = adresse
        //validate
        if (!newPDV.adresse.valid)
            newPDV.adresse.valid = true
        //setState
        this.setState({newPDV})
    }
    // change Gouvernorat
    handleChangeGouvernorat = (gouvernorat) => {

        let {newPDV} = this.state

        newPDV.gouvernorat.value = gouvernorat
        //validate
        gouvernorat === "Gouvernorat" ? newPDV.gouvernorat.valid = false : newPDV.gouvernorat.valid = true;

        if (gouvernorat === "Gouvernorat") {
            if (newPDV.delegation.valid)
                newPDV.delegation.valid = false
            if (newPDV.delegation.isEnabled)
                newPDV.delegation.isEnabled = false
            this.setState({
                newPDV
            })
            return;
        }
        let delegations = []
        // get delegation from
        CodePostals.map((elem) => {
            let newDelegation = true
            if (elem.gouvernorat === gouvernorat) {
                delegations.map((del) => {
                    if (elem.delegation === del) {
                        newDelegation = false
                        return
                    }
                })
                if (newDelegation)
                    delegations.push(elem.delegation)
            }
        });
        if (delegations.length === 0) {
            newPDV.delegation.isEnabled = false
            newPDV.delegation.valid = false
        } else {
            newPDV.delegation.isEnabled = true
            newPDV.delegation.valid = true
        }
        //setState
        this.setState({
            newPDV,
            delegations
        }, () => {

        })
    }

    // change Delegation
    handleChangeDelegation = (delegation) => {
        let {newPDV} = this.state
        newPDV.delegation.value = delegation
        //validate
        if (!newPDV.delegation.valid)
            newPDV.delegation.valid = true
        //setState
        CodePostals.map((elem) => {
            if (elem.delegation === delegation) {
                newPDV.codePostal.value = elem.codePostal
            }
        });
        newPDV.codePostal.isEditable = false;
        this.setState({newPDV})
    }


    // change Code postal
    handleChangeCodePostal = (postalCode) => {

        let {newPDV} = this.state
        newPDV.codePostal.value = postalCode
        //validate
        postalCode.length === 4 || postalCode.length === 0 ? newPDV.codePostal.valid = true : newPDV.codePostal.valid = false
        //setState
        this.setState({newPDV}, () => {

        })
    }

    // change Telephone
    handleChangeTel = (telephone) => {
        let {newPDV} = this.state
        newPDV.telephone.value = telephone
        //validate
        telephone.length === 8 || telephone.length === 0 ? newPDV.telephone.valid = true : newPDV.telephone.valid = false
        //setState
        this.setState({newPDV})
    }

    // change Adresse
    handleChangeNom = (nom) => {
        let {newPDV} = this.state
        newPDV.nom.value = nom
        //validate
        if (!newPDV.nom.valid)
            newPDV.nom.valid = true
        //setState
        this.setState({newPDV})
    }

    // change Adresse
    handleChangePrenom = (prenom) => {
        let {newPDV} = this.state
        newPDV.prenom.value = prenom
        //validate
        if (!newPDV.prenom.valid)
            newPDV.prenom.valid = true
        //setState
        this.setState({newPDV})
    }


    validate = () => {
        let {newPDV} = this.state

        if (!newPDV.image.valid ||
            !newPDV.typepdv.valid ||
            !newPDV.branche.valid ||
            !newPDV.nom.valid ||
            !newPDV.prenom.valid ||
            !newPDV.gouvernorat.valid ||
            !newPDV.delegation.valid ||
            !newPDV.telephone.valid ||
            !newPDV.codePostal.valid ||
            !newPDV.adresse.valid ||
            !newPDV.nomMagasin.valid)
            return false
        return true
    }

    render() {
        let {
            telephone,
            isLoading,
            cameramodal,
            newPDV,
            gouvernorats,
            delegations
        } = this.state
        return (

            <Block flex style={{backgroundColor: Colors.backgroundColor2}}>
                {this.renderHeader()}
                <Modal
                    style={{
                        flex: 1,
                        margin: 0
                    }}
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={cameramodal}>
                    <View style={styles.container2}>
                        <Camera
                            ref={(cam) => {
                                this.camera = cam;
                            }}
                            captureQuality={"1080p"}
                            style={styles.preview}
                            aspect={Camera.constants.Aspect.fill}>
                            <Text style={styles.capture} onPress={this.pic}>
                                <MaterialIcons name={'camera-alt'} color={Colors.orange2} size={35}/>

                            </Text>
                        </Camera>
                    </View>

                    <TouchableOpacity onPress={() => this.setState({cameramodal: false})}>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: "rgba(169,5,0,0.82)",
                            paddingVertical: 10,
                            paddingHorizontal: 10,
                        }}>

                            <AntDesign name={"closecircleo"} size={24} color={Colors.white}/>
                            <GalioText style={{
                                fontFamily: fontFamily["Poppins-Regular"],
                                fontSize: 18,
                                color: Colors.white,
                                paddingHorizontal: 15,
                            }}
                                       uppercase={false}>Annuler</GalioText>
                        </Block>
                    </TouchableOpacity>
                </Modal>

                <Modal backdropOpacity={0.3} hasBackdrop={true} isVisible={isLoading}>
                    <Block style={{
                        backgroundColor: Colors.lightGray,
                        padding: 15,
                        borderRadius: 10,
                    }}>
                        <Block center>
                            <ImageLoad
                                style={{
                                    height: 100,
                                    width: 100,
                                    resizeMode: 'contain'
                                }}
                                loadingStyle={{size: 'small', color: Colors.orange}}
                                source={{uri: 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/WINSHOT_PNG.png'}}
                            />
                        </Block>
                        <Block>
                            <ProgressBar progress={(this.state.progress / 100)} color={Colors.orange}/>
                        </Block>
                        <Block center>
                            <GalioText h5 color={Colors.blue}> {`${this.state.progress}%`} </GalioText>
                        </Block>
                    </Block>
                </Modal>
                <ScrollView style={{
                    flex: 1
                }}>
                    <Block style={{
                        marginVertical: 5,
                        paddingVertical: 10,
                        backgroundColor: Colors.white
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                        }}>
                            <Block>
                                <Block row style={{
                                    padding: 10,
                                    width: SIZES.width - 20,
                                }}>
                                    <Block>
                                        <GalioText size={15} color={Colors.textColor}>
                                            {`1. `}
                                        </GalioText>
                                    </Block>
                                    <Block>
                                        <GalioText size={15} color={Colors.textColor}>
                                            Prenez en photo la facade du point de vente comme la photo en dessous.
                                            <GalioText color={Colors.red}>
                                                {` *`}
                                            </GalioText>
                                        </GalioText>
                                    </Block>
                                </Block>
                            </Block>
                            <Block row space={"between"} style={{
                                marginTop: 7,
                            }}>
                                <Block style={{
                                    backgroundColor: Colors.lightGray
                                }}>
                                    <Block center>
                                        <Image
                                            PlaceholderContent={<ActivityIndicator/>}
                                            style={styles.defaultImage}
                                            source={{uri: newPDV.image.value ? newPDV.image.value : 'https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/welcome/Fa%C3%A7ade+boutique+Orange+2.jpg'}}

                                        />
                                    </Block>
                                </Block>
                            </Block>
                            <Block center style={{
                                marginTop: 10,
                            }}>
                                <TouchableOpacity onPress={() => this.setState({cameramodal: true})}>
                                    <MaterialIcons name={'camera-alt'} color={Colors.orange2} size={28}/>
                                </TouchableOpacity>
                            </Block>
                        </Block>
                    </Block>


                    {/*///////////////////////////////////////TYPE PDV////////////////////////////////////////////////////*/}
                    <Block style={{
                        marginVertical: 5,
                        paddingVertical: 10,
                        backgroundColor: Colors.white
                    }}>
                        <Block row style={{
                            padding: 10
                        }}>
                            <Entypo color={Colors.orange} name={'shop'} size={22}/>
                            <Text style={{color: '#333', fontSize: 16, marginStart: 10}}>Type PDV
                                <GalioText color={Colors.red}>
                                    {` *`}
                                </GalioText>
                            </Text>
                        </Block>

                        <Ripple rippleOpacity={0.1} rippleDuration={200} rippleColor={"red"} onPress={() => {
                            ActionSheet.show(
                                {
                                    options: pdv,
                                    cancelButtonIndex: 0,
                                    title: "Type PDV",
                                },
                                buttonIndex => {
                                    this.handleChangeTypePdv(buttonIndex);
                                }
                            )
                        }}>
                            <View style={styles.SectionStyle}>
                                <TextInput
                                    style={styles.input}
                                    editable={false}
                                    value={this.state.pdv}
                                />
                                {this.state.pdv === "" || this.state.pdv === "selectionner type PDV" ? (
                                    <Ionicons style={styles.iconInput} name="ios-arrow-down" size={24}/>) : (
                                    <Ionicons color={Colors.orange} style={styles.iconInput} name="md-checkmark"
                                              size={24}/>)}
                            </View>
                        </Ripple>
                    </Block>


                    {/*///////////////////////////////////////Operateur////////////////////////////////////////////////////*/}
                    <Block style={{
                        marginVertical: 5,
                        paddingVertical: 10,
                        backgroundColor: Colors.white
                    }}>
                        < Block row style={{
                            padding: 10
                        }}>
                            <Block>
                                <MaterialIcons name={'sim-card'} color={Colors.orange} size={24}/>
                            </Block>
                            <Block center>
                                <Text style={{color: '#333', fontSize: 16, marginStart: 10}}>Choix opérateur <GalioText
                                    color={Colors.red}>
                                    {` *`}
                                </GalioText></Text>
                            </Block>
                        </Block>
                        <Ripple rippleOpacity={0.1} rippleDuration={200} onPress={() => {
                            ActionSheet.show(
                                {
                                    options: operateur,
                                    cancelButtonIndex: 0,
                                    title: "Choix Operateur",
                                },
                                buttonIndex => {
                                    this.handleChangeOperateur(buttonIndex)
                                }
                            )
                        }}>
                            <View style={styles.SectionStyle}>
                                <TextInput
                                    style={styles.input}
                                    editable={false}
                                    value={this.state.operateur}
                                />
                                {this.state.operateur === "" || this.state.operateur === "selectionner un operateur" ? (
                                    <Ionicons style={styles.iconInput} name="ios-arrow-down" size={24}/>) : (
                                    <Ionicons color={Colors.orange} style={styles.iconInput} name="md-checkmark"
                                              size={24}/>)}
                            </View>
                        </Ripple>
                    </Block>
                    <Block style={{
                        marginVertical: 5,
                        paddingVertical: 10,
                        backgroundColor: Colors.white
                    }}>
                        <Block style={{
                            padding: 10,
                        }}>
                            <GalioText size={15} color={Colors.textColor}>
                                2. Quel-est le nom du point de vente ? <GalioText color={Colors.red}>
                                {` *`}
                            </GalioText>
                            </GalioText>
                            <Block style={{
                                marginTop: 7
                            }}>
                                <Block>
                                    <Input
                                        style={{
                                            borderColor: Colors.gray,
                                        }}
                                        color={Colors.textColor}
                                        rounded
                                        value={newPDV.nomMagasin.value}
                                        onChangeText={(nomMagasin) => {
                                            this.handleChangeNomMagasin(nomMagasin)
                                        }}
                                    />
                                </Block>
                            </Block>
                        </Block>
                    </Block>

                    {/* new block adress */}
                    <Block style={{
                        marginVertical: 5,
                        paddingVertical: 10,
                        backgroundColor: Colors.white
                    }}>
                        <Block style={{
                            padding: 10,
                        }}>
                            <GalioText style={{
                                marginBottom: 10,
                            }} center h5 color={Colors.textColor}>
                                Adresse du point de vente
                            </GalioText>
                            <Block row>
                                <Block>
                                    <GalioText size={15} color={Colors.textColor}>{`3. `}</GalioText>
                                </Block>
                                <Block>
                                    <GalioText size={15} color={Colors.textColor}>Indiquez l'adresse du
                                        point de vente <GalioText size={14} color={Colors.red}>{`*`}</GalioText>
                                    </GalioText>
                                </Block>
                            </Block>
                            <Block>
                                {/* gouvernorats */}
                                <Block style={{
                                    marginTop: 7
                                }}>
                                    {gouvernorats.length >= 1 &&
                                    <Picker
                                        mode={"dialog"}

                                        selectedValue={newPDV.gouvernorat.value}
                                        style={{
                                            borderColor: Colors.gray,
                                            borderWidth: 1,
                                            paddingHorizontal: 20,
                                            borderRadius: 50,
                                        }}
                                        onValueChange={(itemValue, itemIndex) =>
                                            this.handleChangeGouvernorat(itemValue)
                                        }>
                                        {gouvernorats.map((gov, index) => (
                                            <Picker.Item key={index} label={gov} value={gov}/>))}
                                    </Picker>
                                    }
                                </Block>

                                <Block row space={"around"}>
                                    <Block flex>
                                        <Picker
                                            enabled={newPDV.delegation.isEnabled}
                                            mode={"dialog"}
                                            selectedValue={newPDV.delegation.value}
                                            style={{
                                                borderColor: Colors.gray,
                                                borderWidth: 1,
                                                paddingHorizontal: 20,
                                                borderRadius: 50,
                                            }}
                                            onValueChange={(itemValue, itemIndex) =>
                                                this.handleChangeDelegation(itemValue)
                                            }>
                                            {delegations.map((gov, index) => (
                                                <Picker.Item key={index} label={gov} value={gov}/>))}
                                        </Picker>
                                    </Block>
                                    <Block flex>
                                        <TextInput
                                            editable={false}
                                            style={{
                                                borderColor: newPDV.codePostal.valid ? Colors.gray : Colors.red,
                                                borderWidth: 1,
                                                paddingHorizontal: 20,
                                                borderRadius: 50,
                                            }}
                                            placeholder={"Code postal"}
                                            value={newPDV.codePostal.value}
                                            onChangeText={(codePostal) => {
                                                this.handleChangeCodePostal(codePostal)
                                            }}
                                            keyboardType={"numeric"}
                                        />
                                    </Block>
                                </Block>


                                <Block style={{
                                    marginTop: 7
                                }}>
                                    <Input
                                        style={{
                                            borderColor: Colors.gray,
                                        }}
                                        color={Colors.textColor}
                                        rounded
                                        placeholder={"Adresse (facultatif)"}
                                        value={newPDV.adresse.value}
                                        onChangeText={(adresse) => {
                                            this.handleChangeAdresse(adresse)
                                        }}
                                    />
                                </Block>


                            </Block>

                        </Block>
                    </Block>
                    {/*end new block adress*/}


                    <Block style={{
                        marginTop: 5,
                        marginBottom: 10,
                        paddingVertical: 10,
                        backgroundColor: Colors.white
                    }}>
                        <Block style={{
                            padding: 10,
                        }}>
                            <GalioText style={{
                                marginBottom: 10,
                            }} center h5 color={Colors.textColor}>
                                Propriétaire du magasin
                            </GalioText>
                            <Block row>
                                <Block>
                                    <GalioText size={15} color={Colors.textColor}>{`4. `}</GalioText>
                                </Block>
                                <Block>
                                    <GalioText size={15} color={Colors.textColor}>Quel est le prénom et le nom du
                                        propriétaire ? <GalioText size={14} muted>
                                            {`(facultatif)`}
                                        </GalioText></GalioText>
                                </Block>
                            </Block>
                            <Block>
                                <Block row space="between" style={{
                                    marginTop: 5,
                                    width: SIZES.width - 60,
                                }}>
                                    <Input
                                        style={{
                                            width: '99%',
                                            borderColor: Colors.gray,
                                        }}
                                        color={Colors.textColor}
                                        rounded
                                        placeholder={"Prénom"}
                                        value={newPDV.prenom.value}
                                        onChangeText={(prenom) => {
                                            this.handleChangePrenom(prenom)
                                        }}
                                    />
                                    <Input
                                        style={{
                                            width: '99%',
                                            borderColor: Colors.gray,
                                        }}
                                        color={Colors.textColor}
                                        rounded
                                        placeholder={"Nom"}
                                        value={newPDV.nom.value}
                                        onChangeText={(nom) => {
                                            this.handleChangeNom(nom)
                                        }}
                                    />
                                </Block>
                            </Block>
                            <Block row style={{
                                marginTop: 7
                            }}>
                                <Block>
                                    <GalioText size={15} color={Colors.textColor}>{`5. `}</GalioText>
                                </Block>
                                <Block>
                                    <GalioText size={15} color={Colors.textColor}>Veuillez noter le numéro de téléphone
                                        du propriétaire du magasin? <GalioText size={14} muted>
                                            {`(facultatif)`}
                                        </GalioText>
                                    </GalioText>
                                </Block>
                            </Block>
                            <Block>
                                <Block style={{
                                    marginTop: 5,
                                }}>

                                    <TextInputMask
                                        style={{
                                            borderColor: Colors.gray,
                                            borderWidth: 1,
                                            paddingHorizontal: 20,
                                            borderRadius: 50,
                                        }}
                                        keyboardType={"numeric"}
                                        onChangeText={(formatted, extracted) => {
                                            this.handleChangeTel(extracted)
                                        }}
                                        type={"phone-pad"}
                                        placeholder={"+216"}
                                        value={telephone}
                                        mask={"+216 [00] [000] [000]"}
                                        keyboardType={"numeric"}
                                    />
                                </Block>
                            </Block>
                            <Block style={{
                                paddingTop: 7,
                                marginLeft: SIZES.width / 2
                            }}>
                                <Button
                                    disabled={!this.validate()}
                                    style={{
                                        paddingHorizontal: 20,
                                        elevation: 0,
                                        borderRadius: 0,
                                        backgroundColor: !this.validate() ? Colors.gray : Colors.orange
                                    }}
                                    onPress={this.goNext}
                                >
                                    <GalioText size={18} color={Colors.white}>APPLIQUER</GalioText>
                                    <AntDesign
                                        style={{marginLeft: 5}}
                                        name="arrowright" size={24}
                                        color={Colors.white}/>
                                </Button>
                            </Block>
                        </Block>
                    </Block>
                </ScrollView>
            </Block>
        );
    };

    takePicture() {
        const options = {fixOrientation: true};
        //options.location = ...
        this.camera.capture(options)
            .then((data) => {


            })


            .catch(err => {});
    }
}


const styles = StyleSheet.create({
    defaultImage: {
        borderRadius: 2,
        width: SIZES.width,
        height: SIZES.width,
        resizeMode: "contain",
    },
    container2: {
        flex: 1,
        flexDirection: 'row',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40
    },
    title: {
        paddingLeft: 45,
    },
    item: {
        width: SIZES.width - 100,
        height: SIZES.width - 200,
        borderRadius: 20,
    },
    imageContainer: {
        flex: 1,
        backgroundColor: 'white',

        borderRadius: 0,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        elevation: 1.1,
        height: 60,
        marginHorizontal: 5,
        borderWidth: 0.5,
        borderColor: Colors.inactiveTintColor,

        margin: 0,
    },
    SectionStyle2: {
        flex: 1,
        backgroundColor: Colors.backGroundGray,
        flexDirection: 'row',
        borderWidth: 0,
        height: 50,
        borderRadius: 30,
        marginVertical: 5,
        marginHorizontal: 10,
    },
    input: {
        marginStart: 10,
        flex: 1,
        borderRadius: 0,
        color: "#333"
    },
    iconInput: {
        marginEnd: 12,
    },
    container: {
        flex: 1,
        backgroundColor: "#ffffff",
    },
    tabsContainer: {
        alignSelf: 'stretch',
        marginTop: 30,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeContainer3: {
        backgroundColor: 'white',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,

    },
    itemThreeContainer: {
        backgroundColor: 'white',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeSubContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
    },
    itemThreeImage: {
        height: 100,
        width: 100,
        marginRight: 5,
        marginLeft: 5,
    },
    itemThreeContent: {
        paddingLeft: 15,
        textAlign: 'right',
        marginTop: 10,
        display: 'flex', flexDirection: 'row',
        position: 'absolute', right: 5
    },
    itemThreeBrand: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5F5F5F',
        textAlign: 'right',

    },
    itemThreeTitle: {
        fontFamily: "Lato-Regular",
        fontSize: 14,
        color: '#5F5F5F',

    },
    itemThreeSubtitle: {
        fontFamily: "Lato-Regular",
        fontSize: 12,
        color: '#a4a4a4',
    },
    itemThreeMetaContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemThreePrice: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5f5f5f',
        textAlign: 'right',
        marginRight: 10,
    },
    itemThreeHr: {
        flex: 1,
        height: 1,
        backgroundColor: '#e3e3e3',
        marginRight: -15,
    },
    badge: {
        backgroundColor: '#6DD0A3',
        borderRadius: 10,
        paddingHorizontal: 7,
        paddingVertical: 5,
        display: 'flex', flexDirection: 'row'
    },
});