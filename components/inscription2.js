import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    View,
    TextInput,
    ScrollView,
    SafeAreaView
} from 'react-native';
import {Button} from "native-base";
import { Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from "axios";
import {PermissionsAndroid} from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";
import {Button as BaseButton, Toast, Text as BaseText} from 'native-base'
import {fontFamily, Urh} from "./utils/Const";
import {Logo} from './utils/Const'
import Colors from './utils/Colors'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Modal from "react-native-modal";
import {Block, Text as GalioText} from "galio-framework";
import AntDesign from "react-native-vector-icons/AntDesign";
export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inputs: <View/>,
            button: <View/>,

            confirmpass: "",
            visibleinsc: false,
            user: "",
            passe: "",
            passeconfirme: "",
            code: "",
            phone: "",
            email: "",

            infomodal:false,
            message:"",
            disable: "",

            codeparrain: "",
            visible: false,

            buttons:
                <Button

                    onPress={() => this.verifcode()}
                    buttonStyle={{
                        alignSelf: 'center',
                        width: 120,
                        marginTop: 35,
                        marginBottom: 35,
                        backgroundColor: "rgba(34,81,125,1)",
                        height: 35,
                        borderRadius: 15
                    }}
                    title="vérifier code"
                    color="#53a1dc"

                />,
            afterverif: "lightgrey",
            afterverifbac: "lightgrey",
            userpass: false,
            parrainstyle: "rgba(34,81,125,1)",
            parrainstylebac: "#ffffff",
            parraindisable: true
        };

        this.checkuser = this.checkuser.bind(this)

        this.inscription = this.inscription.bind(this)

        const {navigation} = this.props;
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
            }
        )

        this.state.code = navigation.getParam('code', 'some default value')
        this.state.phone = navigation.getParam('phone', 'some default value')
        // this.state.email = navigation.getParam('email', 'some default value')



    }

    inscription() {
        var inscrit = this.inscription
        this.setState({visibleinsc: true})
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const initialPosition = position

                let configpic = {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }

                let data =
                    {
                        email: this.state.email,
                        password: this.state.passe,
                        phone: this.state.phone,
                        adresse_mail: this.state.email,
                        longitude: initialPosition.coords.longitude,
                        latitude: initialPosition.coords.latitude,
                        code_parrain: this.state.code,

                    }
                axios.post(Urh + 'signup', data, configpic)
                    .then((res) => {
                        this.setState({visibleinsc: false})


                        if (res.data.success) {

                            this.props.navigation.navigate('welcome')

                        } else if (!res.data.success) {


                        }


                    })
                    .catch((err) => {
                        this.setState({visibleinsc: false})

                    })


                this.setState({visibleinsc: false})




            },
            (error) => {
                this.setState({visibleinsc: false})
                inscrit()
            },
            {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000}
        )
    }


    checkuser() {

        if (this.state.email == "" || this.state.passe == "" || this.state.passeconfirme == "") {
            this.setState({infomodal:true,message:"Veuiller remplire tous les champs"})

        } else {

            if (this.state.passe != this.state.passeconfirme) {
                this.setState({infomodal:true,message:"Vos mots de passe ne sont pas identique, merci de vérifier "})

            } else {
                this.setState({visible: true})
                let configpic = {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                };

                let data =
                    {
                        email: this.state.email,


                    }


                axios.post(Urh + 'checkuser', data, configpic)
                    .then((res) => {
                        this.setState({visible: false})

                        if (res.data.user == 1) {
                            this.setState({infomodal:true,message:"Cet email est déja inscrit sur WinShot connectez vous à l'aide" +
                                    " de votre mot de passe."+" \nsi vous avez oublié votre mot de passe possédez a sa réinitialisation"})


                        } else {
                            this.inscription()
                        }


                    })
                    .catch((err) => {
                        this.setState({visible: false})

                    })

            }
        }

    }

    componentDidMount() {

    }


    render() {
        // const { navigate } = this.props.navigation;
        let {infomodal,message}= this.state
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>




                <Modal
                    backdropOpacity={0.3} hasBackdrop={true}
                    isVisible={infomodal}>
                    <Block style={{
                        backgroundColor: Colors.white,
                        borderRadius: 10,
                    }}>
                        <Block style={{
                            paddingVertical: 10,
                            paddingHorizontal: 15,
                        }}>
                            <Block center>
                                <AntDesign
                                    name={"infocirlceo"}
                                    size={34}
                                    color={Colors.blue}/>
                            </Block>
                            <GalioText center style={{marginVertical: 5}}>
                                {message}
                            </GalioText>
                        </Block>
                        <Block row style={{
                            justifyContent: 'center',
                            backgroundColor: Colors.lightGray,
                            paddingVertical: 5,
                            paddingHorizontal: 10,

                        }}>
                            <Button rounded
                                    onPress={() => this.setState({infomodal:false})}
                                    style={{
                                        paddingHorizontal: 7,
                                        elevation: 0,
                                        backgroundColor: Colors.orange
                                    }}
                            >
                                <GalioText style={{
                                    fontFamily: fontFamily["Poppins-Regular"],
                                    fontSize: 18,
                                    color: Colors.white,
                                    paddingHorizontal: 15,
                                }}
                                           uppercase={false}>OK</GalioText>
                            </Button>
                        </Block>


                    </Block>

                </Modal>




                <ScrollView contentContainerStyle={{
                    alignItems: 'center',
                    justifyContent: 'space-around',
                    paddingHorizontal: 10
                }}>
                    <Image style={{
                        width: 180,
                        height: 180,
                        resizeMode: 'contain',
                    }} source={{uri: Logo}}/>


                    <View style={{alignItems: 'center'}}>


                        {/*email adress */}
                        {/*email adress */}
                        <View style={styles.SectionStyle}>
                            <MaterialCommunityIcons style={styles.iconInput} name="email" size={24}/>
                            <TextInput
                                style={styles.input}
                                onChangeText={(e) => {
                                    this.setState({email: e})
                                }}
                                placeholder="Adresse Email"
                                value={this.state.email}
                                keyboardType={'email-adress'}
                            />
                        </View>
                        {/* Téléphone */}
                        <View style={styles.SectionStyle}>
                            <FontAwesome5 style={styles.iconInput} name="lock" size={24}/>
                            <TextInput
                                style={styles.input}
                                onChangeText={(e) => {
                                    e = e.trim()
                                    this.setState({passe: e})
                                }}

                                placeholder="Mot de passe"
                                value={this.state.passe}
                                secureTextEntry={true}
                            />
                        </View>
                        {/*parinage code*/}
                        <View style={styles.SectionStyle}>
                            <FontAwesome5 style={styles.iconInput} name="lock" size={24}/>
                            <TextInput
                                style={styles.input}
                                onChangeText={(e) => {
                                    e = e.trim()
                                    this.setState({passeconfirme: e})
                                }}
                                placeholder="Confirmer Mot de passe"
                                value={this.state.passeconfirme}
                                secureTextEntry={true}
                            />
                        </View>
                    </View>
                    <View style={{width: '90%', marginVertical: 20}}>
                        <BaseButton
                            onPress={() => this.checkuser()}
                            rounded info full style={{borderRadius: 30}}
                        >
                            <BaseText style={{fontSize: 16}}>Inscription</BaseText>
                        </BaseButton>
                    </View>
                    <View style={{flex: 1}}>
                        <Spinner visible={this.state.visibleinsc} textContent={"inscription"}
                                 textStyle={{color: '#FFF'}}/>
                    </View>
                    <View style={{flex: 1}}>
                        <Spinner visible={this.state.visible} textContent={"verification"} textStyle={{color: '#FFF'}}/>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    SectionStyle: {
        backgroundColor: Colors.backGroundGray,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 0,
        height: 60,
        borderRadius: 30,
        margin: 10,
    },
    input: {
        fontSize: 16,
        paddingLeft: 10,
        flex: 1,
        borderRadius: 30,
    },
    iconInput: {
        color: Colors.blue,
        paddingRight: 7,
        borderRightWidth: 1,
        borderColor: Colors.lightBlue,
        marginStart: 12,
    },
});
