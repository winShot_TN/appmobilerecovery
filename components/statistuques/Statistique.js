import React, {Component} from 'react';
import PieChart from 'react-native-pie-chart';
import {Dimensions, Image, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Divider} from 'react-native-paper'
import {Block, Text as GalioText, Text as GaliText} from 'galio-framework'
import AsyncStorage from '@react-native-community/async-storage';
import Colors from "../utils/Colors";
import axios from "axios";
import {Urh} from "../utils/Const";
import AntDesign from "react-native-vector-icons/AntDesign";
export default class Historique extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            showmail: 'none',
            showpass: 'none',
            disable: "",
            indexslick: 0,
            pie: <View/>,
            encours: 0,
            rejete: 0,
            valide: 0,
            reserved: 0,

        };
    }

    _promiseHandling = async () => {
        try {
            const token = await AsyncStorage.getItem('@storage_Key');
            this.user = JSON.parse(token)
            axios.get(Urh + 'reservedforuser/' + this.user._id)
                .then((res) => {
                    let reserved = 0
                    if (res.data.length > 0) {
                        reserved = 1
                    }
                    var series = [  this.user.missvalidpaye + this.user.missvalidnonpay,this.user.missabondane, this.user.missrejete]
                    if (this.user.missabondane == 0 && this.user.missvalidpaye == 0 && this.user.missvalidnonpay == 0 && this.user.missrejete == 0 && reserved == 0)
                        series = [ 0, 0, 0, 1]
                    this.setState({
                        pie:
                            <PieChart
                                chart_wh={170}
                                series={series}
                                sliceColor={[ Colors.success,Colors.warning,  Colors.danger]}
                                doughnut={true}
                                coverRadius={0.50}
                                coverFill={'#FFF'}

                            />,
                        reserved: reserved,
                        abondonne: this.user.missabondane,
                        valide: this.user.missvalidpaye + this.user.missvalidnonpay,
                        rejete: this.user.missrejete,
                    })
                })
                .catch((err) => {

                })
        } catch (error) {
        }
    }


    componentDidMount() {
        this._promiseHandling()
    }
    renderHeader() {
        let {user: {photo}} = this.props.screenProps;
        return (
            <Block row shadow right middle style={{
                height: 50,
                backgroundColor: Colors.orange,
                paddingHorizontal: 10,
            }}>
                <Block center>
                </Block>
                <Block flex center style={{marginLeft: 20}}>
                    <GalioText color={Colors.white} style={{fontSize: 20}}> Statistique </GalioText>
                </Block>
                <Block center style={{marginRight: 5}}>
                    {photo && photo.length > 0 ?
                        <TouchableOpacity onPress={this.goToPlus}>
                            <Image
                                style={{
                                    height: 35,
                                    width: 35,
                                    borderRadius: 18,
                                    borderColor: Colors.white,
                                    borderWidth: 1,
                                }}
                                source={{uri: "https://uploadsimageswinshot.s3.us-east-2.amazonaws.com/profilepic/" + photo}}/>
                        </TouchableOpacity>
                        : null}
                </Block>
            </Block>
        )
    }

    render() {
        return (
            <SafeAreaView style={{backgroundColor: Colors.white,flex : 1}}>
                {this.renderHeader()}
                <ScrollView contentContainerStyle={{padding: 15,flex : 1}}>
                    <GaliText h5 style={{color: Colors.textColor}}>Historique des check-liste</GaliText>
                    <View style={{alignItems: 'center', marginTop: 10}}>
                        {this.state.pie}
                    </View>
                    <Block style={{marginTop : 20}}>
                        {/* Validé*/}
                        <Block middle style={{
                            paddingVertical: 8,
                            marginHorizontal: 40
                        }} row space={"between"}>
                            <Block right style={{
                                width: 18,
                                height: 18,
                                borderRadius: 15,
                                backgroundColor: Colors.success,
                                overflow: 'hidden',
                                marginRight: 7
                            }}>
                            </Block>
                            <Block flex style={{
                                marginRight: 50
                            }}>
                                <GaliText style={{
                                    fontSize: 18,
                                }} color={Colors.textColor}>
                                    Validé
                                </GaliText>
                            </Block>
                            <Block style={{
                                marginRight: 20,
                            }}>
                                <GaliText h5 bold color={Colors.success}>
                                    {this.state.valide}
                                </GaliText>
                            </Block>
                        </Block>
                        <Divider style={{
                            marginHorizontal : 30
                        }}/>
                        {/* Abondonnée*/}
                        <Block middle style={{
                            paddingVertical: 8,
                            marginHorizontal: 40
                        }} row space={"between"}>
                            <Block right style={{
                                width: 18,
                                height: 18,
                                borderRadius: 15,
                                backgroundColor: Colors.warning,
                                overflow: 'hidden',
                                marginRight: 7
                            }}>
                            </Block>
                            <Block flex>
                                <GaliText style={{
                                    fontSize: 18,
                                }} color={Colors.textColor}>
                                    Abondonnée
                                </GaliText>
                            </Block>
                            <Block style={{
                                marginRight: 20,
                            }}>
                                <GaliText h5 bold color={Colors.warning}>
                                    {this.state.abondonne}
                                </GaliText>
                            </Block>
                        </Block>
                        <Divider style={{
                            marginHorizontal : 30
                        }}/>
                        {/* Rejetéé*/}
                        <Block middle style={{
                            paddingVertical: 8,
                            marginHorizontal: 40
                        }} row space={"between"}>
                            <Block right style={{
                                width: 18,
                                height: 18,
                                borderRadius: 15,
                                backgroundColor: Colors.danger,
                                overflow: 'hidden',
                                marginRight: 7
                            }}>
                            </Block>
                            <Block flex style={{
                                marginRight: 50
                            }}>
                                <GaliText style={{
                                    fontSize: 18,
                                }} color={Colors.textColor}>
                                    Rejetéé
                                </GaliText>
                            </Block>
                            <Block style={{
                                marginRight: 20,
                            }}>
                                <GaliText h5 bold color={Colors.danger}>
                                    {this.state.rejete}
                                </GaliText>
                            </Block>
                        </Block>
                        <Divider style={{
                            marginHorizontal : 30
                        }}/>


                    </Block>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ffffff",
    },
    tabsContainer: {
        alignSelf: 'stretch',
        marginTop: 30,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeContainer3: {
        backgroundColor: 'rgba(50,165,231,0.16)',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,

    },
    itemThreeContainer: {
        backgroundColor: 'rgba(34,109,156,0.84)',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeContainer2: {
        backgroundColor: 'rgba(43,127,182,0.64)',
        marginTop: 3,
        height: 68,
        paddingTop: 12,
        paddingLeft: 8,
        paddingRight: 8,
        marginRight: 15,
        marginLeft: 7,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(136,136,136,0.36)"
    },
    itemThreeSubContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
    },
    itemThreeImage: {
        height: 100,
        width: 100,
        marginRight: 5,
        marginLeft: 5,
    },
    itemThreeContent: {
        paddingLeft: 15,
        textAlign: 'right',
        marginTop: 10,
        display: 'flex', flexDirection: 'row',
        position: 'absolute', right: 5
    },
    itemThreeBrand: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5F5F5F',
        textAlign: 'right',

    },
    itemThreeTitle: {
        fontFamily: "Lato-Regular",
        fontSize: 14,
        color: '#5F5F5F',

    },
    itemThreeSubtitle: {
        fontFamily: "Lato-Regular",
        fontSize: 12,
        color: '#a4a4a4',
    },
    itemThreeMetaContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemThreePrice: {
        fontFamily: "Lato-Regular",
        fontSize: 15,
        color: '#5f5f5f',
        textAlign: 'right',
        marginRight: 10,
    },
    itemThreeHr: {
        flex: 1,
        height: 1,
        backgroundColor: '#e3e3e3',
        marginRight: -15,
    },
    badge: {
        backgroundColor: '#6DD0A3',
        borderRadius: 10,
        paddingHorizontal: 7,
        paddingVertical: 5,
        display: 'flex', flexDirection: 'row'
    },
});
